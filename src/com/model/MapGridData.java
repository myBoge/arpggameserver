package com.model;

import java.util.ArrayList;
import java.util.List;

import com.entity.Monster;
import com.entity.UserInfo;

/**
 * 地图数据管理类
 * @author xujinbo
 *
 */
public class MapGridData {
	
	private List<Grid> gridList = new ArrayList<>();
	
	/** 地图宽 */
	private int mapW = 800;
	/** 地图高 */
	private	int mapH = 600;
	
	/** 地图小块宽 */
	private	int tileW = 60;
	/** 地图小块高 */
	private	int tileH = 60;
	
	/** 地图可视宽 */
	private	int areaX;
	/** 地图可视高 */
	private	int areaY;
	/** 地图横数量 */
	private	int rw;
	/** 地图列数量 */
	private	int rh;
	/** 地图id */
	private long mapId;
	
	public MapGridData(long mapId, int mapW, int mapH, int tileW, int tileH) {
		this.mapId = mapId;
		this.mapW = mapW;
		this.mapH = mapH;
		this.tileW = tileW;
		this.tileH = tileH;
		
		initData();
	}

	/**
	 * 根据怪物id获取怪物
	 * @param monsterId
	 * @return
	 */
	public Monster getMonster(long monsterId) {
		Monster monster;
		List<Monster> monsters = getMonsterAll();
		for (int i = 0; i < monsters.size(); i++) {
			monster = monsters.get(i);
			if (monsterId == monster.getId()) {
				return monster;
			}
		}
		return null;
	}
	
	/**
	 * 根据怪物id删除怪物
	 * @param monsterId
	 * @return
	 */
	public synchronized Monster removeMonster(long monsterId) {
		List<Monster> monsters = getMonsterAll();
		Monster monster;
		for (int i = 0; i < monsters.size(); i++) {
			monster = monsters.get(i);
			if (monsterId == monster.getId()) {
				Grid grid = getGrid(getUserGrid(monster.getX(), monster.getY()));
				if (grid != null) {
					grid.removeMonster(monster);
				}
				monsters.clear();
				return monster;
			}
		}
		return null;
	}
	
	/**
	 * 获取指定点位附近的所有玩家
	 * @param startx
	 * @param starty
	 * @return
	 */
	public List<UserInfo> getNearbyUser(int startx, int starty) {
		List<Integer> ints = getPointData(startx, starty);
		List<UserInfo> userInfo = new ArrayList<>();
		Grid grid;
		for (int i = 0; i < ints.size(); i++) {
			grid = getGrid(ints.get(i));
			if (grid != null) {
				userInfo.addAll(grid.getUserInfoList());
			}
		}
		return userInfo;
	}
	
	/**
	 * 获取指定点位附近的所有怪物
	 * @param startx
	 * @param starty
	 * @return
	 */
	public List<Monster> getNearbyMonster(int startx, int starty) {
		List<Integer> ints = getPointData(startx, starty);
		List<Monster> monsters = new ArrayList<>();
		Grid grid;
		for (int i = 0; i < ints.size(); i++) {
			grid = getGrid(ints.get(i));
			if (grid != null) {
				monsters.addAll(grid.getMonsterList());
			}
		}
		return monsters;
	}
	
	/**
	 * 根据位置获取格子
	 * @param index
	 * @return 
	 */
	public Grid getGrid(int index) {
		if (index < gridList.size()) {
			return gridList.get(index);
		}
		return null;
	}
	
//	/**
//	 * 根据格子id获取格子
//	 * @param gridId 格子id
//	 * @return 
//	 */
//	public Grid getGridId(int gridId) {
//		for (int i = 0; i < gridList.size(); i++) {
//			if (gridList.get(i).getId() == gridId) {
//				return gridList.get(i);
//			}
//		}
//		return null;
//	}
	
	/**
	 * 根据此点 获取所在的格子
	 * @param startx 世界X坐标
	 * @param starty 世界Y坐标
	 * @return 
	 */
	public Grid getPointGrid(int startx, int starty) {
		startx = startx/tileW;
		starty = starty/tileH;
		return getGrid(starty*rw+startx);
	}
	
	/**
	 * 根据此点 获取所在的格子下标
	 * @param startx 世界X坐标
	 * @param starty 世界Y坐标
	 * @return 
	 */
	public int getUserGrid(int startx, int starty) {
		startx = startx/tileW;
		starty = starty/tileH;
		return starty*rw+startx;
	}
	
	/**
	 * 格子周围的格子
	 * @param index
	 * 
	 */
	public List<Integer> getGridData(int gridIndex) {
		int rol = (int)gridIndex/rw;
		int row = gridIndex-((int)rol*rw);
//		System.out.println("getGridData="+gridIndex+","+row*tileW+","+rol*tileH);
		return getPointData(row*tileW, rol*tileH);
	}
	
	/**
	 * 根据此点 获取周围的格子
	 * @param startx 世界X坐标
	 * @param starty 世界Y坐标
	 * @return 
	 */
	public List<Integer> getPointData(int startx, int starty) {
		startx = startx/tileW;
		starty = starty/tileH;
		List<Integer> list = new ArrayList<>();
		int tx = startx-1;
		int ty = starty-1;
		if (tx < 0) {
			tx = 0;
		}
		if (ty < 0) {
			ty = 0;
		}
		int maxX = Math.min(tx+areaX, (int)(mapW/tileW));
		int maxY = Math.min(ty+areaY, (int)(mapH/tileH));
		
		for(int y=ty; y<=maxY; y++) {
			for(int x=tx; x<maxX; x++) {
				if(x<0 || y<0) {
					continue;
				} else {
					list.add(y*rw+x);
//					System.out.println("", y, x, y*rw+x);
				}
			}
		}
//		System.out.println("", startx, starty, tx, ty, starty*rw+startx);
		return list;
	}
	
	private void initData() {
		rw = (int) Math.ceil((double)mapW/tileW);
		rh = (int) Math.ceil((double)mapH/tileH);
		
		areaX = (int) Math.ceil((double)tileW*3/tileW);
		areaY = (int) Math.ceil((double)tileH*3/tileH);
		
		int len = rw*rh;
		Grid grid;
		for (int i = 0; i < len; i++) {
			grid = new Grid();
			grid.setId(i);
			grid.setX((i%rw)*tileW);
			grid.setY( ((int)i/rw) *tileH);
			gridList.add(grid);
//			System.out.println("地图="+mapId+"， 注册Grid="+grid);
		}
		
	}

	/**
	 * 添加新怪物
	 * @param x 世界X坐标
	 * @param y 世界Y坐标
	 * @param monster 怪物
	 */
	public void addMonster(int x, int y, Monster monster) {
		int startx = x/tileW;//
		int starty = y/tileH;//
		int index = starty*rw+startx;
		if (index < gridList.size()) {
			monster.setGridId(index);
			gridList.get(index).addMonster(monster);
		}
	}
	
	/**
	 * 添加新玩家
	 * @param x 世界X坐标
	 * @param y 世界Y坐标
	 * @param monster 玩家
	 */
	public void addUserInfo(int x, int y, UserInfo userInfo) {
		int startx = x/tileW;//
		int starty = y/tileH;//
		int index = starty*rw+startx;
		if (index < gridList.size()) {
			userInfo.setGridId(index);
			gridList.get(index).addUserInfo(userInfo);
		}
	}
	
	/**
	 * 更新此玩家的位置
	 * @param gridId 新格子
	 * @param userInfo 新格子存储的用户
	 */
	public void updateUser(int gridId, UserInfo userInfo) {
		Grid grid = getGrid(userInfo.getGridId());
		if (grid != null) {
			grid.removeUserInfo(userInfo);
		}
		grid = getGrid(gridId);
		if (grid != null) {
			userInfo.setGridId(gridId);
			grid.addUserInfo(userInfo);
		}
	}
	
	/**
	 * 添加新怪物
	 * @param x 世界X坐标
	 * @param y 世界Y坐标
	 * @param monster 怪物
	 * @return 
	 */
	public boolean removeMonster(int x, int y, Monster monster) {
		int startx = x/tileW;//
		int starty = y/tileH;//
		int len = starty*rw+startx;
		if (len < gridList.size()) {
			return gridList.get(len).removeMonster(monster);
		}
		return false;
	}
	
	/**
	 * 添加玩家
	 * @param x 世界X坐标
	 * @param y 世界Y坐标
	 * @param monster 玩家
	 * @return 
	 */
	public boolean removeUserInfo(int x, int y, UserInfo userInfo) {
		int startx = x/tileW;//
		int starty = y/tileH;//
		int len = starty*rw+startx;
		if (len < gridList.size()) {
			userInfo.setGridId(0);
			return gridList.get(len).removeUserInfo(userInfo);
		}
		return false;
	}

	/**
	 * 获取地图上所有的玩家数量
	 * @return
	 */
	public int getUserLength() {
		int count = 0;
		Grid grid;
		for (int i = 0; i < gridList.size(); i++) {
			grid = gridList.get(i);
			count += grid.getUserInfoList().size();
		}
		return count;
	}
	
	/**
	 * 获取地图上所有的玩家  尽量勿调用
	 * @return 
	 * @return
	 */
	public List<UserInfo> getUserInfoAll() {
		List<UserInfo> userInfo = new ArrayList<>();
		Grid grid;
		for (int i = 0; i < gridList.size(); i++) {
			grid = gridList.get(i);
			userInfo.addAll(grid.getUserInfoList());
		}
		return userInfo;
	}
	
	/**
	 * 获取地图上所有的怪物数量
	 * @return
	 */
	public int getMonsterLength() {
		int count = 0;
		Grid grid;
		for (int i = 0; i < gridList.size(); i++) {
			grid = gridList.get(i);
			count += grid.getMonsterList().size();
		}
		return count;
	}
	
	/**
	 * 获取地图上所有的怪物  尽量勿调用
	 * @return 
	 * @return
	 */
	public List<Monster> getMonsterAll() {
		List<Monster> monster = new ArrayList<>();
		Grid grid;
		for (int i = 0; i < gridList.size(); i++) {
			grid = gridList.get(i);
			monster.addAll(grid.getMonsterList());
		}
		return monster;
	}
	
	
	
	
	/**
	 * 获取地图横数量
	 * @return rw 地图横数量
	 */
	public int getRw() {
		return rw;
	}



	/**
	 * 获取地图列数量
	 * @return rh 地图列数量
	 */
	public int getRh() {
		return rh;
	}

	/**
	 * 获取地图id
	 * @return mapId 地图id
	 */
	public long getMapId() {
		return mapId;
	}

	/**
	 * 设置地图id
	 * @param mapId 地图id
	 */
	public void setMapId(long mapId) {
		this.mapId = mapId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MapGridData [gridList=" + gridList + ", mapW=" + mapW + ", mapH=" + mapH + ", tileW=" + tileW
				+ ", tileH=" + tileH + ", areaX=" + areaX + ", areaY=" + areaY + ", rw=" + rw + ", rh=" + rh
				+ ", mapId=" + mapId + ", getUserLength()=" + getUserLength() + ", getUserInfoAll()=" + getUserInfoAll()
				+ ", getMonsterLength()=" + getMonsterLength() + ", getMonsterAll()=" + getMonsterAll() + ", getRw()="
				+ getRw() + ", getRh()=" + getRh() + ", getMapId()=" + getMapId() + "]";
	}

}
