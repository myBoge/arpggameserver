package com.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.configs.PropConfig;
import com.entity.Prop;
import com.entity.UserInfo;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;
import com.utils.PropComparable;

public class BaseStorage implements IAttributDataStream {

	/** 排序算法 */
	protected PropComparable comparable = new PropComparable();
	
	/** 背包一栏的列 */
	protected int col = 0;
	/** 背包一栏的行 */
	protected int row = 0;
	/** 背包栏数 */
	protected int totalBarCount = 0;
	/** 免费背包栏数 */
	protected int freeBarCount = 0;
	/** 背包中的物品 */
	protected List<Prop> props = new ArrayList<Prop>();

	protected UserInfo userInfo;
	
	public BaseStorage(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * 检查背包是否有空位置
	 * @return
	 */
	public synchronized boolean checkEmpty() {
		return checkEmptyCount(1);
	}
	
	/**
	 * 检查背包是否有指定数量的空闲位置
	 * @param count
	 * @return
	 */
	public synchronized boolean checkEmptyCount(int count) {
		int len = getPropCount();
		return (len+count) <= getFreeCount();
	}
	
	/**
	 * 获取当前已经开启的所有栏位总数量
	 * @return
	 */
	public synchronized int getFreeCount(){
		return (getDoBar()*(col*row));
	}
	
	/**
	 * 获取背包中物品数量
	 * @return
	 */
	public synchronized int getPropCount() {
		return props.size();
	}
	

	
	/**
	 * 添加一个物品到背包
	 * @param prop
	 */
	public synchronized void addProp(Prop prop) {
		if (checkEmpty()) {
			prop.setIndex(getEmptyBar());
			prop.setUid(userInfo.getOnlyIdManage().getGrowableId());
			props.add(prop);
		}
	}
	
	/**
	 * 获取一个空栏  如果没有  返回-1
	 * @return
	 */
	public synchronized int getEmptyBar() {
		int index = -1;
		// 初始化一组空下标
		List<Integer> list = new ArrayList<>();
		int freeCount = getFreeCount();
		for (int i = 0; i < freeCount; i++) {
			list.add(i);
		}
		// 将已经被占用的下标删除
		for (int i = 0; i < props.size(); i++) {
			list.remove((Object)props.get(i).getIndex());
		}
		// 赋值返回值下标
		if (list.size() > 0) {
			index = list.remove(0);
		}
		return index;
	}
	
	/**
	 * 返回此用户可用栏位数量
	 * @return
	 */
	public synchronized int getDoBar() {
		return userInfo.isMember()?totalBarCount:freeBarCount;
	}

	/**
	 * 从背包数组中获取数据
	 * @param index 在数组中的下标
	 * @return
	 */
	public synchronized Prop getProp(int index){
		if (index < getPropCount()) {
			return props.get(index);
		}
		return null;
	}
	
	/**
	 * 根据物品模版id 获取物品
	 * @param id 模版id
	 */
	public synchronized Prop getPropId(long id) {
		for (int i = 0; i < props.size(); i++) {
			if (props.get(i).getId() == id) {
				return props.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 根据物品模版id 获取物品数量
	 * @param id 模版id
	 */
	public synchronized int getPropIdCount(long id) {
		int count = 0;
		for (int i = 0; i < props.size(); i++) {
			if (props.get(i).getId() == id) {
				count += props.get(i).getCount();
			}
		}
		return count;
	}
	

	/**
	 * 根据物品位置  获取物品
	 * @param index 在包裹中的位置
	 * @return
	 */
	public synchronized Prop getPropIndex(int index) {
		for (int i = 0; i < props.size(); i++) {
			if (props.get(i).getIndex() == index) {
				return props.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 删除背包中指定位置的一个物品
	 * @param prop 要删除的物品
	 * @return 
	 */
	public synchronized Prop removeProp(Prop prop){
		for (int i = 0; i < props.size(); i++) {
			if (props.get(i).getIndex() == prop.getIndex()) {
				userInfo.getOnlyIdManage().gc(props.get(i).getUid());
				return props.remove(i);
			}
		}
		return null;
	}
	
	/**
	 * 根据物品模版id  删除指定数量   如果没有了   那么删除这个物品
	 * @param propIndex 物品在包裹中的位置
	 * @param count 要删除的数量
	 */
	public synchronized void removePropIndexCount(int propIndex, int count) {
		Prop prop = getPropIndex(propIndex);
		if (prop != null) {
			prop.setCount(prop.getCount()-count);
			if (prop.getCount() <= 0) {
				removeProp(prop);
			}
		}
	}
	
	/**
	 * 根据物品模版id  删除指定数量   如果没有了   那么删除这个物品
	 * @param propId 物品的模版id
	 * @param count 要删除的数量
	 * @return 
	 */
	public synchronized Prop removePropIdCount(long propId, int count) {
		Prop prop = getPropId(propId);
		if (prop != null) {
			prop.setCount(prop.getCount()-count);
			if (prop.getCount() <= 0) {
				removeProp(prop);
			}
		}
		return prop;
	}
	
	/**
	 * 向包裹指定位置添加物品
	 * @param index
	 * @param prop
	 */
	public void addIndexProp(int index, Prop prop) {
		if(checkIndexExist(index)) {
			index = getEmptyBar();
			if (index == -1) {
				return;
			}
		}
		prop.setIndex(index);
		prop.setUid(userInfo.getOnlyIdManage().getGrowableId());
		props.add(prop);
	}
	
	/**
	 * 检查包裹这个位置是否存在物品
	 * @param index
	 * @return 
	 */
	private synchronized boolean checkIndexExist(int index) {
		for (int i = 0; i < props.size(); i++) {
			if (props.get(i).getIndex() == index) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 整理一次包裹
	 */
	public synchronized void arrange() {
		Collections.sort(props, comparable);
		Prop prop;
		Prop prop2;
		int count;
		for (int i = 0; i < props.size()-1; i++) {
			prop = props.get(i);
			prop2 = props.get(i+1);
			// id一样是一个物品  同样的锁定状态  同样的交易状态   进行整理
			if (prop.getId() == prop2.getId() && prop.isLock() == prop2.isLock() && prop.isTrade() == prop2.isTrade()) {
				count = prop.getMaxCount()-prop.getCount();// 计算还可以放多少个
				if (count > 0) {
					if (count < prop2.getCount()) { // 下一个物品完全满足堆叠数量  还有剩余
						prop.setCount(prop.getCount()+count);
						prop2.setCount(prop2.getCount()-count);
					} else {
						prop.setCount(prop.getCount()+prop2.getCount());
						props.remove(i+1);
						i--;
					}
				}
			}
		}
		count = props.size();
		for (int i = 0; i < count; i++) {
			prop = props.get(i);
			prop.setIndex(i);
		}
	}
	
	
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		int len = ois.readInt();
		PropConfig propConfig = ResourceCell.getResource(PropConfig.class);
		Prop prop;
		long propId;
		for (int i = 0; i < len; i++) {
			propId = ois.readLong();
			prop = propConfig.getSample(propId);
			prop.setAttributeData(ois);
			addIndexProp(prop.getIndex(), prop);
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		int len = getPropCount();
		oos.writeInt(len);
		Prop prop;
		for (int i = 0; i < len; i++) {
			prop = props.get(i);
			oos.writeLong(prop.getId());
			prop.getAttributeData(oos);
		}
	}
	
	public void writeAllPropData(GameOutput gameOutput) throws IOException {
		int len = getPropCount();
		gameOutput.writeInt(len);
		Prop prop;
		for (int i = 0; i < len; i++) {
			prop = getProp(i);
			prop.writeData(gameOutput);
		}
	}

}
