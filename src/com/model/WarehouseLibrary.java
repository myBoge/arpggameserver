package com.model;

import com.entity.UserInfo;

/**
 * 仓库
 * @author xujinbo
 *
 */
public class WarehouseLibrary extends BaseStorage {

	public WarehouseLibrary(UserInfo userInfo) {
		super(userInfo);
		col = 6;
		row = 7;
		totalBarCount = 4;
		freeBarCount = 4;
	}

	
	
}
