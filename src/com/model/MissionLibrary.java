package com.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.configs.MissionConfig;
import com.entity.MissionTask;
import com.entity.UserInfo;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;
import com.net.SocketCmd;

/**
 * 剧情任务保存记录
 * @author xujinbo
 *
 */
public class MissionLibrary implements IAttributDataStream {

	/** 当前正在做的剧情 */
	private List<MissionTask> missions;
	private UserInfo userInfo;
	
	public MissionLibrary(UserInfo userInfo) {
		missions = new ArrayList<>();
		this.userInfo = userInfo;
	}
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		int len = ois.readInt();
		MissionConfig missionConfig = ResourceCell.getResource(MissionConfig.class);
		MissionTask mission;
		boolean complete;
		for (int i = 0; i < len; i++) {
			mission = missionConfig.getMissionXML("data/mission/"+ois.readLong()+".xml");
			complete = ois.readBoolean();
			if (mission != null) {
				mission.setComplete(complete);
				missions.add(mission);
			}
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		int len = missions.size();
		oos.writeInt(len);
		MissionTask mission;
		for (int i = 0; i < len; i++) {
			mission = missions.get(i);
			oos.writeLong(mission.getId());
			oos.writeBoolean(mission.isComplete());
		}
	}
	
	/**
	 * 删除剧情任务
	 * @param missionId 剧情任务id
	 * @return 
	 */
	public void deleteMission(int missionId) {
		for (int i = 0; i < missions.size(); i++) {
			if (missions.get(i).getId() == missionId) {
				missions.remove(i);
				break;
			}
		}
	}

	/**
	 * 添加一个新的剧情任务
	 * @param missionId 剧情任务id
	 * @return 
	 */
	public MissionTask addMission(int missionId) {
		MissionConfig missionConfig = ResourceCell.getResource(MissionConfig.class);
		MissionTask mission = missionConfig.getMissionXML("data/mission/"+missionId+".xml");
		if (mission != null) {
			if (!missions.contains(mission)) {
				missions.add(mission);
			} else {
				mission = null;
			}
		}
		return mission;
	}

	/**
	 * 写入当前正在做的所有剧情任务
	 * @param gameOutput
	 * @throws IOException
	 */
	public void writeAllData(GameOutput gameOutput) throws IOException {
		int len = missions.size();
		gameOutput.writeInt(len);
		MissionTask missionTask;
		for (int i = 0; i < len; i++) {
			missionTask = missions.get(i);
			missionTask.writeData(gameOutput);
		}
	}

	/** 根据任务id获取任务 */
	public MissionTask getMissionTask(long missionId) {
		MissionTask missionTask;
		for (int i = 0; i < missions.size(); i++) {
			missionTask = missions.get(i);
			if (missionTask.getId() == missionId) {
				return missionTask;
			}
		}
		return null;
	}
	
	/** 是否具备某个任务 */
	public boolean hasMission(int missionId) {
		MissionTask missionTask;
		for (int i = 0; i < missions.size(); i++) {
			missionTask = missions.get(i);
			if (missionTask.getId() == missionId) {
				return true;
			}
		}
		return false;
	}

	/** 已经和此npc对过话 */
	public void addTalkedNpcId(long npcId) {
		MissionTask missionTask;
		for (int i = 0; i < missions.size(); i++) {
			missionTask = missions.get(i);
			missionTask.addTalkedNpcId(npcId);
		}
	}

	/** 完成押镖地图任务 */
	public void completeEscortMap(long mapId) {
		MissionTask missionTask;
		for (int i = 0; i < missions.size(); i++) {
			missionTask = missions.get(i);
			missionTask.completeEsocrtMap(mapId);
			missionTask.checkNeed(userInfo);
			if(missionTask.isComplete()) userInfo.sendData(SocketCmd.MISSION_UPDATE, missionTask.getId(), missionTask.isComplete());
		}
	}

	/** 地图战斗完成检查 */
	public void completeGrind(long mapId) {
		MissionTask missionTask;
		for (int i = 0; i < missions.size(); i++) {
			missionTask = missions.get(i);
			missionTask.completeGrind(mapId, userInfo);
			missionTask.checkNeed(userInfo);
			if(missionTask.isComplete()) userInfo.sendData(SocketCmd.MISSION_UPDATE, missionTask.getId(), missionTask.isComplete());
		}
	}

	/** 满足玩家升级 */
	public void completeLevel() {
		MissionTask missionTask;
		for (int i = 0; i < missions.size(); i++) {
			missionTask = missions.get(i);
			missionTask.completeLevel(userInfo);
			missionTask.checkNeed(userInfo);
			if(missionTask.isComplete()) userInfo.sendData(SocketCmd.MISSION_UPDATE, missionTask.getId(), missionTask.isComplete());
		}
	}
	
}
