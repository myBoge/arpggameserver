package com.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.boge.entity.GameOutput;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

/**
 * 礼包领取库
 * @author xujinbo
 *
 */
public class GiftLibrary implements IAttributDataStream {

	/** 每月签到次数 */
	private int onLineSignCount = -1;
	/** 上次签到时间 毫秒 */
	private long lastTimeSignTimer = -1;
	/** 已领取的等级礼包  */
	private List<Integer> levelGift = new ArrayList<>();
	
	public GiftLibrary() {
	}

	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		onLineSignCount = ois.readInt();
		lastTimeSignTimer = ois.readLong();
		int len = ois.readInt();
		for (int i = 0; i < len; i++) {
			levelGift.add(ois.readInt());
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		oos.writeInt(onLineSignCount);
		oos.writeLong(lastTimeSignTimer);
		int len = levelGift.size();
		oos.writeInt(len);
		for (int i = 0; i < len; i++) {
			oos.writeInt(levelGift.get(i));
		}
	}

	/**
	 * 获取已领取的等级礼包
	 * @return levelGift 已领取的等级礼包
	 */
	public List<Integer> getLevelGift() {
		return levelGift;
	}

	/**
	 * 设置已领取的等级礼包
	 * @param levelGift 已领取的等级礼包
	 */
	public void setLevelGift(List<Integer> levelGift) {
		this.levelGift = levelGift;
	}

	/**
	 * 获取每月签到次数
	 * @return onLineSignCount 每月签到次数
	 */
	public int getOnLineSignCount() {
		return onLineSignCount;
	}

	/**
	 * 设置每月签到次数
	 * @param onLineSignCount 每月签到次数
	 */
	public void setOnLineSignCount(int onLineSignCount) {
		this.onLineSignCount = onLineSignCount;
	}

	/**
	 * 获取上次签到时间毫秒
	 * @return lastTimeSignTimer 上次签到时间毫秒
	 */
	public long getLastTimeSignTimer() {
		return lastTimeSignTimer;
	}

	/**
	 * 设置上次签到时间毫秒
	 * @param lastTimeSignTimer 上次签到时间毫秒
	 */
	public void setLastTimeSignTimer(long lastTimeSignTimer) {
		this.lastTimeSignTimer = lastTimeSignTimer;
	}

	/**
	 * 写入已领取的等级礼包
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void writeAllData(GameOutput gameOutput) throws IOException {
		gameOutput.writeInt(levelGift.size());
		for (int i = 0; i < levelGift.size(); i++) {
			gameOutput.writeInt(levelGift.get(i));
		}
	}
	
	
}
