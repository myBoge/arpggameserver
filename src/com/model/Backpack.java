package com.model;

import com.entity.Equips;
import com.entity.Prop;
import com.entity.UseProp;
import com.entity.UserInfo;

/**
 * 玩家包裹
 * @author xujinbo
 *
 */
public class Backpack extends BaseStorage {

	public Backpack(UserInfo userInfo) {
		super(userInfo);
		col = 6;
		row = 5;
		totalBarCount = 4;
		freeBarCount = 2;
	}

	/**
	 * 添加脱下的装备到包裹
	 * @param equips
	 */
	public void addStripProp(Equips equips) {
		props.add(equips);
	}
	
	/**
	 * 将包裹中的物品放到装备栏
	 * @param equips
	 */
	public void removeWearProp(Equips equips) {
		for (int i = 0; i < props.size(); i++) {
			if (props.get(i).getIndex() == equips.getIndex()) {
				props.remove(i);
				break;
			}
		}
	}

	/**
	 * 获取一个可以恢复血量的物品
	 * @return
	 */
	public synchronized UseProp getRecoveryBloodProp() {
		int len = getFreeCount();
		Prop prop;
		for (int i = 0; i < len; i++) {
			prop = getPropIndex(i);
			if (prop !=null && prop.getType() == 2) {
				return (UseProp) prop;
			}
		}
		return null;
	}
	
	/**
	 * 获取一个可以恢复蓝量的物品
	 * @return
	 */
	public synchronized UseProp getRecoveryMagicProp() {
		int len = getFreeCount();
		Prop prop;
		for (int i = 0; i < len; i++) {
			prop = getPropIndex(i);
			if (prop != null && prop.getType() == 3) {
				return (UseProp) prop;
			}
		}
		return null;
	}

}
