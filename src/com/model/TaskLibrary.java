package com.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.boge.entity.GameOutput;
import com.entity.Task;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

/**
 * 任务集合
 * @author xujinbo
 */
public class TaskLibrary implements IAttributDataStream {

	/** 拥有的任务 */
	private List<Task> taskList = new ArrayList<>();
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		
	}

	/**
	 * 获取拥有的任务
	 * @return taskList 拥有的任务
	 */
	public List<Task> getTaskList() {
		return taskList;
	}

	public void writeAllTask(GameOutput gameOutput) throws IOException {
		gameOutput.writeInt(taskList.size());
		for (int i = 0; i < taskList.size(); i++) {
			taskList.get(i).writeData(gameOutput);
		}
	}

}
