package com.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.share.Action;
import com.boge.share.Resource;
import com.configs.MapConfig;
import com.configs.RoleConfig;
import com.data.ConnectData;
import com.entity.Escort;
import com.entity.EscortData;
import com.entity.EscortMap;
import com.entity.UserInfo;
import com.mapper.EscortMapper;
import com.net.CommonCmd;
import com.server.EscortServer;
import com.utils.BytesFactory;
import com.utils.LuaUtils;
import com.utils.OnlyIdManage;
import com.utils.RandomUtils;

/**
 * 押镖数据
 * @author xujinbo
 *
 */
public class EscortModel {

	private int mapWidth;
	@Resource
	private MapConfig mapConfig;
	@Resource
	private EscortServer escortServer;
	@Resource
	private RoleConfig roleConfig;
	
	/** 指定地图 当前的押镖id 增长值 */
	private Map<Long, OnlyIdManage> mapEscortID = new HashMap<Long, OnlyIdManage>();

	/**
	 * 初始化押镖数据
	 */
	@Action({CommonCmd.INIT_DATA})
	protected void initMapData() {
		
		System.out.println("initMapData");
		
		LuaUtils.get("escortInitData").call(CoerceJavaToLua.coerce(this));
		System.out.println("mapWidth="+mapWidth);
		
		// 从数据库初始化  正在押镖的数据
		SqlSessionFactory sqlSessionFactory = ConnectData.getSqlSessionFactory();
		SqlSession sqlSession = sqlSessionFactory.openSession();
		List<EscortData> escortDataList;
		try {
			EscortMapper escortMapper = sqlSession.getMapper(EscortMapper.class);
			List<EscortMap> escortMap = mapConfig.getEscortMaps();
			EscortData escortData = escortMapper.queryId(escortMap.get(escortMap.size()-1).getId());
			List<Escort> escortList = null;
			if (escortData == null) { // 说明押镖地图还处于空状态  需要第一次初始化
				escortDataList = new ArrayList<>();
				for (int i = 0; i < escortMap.size(); i++) {
					mapEscortID.put(escortMap.get(i).getId(), new OnlyIdManage());
					// 创建电脑押镖AI
					escortList = createEscrotAI(RandomUtils.nextInt(10, 20), escortMap.get(i));
					byte[] bytes = BytesFactory.listToBytes(escortList);
					escortData = new EscortData(escortMap.get(i).getId(), bytes);
					escortDataList.add(escortData);
					escortServer.getEscortThread().addEscortData(escortDataList.get(i).getMapId(), escortList);
					escortMapper.insert(escortData);
				}
				sqlSession.commit();
			} else {
				escortDataList = escortMapper.queryAll();
				int len = escortDataList.size();
				int escortLen;
				Escort escort = null;
				for (int i = 0; i < len; i++) {
//					System.out.println("押镖数据长度"+escortDataList.get(i).getRoles().length);
					escortData = escortDataList.get(i);
					escortList = BytesFactory.byteToObject(escortData.getRoles(), Escort.class);
					escortLen = escortList.size();
					for (int j = 0; j < escortLen; j++) {
						escort = escortList.get(j);
						escort.setId(j+1);
						if (escort.getTypes() != null) {
							getEscortAIUserInfo((EscortMap) mapConfig.getCacheSample(escort.getMapId()), escort);
						}
					}
					if (escort == null) {
						mapEscortID.put(escortData.getMapId(), new OnlyIdManage(0));
					} else {
						mapEscortID.put(escortData.getMapId(), new OnlyIdManage(escort.getId()));
					}
					escortServer.getEscortThread().addEscortData(escortData.getMapId(), escortList);
				}
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("初始化押镖地图信息", e);
		} finally {
			if(sqlSession!=null) sqlSession.close();
		}
	}
	
	/**
	 * 获取一个新的押镖信息对象   
	 * @param mapId
	 * @return
	 */
	public Escort getNewEsocrt(long mapId) {
		OnlyIdManage idManage = mapEscortID.get(mapId);
		Escort escort = new Escort();
		escort.setId(idManage.getGrowableId());
		escort.setMapId(mapId);
		return escort;
	}
	
	/**
	 * 删除此押镖信息
	 * @param escort
	 */
	public void removeEscort(Escort escort) {
		OnlyIdManage idManage = mapEscortID.get(escort.getMapId());
		if (idManage != null) {
			idManage.gc(escort.getId());
		}
	}
	
	/**
	 * 创建押镖多个电脑ai
	 * @param num 创建数量
	 * @param escortMap 地图 
	 * @return 
	 */
	public List<Escort> createEscrotAI(int num, EscortMap escortMap) {
		List<Escort> escortList = new ArrayList<>();
		Escort escort;
		int tempX;
		if (escortMap.getId() == 3001) {
			num = 0;
		}
		for (int i = 0; i < num; i++) {
			escort = createEscrot(escortMap);
			tempX = RandomUtils.nextInt(escortMap.getWidth()-100);
//			System.out.println(tempX);
			escort.setX(tempX);
			escortList.add(escort);
		}
		return escortList;
	}
	
	/**
	 * 创建一个单独的押镖对象
	 * @param escortMap
	 * @return
	 */
	public Escort createEscrot(EscortMap escortMap) {
		Escort escort = getNewEsocrt(escortMap.getId());
		List<Long> types = new ArrayList<>();
		int len = RandomUtils.nextInt(2, 6);
		for (int k = 0; k < len; k++) {
			types.add(RandomUtils.nextLong(5001, 5011));// 模版id
		}
		escort.setTypes(types);
		escort.setResidueGold(escortMap.getForegift()*2);
		escort.setTotalGold(escort.getResidueGold());
		escort.setStartDate(System.currentTimeMillis());
		escort.setX(0);
		escort.setY(getEscortY());
		getEscortAIUserInfo(escortMap, escort);
		return escort;
	}
	
	/**
	 * 过去一个新的y坐标
	 * @return
	 */
	public int getEscortY() {
		int keshifanwei = GameData.gameData.getStageHeight()-300;//可视范围
		int grid = keshifanwei/6;// 分层5个区域
		int tempY = grid*RandomUtils.nextInt(6); // 选择一个区域
		tempY += 100; // 加上偏移量
		return tempY;
	}
	
	/**
	 * 获取电脑AI的属性
	 * @param escortMap 级别 1开始
	 * @param escort 押镖信息
	 * @return
	 */
	private void getEscortAIUserInfo(EscortMap escortMap, Escort escort) {
		List<Long> types = escort.getTypes();
		int len = types.size();
		UserInfo userInfo;
		for (int i = 0; i < len; i++) {
			userInfo = (UserInfo) LuaUtils.get("createEsocrtUserInfo").call(
					CoerceJavaToLua.coerce(escortMap),
					LuaValue.valueOf(types.get(i))
				).touserdata();
			escort.getUserInfoList().add(userInfo);
		}
	}

	public int getMapWidth() {
		return mapWidth;
	}

	public void setMapWidth(int mapWidth) {
		this.mapWidth = mapWidth;
	}

}
