package com.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.configs.PropConfig;
import com.entity.Equips;
import com.entity.UserInfo;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

/**
 * 装备库
 * @author xujinbo
 *
 */
public class EquipsLibrary implements IAttributDataStream{

	/** 穿戴的装备 */
	private List<Equips> equipsList;
	private UserInfo userInfo;
	
	public EquipsLibrary(UserInfo userInfo) {
		this.userInfo = userInfo;
		equipsList = new ArrayList<Equips>();
	}
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		PropConfig propConfig = ResourceCell.getResource(PropConfig.class);
		Equips equips;
		int len = ois.readInt();
		long id;
		for (int i = 0; i < len; i++) {
			id = ois.readLong();
			equips = (Equips) propConfig.getSample(id);
			equips.setAttributeData(ois);
			equips.setUid(userInfo.getOnlyIdManage().getGrowableId());
			equipsList.add(equips);
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		int len = equipsList.size();
		oos.writeInt(len);
		for (int i = 0; i < len; i++) {
			oos.writeLong(equipsList.get(i).getId());
			equipsList.get(i).getAttributeData(oos);
		}
	}
	
	/**
	 * 根据装备uid获取装备
	 * @param uid
	 * @return
	 */
	public Equips getEquips(long uid) {
		Equips equips = null;
		int len = equipsList.size();
		for (int i = 0; i < len; i++) {
			equips = equipsList.get(i);
			if(equips.getUid() == uid) {
				return equips;
			}
		}
		return null;
	}
	
	/**
	 * 添加一个新装备
	 * @param equips
	 */
	public void addEquips(Equips equips) {
		equipsList.add(equips);
	}
	
	/**
	 * 删除指定的装备
	 * @param role
	 * @return 
	 */
	public Equips removeEquips(Equips equips) {
		return removeEquips(equips.getUid());
	}

	/**
	 * 删除指定uid的装备
	 * @param uid
	 * @return 
	 */
	public Equips removeEquips(long uid) {
		int len = equipsList.size();
		for (int i = 0; i < len; i++) {
			if (equipsList.get(i).getUid() == uid) {
				return equipsList.remove(i);
			}
		}
		return null;
	}

	/**
	 * 获取穿戴的装备
	 * @return equipsList 穿戴的装备
	 */
	public List<Equips> getEquipsList() {
		return equipsList;
	}

	/**
	 * 将穿戴装备写入发送流
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void writeData(GameOutput gameOutput) throws IOException {
		int len = equipsList.size();
		gameOutput.writeInt(len);
		for (int i = 0; i < len; i++) {
			equipsList.get(i).writeData(gameOutput);
		}
	}

	/**
	 * 检查是否存在此类型的装备
	 * @param equips
	 * @return 
	 */
	public boolean checkEquipsType(Equips equips) {
		if (equips.getType() == 10) {
			if(getTypeCount(equips.getType()) >= 2) {
				return true;
			}
			return false;
		}
		for (int i = 0; i < equipsList.size(); i++) {
			if (equipsList.get(i).getType() == equips.getType()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 检查是否此装备已经穿戴
	 * @param equips
	 * @return 
	 */
	public boolean checkWear(Equips equips) {
		for (int i = 0; i < equipsList.size(); i++) {
			if (equipsList.get(i).getUid() == equips.getUid()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 根据指定类型 获取装备
	 * @param type
	 * @return 
	 */
	public Equips getEquipsType(int type) {
		for (int i = 0; i < equipsList.size(); i++) {
			if (equipsList.get(i).getType() == type) {
				return equipsList.get(i);
			}
		}
		return null;
	}

	private int getTypeCount(int type) {
		int count = 0;
		for (int i = 0; i < equipsList.size(); i++) {
			if (equipsList.get(i).getType() == type) {
				count++;
			}
		}
		return count;
	}

}
