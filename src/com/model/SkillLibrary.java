package com.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.configs.SkillConfig;
import com.entity.Skill;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

/**
 * 技能库
 * @author xujinbo
 *
 */
public class SkillLibrary implements IAttributDataStream {

	/** 技能 */
	private List<Skill> skills;
	
	public SkillLibrary() {
		skills = new ArrayList<>();
	}
	
	/**
	 * 获取技能
	 * @return skills 技能
	 */
	public List<Skill> getSkills() {
		return skills;
	}
	
	/**
	 * 根据技能id获取技能
	 * @param skillId
	 * @return
	 */
	public Skill getSkill(long skillId) {
		Skill skill = null;
		int len = skills.size();
		for (int i = 0; i < len; i++) {
			skill = skills.get(i);
			if(skill.getId() == skillId) {
				return skill;
			}
		}
		return null;
	}
	
	/**
	 * 添加一个新技能
	 * @param skill
	 */
	public void addSkill(Skill skill) {
		skills.add(skill);
	}
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		SkillConfig skillConfig = ResourceCell.getResource(SkillConfig.class);
		Skill skill;
		int len = ois.readInt();
		long id;
		for (int i = 0; i < len; i++) {
			id = ois.readLong();
			skill = skillConfig.getSample(id);
			skill.setLevel(ois.readInt());
			skills.add(skill);
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		int len = skills.size();
		oos.writeInt(len);
		for (int i = 0; i < len; i++) {
			oos.writeLong(skills.get(i).getId());
			oos.writeInt(skills.get(i).getLevel());
		}
	}

	/**
	 * 将技能写入发送流
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void writeData(GameOutput gameOutput) throws IOException {
		int len = skills.size();
		gameOutput.writeInt(len);
		for (int i = 0; i < len; i++) {
			skills.get(i).writeData(gameOutput);
		}
	}

}
