package com.model;

import java.io.IOException;

import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

/**
 * 充值记录
 * @author xujinbo
 *
 */
public class RechargeLibrary implements IAttributDataStream {

	/** 一天的充值次数 */
	private int dayRechargeCount;
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		dayRechargeCount = ois.readInt();
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		oos.writeInt(dayRechargeCount);
	}

	/**
	 * 获取一天的充值次数
	 * @return dayRechargeCount 一天的充值次数
	 */
	public int getDayRechargeCount() {
		return dayRechargeCount;
	}

	/**
	 * 设置一天的充值次数
	 * @param dayRechargeCount 一天的充值次数
	 */
	public void setDayRechargeCount(int dayRechargeCount) {
		this.dayRechargeCount = dayRechargeCount;
	}

}
