package com.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import com.boge.entity.GameOutput;
import com.boge.share.Action;
import com.boge.util.ResourceCell;
import com.configs.BaseConfig;
import com.entity.MapData;
import com.entity.Monster;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.thread.TimerManager;
import com.utils.ActionTimerListener;
import com.utils.FileControl;
import com.utils.LuaUtils;

/**
 * 游戏所有地图信息
 * @author xujinbo
 *
 */
public class MapModel {

	/** 地图信息数据 */
	private List<MapData> mapList = new ArrayList<>();
	private TimerManager timerThread;
	
	/** 初始化地图数据 */
	@Action(CommonCmd.INIT_MAP_DATA)
	protected void initMapData() {
		
		File file = new File("data/map");
		if (file.exists()) {
			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					initMap(files[i].getPath()+"/mapconf.d5");
				}
			}
		}
//		System.out.println("测试点="+mapData.getRoadMap(mapData.Postion2Tile(4331, 2072)));
		timerThread = ResourceCell.getResource(TimerManager.class);
		timerThread.doLoop(1000*60, actionTimerHandler, 1001);
	}
	
	private void initMap(String files) {
		try {
			String str = FileControl.readFile(files);
//			System.out.println(str);
			MapData mapData = BaseConfig.getXStream().fromXML(str);
			mapData.updateAstar();
			Varargs lua = LuaUtils.get("getMapStartPos").invoke(LuaValue.valueOf(mapData.getId()));
			mapData.setStartX(lua.toint(1));
			mapData.setStartY(lua.toint(2));
			if (mapData.getId() == 1001) {
				mapData.createRandomMonster();
			}
			mapList.add(mapData);
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("初始化地图数据报错", e);
		}
	}

	ActionTimerListener actionTimerHandler = new ActionTimerListener() {
		
		@SuppressWarnings("unused")
		public void execute(long mapId) {
			MapData mapData = getMapData(mapId);
			if (mapData != null) {
//				System.out.println("execute="+mapData.getMonsterLength());
				createNewMonster(mapData);
			}
		}

		private void createNewMonster(MapData mapData) {
			try {
				List<Monster> monsters = mapData.createRandomMonster();
				GameOutput gameOutput = new GameOutput();
				Monster monster;
				for (int i = 0; i < monsters.size(); i++) {
					monster = monsters.get(i);
					gameOutput.reset();
					mapData.writeMonster(gameOutput, monster);
					mapData.sendData(monster, SocketCmd.BATTLEFIELD_ADD_MONSTER, gameOutput.toByteArray());
					mapData.sendData(monster, SocketCmd.ADD_CHAT_MESSAGE, 8, 
							monster.getRole().getName(),
							monster.getRole().getLevel(),
							monster.getX(),
							monster.getY()
						);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	};

	/**
	 * 根据地图id获取地图信息
	 * @param mapId
	 * @return 
	 */
	public MapData getMapData(long mapId) {
		for (int i = 0; i < mapList.size(); i++) {
			if (mapList.get(i).getId() == mapId) {
				return mapList.get(i);
			}
		}
		return null;
	}

	/**
	 * 获取地图信息数据
	 * @return mapList 地图信息数据
	 */
	public List<MapData> getMapList() {
		return mapList;
	}

}
