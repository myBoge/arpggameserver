package com.model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.entity.Entity;
import com.entity.Monster;
import com.entity.UserInfo;
import com.utils.RandomUtils;

@SuppressWarnings("serial")
public class Grid extends Entity {

	/** 此模块里面的角色 */
	private List<UserInfo> userInfoList = new ArrayList<>();
	/** 此模块里面的怪物 */
	private List<Monster> monsterList = new ArrayList<>();
	/** 地图中可移动的坐标 */
	private List<Point> pointList = new ArrayList<>();
	/** 起点坐标X */
	private int x;
	/** 起点坐标Y */
	private int y;
	
	public Grid() {
	}

	/**
	 * 删除一个怪物
	 * @param monster
	 * @return 
	 */
	public boolean removeMonster(Monster monster) {
		return monsterList.remove(monster);
	}
	
	/**
	 * 删除一个玩家
	 * @param userInfo
	 * @return 
	 */
	public boolean removeUserInfo(UserInfo userInfo) {
		return userInfoList.remove(userInfo);
	}
	
	/**
	 * 添加一个玩家
	 * @param userInfo
	 */
	public void addUserInfo(UserInfo userInfo) {
		if (!userInfoList.contains(userInfo)) {
			userInfoList.add(userInfo);
		}
	}
	
	/**
	 * 添加一个怪物
	 * @param monster
	 */
	public void addMonster(Monster monster) {
		monsterList.add(monster);
	}
	
	/**
	 * 获取此模块里面的角色
	 * @return userInfoList 此模块里面的角色
	 */
	public List<UserInfo> getUserInfoList() {
		return userInfoList;
	}

	/**
	 * 获取此模块里面的怪物
	 * @return monsterList 此模块里面的怪物
	 */
	public List<Monster> getMonsterList() {
		return monsterList;
	}

	/**
	 * 添加可移动点坐标
	 * @param point
	 */
	public void addMovePoint(Point point) {
		pointList.add(point);
	}

	/**
	 * 获取地图中可移动的全局坐标
	 * @return pointList 地图中可移动的全局坐标
	 */
	public List<Point> getPointList() {
		return pointList;
	}

	/**
	 * 随机一个全局坐标
	 */
	public Point randomPoint() {
//		System.out.println(this.getId()+" | "+pointList.size());
		return pointList.get(RandomUtils.nextInt(pointList.size()));
	}
	
	/**
	 * 随机一个指定点周围的全局坐标
	 */
	public Point randomPointRound(double px, double py) {
//		System.out.println(this.getId()+" | "+pointList.size());
		double distance;
		for (int i = 0; i < pointList.size(); i++) {
			distance = pointList.get(i).distance(px, py);
			if (distance > 20 && distance < 130) {
				return pointList.get(i);
			}
		}
		return pointList.get(RandomUtils.nextInt(pointList.size()));
	}
	
	/**
	 * 随机N个指定点周围的全局坐标
	 */
	public List<Point> randomPointRound(double px, double py, int n) {
//		System.out.println(this.getId()+" | "+pointList.size());
		List<Point> list = new ArrayList<>();
		double distance;
		for (int i = 0; i < pointList.size(); i++) {
			distance = pointList.get(i).distance(px, py);
			if (distance > 20 && distance < 200) {
				list.add(pointList.get(i));
				if (list.size() >= n) {
					break;
				}
			}
		}
		return list;
	}
	
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Grid [x=" + x + ", y=" + y + ", id=" + id + ", uid=" + uid + "]";
	}

	/**
	 * 获取起点坐标X
	 * @return x 起点坐标X
	 */
	public int getX() {
		return x;
	}

	/**
	 * 设置起点坐标X
	 * @param x 起点坐标X
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * 获取起点坐标Y
	 * @return y 起点坐标Y
	 */
	public int getY() {
		return y;
	}

	/**
	 * 设置起点坐标Y
	 * @param y 起点坐标Y
	 */
	public void setY(int y) {
		this.y = y;
	}
	
}
