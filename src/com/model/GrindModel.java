package com.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.boge.share.Action;
import com.boge.share.Resource;
import com.configs.MapConfig;
import com.entity.GrindMap;
import com.entity.GrindTeam;
import com.net.CommonCmd;
import com.server.GrindServer;

/**
 * 练级地图数据
 * @author xujinbo
 *
 */
public class GrindModel {

	@Resource
	private GrindServer grindServer;
	@Resource
	private MapConfig grindConfig;
	
	public GrindModel() {
	}
	
	@Action({CommonCmd.INIT_GRIND_DATA})
	protected void initMapData() {
		Map<Long, List<GrindTeam>> maps = grindServer.getGrindThread().getMapGrindData();
		maps.clear();
		List<GrindMap> lists = grindConfig.getGrindMaps();
		int len = lists.size();
		GrindMap grindMap;
		for (int i = 0; i < len; i++) {
			grindMap = lists.get(i);
			maps.put(grindMap.getId(), new ArrayList<GrindTeam>());
		}
	}

}
