package com.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.configs.RoleConfig;
import com.entity.Role;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;
import com.utils.OnlyIdManage;

public class PetLibrary implements IAttributDataStream {

	/** 宠物 */
	private List<Role> roles;
	/** 宠物id生成器 */
	private OnlyIdManage onlyIdManage;
	
	public PetLibrary() {
		roles = new ArrayList<>();
		onlyIdManage = new OnlyIdManage();
	}
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		int len = ois.readInt();
		RoleConfig roleConfig = ResourceCell.getResource(RoleConfig.class);
		Role role = null;
		for (int i = 0; i < len; i++) {
			role = roleConfig.getSample(ois.readLong());
			role.setAttributeData(ois);
			role.castAtt();
			addRole(role);
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		int len = roles.size();
		oos.writeInt(len);
		for (int i = 0; i < len; i++) {
			oos.writeLong(roles.get(i).getId());
			roles.get(i).getAttributeData(oos);
		}
	}
	
	/**
	 * 根据宠物uid获取宠物
	 * @param uid
	 * @return
	 */
	public Role getRole(long uid) {
		Role role = null;
		int len = roles.size();
		for (int i = 0; i < len; i++) {
			role = roles.get(i);
			if(role.getUid() == uid) {
				return role;
			}
		}
		return null;
	}
	
	/**
	 * 添加一个新宠物
	 * @param pet
	 */
	public void addRole(Role role) {
		role.setUid(onlyIdManage.getGrowableId());
		roles.add(role);
	}
	
	/**
	 * 删除指定的宠物
	 * @param role
	 * @return 
	 */
	public Role deleteRole(Role role) {
		return deleteRole(role.getUid());
	}

	/**
	 * 删除指定uid的宠物
	 * @param uid
	 * @return 
	 */
	public Role deleteRole(long uid) {
		int len = roles.size();
		for (int i = 0; i < len; i++) {
			if (roles.get(i).getUid() == uid) {
				onlyIdManage.gc(uid);
				return roles.remove(i);
			}
		}
		return null;
	}

	/**
	 * 将所有宠物基础信息   写入到发送流
	 * @param gameOutput
	 * @throws IOException
	 */
	public void writeData(GameOutput gameOutput) throws IOException {
		int len = roles.size();
		gameOutput.writeInt(len);
		for (int i = 0; i < len; i++) {
			roles.get(i).writeData(gameOutput);
		}
	}

	/**
	 * 获取当前宠物数量
	 * @return
	 */
	public int getPetSize() {
		return roles.size();
	}

	/**
	 * 将指定的宠物设置为上阵
	 * @param uid
	 */
	public void updateJoinBattle(long uid) {
		int len = roles.size();
		for (int i = 0; i < len; i++) {
			if(roles.get(i).getUid() == uid) {
				roles.get(i).setJoinBattlePet(true);
			} else {
				roles.get(i).setJoinBattlePet(false);
			}
		}
	}

	/**
	 * 获取当前上阵的宠物
	 * @return
	 */
	public Role getJoinBattlePet() {
		int len = roles.size();
		for (int i = 0; i < len; i++) {
			if(roles.get(i).isJoinBattlePet()) {
				return roles.get(i);
			}
		}
		return null;
	}

	/**
	 * 获取宠物
	 * @return roles 宠物
	 */
	public List<Role> getRoles() {
		return roles;
	}

}
