package com.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.configs.PropConfig;
import com.entity.UseProp;
import com.entity.UserInfo;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;
import com.net.SocketCmd;
import com.server.UserServer;

/**
 * 玩家使用的时间道具
 * @author xujinbo
 *
 */
public class BuffLibrary implements IAttributDataStream {

	/** 使用的时间道具 */
	private List<UseProp> usePropList = new ArrayList<>();
	private UserInfo userInfo;
	
	public BuffLibrary(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		int len = ois.readInt();
		UseProp useProp;
		PropConfig propConfig = ResourceCell.getResource(PropConfig.class);
		for (int i = 0; i < len; i++) {
			useProp = (UseProp) propConfig.getSample(ois.readLong());
			useProp.setAttributeData(ois);
			usePropList.add(useProp);
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		oos.writeInt(usePropList.size());
		UseProp useProp;
		for (int i = 0; i < usePropList.size(); i++) {
			useProp = usePropList.get(i);
			oos.writeLong(useProp.getId());
			useProp.getAttributeData(oos);
		}
	}

	/**
	 * 获取使用的时间道具
	 * @return userPropList 使用的时间道具
	 */
	public List<UseProp> getUsePropList() {
		return usePropList;
	}

	public void writeAllBuff(GameOutput gameOutput) throws IOException {
		gameOutput.writeInt(usePropList.size());
		for (int i = 0; i < usePropList.size(); i++) {
			usePropList.get(i).writeData(gameOutput);
		}
	}

	/**
	 * 检查过期
	 * @return 是否有删除
	 */
	public boolean checkOverdue() {
		boolean b = false;
		for (int i = 0; i < usePropList.size(); i++) {
			if(checkPropData(usePropList.get(i))) {
				usePropList.remove(i);
				i--;
				b = true;
			}
		}
		if (b) {
			ResourceCell.getResource(UserServer.class).updateBuffByte(userInfo);
		}
		return b;
	}

	/**
	 * 检查此物品buff是否过期
	 * @param useProp
	 * @return 
	 */
	private boolean checkPropData(UseProp useProp) {
		if (useProp != null) {
			if (useProp.getStartDate() > 0) {
//				System.out.println(  (System.currentTimeMillis()-usePropList.get(i).getStartData())  );
//				System.out.println(  (usePropList.get(i).getTimeline()*60*1000)  );
				if ((System.currentTimeMillis()-useProp.getStartDate()) >= (useProp.getTimeline()*60*1000) ) {
					GameOutput gameOutput = new GameOutput();
					try {
						gameOutput.writeLong(useProp.getId());
						userInfo.sendData(SocketCmd.DELETE_BUFF_PROP, gameOutput.toByteArray());
					} catch (Exception e) {
						Logger.getLogger(getClass()).error("检查此物品buff是否过期报错", e);
					} finally {
						try {
							gameOutput.close();
						} catch (IOException e) {
							Logger.getLogger(getClass()).error("检查此物品buff是否过期关闭流报错", e);
						}
					}
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 检查是否存在此物品
	 * @param id 物品模版id
	 * @return 是否存在
	 */
	public boolean checkProp(int id) {
		for (int i = 0; i < usePropList.size(); i++) {
			if (usePropList.get(i).getId()==id) {
				boolean b = checkPropData(usePropList.get(i));
				if (b) {
					ResourceCell.getResource(UserServer.class).updateBuffByte(userInfo);
				}
				return !b;
			}
		}
		return false;
	}

	/**
	 * 根据物品模版id获取物品
	 * @param propId
	 * @return 
	 */
	public UseProp getPropId(int propId) {
		for (int i = 0; i < usePropList.size(); i++) {
			if (usePropList.get(i).getId()==propId) {
				return usePropList.get(i);
			}
		}
		return null;
	}

	/**
	 * 根据物品模版id删除物品
	 * @param propId
	 * @return
	 */
	public UseProp removeProp(int propId) {
		for (int i = 0; i < usePropList.size(); i++) {
			if (usePropList.get(i).getId()==propId) {
				return usePropList.remove(i);
			}
		}
		return null;
	}

}
