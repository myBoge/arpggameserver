package com.model;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import com.annotation.XMLParse;
import com.boge.entity.GameOutput;
import com.boge.share.BaseShareBox;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.util.ResourceCell;
import com.configs.UplevelExpConfig;
import com.entity.Role;
import com.entity.UplevelExp;
import com.entity.UserInfo;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.server.UserServer;
import com.utils.BytesFactory;
import com.utils.LuaUtils;

/**
 * 
 * 游戏数据
 * @author xujinbo
 *
 */
@XMLParse("GameData")
public class GameData {
	
	public static GameData gameData;
	
	@Resource
	private UplevelExpConfig uplevelExpConfig;
	@Resource
	private UserServer userServer;
	/** 舞台宽度 */
	private int stageWidth;
	/** 舞台高度 */
	private int stageHeight;
	/** 战斗最大回合数 */
	private int fightMaxCount;
	/** 默认遇怪最小间隔时间  秒 */
	private int defaultGrindMinSpace;
	/** 默认遇怪最大间隔时间  秒 */
	private int defaultGrindMaxSpace;
	/** 注册名字检验 */
	private String illegalText;

	private String[] texts;
	
	public GameData() {
		gameData = this;
		BaseShareBox baseShareBox = ResourceCell.getResource(BaseShareBox.class);
		baseShareBox.associate(this);
	}
	
	/**
	 * 用户升级
	 * @param role
	 */
	public void userInfoUpLevel(UserInfo userInfo) {
		Role role = userInfo.getRole();
		if (uplevelExpConfig.getCacheSample(role.getLevel()) != null) {// 检查是否满足升级
			GameOutput gameOutput = new GameOutput();
			try {
				long needExp = getRoleNeedExp(role.getLevel());
				long totleExp = role.getExperience();
				if (totleExp >= needExp) {// 满足升级
					role.setLevel(role.getLevel()+1);
					role.setExperience(totleExp-needExp);
					role.castAtt();
					role.setBlood(role.getMaxBlood());
					role.setMagic(role.getMaxMagic());
					
					userInfo.getMissionLibrary().completeLevel();
					
					// 发送等级更新
					gameOutput.writeInt(role.getLevel());
					userInfo.sendData(new ActionData<>(SocketCmd.UPDATE_UPLEVEL, gameOutput.toByteArray()));
					// 发送血量更新
					userInfo.sendHpMp(gameOutput);
					userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 3, role.getUid(), role.getLevel());
					userInfoUpLevel(userInfo);
					return;
				}
				// 发送经验
				gameOutput.reset().writeLong(totleExp);
				userInfo.sendData(new ActionData<>(SocketCmd.UPDATE_EXP, gameOutput.toByteArray()));
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("用户押镖结束升级报错!", e);
			} finally {
				if (gameOutput != null) {
					try {
						gameOutput.close();
					} catch (IOException e) {
						Logger.getLogger(getClass()).error("用户押镖结束升级关闭流报错!", e);
					}
				}
			}
		}
	}
	
	/**
	 * 用户宠物升级
	 * @param role
	 */
	public void userPetUpLevel(UserInfo userInfo, Role role) {
		if (uplevelExpConfig.getCacheSample(role.getLevel()) != null) {// 检查是否满足升级
			GameOutput gameOutput = new GameOutput();
			try {
				long needExp = getPetNeedExp(role.getLevel());
				long totleExp = role.getExperience();
				if (totleExp >= needExp) {
					role.setLevel(role.getLevel()+1);
					role.setExperience(totleExp-needExp);
					role.castAtt();
					role.setBlood(role.getMaxBlood());
					role.setMagic(role.getMaxMagic());
					role.setStrength(100);
					// 发送等级更新
					gameOutput.writeLong(role.getUid());
					gameOutput.writeInt(role.getLevel());
					userInfo.sendData(new ActionData<>(SocketCmd.PET_UPDATE_UPLEVEL, gameOutput.toByteArray()));
					// 发送血量更新
					userInfo.sendPetHpMp(gameOutput, role);
					userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 3, role.getUid(), role.getLevel());
					userPetUpLevel(userInfo, role);
					return;
				}
				// 发送经验
				gameOutput.reset().writeLong(role.getUid()).writeLong(totleExp);
				userInfo.sendData(new ActionData<>(SocketCmd.UPDATE_PET_EXP, gameOutput.toByteArray()));
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("用户押镖结束升级报错!", e);
			} finally {
				if (gameOutput != null) {
					try {
						gameOutput.close();
					} catch (IOException e) {
						Logger.getLogger(getClass()).error("用户押镖结束升级关闭流报错!", e);
					}
				}
			}
		}
	}
	
	/**
	 * 获取此等级人物升级需要的经验
	 * @param level
	 * @return
	 */
	public long getRoleNeedExp(int level) {
		UplevelExp uplevelExp = uplevelExpConfig.getCacheSample(level);
		if (uplevelExp != null) {
			return uplevelExp.getExp();
		}
		return 0;
	}
	
	/**
	 * 获取此等级宠物升级经验
	 * @param level
	 * @return
	 */
	public long getPetNeedExp(int level) {
		UplevelExp uplevelExp = uplevelExpConfig.getCacheSample(level);
		if (uplevelExp != null) {
			return uplevelExp.getBbExp();
		}
		return 0;
	}
	
	/**
	 * 检查此角色是否满足升级
	 * @param role
	 * @return
	 */
	public boolean checkUpdateLevel(Role role) {
		if (uplevelExpConfig.getCacheSample(role.getLevel()) != null) {
			// 查看当前等级升级所需要的经验
			long needExp = getRoleNeedExp(role.getLevel());
			return role.getExperience() >= needExp;
		}
		return false;
	}

	/**
	 * 检查此文字是否有不合法的文字
	 * @param value
	 * @return
	 */
	public boolean checkUserName(String value) {
		String[] strs = getIllegal();
		int len = strs.length;
		for (int i = 0; i < len; i++) {
			if (value.indexOf(strs[i]) != -1) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 角色设置场景
	 * @param userInfo 要设置的角色
	 * @param currentScene 场景  CommonCmd.SCENE_GAME_MAIN
	 */
	public void setupCurrentScene(UserInfo userInfo, int currentScene) {
		setupCurrentScene(userInfo, currentScene, 0);
	}
	
	/**
	 * 角色设置场景
	 * @param userInfo 要设置的角色
	 * @param currentScene 场景  CommonCmd.SCENE_GAME_MAIN
	 * @param mapId
	 */
	public void setupCurrentScene(UserInfo userInfo, int currentScene, long mapId) {
		setupCurrentScene(userInfo, currentScene, mapId, 0, 0);
	}
	
	/**
	 * 角色设置场景
	 * @param userInfo 要设置的角色
	 * @param currentScene 场景  CommonCmd.SCENE_GAME_MAIN
	 * @param mapId
	 * @param startX
	 * @param startY
	 */
	public void setupCurrentScene(UserInfo userInfo, int currentScene, long mapId, int startX, int startY) {
		if (currentScene == CommonCmd.SCENE_GAME && mapId == 0) {
			userInfo.setCurrentScene(currentScene);
			userInfo.setCurrentSceneValue(LuaUtils.get("mainMapId").tolong());
			Varargs lua = LuaUtils.get("getMapStartPos").invoke(LuaValue.valueOf(userInfo.getCurrentSceneValue()));
			userInfo.setCurrentSceneStartX(lua.toint(1));
			userInfo.setCurrentSceneStartY(lua.toint(2));
		} else {
			if (startX == 0 && startY == 0) {
				userInfo.setCurrentScene(currentScene);
				userInfo.setCurrentSceneValue(mapId);
				Varargs lua = LuaUtils.get("getMapStartPos").invoke(LuaValue.valueOf(mapId));
				userInfo.setCurrentSceneStartX(lua.toint(1));
				userInfo.setCurrentSceneStartY(lua.toint(2));
			} else {
				userInfo.setCurrentScene(currentScene);
				userInfo.setCurrentSceneValue(mapId);
				userInfo.setCurrentSceneStartX(startX);
				userInfo.setCurrentSceneStartY(startY);
			}
		}
	}
	
	/**
	 * 给指定的角色设置场景
	 * @param userInfo 要设置的角色
	 * @param userTwo 要仿照的角色
	 */
	public void setupCurrentScene(UserInfo userInfo, UserInfo userTwo) {
		userInfo.setCurrentScene(userTwo.getCurrentScene());
		userInfo.setCurrentSceneValue(userTwo.getCurrentSceneValue());
		userInfo.setCurrentSceneStartX(userTwo.getCurrentSceneStartX());
		userInfo.setCurrentSceneStartY(userTwo.getCurrentSceneStartY());
	}
	
	private String[] getIllegal() {
		if (texts == null) {
			texts = illegalText.split(",");
		}
		return texts;
	}

	public int getStageWidth() {
		return stageWidth;
	}

	public void setStageWidth(int stageWidth) {
		this.stageWidth = stageWidth;
	}

	public int getStageHeight() {
		return stageHeight;
	}

	public void setStageHeight(int stageHeight) {
		this.stageHeight = stageHeight;
	}

	/**
	 * 获取战斗最大回合数
	 * @return fightMaxCount 战斗最大回合数
	 */
	public int getFightMaxCount() {
		return fightMaxCount;
	}

	/**
	 * 获取默认遇怪最小间隔时间秒
	 * @return defaultGrindMinSpace 默认遇怪最小间隔时间秒
	 */
	public int getDefaultGrindMinSpace() {
		return defaultGrindMinSpace;
	}

	/**
	 * 设置默认遇怪最小间隔时间秒
	 * @param defaultGrindMinSpace 默认遇怪最小间隔时间秒
	 */
	public void setDefaultGrindMinSpace(int defaultGrindMinSpace) {
		this.defaultGrindMinSpace = defaultGrindMinSpace;
	}

	/**
	 * 获取默认遇怪最大间隔时间秒
	 * @return defaultGrindMaxSpace 默认遇怪最大间隔时间秒
	 */
	public int getDefaultGrindMaxSpace() {
		return defaultGrindMaxSpace;
	}

	/**
	 * 设置默认遇怪最大间隔时间秒
	 * @param defaultGrindMaxSpace 默认遇怪最大间隔时间秒
	 */
	public void setDefaultGrindMaxSpace(int defaultGrindMaxSpace) {
		this.defaultGrindMaxSpace = defaultGrindMaxSpace;
	}

	/**
	 * 重置此用户的每日数据  自动保存数据库
	 * @param userInfo
	 */
	public void resetDayUser(UserInfo userInfo) {
		resetDayUser(userInfo, true);
	}
	
	/**
	 * 重置此用户的每日数据
	 * @param userInfo
	 * @param isAutoSave 是否自动保存
	 */
	public void resetDayUser(UserInfo userInfo, boolean isAutoSave) {
		userInfo.setOnLineDate(0);
		userInfo.setOnLineGiftCount(0);
		RechargeLibrary recharge = userInfo.getRechargeLibrary();
		recharge.setDayRechargeCount(0);
		
		if (isAutoSave) {
			UserInfo info = new UserInfo(userInfo.getId());
			info.setOnLineDate(userInfo.getOnLineDate());
			info.setOnLineGiftCount(userInfo.getOnLineGiftCount());
			info.setRechargeRecord(BytesFactory.objectToByteData(userInfo.getRechargeLibrary()));
			userServer.update(info);
		}
	}
	
}
