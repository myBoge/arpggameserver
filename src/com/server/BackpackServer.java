package com.server;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.UserDataManage;
import com.entity.Gift;
import com.entity.GiftProp;
import com.entity.Prop;
import com.entity.SpaceDate;
import com.entity.UseProp;
import com.entity.UserInfo;
import com.model.Backpack;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;
import com.utils.LuaUtils;

/**
 * 背包管理
 * @author xujinbo
 *
 */
public class BackpackServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	@Resource
	private GiftServer giftServer;
	
	public BackpackServer() {
		
	}

	/**
	 * 整理包裹
	 * @param buf
	 * @param session
	 */
	public void arrangeProp(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 上次整理包裹的时间
			SpaceDate spaceDate = userInfo.getSpaceDate();
			if (!spaceDate.checkType(SpaceDate.BACKPACK_ARRANGE)) { // 距离上次整理包裹  小于10s
				gameOutput.writeInt(CommonCmd.ACTION_COOLING);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			spaceDate.setValue(SpaceDate.BACKPACK_ARRANGE, (int) (System.currentTimeMillis()/1000));
			Backpack backpack = userInfo.getBackpack();
			backpack.arrange();
			backpack.writeAllPropData(gameOutput);
			userInfo.sendData(SocketCmd.BACKPACK_ALL_PROP, gameOutput.toByteArray());
			userServer.updateBackpack(userInfo);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("更新物品位置报错", e);
		} finally {
			try {
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("更新物品位置关闭流报错", e);
			}
		}
	}

	/**
	 * 更新物品位置
	 * @param buf
	 * @param session
	 */
	public void updatePropIndex(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			int newIndex = gameInput.readInt();// 新位置
			int index = gameInput.readInt();// 旧位置
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Backpack backpack = userInfo.getBackpack();
			
			Prop newIndexProp = backpack.getPropIndex(newIndex);// 新位置的物品
			Prop prop = backpack.getPropIndex(index); // 要换位置的物品
			if (prop == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (newIndexProp == null) { // 新位置不存在物品 
				prop.setIndex(newIndex);// 直接设置新位置
			} else {
				// 新位置存在物品 
				if (newIndexProp.getId() == prop.getId()) { // 模版一样  添加数量
					int count = newIndexProp.getMaxCount()-newIndexProp.getCount();// 还可以放多少个进去
					if (count <= 0) {
						return;
					}
					if(prop.getCount() <= count) {
						count = prop.getCount();
					}
					newIndexProp.setCount(newIndexProp.getCount()+count);
					backpack.removePropIndexCount(prop.getIndex(), count);
				} else { // 更换位置
					newIndexProp.setIndex(index);
					prop.setIndex(newIndex);
				}
			}
			gameOutput.writeInt(index);// 原始位置
			gameOutput.writeInt(newIndex);// 新位置
			userInfo.sendData(SocketCmd.UPDATE_PROP_INDEX, gameOutput.toByteArray());
			userServer.updateBackpack(userInfo);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("更新物品位置报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("更新物品位置关闭流报错", e);
			}
		}
	}
	
	/**
	 * 使用物品
	 * @param buf
	 * @param session
	 */
	public void useProp(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			int index = gameInput.readInt();// 位置
			int count = gameInput.readInt();// 使用数量
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Backpack backpack = userInfo.getBackpack();
			Prop prop = backpack.getPropIndex(index); // 物品
			if (prop == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (count == 0 || count > prop.getCount()) {
				gameOutput.writeInt(CommonCmd.PROP_COUNT_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!(prop instanceof UseProp)) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_USE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			UseProp useProp = (UseProp) prop;
			if (useProp.getUseLevel() > userInfo.getRole().getLevel()) {
				gameOutput.writeInt(CommonCmd.YOU_LEVEL_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int useCount = count;
			int realCount = 0;//真实使用数量
			switch (useProp.getType()) {
				case 1:// 使用装备
					
					break;
				case 2:
				case 3:
				case 4:
					while(useCount > 0){
						useCount--;
						if (useProp.getCount() <= 0) {
							return;
						}
						switch (useProp.getType()) {
							case 2:
								if (userInfo.getRole().getBlood()>=userInfo.getRole().getMaxBlood()) {
									useCount = 0;
									return;
								}
								break;
							case 3:
								if (userInfo.getRole().getMagic()>=userInfo.getRole().getMaxMagic()) {
									useCount = 0;
									return;
								}
								break;
							case 4:
								if (userInfo.getRole().getBlood()>=userInfo.getRole().getMaxBlood() && userInfo.getRole().getMagic()>=userInfo.getRole().getMaxMagic()) {
									useCount = 0;
									return;
								}
								break;
						}
						LuaValue luaValue = LuaUtils.get("useBloodAndMagicProp").call(
								CoerceJavaToLua.coerce(useProp), 
								CoerceJavaToLua.coerce(userInfo),
								CoerceJavaToLua.coerce(userInfo.getRole())
								);
						boolean isDeleteProp = luaValue.toboolean();
						if (isDeleteProp) {
							realCount++;
							backpack.removePropIndexCount(useProp.getIndex(), 1);
						}
					}
					if (realCount > 0) {
						userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 11, useProp.getName(), realCount);
					}
					break;
				case 11:// 使用道具
					Varargs varargs = LuaUtils.getCommon().get("backpackUseProp").invoke(new LuaValue[]{
						CoerceJavaToLua.coerce(useProp),
						CoerceJavaToLua.coerce(userInfo),
						CoerceJavaToLua.coerce(userServer),
						CoerceJavaToLua.coerce(gameOutput)
					});
					if (varargs.toboolean(1)) {
						realCount++;
					}
					break;
				case 12:// 使用礼包
					GiftProp giftProp = (GiftProp) useProp;
					List<Prop> propList;
					if (giftProp.getGiftId() != 0) {
						Gift gift = giftServer.getGift(giftProp.getGiftId());
						if (gift == null) {
							gameOutput.writeInt(CommonCmd.YOU_GIFT_INVALID);
							session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
							return;
						}
						propList = gift.getGiftList();
					} else {
						propList = giftProp.getGiftList();
					}
					if (propList.size() < 1) {
						return;
					}
					// 检查是否满足空间
					if (!backpack.checkEmptyCount(propList.size())) {
						gameOutput.writeInt(CommonCmd.BACKPACK_INSUFFICIENT_SPACE);
						session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
						return;
					}
					realCount++;
					backpack.removePropIndexCount(giftProp.getIndex(), realCount);
					userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 11, giftProp.getName(), realCount);
					gameOutput.reset().writeInt(propList.size());
					for (int i = 0; i < propList.size(); i++) {
						prop = propList.get(i);
						prop = BytesFactory.deepCopy(prop);
						backpack.addProp(prop);
						userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 2, prop.getId(), prop.getCount());
						prop.writeData(gameOutput);
					}
					userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
					break;
				default:
					break;
			}
			if (realCount > 0) {
				// 发送更新物品数量
				gameOutput.reset().write(1, useProp.getIndex(), realCount);
				userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
			}
			// 发送更新血量和蓝量
			if (useProp.getType()==2||useProp.getType()==3||useProp.getType()==4) {
				userInfo.sendHpMp(gameOutput);
			}
			userServer.update(new UserInfo(userInfo.getId(), 
					BytesFactory.objectToByteData(backpack), 
					BytesFactory.objectToByteData(userInfo.getRole()))
					);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("使用物品报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("使用物品关闭流报错", e);
			}
		}
	}

	/**
	 * 卖出物品
	 * @param buf
	 * @param session
	 */
	public void sellProp(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			int index = gameInput.readInt();// 位置
			int count = gameInput.readInt();// 使用数量
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Backpack backpack = userInfo.getBackpack();
			Prop prop = backpack.getPropIndex(index); // 物品
			if (prop == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (count > prop.getCount()) {
				gameOutput.writeInt(CommonCmd.PROP_COUNT_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (prop.isLock()) {
				gameOutput.writeInt(CommonCmd.PROP_LOCK);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 如果购买方式是商城   那么设置为500
			double goldPrice = prop.getBuysType()==3?500:prop.getGoldPrice();
			goldPrice = goldPrice*0.2*count;
//			System.out.println(prop.isLock()+" | "+prop.isTrade());
			if (prop.isTrade()) {
				userInfo.setPlayerGold((long) (userInfo.getPlayerGold()+goldPrice));
			} else {
				userInfo.setLockGold((long) (userInfo.getLockGold()+goldPrice));
			}
			backpack.removePropIndexCount(index, count);
			
			gameOutput.writeLong(userInfo.getPlayerGold()).writeLong(userInfo.getLockGold());
			userInfo.sendData(SocketCmd.UPDATE_GOLD, gameOutput.toByteArray());
			
			userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, 1, prop.getIndex(), count);
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setPlayerGold(userInfo.getPlayerGold());
			info.setLockGold(userInfo.getLockGold());
			info.setBackpackByte(BytesFactory.objectToByteData(backpack));
			userServer.update(info);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("使用物品报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("使用物品关闭流报错", e);
			}
		}
	}

}
