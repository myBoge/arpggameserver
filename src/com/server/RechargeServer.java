package com.server;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.UserDataManage;
import com.entity.UserInfo;
import com.model.RechargeLibrary;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;

/**
 * 充值处理
 * @author xujinbo
 *
 */
public class RechargeServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	
	/**
	 * 充值信息
	 * @param buf
	 * @param session
	 */
	public void rechargeInfo(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			RechargeLibrary rechargeLibrary = userInfo.getRechargeLibrary();
			if (rechargeLibrary.getDayRechargeCount() >= 10) {
				gameOutput.writeInt(CommonCmd.DAY_RECHARGE_LIMIT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			rechargeLibrary.setDayRechargeCount(rechargeLibrary.getDayRechargeCount()+1);
				
			userInfo.setRmb(userInfo.getRmb()+500);
			userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 12, 2, 500);
			
			gameOutput.reset().writeLong(userInfo.getRmb()).writeLong(userInfo.getLockrmb());
			userInfo.sendData(SocketCmd.UPDATE_RMB, gameOutput.toByteArray());
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setRmb(userInfo.getRmb());
			info.setRechargeRecord(BytesFactory.objectToByteData(userInfo.getRechargeLibrary()));
			userServer.update(info);
			
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("充值信息更新报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("充值信息关闭流报错", e);
			}
		}
	}

	
	
}
