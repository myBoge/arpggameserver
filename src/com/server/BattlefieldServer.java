package com.server;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.ConnectData;
import com.data.UserDataManage;
import com.entity.FightData;
import com.entity.MapData;
import com.entity.Monster;
import com.entity.Role;
import com.entity.UserInfo;
import com.mapper.UserMapper;
import com.model.GameData;
import com.model.MapModel;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.server.fight.FightBox;
import com.thread.SendDataThread;
import com.utils.BytesFactory;
import com.utils.CommonUtils;
import com.utils.LuaUtils;

/**
 * 战场处理
 * @author xujinbo
 *
 */
public class BattlefieldServer {

	@Resource
	private MapModel mapModel;
	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	@Resource
	private TeamServer teamServer;
	@Resource
	private FightServer fightServer;
	@Resource
	private MapServer mapServer;
	@Resource
	private SendDataThread sendDataThread;
	
	/**
	 * 战斗战场
	 * @param buf
	 * @param session
	 */
	public void battlefieldFight(byte[] buf, IoSession session) {
		GameInput gameInput = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long mapId = userInfo.getCurrentSceneValue();// 押镖地图id
			long monsterId = gameInput.readInt();// 怪物id
			MapData mapData = mapModel.getMapData(mapId);
			if (mapData == null) {
				gameOutput.writeInt(CommonCmd.MAP_NON_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getRole().getState() == CommonCmd.ESCORT) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Monster monster = mapData.getMonster(monsterId);
			if (monster == null) {
				return;
			}
			if (monster.isFight()) {
				gameOutput.writeInt(CommonCmd.THIS_AT_FIGHT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			createFight(monster, mapId, userInfo);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("战斗战场报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("战斗战场关闭流报错", e);
			}
		}
	}
	
	/**
	 * 创建一场战斗
	 * @param monster 怪兽
	 * @param mapId 
	 * @param userInfo 玩家 
	 */
	public void createFight(Monster monster, long mapId, UserInfo userInfo) {
		monster.setFight(true);
		List<FightData> robList = CommonUtils.getFightData(userInfo, false);
		int lenRole = CommonUtils.getFightCount(robList);
		
		List<FightData> robberyList = monster.createWaveMonster(lenRole, userInfo);
		FightBox fightBox = fightServer.createAIFight(robList, robberyList, false);
		fightBox.setFightType(CommonCmd.BATTLE_PVE);
		fightBox.obj = Arrays.asList(monster.getId(), mapId, 0);
		GameOutput gameOutput = new GameOutput();
		try {
			fightBox.writeTeamData(gameOutput);
			ActionData<Object> actionData = new ActionData<>(SocketCmd.FIGHT_DATA, gameOutput.toByteArray());
			fightBox.sendData(actionData);
			fightServer.startFight(fightBox.getId());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("创建一场战斗报错", e);
		} finally {
			try {
				if (gameOutput != null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("创建一场战斗关闭流报错", e);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void fightEnd(FightBox fightBox) {
		List<Long> escortList = (List<Long>) fightBox.obj;
		long monsterId = escortList.get(0);
		long mapId = escortList.get(1);
		MapData mapData = mapModel.getMapData(mapId);
		if (mapData == null) {
			return;
		}
//		long robberyEscortId = escortList.get(2);
		fightBox.obj = null;
		List<FightData> fightDataList = fightBox.getRobList();
		FightData fightData;
		UserInfo userInfo;
		Role role;
		// 攻击队伍
		if (fightBox.getFightResult() == 1) {// 进攻方胜利
			SqlSession sqlSession = ConnectData.openSession();
			try {
				Monster monster = mapData.removeMonster(monsterId);
				mapData.sendData(monster, SocketCmd.BATTLEFIELD_DELETE_MONSTER, monsterId);
				
				LuaValue luaValue = LuaUtils.getCommon().get("castBattleExp").
						call(CoerceJavaToLua.coerce(monster), CoerceJavaToLua.coerce(fightBox.getRobberyList()));
				int totalExp = luaValue.toint();// 总经验
				int exp = totalExp;// 经验临时存储
				
				int potential;
				UserMapper userManage = sqlSession.getMapper(UserMapper.class);
				for (int i = 0; i < fightDataList.size(); i++) {
					fightData = fightDataList.get(i);
					if (fightData != null) {
						userInfo = fightData.getUserInfo();
						role = userInfo.getRole();
						luaValue = LuaUtils.getCommon().get("battleFightEnd").
								call(CoerceJavaToLua.coerce(userInfo), CoerceJavaToLua.coerce(monster), 
										LuaValue.valueOf(totalExp));
						exp = luaValue.toint();
						if (exp == 0) {
							continue;
						}
						
						UserInfo userData = new UserInfo(userInfo.getId());
						// 玩家处理经验
						if (!role.isPet()) {
							if(userInfo.getBuffLibrary().checkProp(LuaUtils.get("chaojixianfenshan").toint())) {
								exp *= 2;
							}
							userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 1, role.getUid(), exp);
							role.setExperience(role.getExperience()+exp);
							GameData.gameData.userInfoUpLevel(userInfo);
							
							luaValue = LuaUtils.getCommon().get("battlePotential").
									call(CoerceJavaToLua.coerce(userInfo), CoerceJavaToLua.coerce(monster));
							potential = luaValue.toint();
							if (potential > 0) {
								if(userInfo.getBuffLibrary().checkProp(LuaUtils.get("chaojixianfenshan").toint())) {
									potential *= 2;
								}
								userInfo.setPotential(userInfo.getPotential()+potential);
								userInfo.sendData(SocketCmd.UPDATE_POTENTIAL, userInfo.getPotential());
								userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 10, potential);
							}
							
							luaValue = LuaUtils.getCommon().get("battleProp").
									call(CoerceJavaToLua.coerce(userInfo),
											CoerceJavaToLua.coerce(monster),
											LuaValue.valueOf(monster.getRole().getLevel()/10));
							userData.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
							userData.setPotential(userInfo.getPotential());
							userData.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
						} else {
							if(userInfo.getBuffLibrary().checkProp(LuaUtils.get("chongfengshan").toint())) {
								exp *= 3;
							}
							// 处理宠物经验
							userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 1, role.getUid(), exp);
							role.setExperience(role.getExperience()+exp);
							GameData.gameData.userPetUpLevel(userInfo, userInfo.getRole());
							
							userData.setPetByte(BytesFactory.objectToByteData(userInfo.getPetLibrarys()));
						}
						userManage.update(userData);
					}
				}
				sqlSession.commit();
			} catch (Exception e) {
				Logger.getLogger(getClass()).error("战斗结束奖励处理报错", e);
			} finally {
				if(sqlSession!=null)sqlSession.close();
			}
		} else if(fightBox.getFightResult() == 2){
			for (int i = 0; i < fightDataList.size(); i++) {
				fightData = fightDataList.get(i);
				if (fightData != null) {
					userInfo = fightData.getUserInfo();
					role = userInfo.getRole();
					if (!role.isPet()) {// 如果是玩家
						// 战斗中死亡
						if (fightData.isDeath()) {
							// 死亡惩罚
							LuaUtils.get("fightUserDeath").call(CoerceJavaToLua.coerce(userInfo));
						}
					}
				}
			}
			Monster monster = mapData.getMonster(monsterId);
			if (monster == null) {
				return;
			}
			monster.setFight(false);
		}
	}
	
	
	/**
	 * 根据地图id获取地图上的玩家
	 * @param mapId 地图id
	 * @param x 玩家的x坐标
	 * @param y 玩家的y坐标
	 * @return
	 */
	public List<UserInfo> getMapUserList(long mapId, int x, int y) {
		MapData mapData = mapModel.getMapData(mapId);
		if (mapData == null) {
			return null;
		}
		return mapData.getUserList(x, y);
	}
	
}
