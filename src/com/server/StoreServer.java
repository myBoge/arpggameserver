package com.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Action;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.PropConfig;
import com.data.UserDataManage;
import com.entity.Prop;
import com.entity.UserInfo;
import com.model.Backpack;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;

/**
 * 元宝商城
 * @author xujinbo
 */
public class StoreServer {

	@Resource
	private PropConfig propConfig;
	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	
	/** 在卖的商品 */
	private List<Prop> shops = new ArrayList<>();
	
	@Action({ CommonCmd.INIT_STORE_BUYS_SHOP })
	protected void init() {
		int len = 1058-1001+1;
		Prop prop;
		for (int i = 0; i < len; i++) {
			prop = propConfig.getSample(i+1001);
			if (prop.getBuysType() == 3) {
				shops.add(prop);
			}
		}
		len = 2019-2001+1;
		for (int i = 0; i < len; i++) {
			prop = propConfig.getSample(2001+i);
			if (prop.getBuysType() == 3) {
				shops.add(prop);
			}
		}
	}
	
	/**
	 * 根据模版id从商城中获取物品
	 * @param id 模版id
	 * @return
	 */
	private Prop getPropId(long id) {
		int len = shops.size();
		for (int i = 0; i < len; i++) {
			if (shops.get(i).getId() == id) {
				return shops.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 商店购买
	 * @param buf
	 * @param session
	 */
	public void buysShop(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		UserInfo userInfo = null;
		try {
			userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long propId = input.readInt();// 购买的物品id
			int buysCount = input.readInt();// 购买数量
			boolean bool = input.readBoolean();// 是否优先使用绑定元宝
			if (buysCount <= 0) {
				gameOutput.writeInt(CommonCmd.PROP_COUNT_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Prop prop = getPropId(propId);
			if (prop == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (prop.getBuysType() != 3) {
				gameOutput.writeInt(CommonCmd.ERROR_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long rmb = buysCount*prop.getGoldPrice();// 购买此数量的商品  需要消耗的钱
			if (prop.isLockBuy()) {
				if ((userInfo.getRmb()+userInfo.getLockrmb()) < rmb) { // 元宝不满足购买商品
					gameOutput.writeInt(CommonCmd.RMB_NOT_LOW);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
			} else {
				if (userInfo.getRmb() < rmb) { // 元宝不满足购买商品
					gameOutput.writeInt(CommonCmd.RMB_NOT_LOW);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				bool = false;
			}
			int propCount = (int) Math.ceil((double)buysCount/prop.getMaxCount()); // 购买后  此物品生成个数
			Backpack backpack = userInfo.getBackpack();
			if (!backpack.checkEmptyCount(propCount)) {
				gameOutput.writeInt(CommonCmd.BACKPACK_INSUFFICIENT_SPACE);//背包空间不足 
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			prop = propConfig.getSample(propId);
			prop.setCount(0);
			if (bool) {
				if (userInfo.getLockrmb() == 0) {
					bool = false;
				}
			} else {
				if (userInfo.getRmb() == 0) {
					bool = true;
				}
			}
			List<Prop> lists = new ArrayList<>();
			for (int i = 0; i < buysCount; i++) {
				if (bool) {
					if (userInfo.getLockrmb() == 0) {
						bool = false;
						lists.add(prop);
						prop = propConfig.getSample(propId);
						prop.setCount(0);
					}
				} else {
					if (userInfo.getRmb() == 0) {
						bool = true;
						lists.add(prop);
						prop = propConfig.getSample(propId);
						prop.setCount(0);
					}
				}
				if (prop.getCount() == prop.getMaxCount()) {
					lists.add(prop);
					prop = propConfig.getSample(propId);
					prop.setCount(0);
				}
				prop.setCount(prop.getCount()+1);
				buysProp(userInfo, prop, bool);
			}
			lists.add(prop);
			int len = lists.size();
			int buyCount = 0;// 实际购买的数量
			gameOutput.writeInt(len);
			for (int i = 0; i < len; i++) {
				prop = lists.get(i);
				backpack.addProp(prop);
				buyCount += prop.getCount();
				prop.writeData(gameOutput);
			}
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setRmb(userInfo.getRmb());
			info.setLockrmb(userInfo.getLockrmb());
			info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
			userServer.update(info);
			
			userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 2, propId, buyCount);
			
			userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
			gameOutput.reset().writeLong(userInfo.getRmb()).writeLong(userInfo.getLockrmb());
			userInfo.sendData(SocketCmd.UPDATE_RMB, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("商店购买报错!" + userInfo, e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("商店购买关闭流报错!", e);
			}
		}
	}

	/**
	 * 购买此物品
	 * @param userInfo 
	 * @param prop 要购买的物品
	 * @param bool 是否优先使用元宝
	 */
	private void buysProp(UserInfo userInfo, Prop prop, boolean bool) {
		long gold = prop.getGoldPrice();
		if (bool) {
			gold = userInfo.getLockrmb()-gold;// 当前的绑定元宝  减去 购买金币
			if (gold < 0) {// 不够 
				if (gold != prop.getGoldPrice()) {
					prop.setTrade(false);
				}
				userInfo.setLockrmb(0);
				gold = userInfo.getRmb()-(gold*-1);
				if (gold < 0) {
					userInfo.setRmb(0);
				} else {
					userInfo.setRmb(gold);
				}
			} else {
				prop.setTrade(false);
				// 大于0  说明绑定元宝  还有剩余
				userInfo.setLockrmb(gold);
			}
		} else {
			gold = userInfo.getRmb()-gold;
			if (gold < 0) {
				prop.setTrade(false);
				userInfo.setRmb(0);
				gold = userInfo.getLockrmb()-(gold*-1);
				if (gold < 0) {
					userInfo.setLockrmb(0);
				} else {
					userInfo.setLockrmb(gold);
				}
			} else {
				// 大于0  说明元宝  还有剩余
				userInfo.setRmb(gold);
			}
		}
	}


	/**
	 * 元宝商城可以购买的商品
	 * @param buf
	 * @param session
	 */
	public void storeAllGoods(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int len = shops.size();
			gameOutput.writeInt(len);
			for (int i = 0; i < len; i++) {
				gameOutput.writeLong(shops.get(i).getId());
			}
			userInfo.sendData(SocketCmd.STORE_ALL_GOODS, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("元宝商城可以购买的商品报错", e);
		} finally {
			try {
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("元宝商城可以购买的商品关闭流报错", e);
			}
		}
	}
	
}
