package com.server;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.PropConfig;
import com.data.UserDataManage;
import com.entity.MissionTask;
import com.entity.UseProp;
import com.entity.UserInfo;
import com.model.BuffLibrary;
import com.model.MissionLibrary;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;
import com.utils.LuaUtils;

/**
 * 任务处理
 * @author xujinbo
 *
 */
public class TaskServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	@Resource
	private PropConfig propConfig;
	
	/**
	 * 放弃buff效果
	 * @param buf
	 * @param session
	 */
	public void deleteBuffProp(byte[] buf, IoSession session) {
		GameInput gameInput = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			int propId = gameInput.readInt();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			BuffLibrary buffLib = userInfo.getBuffLibrary();
			if (propId == 2019) {
				return;
			}
			UseProp prop = buffLib.removeProp(propId);
			if (prop != null) {
				userServer.updateBuffByte(userInfo);
				gameOutput.writeLong(prop.getId());
				userInfo.sendData(SocketCmd.DELETE_BUFF_PROP, gameOutput.toByteArray());
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("放弃buff效果报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("放弃buff效果关闭流报错", e);
			}
		}
	}
	
	
	
	
	/**
	 * 剧情任务下标更新
	 * @param buf
	 * @param session
	 */
	public void missionUpdate(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
//			int missionIndex = gameInput.readInt();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
//			MissionLibrary mission = userInfo.getMissionLibrary();
//			if (mission.getMissionIndex() == missionIndex) {
//				int missionId = LuaUtils.get("missionComplete").call(CoerceJavaToLua.coerce(userInfo), LuaValue.valueOf(missionIndex)).toint();
//				mission.setMissionIndex(missionId);
//				
//				UserInfo info = new UserInfo(userInfo.getId());
//				info.setPlayerGold(userInfo.getPlayerGold());
//				info.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
//				info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
//				info.setMissionRecord(BytesFactory.objectToByteData(mission));
//				userServer.update(info);
//				
////				userInfo.sendData(SocketCmd.MISSION_UPDATE_INDEX, mission.getMissionIndex());
//			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("剧情任务下标更新报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("剧情任务下标更新关闭流报错", e);
			}
		}
	}

	public void missionAdd(byte[] buf, IoSession session) {
		
	}

	/**
	 * 完成剧情任务
	 * @param buf
	 * @param session
	 */
	public void missionComplete(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			long missionId = gameInput.readInt();
			long npcId = gameInput.readInt();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			MissionLibrary mission = userInfo.getMissionLibrary();
			MissionTask missionTask = mission.getMissionTask(missionId);
			if (missionTask == null) {
				gameOutput.writeInt(CommonCmd.MISSION_NOT_EXIST);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			mission.addTalkedNpcId(npcId);
			
			if(!missionTask.checkNeed(userInfo)) {
				gameOutput.writeInt(CommonCmd.MISSION_NOT_NEED);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			LuaUtils.get("missionComplete").call(CoerceJavaToLua.coerce(userInfo), 
					CoerceJavaToLua.coerce(missionTask));

			UserInfo info = new UserInfo(userInfo.getId());
			info.setPlayerGold(userInfo.getPlayerGold());
			info.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
			info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
			info.setMissionRecord(BytesFactory.objectToByteData(mission));
			userServer.update(info);
				
//			userInfo.sendData(SocketCmd.MISSION_UPDATE_INDEX, mission.getMissionIndex());
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("剧情任务下标更新报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("剧情任务下标更新关闭流报错", e);
			}
		}
	}

}
