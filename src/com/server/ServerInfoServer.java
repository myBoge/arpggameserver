package com.server;

import java.sql.Timestamp;

import org.apache.ibatis.session.SqlSession;

import com.boge.share.Resource;
import com.data.ConnectData;
import com.entity.ServerInfo;
import com.mapper.ServerInfoMapper;
import com.net.SocketCmd;

/**
 * 服务器信息管理
 * @author xujinbo
 *
 */
public class ServerInfoServer {
	
	@Resource
	private ServerManageServer serverConfigServer;
	/** 上一次开服当天时间 */
	private Timestamp beforeOpenTimer;
	
	/**
	 * 开启服务器
	 */
	public void openServer() {
		SqlSession session = ConnectData.openSession();
		try {
			ServerInfoMapper serverInfoManage = session.getMapper(ServerInfoMapper.class);
			ServerInfo serverInfo = serverInfoManage.queryId(1);// 根据服务器id获取  服务器信息
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			if (serverInfo.getOpenDate().getTime() < 0) {// 如果是第一次开启这个服务器
				serverInfo.setOpenDate(timestamp);// 写入首次开服时间
			}
			beforeOpenTimer = serverInfo.getTodayData();
			serverInfo.setCurrentlyOpenDate(timestamp);// 更改当前开服时间
			serverInfo.setTodayData(timestamp);
			serverInfo.setState(SocketCmd.SERVER_INFO_NO);
			serverInfoManage.update(new ServerInfo(serverInfo.getId(), serverInfo.getState(),
					serverInfo.getOpenDate(), serverInfo.getCurrentlyOpenDate()));
			session.commit();
			serverConfigServer.serverInfo = serverInfo;
		} finally {
			session.close();
		}
	}
	
	/**
	 * 关闭服务器
	 */
	public void closeServer() {
		SqlSession session = ConnectData.openSession();
		try {
			ServerInfoMapper serverInfoManage = session.getMapper(ServerInfoMapper.class);
			ServerInfo serverInfo = serverConfigServer.serverInfo;
			serverInfo = new ServerInfo(serverInfo.getId(), SocketCmd.SERVER_INFO_OFF);
			serverInfoManage.update(serverInfo);
			session.commit();
		} finally {
			session.close();
		}
	}

	public Timestamp getBeforeOpenTimer() {
		return beforeOpenTimer;
	}

	public void setBeforeOpenTimer(Timestamp beforeOpenTimer) {
		this.beforeOpenTimer = beforeOpenTimer;
	}

	/**
	 * 更新当天时间
	 * @param todayDate
	 */
	public void updateTodayData(long todayDate) {
		SqlSession session = ConnectData.openSession();
		try {
			ServerInfoMapper serverInfoManage = session.getMapper(ServerInfoMapper.class);
			ServerInfo serverInfo = serverConfigServer.serverInfo;
			serverInfo = new ServerInfo(serverConfigServer.serverInfo.getId(), new Timestamp(todayDate));
			serverInfoManage.update(serverInfo);
			session.commit();
		} finally {
			session.close();
		}
	}

}
