package com.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Action;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.PropConfig;
import com.data.UserDataManage;
import com.entity.Prop;
import com.entity.UserInfo;
import com.model.Backpack;
import com.net.CommonCmd;
import com.net.SocketCmd;

/**
 * 商店处理
 * @author xujinbo
 */
public class ShopServer {

	@Resource
	private PropConfig propConfig;
	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	
	/** 在卖的商品 */
	private List<Prop> shops = new ArrayList<>();
	
	@Action({ CommonCmd.INIT_BUYS_SHOP })
	protected void init() {
		List<Integer> ints = new ArrayList<>();
		int len = 1058-1001+1;
		for (int i = 0; i < len; i++) {
			ints.add(i+1001);
		}
		len = 2019-2001+1;
		for (int i = 0; i < len; i++) {
			ints.add(i+2001);
		}
		
		len = (3015-3001)+1;
		for (int i = 0; i < len; i++) {
			ints.add(i+3001);
		}
		len = (3115-3101)+1;
		for (int i = 0; i < len; i++) {
			ints.add(i+3101);
		}
		len = (3215-3201)+1;
		for (int i = 0; i < len; i++) {
			ints.add(i+3201);
		}
		len = (3315-3301)+1;
		for (int i = 0; i < len; i++) {
			ints.add(i+3301);
		}
		Prop prop;
		for (int i = 0; i < ints.size(); i++) {
			prop = propConfig.getSample(ints.get(i));
			if (prop.getBuysType() == 1) {
				shops.add(prop);
			}
		}
	}
	
	/**
	 * 根据模版id从商城中获取物品
	 * @param id
	 * @return
	 */
	private Prop getPropId(long id) {
		int len = shops.size();
		for (int i = 0; i < len; i++) {
			if (shops.get(i).getId() == id) {
				return shops.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 商店购买
	 * @param buf
	 * @param session
	 */
	public void buysShop(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		UserInfo userInfo = null;
		try {
			userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long propId = input.readInt();// 购买的物品id
			int buysCount = input.readInt();// 购买数量
			boolean bool = input.readBoolean();// 是否优先使用绑定金币
			if (buysCount <= 0) {
				gameOutput.writeInt(CommonCmd.PROP_COUNT_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Prop prop = getPropId(propId);
			if (prop == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (prop.getBuysType() != 1) {
				gameOutput.writeInt(CommonCmd.ERROR_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long gold = buysCount*prop.getGoldPrice();// 购买此数量的商品  需要消耗的钱
			if ((userInfo.getPlayerGold()+userInfo.getLockGold()) < gold) { // 金币不满足购买商品
				gameOutput.writeInt(CommonCmd.GOLD_INSUFFICIENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int propCount = (int) Math.ceil((double)buysCount/prop.getMaxCount()); // 购买后  此物品生成个数
			Backpack backpack = userInfo.getBackpack();
			
			if (!backpack.checkEmptyCount(propCount)) {
				gameOutput.writeInt(CommonCmd.BACKPACK_INSUFFICIENT_SPACE);//背包空间不足 
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			List<Prop> lists = new ArrayList<>();
			prop = propConfig.getSample(propId);
			prop.setCount(0);
			if (bool) {
				if (userInfo.getLockGold() == 0) {
					bool = false;
				}
			} else {
				if (userInfo.getPlayerGold() == 0) {
					bool = true;
				}
			}
			for (int i = 0; i < buysCount; i++) {
				if (bool) {// 是否优先使用绑定金币
					if (userInfo.getLockGold() == 0) {
						bool = false;
						lists.add(prop);
						prop = propConfig.getSample(propId);
						prop.setCount(0);
					}
				} else {
					if (userInfo.getPlayerGold() == 0) {
						bool = true;
						lists.add(prop);
						prop = propConfig.getSample(propId);
						prop.setCount(0);
					}
				}
				if (prop.getCount() == prop.getMaxCount()) {
					lists.add(prop);
					prop = propConfig.getSample(propId);
					prop.setCount(0);
				}
				prop.setCount(prop.getCount()+1);
				buysProp(userInfo, prop, bool);
			}
//			System.out.println(prop);
			lists.add(prop);
			int len = lists.size();
			int buyCount = 0;// 实际购买的数量
			gameOutput.writeInt(len);
			for (int i = 0; i < len; i++) {
				prop = lists.get(i);
				backpack.addProp(prop);
				buyCount += prop.getCount();
				prop.writeData(gameOutput);
			}
			
			userServer.updateBackpack(userInfo);
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setPlayerGold(userInfo.getPlayerGold());
			info.setLockGold(userInfo.getLockGold());
			userServer.update(info);
			
			userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 2, propId, buyCount);
			
			userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
			gameOutput.reset().writeLong(userInfo.getPlayerGold()).writeLong(userInfo.getLockGold());
			userInfo.sendData(SocketCmd.UPDATE_GOLD, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("商店购买报错!" + userInfo, e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("商店购买关闭流报错!", e);
			}
		}
	}

	/**
	 * 购买此物品
	 * @param userInfo 
	 * @param prop 要购买的物品
	 * @param bool 是否优先使用金币
	 */
	private void buysProp(UserInfo userInfo, Prop prop, boolean bool) {
		long gold = prop.getGoldPrice();
		if (bool) {
			gold = userInfo.getLockGold()-gold;// 当前的绑定金币  减去 购买金币
			if (gold < 0) {// 不够 
				if (gold != prop.getGoldPrice()) {
					prop.setTrade(false);
				}
				userInfo.setLockGold(0);
				gold = userInfo.getPlayerGold()-(gold*-1);
				if (gold < 0) {
					userInfo.setPlayerGold(0);
				} else {
					userInfo.setPlayerGold(gold);
				}
			} else {
				prop.setTrade(false);
				// 大于0  说明绑定金币  还有剩余
				userInfo.setLockGold(gold);
			}
		} else {
			gold = userInfo.getPlayerGold()-gold;
			if (gold < 0) {
				prop.setTrade(false);
				userInfo.setPlayerGold(0);
				gold = userInfo.getLockGold()-(gold*-1);
				if (gold < 0) {
					userInfo.setLockGold(0);
				} else {
					userInfo.setLockGold(gold);
				}
			} else {
				// 大于0  说明金币  还有剩余
				userInfo.setPlayerGold(gold);
			}
		}
	}

	public void shopAllGoods(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int len = shops.size();
			gameOutput.writeInt(len);
			for (int i = 0; i < len; i++) {
				gameOutput.writeLong(shops.get(i).getId());
			}
			userInfo.sendData(SocketCmd.SHOP_ALL_GOODS, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("元宝商城可以购买的商品报错", e);
		} finally {
			try {
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("元宝商城可以购买的商品关闭流报错", e);
			}
		}
	}

}
