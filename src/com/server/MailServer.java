package com.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.PropConfig;
import com.data.UserDataManage;
import com.entity.Mail;
import com.entity.Prop;
import com.entity.UserInfo;
import com.model.Backpack;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.thread.SendDataThread;
import com.utils.BytesFactory;

/**
 * 邮件处理
 * @author xujinbo
 *
 */
public class MailServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private SendDataThread sendDataThread;
	@Resource
	private UserServer userServer;
	@Resource
	private PropConfig propConfig;
	
	public MailServer() {
	}
	
	/**
	 * 玩家添加邮件
	 * @param buf
	 * @param session
	 */
	public void addMail(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			String receiveUser = gameInput.readUTF();// 收件人
			
			UserInfo userTwo = userDataManage.getUserInfoData(receiveUser);
			if (userTwo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userTwo.getMails().size() >= 100) {
				gameOutput.writeInt(CommonCmd.MAIL_FULL);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			String theme = gameInput.readUTF().trim();// 主题
			if (theme.length() > 10) {
				gameOutput.writeInt(CommonCmd.INPUT_LEN_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			String content = gameInput.readUTF().trim();// 内容
			if (content.length() > 300) {
				gameOutput.writeInt(CommonCmd.INPUT_LEN_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int attachmentLen = gameInput.readInt(); // 附件数量
			if (attachmentLen > 5) {
				gameOutput.writeInt(CommonCmd.INPUT_LEN_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Backpack backpack = userInfo.getBackpack();
			// 要发邮件发出去的  物品
			List<Object> propIdList = new ArrayList<>();
			// 客户端要删除的物品
			List<Prop> propList = new ArrayList<>();
			Prop prop;
			Prop propTow;
			int count;
			gameOutput.writeInt(attachmentLen);
			for (int i = 0; i < attachmentLen; i++) {
				prop = backpack.getPropIndex(gameInput.readInt());
				if (prop == null) {
					gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				count = gameInput.readInt();
				if (prop.getCount() < count || count < 1) {
					gameOutput.writeInt(CommonCmd.PROP_COUNT_ERROR);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				if (prop.isLock() || !prop.isTrade()) {
					gameOutput.writeInt(CommonCmd.PROP_NOT_TRADE);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				propIdList.add(prop.getId());
				propIdList.add(count);
				propIdList.add(prop.isTrade());
				
				gameOutput.writeInt(prop.getIndex());
				gameOutput.writeInt(count);
				
				propTow = propConfig.getSample(prop.getId());
				propTow.setCount(count);
				propTow.setIndex(prop.getIndex());
				propList.add(propTow);
				
			}
			int len = propList.size();
			for (int i = 0; i < len; i++) {
				prop = propList.get(i);
				backpack.removePropIndexCount(prop.getIndex(), prop.getCount());
			}
			if (len != 0) {
				userServer.updateBackpack(userInfo);
				userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
			}
			
			// 添加到此玩家的邮件箱
			gameOutput.reset();
			Mail mail = new Mail();
			mail.setId(userTwo.getMailId());
			mail.setType(1);
			mail.setSender(userInfo.getRoleName());
			mail.setTheme(theme);
			mail.setContent(content);
			mail.setLists(propIdList);
			mail.setSendData(System.currentTimeMillis());
			mail.writeData(gameOutput);
			userTwo.getMails().add(mail);
			
			userServer.updateMailByte(userTwo);
			sendDataThread.addActionData(SocketCmd.ADD_MAIL, gameOutput.toByteArray(), userTwo);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("添加邮件报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("添加邮件关闭流报错", e);
			}
		}
	}

	/**
	 * 删除邮件
	 * @param buf
	 * @param session
	 */
	public void deleteMail(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			gameInput = new GameInput(buf);
			List<Mail> mails = userInfo.getMails();
			int len = gameInput.readInt(); // 要删除的数量
			if (len > mails.size()) {
				gameOutput.writeInt(CommonCmd.DELETE_MAIL_NUM_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			for (int i = 0; i < len; i++) {
				int mailId = gameInput.readInt();// 邮件id
				if(mails.remove(new Mail(mailId))) {
					userServer.updateMailByte(userInfo);
					userInfo.sendData(SocketCmd.DELETE_MAIL, mailId);
				}
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("删除邮件报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("删除邮件关闭流报错", e);
			}
		}
	}
	
	/**
	 * 阅读邮件
	 * @param buf
	 * @param session
	 */
	public void readMail(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int mailId = gameInput.readInt();// 邮件id
			List<Mail> mails = userInfo.getMails();
			int index = mails.indexOf(new Mail(mailId));
			if(index != -1) {
				mails.get(index).setRead(true);
			}
			userServer.updateMailByte(userInfo);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("阅读邮件报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("阅读邮件关闭流报错", e);
			}
		}
	}
	
	/**
	 * 请求邮件数据
	 * @param buf
	 * @param session
	 */
	public void requestMailData(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			List<Mail> mails = userInfo.getMails();
			int len = mails.size();
			gameOutput.writeInt(len);
			for (int i = 0; i < len; i++) {
				mails.get(i).writeData(gameOutput);
			}
			userInfo.sendData(SocketCmd.REQUEST_MAIL_DATA, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("请求邮件数据报错", e);
		} finally {
			try {
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("请求邮件数据关闭流报错", e);
			}
		}
	}
	
	/**
	 * 提取物品
	 * @param buf
	 * @param session
	 */
	public void extractGoods(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			gameInput = new GameInput(buf);
			List<Mail> mails = userInfo.getMails();
			int len = gameInput.readInt(); // 要提取的邮件数量
			mails = getExtraceMail(mails);
			if (len == 0 || mails.size() == 0) {
				gameOutput.writeInt(CommonCmd.MAIL_NOT_EXTRACT_GOODS);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (len > mails.size()) {
				gameOutput.writeInt(CommonCmd.DELETE_MAIL_NUM_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Mail mail;
			Backpack backpack = userInfo.getBackpack();
			List<Object> integers;
			int inteLen;
			//提取的物品 
			List<Prop> propList = new ArrayList<>();
			//处理了的邮件
			List<Mail> mailList = new ArrayList<>();
			Prop prop;
			for (int i = 0; i < len; i++) {
				int mailId = gameInput.readInt();// 邮件id
				mail = getMailId(mails, mailId);
				if (mail != null) {
					integers = mail.getLists();
					inteLen = integers.size();
					if (!backpack.checkEmptyCount(inteLen/3)) { // 如果包裹中领取物品的空栏不足
						gameOutput.writeInt(CommonCmd.BACKPACK_INSUFFICIENT_SPACE);
						userInfo.sendData(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray());
						break;
					}
					for (int j = 0; j < inteLen; j+=3) {
						prop = propConfig.getSample(Long.parseLong(integers.get(j).toString()));
						if (prop != null) {
							prop.setCount((int) integers.get(j+1));
							prop.setTrade((boolean) integers.get(j+2));
							propList.add(prop);
							backpack.addProp(prop);
							userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 2, prop.getId(), prop.getCount());
						}
					}
					mail.setRead(true);
					mail.setReceive(true);
					mailList.add(mail);
				}
			}
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setBackpackByte(BytesFactory.objectToByteData(backpack));
			info.setMailByte(BytesFactory.listToBytes(userInfo.getMails()));
			userServer.update(info);
			int propLen = propList.size();
			if (propLen == 0) {
				return;
			}
			gameOutput.reset().writeInt(propLen);
			for (int i = 0; i < propLen; i++) {
				prop = propList.get(i);
				prop.writeData(gameOutput);
			}
			// 通知客服端添加物品
			userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
			// 通知客户端邮件提取
			int mailLen = mailList.size();
			if (mailLen == 0) {
				return;
			}
			gameOutput.reset();
			gameOutput.writeInt(mailLen);
			for (int i = 0; i < mailLen; i++) {
				mail = mailList.get(i);
				gameOutput.writeLong(mail.getId());
			}
			userInfo.sendData(SocketCmd.EXTRACT_GOODS, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("删除邮件报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("删除邮件关闭流报错", e);
			}
		}
	}

	/**
	 * 根据id从指定数组中获取邮件
	 * @param mails
	 * @param mailId
	 * @return 
	 */
	private Mail getMailId(List<Mail> mails, long mailId) {
		int len = mails.size();
		for (int i = 0; i < len; i++) {
			if (mails.get(i).getId() == mailId) {
				return mails.get(i);
			}
		}
		return null;
	}

	/**
	 * 获取此邮件里面的所有邮件  中可以提取附件的  邮件
	 * @param mails
	 * @return
	 */
	private List<Mail> getExtraceMail(List<Mail> mails) {
		List<Mail> lists = new ArrayList<>();
		int len = mails.size();
		Mail mail;
		for (int i = 0; i < len; i++) {
			mail = mails.get(i);
			if (!mail.isReceive() && mail.getLists().size() > 0) {
				lists.add(mail);
			}
		}
		return lists;
	}

}
