package com.server;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.UserDataManage;
import com.entity.UserInfo;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.thread.RankingThread;

/**
 * 排行榜数据处理
 * @author xujinbo
 *
 */
public class RankingListServer {
	
	@Resource
	private UserDataManage userDataManage;
	private RankingThread rankingThread;
	
	public RankingListServer() {
		rankingThread = new RankingThread();
	}
	
	/**
	 * 请求排行榜数据
	 * @param buf
	 * @param session
	 */
	public void rankingRequest(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			int rankID = gameInput.readInt();// 排行榜id
			gameOutput.writeInt(rankID);
			switch (rankID) {
				case CommonCmd.ROLE_LEVEL://等级排行
					rankingThread.getRankingList().writeDataRoleLevel(gameOutput);
					userInfo.sendData(SocketCmd.RANKING_REQUEST, gameOutput.toByteArray());
					break;
				case CommonCmd.ROLE_ORDINARY_ATTACK://物伤排行
					rankingThread.getRankingList().writeDataRoleOrdinaryAttack(gameOutput);
					userInfo.sendData(SocketCmd.RANKING_REQUEST, gameOutput.toByteArray());
					break;
				case CommonCmd.ROLE_MAGIC_ATTACK://法伤排行
					rankingThread.getRankingList().writeDataRoleMagic(gameOutput);
					userInfo.sendData(SocketCmd.RANKING_REQUEST, gameOutput.toByteArray());
					break;
				case CommonCmd.ROLE_SPEED://速度排行
					rankingThread.getRankingList().writeDataRoleSpeed(gameOutput);
					userInfo.sendData(SocketCmd.RANKING_REQUEST, gameOutput.toByteArray());
					break;
				case CommonCmd.ROLE_DEFENSE://防御排行
					rankingThread.getRankingList().writeDataRoleDefend(gameOutput);
					userInfo.sendData(SocketCmd.RANKING_REQUEST, gameOutput.toByteArray());
					break;
				case CommonCmd.PET_ORDINARY_ATTACK://物伤排行
					rankingThread.getRankingList().writeDataPetOrdinary(gameOutput);
					userInfo.sendData(SocketCmd.RANKING_REQUEST, gameOutput.toByteArray());
					break;
				case CommonCmd.PET_MAGIC_ATTACK://法伤排行
					rankingThread.getRankingList().writeDataPetMagic(gameOutput);
					userInfo.sendData(SocketCmd.RANKING_REQUEST, gameOutput.toByteArray());
					break;
				case CommonCmd.PET_SPEED://速度排行
					rankingThread.getRankingList().writeDataPetSpeed(gameOutput);
					userInfo.sendData(SocketCmd.RANKING_REQUEST, gameOutput.toByteArray());
					break;
				case CommonCmd.PET_DEFENSE://防御排行
					rankingThread.getRankingList().writeDataPetDefend(gameOutput);
					userInfo.sendData(SocketCmd.RANKING_REQUEST, gameOutput.toByteArray());
					break;
				default:
					break;
			}
			
		
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("请求排行榜数据报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("请求排行榜数据关闭流报错", e);
			}
		}
		
	}

	public void start() {
		rankingThread.start();
		Logger.getLogger(getClass()).info("排行榜启动");
	}

	public void stop() {
		rankingThread.isOpen = false;
	}

}
