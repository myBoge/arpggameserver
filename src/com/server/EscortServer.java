package com.server;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Action;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.MapConfig;
import com.data.ConnectData;
import com.data.UserDataManage;
import com.entity.Escort;
import com.entity.EscortData;
import com.entity.EscortMap;
import com.entity.FightData;
import com.entity.Role;
import com.entity.Team;
import com.entity.UserInfo;
import com.mapper.EscortMapper;
import com.mapper.UserMapper;
import com.model.EscortModel;
import com.model.GameData;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.server.fight.FightBox;
import com.thread.EscortThread;
import com.utils.BytesFactory;
import com.utils.LuaUtils;
import com.utils.RandomUtils;

/**
 * 押镖
 * 
 * @author xujinbo
 * 
 */
public class EscortServer {

	@Resource
	private EscortModel escortModel;
	@Resource
	private UserDataManage userDataManage;
	@Resource
	private MapConfig mapConfig;
	@Resource
	private UserServer userServer;
	@Resource
	private FightServer fightServer;
	@Resource
	private TeamServer teamServer;

	private EscortThread escortThread;

	public EscortServer() {
	}

	@Action({ CommonCmd.INIT_ESCORT })
	protected void init() {
		escortThread = new EscortThread();
		escortThread.setEscortServer(this);
		escortThread.setMapConfig(mapConfig);
		escortThread.setEscortModel(escortModel);
	}

	/**
	 * 启动押镖线程
	 */
	public void start() {
		escortThread.start();
		Logger.getLogger(getClass()).info("押镖启动");
	}

	public void stop() {
		escortThread.isOpen = false;
	}

	/**
	 * 创建一个新的押镖 (注意 队长 必须第一个)
	 * 
	 * @param session
	 * @param bs
	 */
	public void createEscort(byte[] bs, IoSession session) {
		GameInput input = new GameInput(bs);
		GameOutput gameOutput = new GameOutput();
		UserInfo userInfo = null;
		try {
			userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int mapId = input.readInt();// 押镖地图id
			long foregift = input.readInt();// 押金
			// System.out.println("挂机信息="+mapId+", "+userInfo.getId());
			EscortMap escortMap = (EscortMap) mapConfig.getCacheSample(mapId);
			if (escortMap == null) {// 没有此押镖地图
				gameOutput.reset().writeInt(CommonCmd.ESCORT_NOT_MAP);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			if (escortMap.get_levels()[0] > userInfo.getRole().getLevel() || escortMap.get_levels()[1] < userInfo.getRole().getLevel()) {
				gameOutput.reset().writeInt(CommonCmd.YOU_LEVEL_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			// 条件判断
			if (userInfo.getPlayerGold() < foregift) {
				gameOutput.reset().writeInt(CommonCmd.GOLD_INSUFFICIENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查体力 体力小于此地图宽度不可挂机
			if (userInfo.getRole().getStrength() < escortMap.getWidth()) {
				gameOutput.reset().writeInt(CommonCmd.STRENGTH_LOW);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// System.out.println("成功挂机"+userInfo);
			List<Escort> list = escortThread.getEscortList(mapId);
			Escort escort = escortThread.getEscortUser(mapId, userInfo);
			if (escort != null || !userInfo.isIdle()) {// 如果该角色有挂机信息存在
				// Logger.getLogger(getClass()).warn("该角色有挂机信息存在!"+userInfo);
				gameOutput.reset().writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			Team team = teamServer.getTeam(userInfo.getTeamId());
			if (team != null) {
				gameOutput.reset().writeInt(CommonCmd.NOT_TEAM_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			escort = escortModel.getNewEsocrt(mapId);
			userInfo.getRole().setState(CommonCmd.ESCORT);
			userInfo.setPlayerGold(userInfo.getPlayerGold() - foregift);

			UserInfo infoData = new UserInfo(userInfo.getId());
			infoData.setPlayerGold(userInfo.getPlayerGold());
			infoData.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
			userServer.update(infoData);// 状态更改 更新此角色的信息

			// 初始化押镖信息
			// 添加挂机时间
			escort.getUserInfoList().add(userInfo);
			escort.setStartDate(System.currentTimeMillis());
			escort.setForegift(foregift);
			escort.setResidueGold(foregift * escortMap.getExchangeRat()* escort.getUserInfoList().size());
			escort.setTotalGold(escort.getResidueGold());

			escort.setY(escortModel.getEscortY());
			// 通知客户端扣钱
			gameOutput.reset().writeLong(userInfo.getPlayerGold()).writeLong(userInfo.getLockGold());
			session.write(new ActionData<>(SocketCmd.UPDATE_GOLD, gameOutput.toByteArray() ));

			// 通知当前在这个地图的玩家
			gameOutput.reset().writeLong(mapId);
			escort.writeData(gameOutput);
			escortThread.sendAllData(mapId, SocketCmd.ADD_ESCORT, gameOutput.toByteArray());

			System.out.println("更新到押镖队列");
			// 添加到地图队列 地图有自动存储功能 会将里面押镖的人自动存入数据库   这里不用存储
			list.add(escort);

			// 向客户端发送用户状态变更
			gameOutput.reset().writeInt(userInfo.getRole().getState());
			userInfo.sendData(SocketCmd.UPDATE_USER_STATE, gameOutput.toByteArray());
			gameOutput.reset().writeLong(mapId);
			userInfo.sendData(SocketCmd.ENTER_ESCORT, gameOutput.toByteArray());
			sendEnterEscortData(session, mapId, userInfo);

		} catch (IOException e) {
			Logger.getLogger(getClass()).error("创建一个新的押镖" + userInfo, e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("创建一个新的押镖关闭流报错", e);
			}
		}
	}

	/**
	 * 用户请求押镖场景数据
	 * @param buf
	 * @param session
	 */
	public void getEscortMapData(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput output = new GameOutput();
		try {
			long mapId = input.readInt();// 押镖地图id
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				output.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, output.toByteArray()));
				return;
			}
			// 用户不再此场景和主场景
			if (userInfo.getCurrentScene() != CommonCmd.SCENE_ESCORT &&
					!(userInfo.getCurrentScene() == CommonCmd.SCENE_GAME && userInfo.getCurrentSceneValue()==1000)) {
				output.writeInt(CommonCmd.USER_NOT_MAIN_SCENE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, output.toByteArray()));
				return;
			}
			// System.out.println("挂机信息="+mapId+", "+userInfo.getId());
			if (!escortThread.containsEscortMap(mapId)) {
				// 没有找到地图
				output.writeInt(CommonCmd.ESCORT_NOT_MAP);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, output.toByteArray()));
			}
			sendEnterEscortData(session, mapId, userInfo);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("用户请求押镖场景数据报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (output != null)
					output.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("用户请求押镖场景数据关闭流报错", e);
			}
		}
	}

	/**
	 * 发送进入地图 和地图信息
	 * 
	 * @param session
	 * @param mapId
	 * @param userInfo
	 * @throws IOException
	 */
	public void sendEnterEscortData(IoSession session, long mapId,
			UserInfo userInfo) throws IOException {
		GameData.gameData.setupCurrentScene(userInfo, CommonCmd.SCENE_ESCORT, mapId);
		// 状态更改 更改玩家当前所在场景
		UserInfo infoData = new UserInfo(userInfo.getId());
		GameData.gameData.setupCurrentScene(infoData, userInfo);
		userServer.update(infoData);
		byte[] bytes = getSendEscortData(mapId);
		session.write(new ActionData<>(SocketCmd.ESCORT_MAP_DATA, bytes));
	}

	/**
	 * 将当前押镖数据处理二进制 数据 准备发送客户端
	 * @param mapId
	 * @param list
	 * @return
	 */
	private byte[] getSendEscortData(long mapId) {
		List<Escort> escortList = escortThread.getEscortList(mapId);
		// System.out.println("押镖数量="+escortList.size());
		byte[] bytes = null;
		GameOutput gameOutput = null;
		try {
			gameOutput = new GameOutput();

			int len = escortList.size();
			Escort escort;
			gameOutput.writeLong(mapId);
			gameOutput.writeInt(len);
			Iterator<Escort> escortIterator = escortList.iterator();
			while (escortIterator.hasNext()) {
				escort = escortIterator.next();
				escort.writeData(gameOutput);
			}
			bytes = gameOutput.toByteArray();
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("向客户端发送押镖数据报错", e);
		} finally {
			try {
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bytes;
	}

	public EscortThread getEscortThread() {
		return escortThread;
	}

	/**
	 * 用户请求停止押镖
	 * 
	 * @param buf
	 * @param session
	 */
	public void stopEscort(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			long mapId = input.readInt();// 押镖地图id
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getCurrentScene() != CommonCmd.SCENE_ESCORT) 
				return;
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			removeEscort(mapId, userInfo, session);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("用户请求停止押镖报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("用户请求停止押镖关闭流报错", e);
			}
		}
	}

	/**
	 * 删除一个押镖信息
	 * @param mapId
	 * @param userInfo
	 * @param session
	 */
	public void removeEscort(long mapId, UserInfo userInfo, IoSession session) {
		GameOutput gameOutput = null;
		try {
			if (userInfo == null)
				return;
			gameOutput = new GameOutput();
			if (!escortThread.containsEscortMap(mapId)) {
				// 没有找到地图
				if (session != null && session.isConnected()) {
					gameOutput.writeInt(CommonCmd.ESCORT_NOT_MAP);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				}
				return;
			}
			Escort escort = escortThread.getEscortUser(mapId, userInfo);
			if (escort == null) {
				if (session != null && session.isConnected()) {
					gameOutput.writeInt(CommonCmd.ESCORT_INFO_NOT_MAP);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				}
				return;
			}
			// 从押镖列表中删除
			escortThread.removeEscort(mapId, userInfo);
			if (escort.getTypes() == null) { // 如果是玩家  继续处理
				userInfo.getRole().setState(CommonCmd.IDLE);
				// 处理用户升级
				// System.out.println(userInfo);
				GameData.gameData.userInfoUpLevel(userInfo);
				// System.out.println(userInfo);
				// 保存一次玩家数据
				userServer.updateUserRole(userInfo);
				// 向客户端发送用户状态变更
				gameOutput.writeInt(userInfo.getRole().getState());
				userInfo.sendData(SocketCmd.UPDATE_USER_STATE, gameOutput.toByteArray());
				
				GameData.gameData.setupCurrentScene(userInfo, CommonCmd.SCENE_GAME);
				userServer.updateCurrentScene(userInfo);
				userInfo.sendData(SocketCmd.EXIT_ESCORT);
				if (escort.getUserInfoList().size() == 0) {
					gameOutput.reset().writeLong(mapId).writeLong(userInfo.getId());
					escortThread.sendAllData(mapId, SocketCmd.MAP_REMOVE_ESCORT, gameOutput.toByteArray());
				}
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("删除一个押镖信息报错", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("删除一个押镖信息关闭流报错", e);
				}
			}

		}
	}

	/**
	 * 押镖数据 保存一次
	 * 
	 * @param iterator
	 */
	public void updateMysqlData(Iterator<Long> iterator) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			EscortMapper escortMapper = sqlSession
					.getMapper(EscortMapper.class);
			UserMapper userManage = sqlSession.getMapper(UserMapper.class);
			long mapID;
			List<Escort> list;
			byte[] bytes;
			int len;
			Escort escort;
			UserInfo userInfo;
			List<UserInfo> userInfoList;
			while (iterator.hasNext()) {
				mapID = iterator.next();
				list = escortThread.getEscortList(mapID);
				bytes = BytesFactory.listToBytes(list);
				// 处理此地图的更新
				EscortData escortData = new EscortData(mapID, bytes);
				escortMapper.update(escortData);
				// 保存玩家体力
				len = list.size();
				for (int i = 0; i < len; i++) {
					escort = list.get(i);
					if (escort.getTypes() == null) {
						userInfoList = escort.getUserInfoList();
						for (int j = 0; j < userInfoList.size(); j++) {
							userInfo = new UserInfo(userInfoList.get(j).getId());
							userInfo.setBytes(BytesFactory.objectToByteData(userInfoList.get(j).getRole()));
							userManage.update(userInfo);
						}
					}
				}

			}
			sqlSession.commit();
			// System.out.println("保存一次");
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("保存地图挂机数据错误", e);
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}
	}

	/**
	 * 玩家打劫
	 * @param buf
	 * @param session
	 */
	public void mapRobEscort(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		try {
			long escortId = input.readInt(); // 被劫镖的人的镖局
			long mapId = input.readInt();
			List<Escort> mapEscortList = escortThread.getEscortList(mapId);
			EscortMap escortMap = (EscortMap) mapConfig.getCacheSample(mapId);
			GameOutput gameOutput = new GameOutput();
			if (mapEscortList == null || escortMap == null) {
				// 地图不存在
				gameOutput.writeInt(CommonCmd.MAP_NON_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.reset().writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getCurrentScene() != CommonCmd.SCENE_ESCORT) 
				return;
			// 打劫玩家的信息
			Escort robEscort = escortThread.getEscortUser(mapId, userInfo);
			if (robEscort == null) {
				gameOutput.reset().writeInt(CommonCmd.MAP_ROB_USER_NON_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE,
						gameOutput.toByteArray()));
				return;
			}
			// 被打劫玩家信息
			Escort robberyEscort = escortThread.getEscortEscortId(mapId, escortId);
			if (robberyEscort == null) {
				// 被打劫玩家不存在
				gameOutput.reset().writeInt(CommonCmd.MAP_ROBBERY_USER_NON_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 对此人可打劫金币判断
			int minLivingGold = escortMap.getMinLivingGold();
			long minGold = robberyEscort.getTotalGold() * (minLivingGold / 100);// 计算出此人最低押送最低金额
			long robGold = robberyEscort.getResidueGold() - minGold;// 可打劫金币
			if (robGold <= 0) {
				gameOutput.reset().writeInt(CommonCmd.MAP_ESCORT_DEFEND_STATE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 判断打劫的玩家是否已经在战斗
			if (robEscort.isFight()) {
				gameOutput.reset().writeInt(CommonCmd.YOU_AT_FIGHT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 判断被打劫的玩家是否已经在战斗
			if (robberyEscort.isFight()) {
				gameOutput.reset().writeInt(CommonCmd.THIS_AT_FIGHT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 此二人停止移动
			robEscort.setFight(true);
			robberyEscort.setFight(true);

			gameOutput.reset()
				.writeLong(mapId)
				.writeLong(robEscort.getId())
				.writeLong(robberyEscort.getId())
				.writeBoolean(true);
			
			escortThread.sendAllData(mapId, SocketCmd.MAP_UPDATE_FIGHT_STATE, gameOutput.toByteArray());

			List<FightData> robList = robEscort.getFightList(false); // 打劫玩家
			List<FightData> robberyList = robberyEscort.getFightList(true);// 被打劫玩家
			
			FightBox fightBox = fightServer.createAIFight(robList, robberyList, false);
			fightBox.setFightType(CommonCmd.ROB);
			fightBox.obj = Arrays.asList(mapId, robEscort.getId(), robberyEscort.getId());
			// 发送 战斗数据
			gameOutput.reset();
			fightBox.writeTeamData(gameOutput);
			
			ActionData<Object> actionData = new ActionData<>(SocketCmd.FIGHT_DATA, gameOutput.toByteArray());
			robEscort.sendAllData(actionData);
			
			gameOutput.close();
			fightServer.startFight(fightBox.getId());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("玩家打劫报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("玩家打劫关闭流报错", e);
			}
		}
	}

	/**
	 * 战斗结束
	 * @param fightServer2
	 */
	@SuppressWarnings("unchecked")
	public void fightEnd(FightBox fightBox) {
		List<Long> escortList = (List<Long>) fightBox.obj;
		// if (escortList == null) return;
		long mapId = escortList.get(0);
		long robEscortId = escortList.get(1);
		long robberyEscortId = escortList.get(2);
		fightBox.obj = null;
		// 打劫
		Escort robEscort = escortThread.getEscortEscortId(mapId, robEscortId);
		EscortMap escortMap = (EscortMap) mapConfig.getCacheSample(mapId);
		// 被打劫
		Escort robberyEscort = escortThread.getEscortEscortId(mapId, robberyEscortId);
		if (robEscort != null && robberyEscort != null) {
			// 此二人恢复移动
			robEscort.setFight(false);
			robberyEscort.setFight(false);

			GameOutput gameOutput = new GameOutput();
			try {
				gameOutput.writeLong(mapId);
				gameOutput.writeLong(robEscort.getId());
				gameOutput.writeLong(robberyEscort.getId());
				gameOutput.writeBoolean(false);
				escortThread.sendAllData(mapId,
						SocketCmd.MAP_UPDATE_FIGHT_STATE,
						gameOutput.toByteArray());

				if (fightBox.getFightResult() == 1) {// 进攻方胜利
					// 通知这两个家伙
					robEscort.setRobCount(robEscort.getRobCount() + 1);
					// 设置打劫到的金额
					int minLivingGold = escortMap.getMinLivingGold();
					long minGold = robberyEscort.getTotalGold()
							* (minLivingGold / 100);// 计算出此人最低押送最低金额
					long robTotalGold = robberyEscort.getTotalGold() - minGold;//
					// 原始总金币中可打劫金币
					long robGold = robberyEscort.getResidueGold() - minGold;//
					// 剩余金币中可打劫金币

					// System.out.println("总金币="+robTotalGold+"  可以打劫金币="+robGold);
					if (robGold != 0) {
						if (robGold > robTotalGold * 0.3) {// 如果已经小于最小值 那么就全部被劫
							double per = RandomUtils.nextDouble(20, 50);
							robGold = (long) (robGold * (per / 100));
							// System.out.println("打劫百分比="+per);
							// System.out.println("不需要全部打劫="+robGold);
						}
						robEscort.setRobGold(robEscort.getRobGold() + robGold);
						robEscort.setResidueGold(robEscort.getResidueGold()
								+ robGold);
					}
					// System.out.println("被打劫金币="+robGold+"  "+robEscort);
					// 修改劫镖的数据
					robberyEscort.setRobberyCount(robberyEscort
							.getRobberyCount() + 1);
					robberyEscort.setResidueGold(robberyEscort.getResidueGold()
							- robGold);

					updateRobInfo(robEscort);
					updateRobInfo(robberyEscort);

					gameOutput.reset();
					gameOutput.writeLong(mapId); // 押镖地图
					gameOutput.writeLong(robEscort.getId()); // 押镖ID
					gameOutput.writeInt(robEscort.getRobCount()); // 劫镖次数
					gameOutput.writeLong(robEscort.getRobGold());// 劫镖所得总金币
					gameOutput.writeLong(robEscort.getResidueGold());//
					// 押镖当前剩余金币
					robEscort.sendAllData(SocketCmd.MAP_ROB_RESULT, gameOutput.toByteArray());

					gameOutput.reset();
					gameOutput.writeLong(mapId); // 押镖地图
					gameOutput.writeLong(robberyEscort.getId()); // 押镖ID
					gameOutput.writeInt(robberyEscort.getRobberyCount()); // 被劫镖次数
					gameOutput.writeLong(robberyEscort.getResidueGold()); // 押镖当前剩余金币
					robberyEscort.sendAllData(SocketCmd.MAP_ROBBERY_RESULT, gameOutput.toByteArray());
				} else if(fightBox.getFightResult() == 2){
					//进攻方失败
					List<FightData> fightDataList = fightBox.getRobList();
					FightData fightData;
					UserInfo userInfo;
					Role role;
					for (int i = 0; i < fightDataList.size(); i++) {
						fightData = fightDataList.get(i);
						if (fightData != null) {
							userInfo = fightData.getUserInfo();
							role = userInfo.getRole();
							if (!role.isPet()) {// 如果是玩家
								// 战斗中死亡
								if (fightData.isDeath()) {
									// 死亡惩罚
									LuaUtils.get("fightUserDeath").call(CoerceJavaToLua.coerce(userInfo));
								}
							}
						}
					}
				}
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("劫镖战斗结束  发送客户端信息错误", e);
			} finally {
				if (gameOutput != null) {
					try {
						gameOutput.close();
					} catch (IOException e) {
						Logger.getLogger(getClass()).error("战斗结束发送客户端数据报错!", e);
					}
				}
			}
		} else {
			Logger.getLogger(getClass()).warn(
					"超大问题!劫镖战斗结束后找不到镖局信息\n劫镖=" + robEscort + "\n被劫="
							+ robberyEscort);
		}
	}

	/**
	 * 更新此人劫镖信息
	 * @param escort
	 * @param userInfo
	 */
	private void updateRobInfo(Escort escort) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			EscortMapper escortMapper = sqlSession
					.getMapper(EscortMapper.class);
			List<Escort> list = escortThread.getEscortList(escort.getMapId());
			byte[] bytes = BytesFactory.listToBytes(list);
			// 处理此地图的更新
			EscortData escortData = new EscortData(escort.getMapId(), bytes);
			escortMapper.update(escortData);
			sqlSession.commit();
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("更新此人劫镖信息报错", e);
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}
	}

	/**
	 * 获取userServer
	 * @return userServer userServer
	 */
	public UserServer getUserServer() {
		return userServer;
	}

}
