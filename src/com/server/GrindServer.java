package com.server;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Action;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.MapConfig;
import com.data.UserDataManage;
import com.entity.FightData;
import com.entity.GrindMap;
import com.entity.GrindTeam;
import com.entity.Role;
import com.entity.Team;
import com.entity.UserInfo;
import com.model.GameData;
import com.model.GrindModel;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.server.fight.FightBox;
import com.thread.GrindThread;
import com.utils.CommonUtils;
import com.utils.LuaUtils;
import com.utils.RandomUtils;

/**
 * 副本练级处理
 * @author xujinbo
 */
public class GrindServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private MapConfig grindConfig;
	@Resource
	private GrindModel grindModel;
	@Resource
	private UserServer userServer;
	@Resource
	private PetServer petServer;
	@Resource
	private FightServer fightServer;
	@Resource
	private TeamServer teamServer;
	
	private GrindThread grindThread;
	
	@Action({ CommonCmd.INIT_GRIND })
	protected void init() {
		grindThread = new GrindThread();
		grindThread.setGrindServer(this);
		grindThread.setGrindConfig(grindConfig);
		
	}
	
	/**
	 * 启动练级线程
	 */
	public void start() {
		grindThread.start();
		Logger.getLogger(getClass()).info("练级地图启动");
	}

	public void stop() {
		grindThread.isOpen = false;
	}
	
	/**
	 * 历练开始
	 * @param buf
	 * @param session
	 */
	public void grindStart(byte[] buf, IoSession session) {
		GameOutput gameOutput = new GameOutput();
		UserInfo userInfo = null;
		try {
			userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.reset().writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if(userInfo.getCurrentScene() != CommonCmd.SCENE_GRIND) {
				gameOutput.writeInt(CommonCmd.ERROR_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long mapId = userInfo.getCurrentSceneValue();
//			System.out.println("练级地图="+mapId);
			GrindMap grindMap = grindConfig.getCacheSample(mapId);
			if (grindMap == null) {
				gameOutput.writeInt(CommonCmd.ERROR_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Map<Long, List<GrindTeam>> grindData = grindThread.getMapGrindData();
			List<GrindTeam> lists = grindData.get(mapId);
			if (lists == null) {
				gameOutput.writeInt(CommonCmd.ERROR_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			Team team = teamServer.getTeam(userInfo.getTeamId());
			if (team == null) {
				// 创建一个队伍
				team = teamServer.createTeam(userInfo, true);
			}
//			System.out.println(team+" | "+userInfo.getTeamId()+" | "+teamServer.getTeamList());
			if (!team.checkCmdExecute(userInfo, gameOutput)) {
				return;
			}
			
			GrindTeam grindTeam = new GrindTeam();
			grindTeam.setId(team.getId());
			grindTeam.setMapId(mapId);
			grindTeam.setStartDate(System.currentTimeMillis());
			grindTeam.setTeam(team);
			lists.add(grindTeam);
			
			System.out.println("更新到练级队列");
			
			gameOutput.reset().writeBoolean(true).writeUTF(userInfo.getRoleName());
			grindTeam.getTeam().sendData(SocketCmd.GRIND_UPDATE_STATE, gameOutput.toByteArray());
			
			castNextTime(grindTeam);
			
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("进入地图报错!" + userInfo, e);
		} finally {
			try {
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("进入地图关闭流报错!", e);
			}
		}
	}
	
	/**
	 * 计算下一次遇到怪物的时间
	 * @param grindTeam
	 */
	private void castNextTime(GrindTeam grindTeam) {
		if (grindTeam.getTeam().getUserList().isEmpty()) {
			return;
		}
		UserInfo userInfo = grindTeam.getTeam().getUserList().get(0);
		// 计算间隔时间
		double space = RandomUtils.nextInt(GameData.gameData.getDefaultGrindMinSpace(), 
				GameData.gameData.getDefaultGrindMaxSpace());
		if (userInfo.getBuffLibrary().checkProp(2009)) {
//			System.out.println("前="+space);
			space = 1.5;
//			System.out.println("中="+space);
//			//毫秒
//			if (space < 1) {
//				space = 1;
//			}
//			System.out.println("后="+space);
		}
//		System.out.println("space="+space);
		long nextTime = System.currentTimeMillis()+(int)((double)space*1000);
		grindTeam.setNextTime(nextTime);
	}

	/**
	 * 进入练级地图
	 * @param buf
	 * @param session
	 */
	public void enterMap(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			long mapId = input.readInt();// 押镖地图id
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 用户不再此场景和主场景
			if (userInfo.getCurrentScene() != CommonCmd.SCENE_GRIND 
					&& !(userInfo.getCurrentScene() == CommonCmd.SCENE_GAME 
					&& userInfo.getCurrentSceneValue()==1000)) {
				gameOutput.writeInt(CommonCmd.USER_NOT_MAIN_SCENE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			GrindMap grindMap = grindConfig.getCacheSample(mapId);
			if (grindMap == null) {
				gameOutput.writeInt(CommonCmd.ERROR_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Team team = teamServer.getTeam(userInfo.getTeamId());
			if (team == null) {
				// 创建一个队伍
				team = teamServer.createTeam(userInfo, true);
			}
			if (team.isFight()) {
				gameOutput.writeInt(CommonCmd.YOU_AT_FIGHT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 如果此人不是队长
			if (team.getUserList().get(0).getId() != userInfo.getId()) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_LEADER);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			team.updateCurrentScene(CommonCmd.SCENE_GRIND, mapId);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("进入练级地图报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("进入练级地图关闭流报错", e);
			}
		}
	}
	
	/**
	 * 玩家退出练级地图
	 * @param buf
	 * @param session
	 */
	public void exitMap(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getCurrentScene() != CommonCmd.SCENE_GRIND)
				return;
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Team team = teamServer.getTeam(userInfo.getTeamId());
			if (team != null) {
				if (team.isFight()) {
					gameOutput.writeInt(CommonCmd.YOU_AT_FIGHT);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				long mapId = userInfo.getCurrentSceneValue();
				GrindTeam grindTeam = grindThread.getGrindTeam(mapId, team.getId());
				if (grindTeam != null) {
					gameOutput.writeInt(CommonCmd.YOU_GRIND_DURING);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				team.updateCurrentScene(CommonCmd.SCENE_GAME, 0);
			} else {
				GameData.gameData.setupCurrentScene(userInfo, CommonCmd.SCENE_GAME);
				userServer.updateCurrentScene(userInfo);
				userInfo.sendData(SocketCmd.USER_ENTER_SCENE, 
						userInfo.getCurrentScene(), 
						userInfo.getCurrentSceneValue(), 
						userInfo.getCurrentSceneStartX(), 
						userInfo.getCurrentSceneStartY());
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("玩家退出练级地图报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("玩家退出练级地图关闭流报错", e);
			}
		}
	}
	
	/**
	 * 停止历练
	 * @param buf
	 * @param session
	 */
	public void stopGrind(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		try {
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getCurrentScene() != CommonCmd.SCENE_GRIND)
				return;
			if(userInfo.getTeamId() == 0) {
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long mapId = userInfo.getCurrentSceneValue();
			GrindTeam grindTeam = grindThread.getGrindTeam(mapId, userInfo.getTeamId());
			if (grindTeam == null) {
				return;
			}
//			grindTeam.updateState(CommonCmd.IDLE, userServer);
			gameOutput.reset().writeBoolean(false).writeUTF(userInfo.getRoleName());
			grindTeam.getTeam().sendData(SocketCmd.GRIND_UPDATE_STATE, gameOutput.toByteArray());
			grindThread.removeGrindTeam(mapId, grindTeam);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("停止历练报错", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("停止历练关闭流报错", e);
				}
			}
		}
	}

	/**
	 * 创建一场战斗
	 * @param grindTeam
	 * @param grindMap 
	 */
	public void createFight(GrindTeam grindTeam, GrindMap grindMap) {
		grindTeam.setFight(true);
		List<FightData> robList = grindTeam.getTeam().getFightData(false);
		int lenRole = CommonUtils.getFightCount(robList);
		List<FightData> robberyList = grindMap.createWaveMonster(lenRole,
				grindTeam.getTeam().getUserList().get(0).getBuffLibrary().checkProp(2008));
		
		FightBox fightBox = fightServer.createAIFight(robList, robberyList, false);
		fightBox.setFightType(CommonCmd.PVE);
		fightBox.obj = Arrays.asList(grindMap.getId(), grindTeam.getId(), 0);
		GameOutput gameOutput = new GameOutput();
		try {
			fightBox.writeTeamData(gameOutput);
			ActionData<Object> actionData = new ActionData<>(SocketCmd.FIGHT_DATA, gameOutput.toByteArray());
			grindTeam.getTeam().sendData(actionData);
			fightServer.startFight(fightBox.getId());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("创建一场战斗报错", e);
		} finally {
			try {
				if (gameOutput != null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("创建一场战斗关闭流报错", e);
			}
		}
		
	}
	
	/**
	 * 战斗结束
	 * @param fightBox
	 */
	@SuppressWarnings("unchecked")
	public void fightEnd(FightBox fightBox) {
		List<Long> escortList = (List<Long>) fightBox.obj;
		long mapId = escortList.get(0);
		long teamId = escortList.get(1);
//		long robberyEscortId = escortList.get(2);
		fightBox.obj = null;
		Role role;
		GrindTeam grindTeam = grindThread.getGrindTeam(mapId, teamId);
		if (grindTeam != null) {
			// 计算奖励
			List<FightData> fightDataList = fightBox.getRobList();
			FightData fightData;
			UserInfo userInfo;
			
			if (fightBox.getFightResult() == 1) {// 进攻方胜利
				GrindMap grindMap = this.grindConfig.getCacheSample(mapId);
				LuaValue luaValue = LuaUtils.getCommon().get("castGrindExp").
						call(CoerceJavaToLua.coerce(grindMap), CoerceJavaToLua.coerce(fightBox.getRobberyList()));
				int totalExp = luaValue.toint();
				int exp = totalExp;// 经验临时存储
				for (int i = 0; i < fightDataList.size(); i++) {
					fightData = fightDataList.get(i);
					if (fightData != null) {
						userInfo = fightData.getUserInfo();
						role = userInfo.getRole();
						luaValue = LuaUtils.getCommon().get("grindFightEnd").
								call(CoerceJavaToLua.coerce(userInfo), CoerceJavaToLua.coerce(grindMap), 
										LuaValue.valueOf(totalExp));
						exp = luaValue.toint();
						if (exp == 0) {
							continue;
						}
						// 玩家处理经验
						if (!role.isPet()) {
							if(userInfo.getBuffLibrary().checkProp(LuaUtils.get("chaojixianfenshan").toint())) {
								exp *= 2;
							}
							userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 1, role.getUid(), exp);
							role.setExperience(role.getExperience()+exp);
							GameData.gameData.userInfoUpLevel(userInfo);
							// 保存一次玩家数据
							userServer.updateUserRole(userInfo);
						} else {
							if(userInfo.getBuffLibrary().checkProp(LuaUtils.get("chongfengshan").toint())) {
								exp *= 3;
							}
							// 处理宠物经验
							userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 1, role.getUid(), exp);
							role.setExperience(role.getExperience()+exp);
							GameData.gameData.userPetUpLevel(userInfo, userInfo.getRole());
							petServer.updatePet(userInfo);
						}
					}
				}
			} else if(fightBox.getFightResult() == 2){
				for (int i = 0; i < fightDataList.size(); i++) {
					fightData = fightDataList.get(i);
					if (fightData != null) {
						userInfo = fightData.getUserInfo();
						role = userInfo.getRole();
						if (!role.isPet()) {
							// 战斗中死亡
							if (fightData.isDeath()) {
								
							}
						}
					}
				}
			}
			grindTeam.checkValid();
			grindTeam.setFight(false);
			castNextTime(grindTeam);
			for (int i = 0; i < fightDataList.size(); i++) {
				fightData = fightDataList.get(i);
				if (fightData != null) {
					if (!fightData.getUserInfo().getRole().isPet()) {
						fightData.getUserInfo().getMissionLibrary().completeGrind(mapId);
					}
				}
			}
		}
	}

	/**
	 * 获取grindThread
	 * @return grindThread grindThread
	 */
	public GrindThread getGrindThread() {
		return grindThread;
	}

}
