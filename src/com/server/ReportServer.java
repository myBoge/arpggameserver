package com.server;

import java.io.IOException;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.ConnectData;
import com.data.UserDataManage;
import com.entity.Report;
import com.entity.UserInfo;
import com.mapper.ReportMapper;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;

/**
 * 举报处理
 * @author xujinbo
 *
 */
public class ReportServer {

	@Resource
	private UserDataManage userDataManage;
	
	/**
	 * 玩家提交的举报信息
	 * @param buf
	 * @param session
	 */
	public void reportInfo(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		SqlSession sqlSession = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int reportType = gameInput.readInt();
			if (reportType < 0 || reportType > 3) {
				gameOutput.writeInt(CommonCmd.ERROR_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			String title = gameInput.readUTF().trim();
			if (title.length() < 1) {
				gameOutput.writeInt(CommonCmd.INPUT_LEN_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			String content = gameInput.readUTF().trim();
			if (content.length() < 1) {
				gameOutput.writeInt(CommonCmd.INPUT_LEN_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			sqlSession = ConnectData.openSession();
			ReportMapper reportMapper = sqlSession.getMapper(ReportMapper.class);
			Report report = reportMapper.queryId(1);
			if (report == null) {
				report = new Report();
				report.setBytes(BytesFactory.listToBytes(report.getReports()));
				reportMapper.insert(report);
			}
			
			Report report2 = new Report(reportType, title, content, userInfo.getId());
			report.getReports().add(report2);
			
			report.setBytes(BytesFactory.listToBytes(report.getReports()));
			reportMapper.update(report);
			
			sqlSession.commit();
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("玩家提交的举报信息报错", e);
		} finally {
			try {
				if(sqlSession!=null) sqlSession.close();
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("玩家提交的举报信息关闭流报错", e);
			}
		}
	}
	
}
