package com.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.UserDataManage;
import com.entity.MapData;
import com.entity.Team;
import com.entity.UserInfo;
import com.model.GameData;
import com.model.Grid;
import com.model.MapModel;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.LuaUtils;

/**
 * 地图处理器
 * @author xujinbo
 *
 */
public class MapServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private MapModel mapModel;
	@Resource
	private UserServer userServer;
	@Resource
	private TeamServer teamServer;
	
	/**
	 * 玩家决定移动到某个位置 
	 * @param buf
	 * @param session
	 */
	public void mapPlayerMove(byte[] buf, IoSession session) {
		GameInput gameInput = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long mapId = userInfo.getCurrentSceneValue();// 地图id
			int x = gameInput.readInt(); // x坐标
			int y = gameInput.readInt(); // y坐标
			MapData mapData = mapModel.getMapData(mapId);
			if (mapData == null) {
				gameOutput.writeInt(CommonCmd.MAP_NON_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Grid grid = mapData.getMapGridData().getPointGrid(x, y);
			if (grid == null || grid.getPointList().isEmpty()) {
				gameOutput.writeInt(CommonCmd.POINT_NOT_MOVE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Team team = null;
			// 不是主场景
			if (mapId != LuaUtils.get("mainMapId").tolong()) {
				team = teamServer.getTeam(userInfo.getTeamId());
				if (team != null && !team.checkCmdExecute(userInfo, gameOutput)) return;
			}
			
			userInfo.setBattleMove(true);
			userInfo.getMovePoint()[0] = x;
			userInfo.getMovePoint()[1] = y;
			
			if (mapData.getId() != LuaUtils.get("mainMapId").tolong()) {// 不是主界面
				if (team != null) {}
				mapData.sendData(userInfo, SocketCmd.MAP_PLAYER_MOVE, userInfo.getId(), x, y);
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("玩家决定移动到某个位置 报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("玩家决定移动到某个位置 关闭流报错", e);
			}
		}
	}
	/**
	 * 玩家决定移动到某个位置结束 
	 * @param buf
	 * @param session
	 */
	public void mapPlayerMoveEnd(byte[] buf, IoSession session) {
		GameInput gameInput = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			long mapId = userInfo.getCurrentSceneValue();// 地图id
			int x = gameInput.readInt(); // x坐标
			int y = gameInput.readInt(); // y坐标
			MapData mapData = mapModel.getMapData(mapId);
			if (mapData == null) {
				gameOutput.writeInt(CommonCmd.MAP_NON_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
//			Team team = teamServer.getTeam(userInfo.getTeamId());
//			if (team != null && !team.checkCmdExecute(userInfo, gameOutput)) return;
			
			int newGridId = mapData.getMapGridData().getUserGrid(x, y);
			List<Integer> ints = mapData.getMapGridData().getGridData(userInfo.getGridId());
			if (!ints.contains(newGridId)) {// 此新格子  是否是之前格子四周的格子之一
//				System.out.println("玩家决定移动到某个位置结束 == 哇,新格子错误了!格子="+userInfo.getGridId()+"，新格子="+newGridId+",周围格子="+ints.toString());
				gameOutput.writeInt(userInfo.getCurrentSceneStartX()).writeInt(userInfo.getCurrentSceneStartY());
				session.write(new ActionData<>(SocketCmd.MAP_CHANGE_RECTIFY, gameOutput.toByteArray()));
				return;
			}
			
			userInfo.setCurrentSceneStartX(x);
			userInfo.setCurrentSceneStartY(y);
			userInfo.setBattleMove(false);
			userInfo.getMovePoint()[0] = x;
			userInfo.getMovePoint()[1] = y;
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setCurrentSceneStartX(userInfo.getCurrentSceneStartX());
			info.setCurrentSceneStartY(userInfo.getCurrentSceneStartY());
			userServer.update(info);
			if (mapData.getId() != LuaUtils.get("mainMapId").tolong()) {// 不是主界面
				mapData.sendData(userInfo, SocketCmd.MAP_PLAYER_MOVE_END, userInfo.getId(), x, y);
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("玩家决定移动到某个位置结束报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("玩家决定移动到某个位置结束关闭流报错", e);
			}
		}
	}

	/**
	 * 地图上所有的角色 
	 * @param buf
	 * @param session
	 */
	public void mapAllPlayerData(byte[] buf, IoSession session) {
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long mapId = userInfo.getCurrentSceneValue();// 押镖地图id
			MapData mapData = mapModel.getMapData(mapId);
			if (mapData == null) {
				gameOutput.writeInt(CommonCmd.MAP_NON_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (mapData.getId() != LuaUtils.get("mainMapId").tolong()) {// 不是主界面
				mapData.writeAllData(gameOutput, userInfo);
				userInfo.sendData(SocketCmd.MAP_ALL_ROLE_DATA, gameOutput.toByteArray());
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("地图上所有的角色报错", e);
		} finally {
			try {
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("地图上所有的角色关闭流报错", e);
			}
		}
	}

	/**
	 * 地图上角色聊天 
	 * @param buf
	 * @param session
	 */
	public void mapChatMsg(byte[] buf, IoSession session) {
		GameInput gameInput = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long mapId = userInfo.getCurrentSceneValue();// 押镖地图id
			String content = gameInput.readUTF().trim();// 聊天的内容
			MapData mapData = mapModel.getMapData(mapId);
			if (mapData == null) {
				gameOutput.writeInt(CommonCmd.MAP_NON_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (mapData.getId() != LuaUtils.get("mainMapId").tolong()) {// 不是主界面
				mapData.sendData(userInfo, SocketCmd.MAP_CHAT_MSG, userInfo.getId(), content);
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("玩家决定移动到某个位置结束报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("玩家决定移动到某个位置结束关闭流报错", e);
			}
		}
	}

	/**
	 * 地图上有角色发生战斗 
	 * @param buf
	 * @param session
	 */
	public void mapPlayerFight(byte[] buf, IoSession session) {
		
	}

	/**
	 * 地图上玩家格子变动 
	 * @param buf
	 * @param session
	 */
	public void mapGridChange(byte[] buf, IoSession session) {
		GameInput gameInput = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long mapId = userInfo.getCurrentSceneValue();// 地图id
			int gridId = gameInput.readInt();// 格子idid
			int x = gameInput.readInt();// 格子x
			int y = gameInput.readInt();// 格子y
			MapData mapData = mapModel.getMapData(mapId);
			if (mapData == null) {
				gameOutput.writeInt(CommonCmd.MAP_NON_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getGridId() == gridId) {
//				System.out.println("当前新格子和缓存的格子一样="+userInfo.getGridId()+" | "+gridId);
				return;
			}
			int newGridId = mapData.getMapGridData().getUserGrid(x, y);
			if (newGridId == gridId) {
				
			}
			List<Integer> ints = mapData.getMapGridData().getGridData(userInfo.getGridId());
			if (!ints.contains(gridId)) {// 此新格子  是否是之前格子四周的格子之一
				System.out.println("哇,新格子错误了!格子="+userInfo.getGridId()+"，新格子="+gridId+",周围格子="+ints.toString());
				gameOutput.writeInt(userInfo.getCurrentSceneStartX()).writeInt(userInfo.getCurrentSceneStartY());
				session.write(new ActionData<>(SocketCmd.MAP_CHANGE_RECTIFY, gameOutput.toByteArray()));
				return;
			}
			userInfo.setCurrentSceneStartX(x);
			userInfo.setCurrentSceneStartY(y);
			if (mapData.getId() == LuaUtils.get("mainMapId").tolong()) {// 如果是主界面
				userInfo.setGridId(gridId);
			} else {
				mapData.getMapGridData().updateUser(gridId, userInfo);
				// 找出新得到的格子
				List<Integer> intTwos = mapData.getMapGridData().getGridData(gridId);
				List<Integer> gridIds = new ArrayList<>();
				for (int i = 0; i < intTwos.size(); i++) {
					if (!ints.contains(intTwos.get(i))) {
						gridIds.add(intTwos.get(i));
					}
				}
				// 通知新得到的格子里面的玩家
				mapData.writeGridData(gameOutput, userInfo, gridIds);
				userInfo.sendData(SocketCmd.MAP_ALL_ROLE_DATA, gameOutput.toByteArray());
				gameOutput.reset();
				// 通知新格子里面的玩家  新添加玩家
				mapData.writeUser(gameOutput, userInfo);
				mapData.sendData(userInfo, SocketCmd.MAP_ADD_PLAYER, gameOutput.toByteArray(), true, gridIds);
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("地图上玩家格子变动 报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("地图上玩家格子变动 关闭流报错", e);
			}
		}
	}
	
	/**
	 * 地图进入
	 * @param buf
	 * @param session
	 */
	public void mapEnter(byte[] buf, IoSession session) {
		GameInput gameInput = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 先清除之前的地图
			long mapId = userInfo.getCurrentSceneValue();// 地图id
			MapData mapData;
			if (userInfo.getCurrentScene() == CommonCmd.SCENE_GAME) {
				mapData = mapModel.getMapData(mapId);
				if (mapData != null) {
					mapData.removeUser(userInfo);
					mapData.sendData(userInfo, SocketCmd.MAP_DELETE_PLAYER, userInfo.getId() );
				}
			}
			mapId = gameInput.readInt();// 战场地图id
			mapData = mapModel.getMapData(mapId);
			if (mapData == null) {
				gameOutput.writeInt(CommonCmd.MAP_NON_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (mapId != LuaUtils.get("mainMapId").tolong()) {// 不是主场景
				// 此地图上如最大限度人数
				if (mapData.getUserLength() >= LuaUtils.getCommon().get("battlefieldMaxCount").toint()) {
					gameOutput.writeInt(CommonCmd.MAP_MAX_COUNT_LIMIT);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
			}
			Team team = teamServer.getTeam(userInfo.getTeamId());
			if (team != null) {
				if (mapId == 1002) {// 家族
					gameOutput.writeInt(CommonCmd.NOT_TEAM_OPERATION);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				if (!team.checkCmdExecute(userInfo, gameOutput)) return;
				
				team.updateCurrentScene(CommonCmd.SCENE_GAME, mapId, mapData.getStartX(), mapData.getStartY());
			} else {
				GameData.gameData.setupCurrentScene(userInfo, CommonCmd.SCENE_GAME, 
						mapId, mapData.getStartX(), mapData.getStartY());
				userInfo.setBattleMove(false);
				UserInfo info = new UserInfo(userInfo.getId());
				GameData.gameData.setupCurrentScene(info, userInfo);
				userServer.update(info);
				userInfo.sendData(SocketCmd.USER_ENTER_SCENE, 
						userInfo.getCurrentScene(),
						userInfo.getCurrentSceneValue(),
						userInfo.getCurrentSceneStartX(),
						userInfo.getCurrentSceneStartY()
						);
				sendMapAddUser(userInfo, mapData);
				if (mapData.getId() != LuaUtils.get("mainMapId").tolong()) {// 不是主界面
					mapData.addUser(userInfo);
				} else {
					userInfo.setGridId(mapData.getMapGridData().getUserGrid(userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY()));
				}
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("地图进入报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("地图进入关闭流报错", e);
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * 发送地图新增成员
	 * @param userInfo
	 */
	public void sendMapAddUser(UserInfo userInfo, MapData mapData) {
//		System.out.println("要发送的数量="+mapData.getUserList().size());
		if (mapData.getUserLength() == 0 || mapData.getId() == LuaUtils.get("mainMapId").tolong()) {
			// 里面的成员长度为0  或者 地图是  主界面
			return;
		}
		GameOutput gameOutput = new GameOutput();
		try {
			mapData.writeUser(gameOutput, userInfo);
			mapData.sendData(userInfo, SocketCmd.MAP_ADD_PLAYER, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("发送地图新增成员报错", e);
		} finally {
			try {
				gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("发送地图新增成员关闭流报错", e);
			}
		}
	}

	/**
	 * 有人离线
	 * @param userInfo
	 */
	public void userOffline(UserInfo userInfo) {
//		System.out.println("userOffline="+userInfo.getCurrentSceneValue());
		MapData mapData = mapModel.getMapData(userInfo.getCurrentSceneValue());
		if (mapData != null) {
			mapData.removeUser(userInfo);
			mapData.sendData(userInfo, SocketCmd.MAP_DELETE_PLAYER, userInfo.getId());
//			System.out.println("离线后剩余="+mapData.getUserList().size());
		}
	}
	
	/**
	 * 重新上线  加入地图列表
	 * @param userInfo
	 * @return 是否成功重新进入地图
	 */
	public boolean reconnect(UserInfo userInfo) {
//		System.out.println("reconnect="+userInfo.getCurrentSceneValue());
		MapData mapData = mapModel.getMapData(userInfo.getCurrentSceneValue());
		if (mapData != null) {
			if (mapData.getUserLength() < LuaUtils.getCommon().get("battlefieldMaxCount").toint()) {
				userInfo.setBattleMove(false);
				sendMapAddUser(userInfo, mapData);
				if (mapData.getId() != LuaUtils.get("mainMapId").tolong()) {// 不是主界面
					mapData.addUser(userInfo);
//					System.out.println(userInfo.getGridId());
//					System.out.println(mapData.getMapGridData().getGrid(userInfo.getGridId()).getUserInfoList().size());
//					System.out.println(mapData.getMapGridData().getGrid(userInfo.getGridId()).getPointList().size());
				} else {
					userInfo.setGridId(mapData.getMapGridData().getUserGrid(userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY()));
				}
				return true;
			}
		}
		return false;
	}

}
