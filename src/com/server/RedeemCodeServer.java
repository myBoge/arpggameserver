package com.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Action;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.PropConfig;
import com.data.ConnectData;
import com.data.UserDataManage;
import com.entity.Gift;
import com.entity.GiftProp;
import com.entity.RedeemCode;
import com.entity.UserInfo;
import com.mapper.GiftMapper;
import com.mapper.RedeemCodeMapper;
import com.mapper.UserMapper;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;

/**
 * 兑换处理
 * @author xujinbo
 *
 */
public class RedeemCodeServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private GiftServer giftServer;
	@Resource
	private PropConfig propConfig;
	@Resource
	private UserServer userServer;
	/** 兑换码 */
	private List<RedeemCode> redeemCodeList = new ArrayList<>();
	
	@Action(CommonCmd.INIT_REDEEM_DATA)
	private void init(){
		SqlSession sqlSession = ConnectData.openSession();
		try {
			RedeemCodeMapper redeemCodeMapper = sqlSession.getMapper(RedeemCodeMapper.class);
			RedeemCode redeemCode = redeemCodeMapper.queryAllType(CommonCmd.ORDINARY_REDEEM);
			BytesFactory.createBytes(redeemCode.getBytes(), redeemCode);
			redeemCodeList.add(redeemCode);
		} finally {
			sqlSession.close();
		}
	}
	
	/**
	 * 根据类型获取兑换码
	 * @param type
	 * @return
	 */
	public RedeemCode getRedeem(int type){
		for (int i = 0; i < redeemCodeList.size(); i++) {
			if (redeemCodeList.get(i).getRedeemType() == type) {
				return redeemCodeList.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 添加兑换码
	 * @param type 类型
	 * @param code 兑换码
	 * @param giftId 礼包id
	 */
	public void addRedeemCode(int type, String code, long giftId) {
		RedeemCode redeem = getRedeem(type);
		boolean isNew = false;
		if (redeem == null) {
			redeem = new RedeemCode();
			redeem.setRedeemType(CommonCmd.ORDINARY_REDEEM);
			redeemCodeList.add(redeem);
			isNew = true;
		}
		List<String> codes = redeem.getCodeList();
		redeem.setGiftId(giftId);
		if (!codes.contains(code)) {
			codes.add(code);
			if (isNew) {
				insertRedeem(redeem);
			} else {
				updateRedeem(redeem);
			}
		}
	}
	
	/**
	 * 清理此类型所有兑换码
	 * @param type
	 */
	public void clearRedeemCode(int type) {
		RedeemCode redeem = getRedeem(type);
		if (redeem != null) {
			redeem.getCodeList().clear();
			updateRedeem(redeem);
		}
	}
	
	/**
	 * 兑换码
	 * @param buf
	 * @param session
	 */
	public void redeemCode(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int type = input.readInt();//兑换类型
			String code = input.readUTF();//兑换码
			RedeemCode redeem = getRedeem(type);
			if (redeem == null) {
				gameOutput.writeInt(CommonCmd.YOU_REDEEM_CODE_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Gift gift = giftServer.getGift(redeem.getGiftId());
			if (gift == null) {
				gameOutput.writeInt(CommonCmd.YOU_REDEEM_CODE_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (gift.getUserIDList().contains(userInfo.getId())) {
				gameOutput.writeInt(CommonCmd.YOU_GIFT_ALREADY_GET);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.getBackpack().checkEmpty()) {
				gameOutput.writeInt(CommonCmd.BACKPACK_INSUFFICIENT_SPACE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			boolean result = redeem.getCodeList().remove(code);
			if (!result) {
				gameOutput.writeInt(CommonCmd.YOU_REDEEM_CODE_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			gift.getUserIDList().add(userInfo.getId());
			// 创建一个礼包
			GiftProp giftProp = (GiftProp) propConfig.getSample(2003);
			giftProp.setGiftId(gift.getId());

			userInfo.getBackpack().addProp(giftProp);
			gameOutput.writeInt(1);
			giftProp.writeData(gameOutput);
			userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
			userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 2, giftProp.getId(), 1);
			SqlSession sqlSession = ConnectData.openSession();
			try {
				UserInfo info = new UserInfo(userInfo.getId());
				info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
				
				UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
				userMapper.update(info);
				
				GiftMapper giftMapper = sqlSession.getMapper(GiftMapper.class);
				gift.setBytes(BytesFactory.objectToByteData(gift));
				giftMapper.update(gift);
				
				RedeemCodeMapper userManage = sqlSession.getMapper(RedeemCodeMapper.class);
				redeem.setBytes(BytesFactory.objectToByteData(redeem));
				userManage.update(redeem);
				
				sqlSession.commit();
			} finally {
				sqlSession.close();
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("兑换码报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("兑换码关闭流报错", e);
			}
		}
		
		
	}

	
	
	
	
	
	/**
	 * 更新兑换码
	 * @param redeem
	 */
	private void updateRedeem(RedeemCode redeem) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			RedeemCodeMapper userManage = sqlSession.getMapper(RedeemCodeMapper.class);
			redeem.setBytes(BytesFactory.objectToByteData(redeem));
			userManage.update(redeem);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}
	
	/**
	 * 插入新的兑换码类型
	 * @param redeem
	 */
	private void insertRedeem(RedeemCode redeem) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			RedeemCodeMapper userManage = sqlSession.getMapper(RedeemCodeMapper.class);
			redeem.setBytes(BytesFactory.objectToByteData(redeem));
			userManage.insert(redeem);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}
	
	
	
	
}
