package com.server;

import java.io.IOException;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Action;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.ConnectData;
import com.data.UserDataManage;
import com.entity.Faction;
import com.entity.UseProp;
import com.entity.UserInfo;
import com.mapper.FactionMapper;
import com.mapper.UserMapper;
import com.model.Backpack;
import com.model.MapModel;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.thread.SendDataThread;
import com.utils.BytesFactory;
import com.utils.LuaUtils;

/**
 * 帮派管理
 * @author xujinbo
 *
 */
public class FactionServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	@Resource
	private SendDataThread sendDataThread;
	@Resource
	private MapModel mapModel;
	@Resource
	private MapServer mapServer;
	
	private List<Faction> list;
	/** 帮派加入 创建等级 */
	private int factionAddLevel;
	
	@Action(CommonCmd.INIT_FACTION_DATA)
	protected void init() {
		factionAddLevel = LuaUtils.get("factionAddLevel").toint();
		SqlSession sqlSession = ConnectData.openSession();
		try {
			FactionMapper factionMapper = sqlSession.getMapper(FactionMapper.class);
			list = factionMapper.queryAll();
			Faction faction;
			for (int i = 0; i < list.size(); i++) {
				faction = list.get(i);
				faction.setUserInfo(userDataManage.getUserInfoData(faction.getUserId()));
				BytesFactory.createBytes(faction.getMember(), faction);
			}
		} finally {
			sqlSession.close();
		}
	}
	
	/**
	 * 根据帮派id获取帮派对象
	 * @param factionId
	 * @return
	 */
	public Faction getFaction(long factionId){
		Faction faction = null;
		for (int i = 0; i < list.size(); i++) {
			faction = list.get(i);
			if (faction.getUserId() == factionId) {
				return faction;
			}
		}
		return null;
	}
	
	/**
	 * 请求所有帮派数据
	 * @param buf
	 * @param session
	 */
	public void factionRequest(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			gameOutput.writeInt(list.size());
			for (int i = 0; i < list.size(); i++) {
				list.get(i).writeBaseData(gameOutput);
			}
			userInfo.sendData(SocketCmd.FACTION_REQUEST, gameOutput.toByteArray());
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("宠物参战状态更新报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("宠物参战状态更新关闭流报错", e);
			}
		}
	}

	/**
	 * 创建一个帮派
	 * @param buf
	 * @param session
	 */
	public void factionCreate(byte[] buf, IoSession session) {
		SqlSession sqlSession = null;
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查是否在帮派中
			if (userInfo.getFactionId() > 0) {
				gameOutput.writeInt(CommonCmd.YOU_EXISTENT_FACTION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			if (userInfo.getRole().getLevel() < factionAddLevel) {
				gameOutput.writeInt(CommonCmd.YOU_LEVEL_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int code = gameInput.readInt();// 选择创建方式
			String factionName = gameInput.readUTF().trim();//帮派名字
			Backpack backpack = userInfo.getBackpack();
			UseProp prop = null;
			if (code == 0) {
				prop = (UseProp) backpack.getPropId(LuaUtils.get("jiazhulin").tolong());
				if (prop == null) {
					gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				backpack.removePropIndexCount(prop.getIndex(), 1);
				userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 11, prop.getName(), 1);
			} else {
				if (userInfo.getPlayerGold() < 50000) {
					gameOutput.writeInt(CommonCmd.GOLD_INSUFFICIENT);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				userInfo.setPlayerGold(userInfo.getPlayerGold()-50000);
			}
			
			sqlSession = ConnectData.openSession();
			
			// 创建新家族
			Faction faction = new Faction();
			faction.setBoom(0);
			faction.setLevel(1);
			faction.setSkill(1);
			faction.setNotice("");
			faction.setCapital(0);
			faction.setTreasure(1);
			faction.setDemonTower(1);
			faction.setName(factionName);
			faction.setUserInfo(userInfo);
			faction.setUserId(userInfo.getId());
			faction.getMemberList().add(userInfo);
			faction.setMember(BytesFactory.objectToByteData(faction));
			
			userInfo.setFactionId(faction.getUserId());
			// 添加到帮派缓存中
			list.add(faction);
			FactionMapper factionMapper = sqlSession.getMapper(FactionMapper.class);
			factionMapper.insert(faction);
			
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			UserInfo info = new UserInfo(userInfo.getId());
			info.setPlayerGold(userInfo.getPlayerGold());
			info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
			info.setFactionId(faction.getUserId());
			userMapper.update(info);
			sqlSession.commit();
			
			// 通知用户
			if (code == 0) {
				gameOutput.reset().write(1, prop.getIndex(), 1);
				userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
			} else {
				gameOutput.reset().writeLong(userInfo.getPlayerGold()).writeLong(userInfo.getLockGold());
				userInfo.sendData(SocketCmd.UPDATE_GOLD, gameOutput.toByteArray());
			}
			gameOutput.reset();
			faction.writeData(gameOutput);
			userInfo.sendData(SocketCmd.FACTION_CREATE, gameOutput.toByteArray());
			
			sendDataThread.addActionData(SocketCmd.ADD_CHAT_MESSAGE, 5, faction.getName());
			
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("创建一个帮派报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
				if(sqlSession!=null) sqlSession.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("创建一个帮派关闭流报错", e);
			}
		}
	}

	/**
	 * 帮派解散
	 * @param buf
	 * @param session
	 */
	public void factionDisband(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查是否在帮派中
			if (userInfo.getFactionId() <= 0) {
				gameOutput.writeInt(CommonCmd.YOU_NOT_EXISTENT_FACTION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long factionId = gameInput.readInt();// 帮派id
			Faction faction = getFaction(factionId);
			if (faction.getUserId() != userInfo.getId()) {
				gameOutput.writeInt(CommonCmd.YOU_NOT_FACTION_WANG);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			removeFaction(faction);
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("创建一个帮派报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("创建一个帮派关闭流报错", e);
			}
		}
	}

	/**
	 * 解散帮派
	 * @param faction
	 */
	private void removeFaction(Faction faction) {
		list.remove(faction);
		faction.sendData(SocketCmd.FACTION_DISBAND);
		
		List<UserInfo> members = faction.getMemberList();
		UserInfo userInfo;
		UserInfo info;
		SqlSession sqlSession = ConnectData.openSession();
		try {
			UserMapper userManage = sqlSession.getMapper(UserMapper.class);
			for (int i = 0; i < members.size(); i++) {
				userInfo = members.get(i);
				userInfo.setFactionId(0);
				info = new UserInfo(userInfo.getId());
				info.setFactionId(userInfo.getFactionId());
				userManage.update(info);
			}
			FactionMapper factionMapper = sqlSession.getMapper(FactionMapper.class);
			factionMapper.delete(faction.getUserId());
			sqlSession.commit();
		} finally {
			if(sqlSession!=null)sqlSession.close();
		}
		sendDataThread.addActionData(SocketCmd.ADD_CHAT_MESSAGE, 6, faction.getName());
	}

	/**
	 * 帮派申请
	 * @param buf
	 * @param session
	 */
	public void factionApply(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查是否在帮派中
			if (userInfo.getFactionId() > 0) {
				gameOutput.writeInt(CommonCmd.YOU_EXISTENT_FACTION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getRole().getLevel() < factionAddLevel) {
				gameOutput.writeInt(CommonCmd.YOU_LEVEL_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long factionId = gameInput.readInt();// 帮派id
			Faction faction = getFaction(factionId);
			if (faction == null) {
				gameOutput.writeInt(CommonCmd.FACTION_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 帮派最大人数
			int maxCount = LuaUtils.getCommon().get("getFactionRoleCount").call(LuaValue.valueOf(faction.getLevel())).toint();
			if (faction.getMemberList().size() >= maxCount) {
				gameOutput.writeInt(CommonCmd.FACTION_FULL);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			gameOutput.write(userInfo.getId(), userInfo.getRoleName(), userInfo.getRole().getLevel());
			faction.getUserInfo().sendData(SocketCmd.FACTION_APPLY, gameOutput.toByteArray());
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("帮派申请报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("帮派申请关闭流报错", e);
			}
		}
	}
	
	/**
	 * 帮派申请决定
	 * @param buf
	 * @param session
	 */
	public void factionApplyDecide(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查是否在帮派中
			if (userInfo.getFactionId() < 1) {
				gameOutput.writeInt(CommonCmd.YOU_NOT_EXISTENT_FACTION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long factionId = gameInput.readInt();// 帮派id
			Faction faction = getFaction(factionId);
			if (faction == null) {
				gameOutput.writeInt(CommonCmd.FACTION_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 帮派最大人数
			int maxCount = LuaUtils.getCommon().get("getFactionRoleCount").call(LuaValue.valueOf(faction.getLevel())).toint();
			if (faction.getMemberList().size() >= maxCount) {
				gameOutput.writeInt(CommonCmd.FACTION_FULL);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (faction.getUserId() != userInfo.getId()) {
				gameOutput.writeInt(CommonCmd.YOU_NOT_FACTION_ADMIN);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查要进入帮派的人
			userInfo = userDataManage.getCacheUserInfo(gameInput.readInt());
			if (userInfo == null || userInfo.getFactionId() > 0 || userInfo.getRole().getLevel() < factionAddLevel) {
				return;
			}
			// 通知帮众 有新成员入伍
			faction.writeUser(userInfo, gameOutput);
			faction.sendData(SocketCmd.FACTION_ADD, gameOutput.toByteArray());
			
			faction.getMemberList().add(userInfo);
			
			userInfo.setFactionId(faction.getUserId());
			
			Faction faction2 = new Faction(faction.getUserId());
			faction2.setMember(BytesFactory.objectToByteData(faction));
			update(faction2);
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setFactionId(userInfo.getFactionId());
			userServer.update(info);
			
			
			gameOutput.reset();
			faction.writeBaseData(gameOutput);
			userInfo.sendData(SocketCmd.FACTION_APPLY_DECIDE, gameOutput.toByteArray());
			
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("帮派申请报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("帮派申请关闭流报错", e);
			}
		}
	}

	/**
	 * 帮派踢人
	 * @param buf
	 * @param session
	 */
	public void factionKick(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查是否在帮派中
			if (userInfo.getFactionId() < 1) {
				gameOutput.writeInt(CommonCmd.YOU_NOT_EXISTENT_FACTION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long factionId = gameInput.readInt();// 帮派id
			Faction faction = getFaction(factionId);
			if (faction == null) {
				gameOutput.writeInt(CommonCmd.FACTION_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (faction.getUserId() != userInfo.getId()) {
				gameOutput.writeInt(CommonCmd.YOU_NOT_FACTION_ADMIN);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查要踢掉的人
			userInfo = faction.removeUser(gameInput.readInt());
			if (userInfo == null) {
				return;
			}
			
			// 通知帮众 有成员退出
			gameOutput.writeLong(userInfo.getId());
			faction.sendData(SocketCmd.FACTION_DELETE, gameOutput.toByteArray());
			
			userInfo.setFactionId(0);
			
			Faction faction2 = new Faction(faction.getUserId());
			faction2.setMember(BytesFactory.objectToByteData(faction));
			update(faction2);
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setFactionId(userInfo.getFactionId());
			userServer.update(info);
			
			userInfo.sendData(SocketCmd.FACTION_DISBAND);
			
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("帮派申请报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("帮派申请关闭流报错", e);
			}
		}
	}

	/**
	 * 帮派退出
	 * @param buf
	 * @param session
	 */
	public void factionQuit(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查是否在帮派中
			if (userInfo.getFactionId() < 1) {
				gameOutput.writeInt(CommonCmd.YOU_NOT_EXISTENT_FACTION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long factionId = gameInput.readInt();// 帮派id
			Faction faction = getFaction(factionId);
			if (faction == null) {
				gameOutput.writeInt(CommonCmd.FACTION_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查要踢掉的人
			userInfo = faction.removeUser(userInfo.getId());
			if (userInfo == null) {
				return;
			}
			
			// 通知帮众 有成员退出
			gameOutput.writeLong(userInfo.getId());
			faction.sendData(SocketCmd.FACTION_DELETE, gameOutput.toByteArray());
			
			userInfo.setFactionId(0);
			
			Faction faction2 = new Faction(faction.getUserId());
			faction2.setMember(BytesFactory.objectToByteData(faction));
			update(faction2);
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setFactionId(userInfo.getFactionId());
			userServer.update(info);
			
			if (faction.getMemberList().isEmpty()) {
				removeFaction(faction);
			}
			userInfo.sendData(SocketCmd.FACTION_DISBAND);
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("帮派申请报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("帮派申请关闭流报错", e);
			}
		}
	}

	/**
	 * 帮派建筑信息
	 * @param buf
	 * @param session
	 */
	public void factionBuildUp(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查是否在帮派中
			if (userInfo.getFactionId() < 1) {
				gameOutput.writeInt(CommonCmd.YOU_NOT_EXISTENT_FACTION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long factionId = gameInput.readInt();// 帮派id
			Faction faction = getFaction(factionId);
			if (faction == null) {
				gameOutput.writeInt(CommonCmd.FACTION_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			
			
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("帮派建筑信息报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("帮派建筑信息关闭流报错", e);
			}
		}
	}
	
	/**
	 * 获取帮派完整信息
	 * @param buf
	 * @param session
	 */
	public void factionInfo(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查是否在帮派中
			if (userInfo.getFactionId() < 1) {
				gameOutput.writeInt(CommonCmd.YOU_NOT_EXISTENT_FACTION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long factionId = gameInput.readInt();// 帮派id
			Faction faction = getFaction(factionId);
			if (faction == null) {
				gameOutput.writeInt(CommonCmd.FACTION_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			faction.writeData(gameOutput);
			userInfo.sendData(SocketCmd.FACTION_INFO, gameOutput.toByteArray());
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("帮派申请报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("帮派申请关闭流报错", e);
			}
		}
	}
	
	/**
	 * 更新此帮派信息
	 * @param faction
	 */
	public void update(Faction faction) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			FactionMapper factionMapper = sqlSession.getMapper(FactionMapper.class);
			factionMapper.update(faction);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}

}
