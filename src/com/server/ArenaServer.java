package com.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Action;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.ConnectData;
import com.data.UserDataManage;
import com.entity.ArenaData;
import com.entity.UserInfo;
import com.mapper.ArenaMapper;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.server.fight.FightBox;
import com.thread.SendDataThread;
import com.utils.CommonUtils;
import com.utils.LuaUtils;
import com.utils.RandomUtils;

/**
 * 竞技场处理
 * @author xujinbo
 */
public class ArenaServer {
	
	@Resource
	private UserDataManage userDataManage;
	@Resource
	private FightServer fightServer;
	@Resource
	private UserServer userServer;
	@Resource
	private SendDataThread sendDataThread;
	
	private List<ArenaData> arenas;

	@Action(CommonCmd.INIT_ARENA_DATA)
	private void init() {
		SqlSessionFactory sqlSessionFactory = ConnectData.getSqlSessionFactory();
		SqlSession sqlSession = sqlSessionFactory.openSession();
		try {
			ArenaMapper arenaMapper = sqlSession.getMapper(ArenaMapper.class);
			arenas = arenaMapper.queryAll();
//			System.out.println(arenas);
			ArenaData arenaData;
			if (arenas.isEmpty()) {
				for (int i = 0; i < 1000; i++) {
					arenaData = new ArenaData();
					arenaData.setUserInfo(
							(UserInfo) LuaUtils.get("createArenaUserInfo").call(LuaValue.valueOf(i+1)).touserdata()
						);
//					arenaData.createUserInfo(i+1);
					arenaData.convertBytes();
					arenaMapper.insert(arenaData);
					arenas.add(arenaData);
				}
				sqlSession.commit();
			} else {
				for (int i = 0; i < arenas.size(); i++) {
					arenaData = arenas.get(i);
//					System.out.println(i+" | "+arenaData);
					arenaData.convertUserInfo();
				}
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("初始化竞技场信息报错", e);
		} finally {
			if(sqlSession!=null) sqlSession.close();
		}
	}
	
	/**
	 * 获取此用户在竞技榜中的排位名次
	 * @param userInfo
	 * @return
	 */
	private int getRank(UserInfo userInfo) {
		ArenaData arenaData;
		for (int i = 0; i < arenas.size(); i++) {
			arenaData = arenas.get(i);
			if (arenaData.getUserInfo().getId() == userInfo.getId()) {
				return i;
			}
		}
		return arenas.size();
	}
	
	/**
	 * 获取此名次可以挑战的对手
	 * @param rank
	 * @return
	 */
	private List<ArenaData> getArenaList(int rank) {
		List<ArenaData> arenaList = new ArrayList<>();
		if (rank > 0) {
			ArenaData arenaData;
			int index;
			if (rank < 5) {
				for (int i = 0; i < rank; i++) {
					arenaData = arenas.get(i);
					arenaList.add(arenaData);
				}
			} else {
				while (arenaList.size()<5) {
					if(rank < 100){
						index = RandomUtils.nextInt(0, rank);
					} else {
						index = RandomUtils.nextInt(rank-100, rank);
					}
					arenaData = arenas.get(index);
					if (arenaData != null && arenaList.indexOf(arenaData)==-1) {
						arenaList.add(arenaData);
					}
				}
			}
		}
		return arenaList;
	}

	/**
	 * 竞技场挑战数据请求
	 * @param buf
	 * @param session
	 */
	public void arenaRequest(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		try {
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int rank = getRank(userInfo);
			List<ArenaData> arenaList = getArenaList(rank);
			gameOutput.writeInt(rank+1);
			gameOutput.writeInt(arenaList.size());
			for (int i = 0; i < arenaList.size(); i++) {
				arenaList.get(i).writeData(gameOutput);
			} 
			userInfo.sendData(SocketCmd.ARENA_REQUEST, gameOutput.toByteArray());
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("竞技场挑战数据请求报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("竞技场挑战数据请求关闭流报错", e);
			}
		}
	}

	/**
	 * 竞技场挑战
	 * @param buf
	 * @param session
	 */
	public void arenaChallenge(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getTeamId() != 0) {
				gameOutput.writeInt(CommonCmd.NOT_TEAM_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 挑战名次
			int rank = gameInput.readInt();
			rank-=1;
			if (rank >= arenas.size()) {
				gameOutput.writeInt(CommonCmd.YOU_ARENA_RANK_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int autoRank = getRank(userInfo);
			if (rank >= autoRank) {
				gameOutput.writeInt(CommonCmd.YOU_ARENA_RANK_TOO_LOW);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			ArenaData challengeUser = arenas.get(rank);
			if (challengeUser.isFight()) {
				gameOutput.writeInt(CommonCmd.YOU_ARENA_FIGHT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			challengeUser.setFight(true);
			if (autoRank < arenas.size()) {
				arenas.get(autoRank).setFight(true);
			}
			FightBox fightBox = fightServer.createAIFight(CommonUtils.getFightData(userInfo, false), 
					CommonUtils.getFightData(challengeUser.getUserInfo(), true), false);
			
			fightBox.setFightType(CommonCmd.PVP);
			fightBox.obj = Arrays.asList(rank, autoRank, userInfo);
			try {
				fightBox.writeTeamData(gameOutput);
				userInfo.sendData(SocketCmd.FIGHT_DATA, gameOutput.toByteArray());
				fightServer.startFight(fightBox.getId());
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("创建一场战斗报错", e);
			} finally {
				try {
					if (gameOutput != null) gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("创建一场战斗关闭流报错", e);
				}
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("竞技场挑战报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("竞技场挑战关闭流报错", e);
			}
		}
	}
	
	/**
	 * 游戏结束
	 * @param fightBox
	 */
	@SuppressWarnings("unchecked")
	public void fightEnd(FightBox fightBox) {
		List<Object> objList = (List<Object>) fightBox.obj;
		int rank = (int) objList.get(0);//挑战的位置
		int autoRank = (int) objList.get(1);//自己的位置
		UserInfo userInfo = (UserInfo) objList.get(2);//挑战者对象
		ArenaData challengeUser = arenas.get(rank);
		ArenaData autoChallengeUser = null;
		if (autoRank < arenas.size()) {
			autoChallengeUser = arenas.get(autoRank);
		}
		fightBox.obj = null;
		if (fightBox.getFightResult() == 1) {// 进攻方胜利
			UserInfo tempUser = challengeUser.getUserInfo();
			challengeUser.setUserInfo(userInfo);
			SqlSessionFactory sqlSessionFactory = ConnectData.getSqlSessionFactory();
			SqlSession sqlSession = sqlSessionFactory.openSession();
			try {
				ArenaMapper arenaMapper = sqlSession.getMapper(ArenaMapper.class);
				challengeUser.convertBytes();
				arenaMapper.update(challengeUser);
				if (autoChallengeUser != null) {
					autoChallengeUser.setUserInfo(tempUser);
					autoChallengeUser.convertBytes();
					arenaMapper.update(autoChallengeUser);
				}
				sqlSession.commit();
				sendDataThread.addActionData(SocketCmd.ADD_CHAT_MESSAGE, 7, 
						userInfo.getRoleName(), 
						tempUser.getRoleName(), challengeUser.getId());
			} catch (Exception e) {
				Logger.getLogger(getClass()).error("初始化竞技场信息报错", e);
			} finally {
				if(sqlSession!=null) sqlSession.close();
			}
		}
		challengeUser.setFight(false);
		if (autoChallengeUser != null) {
			autoChallengeUser.setFight(false);
		}
	}
	
	/**
	 * 更新竞技场数据
	 * @param arenaData
	 */
	public void update(ArenaData arenaData) {
		SqlSessionFactory sqlSessionFactory = ConnectData.getSqlSessionFactory();
		SqlSession sqlSession = sqlSessionFactory.openSession();
		try {
			ArenaMapper arenaMapper = sqlSession.getMapper(ArenaMapper.class);
			arenaMapper.update(arenaData);
			sqlSession.commit();
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("初始化竞技场信息报错", e);
		} finally {
			if(sqlSession!=null) sqlSession.close();
		}
	}

}
