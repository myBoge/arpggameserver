package com.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.CoerceLuaToJava;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Action;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.PropConfig;
import com.data.ConnectData;
import com.data.UserDataManage;
import com.entity.Gift;
import com.entity.GiftProp;
import com.entity.UseProp;
import com.entity.UserInfo;
import com.mapper.GiftMapper;
import com.model.GiftLibrary;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;
import com.utils.LuaUtils;

/**
 * 礼包
 * @author xujinbo
 *
 */
public class GiftServer {
	
	/** 用户数据管理 */
	@Resource
	private UserDataManage userDataManage;
	@Resource
	private PropConfig propConfig;
	@Resource
	private UserServer userServer;
	/** 当前游戏中的礼包数据 */
	private List<Gift> gifts;

	@Action(CommonCmd.INIT_GIFG_DATA)
	private void init(){
		SqlSession sqlSession = ConnectData.openSession();
		try {
			// 获取后台服务器的礼包
			GiftMapper giftMapper = sqlSession.getMapper(GiftMapper.class);
			gifts = giftMapper.queryAll();
			for (int i = 0; i < gifts.size(); i++) {
				BytesFactory.createBytes(gifts.get(i).getBytes(), gifts.get(i));
			}
		} finally {
			sqlSession.close();
		}
	}
	
	/**
	 * 根据礼包id获取礼包
	 * @param id
	 * @return
	 */
	public Gift getGift(long id) {
		for (int i = 0; i < gifts.size(); i++) {
			if (gifts.get(i).getId() == id) {
				return gifts.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 根据礼包id获取礼包
	 * @param start
	 * @param limit 
	 * @return
	 */
	public List<Gift> getGiftPage(int start, int limit) {
		List<Gift> giftList = new ArrayList<>();
		if (start < gifts.size()) {
			int end = start+limit;
			for (int i = start; i < gifts.size(); i++) {
				if (i < end) {
					giftList.add(gifts.get(i));
				} else {
					break;
				}
			}
		}
		return giftList;
	}

	/**
	 * 添加一个新的礼包
	 * @param gift
	 */
	public void addGift(Gift gift) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			GiftMapper giftMapper = sqlSession.getMapper(GiftMapper.class);
			gift.setBytes(BytesFactory.objectToByteData(gift));
			giftMapper.insert(gift);
			sqlSession.commit();
			gifts.add(gift);
		} finally {
			sqlSession.close();
		}
	}
	
	/**
	 * 检查此类型是否存在
	 * @param giftType
	 * @return
	 */
	public boolean checkType(int giftType) {
		for (int i = 0; i < gifts.size(); i++) {
			if (gifts.get(i).getGiftType() == giftType) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 领取每日在线礼包
	 * @param buf
	 * @param session
	 */
	public void giftGetOnLine(byte[] buf, IoSession session) {
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 在线时间经过的时间
			long afterData = System.currentTimeMillis()-userInfo.getLoginDate().getTime();
			// 在线总时间
			long onLineTotalTimer = userInfo.getOnLineDate()+afterData;
			// 领取次数总数
			int giftDateSize = LuaUtils.get("getGiftDateSize").call().toint();
			if (giftDateSize <= userInfo.getOnLineGiftCount()) {//领取次数上限
				gameOutput.writeInt(CommonCmd.GET_COUNT_TOPLIMIT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			LuaValue luaValue = LuaUtils.get("getGiftDate")
					.call(LuaValue.valueOf(userInfo.getOnLineGiftCount()+1));
			//此此奖励需要时间  分钟
			int data = luaValue.toint();
			// 需要在线时间  s
			int needTimer = data*60*1000;
			if (onLineTotalTimer < needTimer) {//不满足在线时间
				gameOutput.writeInt(CommonCmd.NOT_SATISFY_ONLINE_TIME);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.getBackpack().checkEmpty()) {
				gameOutput.writeInt(CommonCmd.BACKPACK_INSUFFICIENT_SPACE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			LuaUtils.get("getOnLineGift").call(
					LuaValue.valueOf(userInfo.getOnLineGiftCount()), 
					CoerceJavaToLua.coerce(userInfo)
				);
			
			userInfo.setOnLineGiftCount(userInfo.getOnLineGiftCount()+1);
			
			userServer.updateOnLine(userInfo);
			
			gameOutput.reset().writeInt(userInfo.getOnLineGiftCount());
			userInfo.sendData(SocketCmd.UPDATE_GIFT_GET_ON_LINE_COUNT, gameOutput.toByteArray());
		
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("兑换码报错", e);
		} finally {
			try {
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("兑换码关闭流报错", e);
			}
		}
	}

	/**
	 * 领取每日签到
	 * @param buf
	 * @param session
	 */
	public void receiceEverydaySign(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.getBackpack().checkEmpty()) {
				gameOutput.writeInt(CommonCmd.BACKPACK_INSUFFICIENT_SPACE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			GiftLibrary giftLibrary = userInfo.getGiftLibrary();
			// 签到的位置
			int sign = gameInput.readInt();
//			System.out.println(giftLibrary.getOnLineSignCount()+" | "+sign);
			if (giftLibrary.getOnLineSignCount() != sign) {
				return;
			}
			Calendar calendar = Calendar.getInstance();// 上次签到时间
			calendar.setTimeInMillis(giftLibrary.getLastTimeSignTimer());
			Calendar current = Calendar.getInstance();// 当前时间
			current.setTimeInMillis(System.currentTimeMillis());
			if(DateUtils.isSameDay(calendar, current)) {// 是同一天
				return;
			}
			LuaValue lua = LuaUtils.get("getEverydayProp").call(LuaValue.valueOf(sign));
			int[] ints = (int[]) CoerceLuaToJava.coerce(lua.checktable(), int[].class);
			if (ints.length==2) {
				UserInfo info = new UserInfo(userInfo.getId());
				UseProp prop = (UseProp) propConfig.getSample(ints[0]);
				if (prop != null) {
					prop.setValue(ints[1]);
					if (prop.getId() == LuaUtils.get("qian").tolong()) {
						userInfo.setLockGold(userInfo.getLockGold()+prop.getValue());
						userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 12, 1, prop.getValue());
						gameOutput.reset().writeLong(userInfo.getPlayerGold()).writeLong(userInfo.getLockGold());
						info.setLockGold(userInfo.getLockGold());
						userInfo.sendData(SocketCmd.UPDATE_GOLD, gameOutput.toByteArray());
					} else if (prop.getId() == LuaUtils.get("yuanbao").tolong()) {
						userInfo.setLockrmb(userInfo.getLockrmb()+prop.getValue());
						userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 12, 2, prop.getValue());
						gameOutput.reset().writeLong(userInfo.getRmb()).writeLong(userInfo.getLockrmb());
						info.setLockrmb(userInfo.getLockrmb());
						userInfo.sendData(SocketCmd.UPDATE_RMB, gameOutput.toByteArray());
					} else {
						userInfo.getBackpack().addProp(prop);
						gameOutput.writeInt(1);
						prop.writeData(gameOutput);
						userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 2, prop.getId(), prop.getCount());
						userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
						info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
					}
					giftLibrary.setLastTimeSignTimer(System.currentTimeMillis());
					giftLibrary.setOnLineSignCount(sign+1);
					gameOutput.reset().writeInt(giftLibrary.getOnLineSignCount());
					userInfo.sendData(SocketCmd.RECEIVE_EVERYDAY_SIGN, gameOutput.toByteArray());
					info.setGetGiftDataByte(BytesFactory.objectToByteData(userInfo.getGiftLibrary()));
					userServer.update(info);
				}
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("领取每日签到报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("领取每日签到关闭流报错", e);
			}
		}
	}

	/**
	 * 等级礼包
	 * @param buf
	 * @param session
	 */
	public void receiveLevelGift(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.getBackpack().checkEmpty()) {
				gameOutput.writeInt(CommonCmd.BACKPACK_INSUFFICIENT_SPACE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			GiftLibrary giftLibrary = userInfo.getGiftLibrary();
			long id = gameInput.readInt();
			GiftProp giftProp = (GiftProp) propConfig.getSample(id);
			if (giftProp != null) {
				if (giftLibrary.getLevelGift().contains(giftProp.getUseLevel())) {
					
					return;
				}
				if (giftProp.getUseLevel() > userInfo.getRole().getLevel()) {
					gameOutput.writeInt(CommonCmd.YOU_LEVEL_ERROR);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				giftLibrary.getLevelGift().add(giftProp.getUseLevel());
				userInfo.getBackpack().addProp(giftProp);
				gameOutput.writeInt(1);
				giftProp.writeData(gameOutput);
				userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 2, giftProp.getId(), giftProp.getCount());
				userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
				
				UserInfo info = new UserInfo(userInfo.getId());
				info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
				info.setGetGiftDataByte(BytesFactory.objectToByteData(giftLibrary));
				userServer.update(info);
				
				userInfo.sendData(SocketCmd.RECEIVE_LEVEL_GIFT, giftProp.getUseLevel());
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("领取等级礼包报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("领取等级礼包关闭流报错", e);
			}
		}
	}

}
