package com.server;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.PropConfig;
import com.data.UserDataManage;
import com.entity.Equips;
import com.entity.Prop;
import com.entity.UserInfo;
import com.model.Backpack;
import com.model.EquipsLibrary;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;
import com.utils.LuaUtils;
import com.utils.RandomUtils;

/**
 * 装备处理
 * @author xujinbo
 *
 */
public class EquipsServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	@Resource
	private PropConfig propConfig;
	
	/**
	 * 穿装备 
	 * @param buf
	 * @param session
	 */
	public void equipsWear(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int index = gameInput.readInt();
			Backpack backpack = userInfo.getBackpack();
			Prop prop = backpack.getPropIndex(index);
			// 物品不存在  或者不是装备
			if (prop == null || !(prop instanceof Equips)) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Equips equips = (Equips) prop;
			
			if (equips.getUseLevel() > userInfo.getRole().getLevel()) {
				gameOutput.writeInt(CommonCmd.YOU_LEVEL_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			EquipsLibrary equipsLibaray = userInfo.getEquipsLibrary();
			// 检查此装备是否已经穿戴
			if (equipsLibaray.checkWear(equips)) {
				gameOutput.writeInt(CommonCmd.EQUIPS_ALREADY_WEAR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			boolean exist = equipsLibaray.checkEquipsType(equips);
			if (exist) { 
				// 替换装备
				// 穿戴的装备
				Equips wearEquips = equipsLibaray.getEquipsType(equips.getType());
				// 移除背包中的一件装备
				backpack.removeWearProp(equips);
				// 移除穿上的一件装备
				equipsLibaray.removeEquips(wearEquips);
				// 添加一件装备
				equipsLibaray.addEquips(equips);
				// 移除的装备放入包裹
				wearEquips.setIndex(equips.getIndex());
				backpack.addStripProp(wearEquips);
				// 发送更新包裹物品
				gameOutput.reset().write(1, equips.getIndex(), 1);
				userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
				
				gameOutput.reset().writeLong(wearEquips.getUid());
				userInfo.sendData(SocketCmd.EQUIPS_STRIP, gameOutput.toByteArray());
				
				gameOutput.reset().writeInt(1);
				wearEquips.writeData(gameOutput);
				userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
			} else {
				// 移除背包中的一件装备
				backpack.removeWearProp(equips);
				// 添加一件装备
				equipsLibaray.addEquips(equips);
				// 发送更新包裹物品
				gameOutput.reset().write(1, equips.getIndex(), 1);
				userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
			}
			// 更新角色属性
			userInfo.updateEquipsRole();
			int blood = userInfo.getRole().getMaxBlood();
			int magic = userInfo.getRole().getMaxMagic();
			userInfo.getRole().castAtt();
			blood = userInfo.getRole().getMaxBlood()-blood;
			magic = userInfo.getRole().getMaxMagic()-magic;
			userInfo.getRole().setBlood(userInfo.getRole().getBlood()+blood);
			userInfo.getRole().setMagic(userInfo.getRole().getMagic()+magic);
			userInfo.sendHpMp();
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
			info.setEquipsByte(BytesFactory.objectToByteData(userInfo.getEquipsLibrary()));
			userServer.update(info);
			// 发送更新穿戴装备
			gameOutput.reset();
			equips.writeData(gameOutput);
			userInfo.sendData(SocketCmd.EQUIPS_WEAR, gameOutput.toByteArray());
			equips.setIndex(0);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("穿装备报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("穿装备关闭流报错", e);
			}
		}
	}

	/**
	 * 脱装备 
	 * @param buf
	 * @param session
	 */
	public void equipsStrip(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long uid = gameInput.readInt();
			EquipsLibrary equipsLibrary = userInfo.getEquipsLibrary();
			Equips equips = equipsLibrary.removeEquips(uid);
			if (equips == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Backpack backpack = userInfo.getBackpack();
			equips.setIndex(backpack.getEmptyBar());
			backpack.addStripProp(equips);
			
			// 更新角色属性
			userInfo.updateEquipsRole();
			int blood = userInfo.getRole().getMaxBlood();
			int magic = userInfo.getRole().getMaxMagic();
			userInfo.getRole().castAtt();
			blood = userInfo.getRole().getMaxBlood()-blood;
			magic = userInfo.getRole().getMaxMagic()-magic;
			userInfo.getRole().setBlood(userInfo.getRole().getBlood()+blood);
			userInfo.getRole().setMagic(userInfo.getRole().getMagic()+magic);
			userInfo.sendHpMp();
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
			info.setEquipsByte(BytesFactory.objectToByteData(userInfo.getEquipsLibrary()));
			userServer.update(info);
			
			
			// 发送更新包裹物品
			gameOutput.reset().writeInt(1);
			equips.writeData(gameOutput);
			userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
			
			gameOutput.reset().writeLong(equips.getUid());
			userInfo.sendData(SocketCmd.EQUIPS_STRIP, gameOutput.toByteArray());
			
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("穿装备报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("穿装备关闭流报错", e);
			}
		}
	}

	/**
	 * 改造装备 
	 * @param buf
	 * @param session
	 */
	public void equipsReform(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			boolean isZhuangbei = gameInput.readBoolean();
			int type = gameInput.readInt();// 装备类型
			int index = gameInput.readInt();// 包裹位置
			Backpack backpack = userInfo.getBackpack();
			Prop prop;
			if (isZhuangbei) {
				prop = userInfo.getEquipsLibrary().getEquipsType(type);
			} else {
				prop = backpack.getPropIndex(index);
			}
			// 物品不存在  或者不是装备
			if (prop == null || !(prop instanceof Equips)) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Equips equips = (Equips) prop;
			if (equips.getReformCount() >= 10) {
				gameOutput.writeInt(CommonCmd.EQUIPS_REFORM_COUNT_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (equips.getType() != 1 && equips.getType() != 5 && equips.getType() != 6 && equips.getType() != 7) {
				gameOutput.writeInt(CommonCmd.EQUIPS_NOT_REFORM);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Prop material;
			if (type == 1) {
				material = backpack.getPropId(LuaUtils.get("chaojilingshi").tolong());
			} else {
				material = backpack.getPropId(LuaUtils.get("chaojijingshi").tolong());
			}
			if (material == null || backpack.getPropIdCount(material.getId()) < 2) {
				gameOutput.writeInt(CommonCmd.REFORM_MATERIAL_NOT_EXITS);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 当前改造所需要消耗的钱
			long consume = LuaUtils.get("castReformGold").call(CoerceJavaToLua.coerce(userInfo),
					CoerceJavaToLua.coerce(equips)).tolong();
			
			if (consume > userInfo.getPlayerGold()) {
				gameOutput.writeInt(CommonCmd.GOLD_INSUFFICIENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int pro = (int) ((double)((double)(10-equips.getReformCount())/10)*100);
//			System.out.println("当前炼制成功率="+pro);
			boolean result = RandomUtils.nextBoolean(pro);
			if(result) {
				equips.setReformCount(equips.getReformCount()+1);
				// 添加属性
//				Attribute attribute;
				equips.setLevel(1);
				if (equips.getAttributesList().isEmpty()) {
					LuaUtils.get("qeuipsRandomAttribute").call(CoerceJavaToLua.coerce(equips));
				} else {
//					for (int i = 0; i < equips.getAttributesList().size(); i++) {
//						attribute = equips.getAttributesList().get(i);
//						attribute.setValue(RandomUtils.nextInt(1, 17));
//					}
				}
				//equips
			} else {
				equips.setReformCount(equips.getReformCount()-1);
				if (equips.getReformCount() < 0) {
					equips.setReformCount(0);
					equips.setLevel(0);
				}
			}
			userInfo.updateEquipsRole();
			userInfo.getRole().castAtt();
			userInfo.setPlayerGold(userInfo.getPlayerGold()-consume);
			gameOutput.reset().writeInt(2);
			for (int i = 0; i < 2; i++) {
				gameOutput.writeInt(backpack.removePropIdCount(material.getId(), 1).getIndex());
				gameOutput.writeInt(1);
			}
			userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setPlayerGold(userInfo.getPlayerGold());
			if (isZhuangbei) {
				info.setEquipsByte(BytesFactory.objectToByteData(userInfo.getEquipsLibrary()));
			}
			info.setBackpackByte(BytesFactory.objectToByteData(backpack));
			userServer.update(info);
			
			gameOutput.reset().writeLong(userInfo.getPlayerGold()).writeLong(userInfo.getLockGold());
			userInfo.sendData(SocketCmd.UPDATE_GOLD, gameOutput.toByteArray());
			// 是否是穿戴装备   改造是否成功  等级    装备类型  装备在包裹中的位置   装备改造次数
			gameOutput.reset().write(isZhuangbei, result, equips.getLevel(),
					equips.getType(), equips.getIndex(), equips.getReformCount());
			equips.writeAttributeData(gameOutput);
			userInfo.sendData(SocketCmd.EQUIPS_REFORM, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("穿装备报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("穿装备关闭流报错", e);
			}
		}
	}

	/**
	 * 首饰合成
	 * @param buf
	 * @param session
	 */
	public void jewelryCompose(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int index = gameInput.readInt();// 包裹位置
			Backpack backpack = userInfo.getBackpack();
			Prop prop = backpack.getPropIndex(index);
			// 物品不存在  或者不是装备
			if (prop == null || !(prop instanceof Equips)) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Equips equips = (Equips) prop;
			if (equips.getUseLevel() >= 130) {
				gameOutput.writeInt(CommonCmd.JEWELRY_COMPOSE_LEVEL_ERR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (equips.getType() != 8 && equips.getType() != 9 && equips.getType() != 10) {
				gameOutput.writeInt(CommonCmd.JEWELRY_COMPOSE_LEVEL_ERR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (backpack.getPropIdCount(prop.getId()) < 3) {
				gameOutput.writeInt(CommonCmd.JEWELRY_COMPOSE_COUNT_ERR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			// 当前改造所需要消耗的钱
			long consume = LuaUtils.get("composeGold").call(CoerceJavaToLua.coerce(userInfo),
					CoerceJavaToLua.coerce(equips)).tolong();
			
			if (consume > userInfo.getPlayerGold()) {
				gameOutput.writeInt(CommonCmd.GOLD_INSUFFICIENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			userInfo.setPlayerGold(userInfo.getPlayerGold()-consume);
			
			gameOutput.reset().writeInt(3);
			for (int i = 0; i < 3; i++) {
				gameOutput.writeInt(backpack.removePropIdCount(prop.getId(), 1).getIndex());
				gameOutput.writeInt(1);
			}
			userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
			// 合成的新首饰
			prop = propConfig.getSample(prop.getId()+1);
			gameOutput.reset().writeInt(1);
			backpack.addProp(prop);
			prop.writeData(gameOutput);
			userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setPlayerGold(userInfo.getPlayerGold());
			info.setBackpackByte(BytesFactory.objectToByteData(backpack));
			userServer.update(info);
			
			gameOutput.reset().writeLong(userInfo.getPlayerGold()).writeLong(userInfo.getLockGold());
			userInfo.sendData(SocketCmd.UPDATE_GOLD, gameOutput.toByteArray());

		} catch (IOException e) {
			Logger.getLogger(getClass()).error("首饰合成报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("首饰合成闭流报错", e);
			}
		}
	}
	
}
