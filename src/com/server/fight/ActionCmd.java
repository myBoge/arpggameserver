package com.server.fight;


/**
 * 回个玩家决定要做的动作
 * @author xujinbo
 *
 */
public class ActionCmd {
	
	/** 行动命令 */
	private int action;
	/** 行动对象id */
	private long actionUserUId;
	/** 作用对象位置 */
	private int robUserIndex;
	/** 使用的物品id */
	private int propId;
	
	/**
	 * @param action 行动命令
	 * @param actionUserUId 行动对象id
	 * @param robUserIndex 作用对象位置
	 * @param propId 使用的物品id
	 */
	public ActionCmd(int action, long actionUserUId, int robUserIndex, int propId) {
		super();
		this.action = action;
		this.actionUserUId = actionUserUId;
		this.robUserIndex = robUserIndex;
		this.propId = propId;
	}

	/**
	 * 获取行动命令
	 * @return action 行动命令
	 */
	public int getAction() {
		return action;
	}

	/**
	 * 设置行动命令
	 * @param action 行动命令
	 */
	public void setAction(int action) {
		this.action = action;
	}

	/**
	 * 获取使用的物品id
	 * @return propId 使用的物品id
	 */
	public int getPropId() {
		return propId;
	}

	/**
	 * 设置使用的物品id
	 * @param propId 使用的物品id
	 */
	public void setPropId(int propId) {
		this.propId = propId;
	}

	/**
	 * 获取行动对象id
	 * @return actionUserUId 行动对象id
	 */
	public long getActionUserUId() {
		return actionUserUId;
	}

	/**
	 * 设置行动对象id
	 * @param actionUserUId 行动对象id
	 */
	public void setActionUserUId(long actionUserUId) {
		this.actionUserUId = actionUserUId;
	}

	/**
	 * 获取作用对象位置
	 * @return robUserIndex 作用对象位置
	 */
	public int getRobUserIndex() {
		return robUserIndex;
	}

	/**
	 * 设置作用对象位置
	 * @param robUserIndex 作用对象位置
	 */
	public void setRobUserIndex(int robUserIndex) {
		this.robUserIndex = robUserIndex;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ActionCmd [action=" + action + ", actionUserUId=" + actionUserUId + ", robUserIndex=" + robUserIndex
				+ ", propId=" + propId + "]";
	}

	

	
	
	
}
