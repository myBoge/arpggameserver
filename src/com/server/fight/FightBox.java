package com.server.fight;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.socket.ActionData;
import com.boge.util.ResourceCell;
import com.configs.PropConfig;
import com.configs.SkillConfig;
import com.entity.Entity;
import com.entity.FightData;
import com.entity.Role;
import com.entity.Round;
import com.entity.Skill;
import com.entity.UseProp;
import com.entity.UserInfo;
import com.interfaces.IPool;
import com.model.GameData;
import com.net.CommonCmd;
import com.net.FightCmd;
import com.net.SocketCmd;
import com.server.FightServer;
import com.server.PetServer;
import com.thread.TimerManager;
import com.utils.ActionTimerListener;
import com.utils.LuaUtils;
import com.utils.RandomUtils;

/**
 * 战斗盒子
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
public class FightBox extends Entity implements IPool {
	
	/** 开始时间 */
	private Timestamp timestamp;
	private TimerManager timerThread;
	/** 当前回合值 */
	private int currentRound;
	/** 当前等待还剩下的秒数 */
	private int currentWaitOvertime;
	/** 当前回合演播的动作 */
	private List<Round> rounds = new ArrayList<>();
	/** 攻击顺序 */
	private List<FightData> attackOrder = new ArrayList<FightData>();
	/** 玩家自己决定要做的动作   uid  动作 */
	private Map<Long, ActionCmd> userDecisionMap = new HashMap<Long, ActionCmd>();
	/** 玩家播放结束 */
	private List<Long> userFightPlayEndList = new ArrayList<>();
	/** 等待做出决定的总人数 */
	private int wiatLen;
	/** 被攻击方队伍数据 */
	private List<FightData> robberyList;
	/** 攻击方队伍数据 */
	private List<FightData> robList;
	private FightServer fightServer;
	/** 此房间是否已经战斗结束 */
	private boolean isGameOver = false;
	/** 当前处于那个阶段   0待命  1决定   2模拟 3演播  */
	private int wiatPeriod = 0;
	/** 战斗对象类型  0 和电脑AI打  1  玩家打 */
	private int fightTargetType = 0;
	/** 战斗类型  1pvp 2rob 3pk 4pve*/
	private int fightType = 0;
	/** 当前的状态是否属于满状态开始的游戏 */
	private boolean fullStatus;
	/** 敌人显示血量剩余次数 */
	private int enemyShowHPCount = 0;
	/** 用于战斗临时存储 */
	public Object obj;
	/** 战斗结果 0 没有结果  1进攻方胜利  2防守方胜利   */
	private int fightResult;
	/** 此战斗房间生成的对象UID */
	private long fightUID;
	private SkillConfig skillConfig;
	private PropConfig propConfig;
	
	public FightBox() {
		super();
		timerThread = ResourceCell.getResource(TimerManager.class);
		skillConfig = ResourceCell.getResource(SkillConfig.class);
		propConfig = ResourceCell.getResource(PropConfig.class);
	}

	/**
	 * 添加参与到战斗的我人员
	 * @param robList 攻击方
	 * @param robberyList 被攻击方(电脑方)
	 * @param fullStatus 满状态
	 */
	public void addUserData(List<FightData> robList, List<FightData> robberyList, boolean fullStatus) {
		fightTargetType = 1;
		addFightData(robList, robberyList, fullStatus);
		castWiatLen();
	}
	
	/**
	 * 添加玩家和电脑之间的战斗信息
	 * @param robList 攻击方
	 * @param robberyList 被攻击方(电脑方)
	 * @param fullStatus 满状态
	 */
	public void addUserAIData(List<FightData> robList, List<FightData> robberyList, boolean fullStatus) {
		fightTargetType = 0;
		addFightData(robList, robberyList, fullStatus);
		castWiatLen();
	}
	
	/**
	 * 添加战斗数据
	 * @param robList 攻击方
	 * @param robberyList 被攻击方(电脑方)
	 * @param fullStatus 满状态
	 */
	private void addFightData(List<FightData> robList, List<FightData> robberyList, boolean fullStatus) {
		this.fullStatus = fullStatus;
		this.robList = robList;
		this.robberyList = robberyList;
		
		addAttackOrder();
		
		userDecisionMap.clear();
		isGameOver = false;
		fightResult = 0;
		currentRound = 0;
		wiatPeriod = 0;
		enemyShowHPCount = 0;
		FightData fightData;
		for (int i = 0; i < attackOrder.size(); i++) {
			fightUID++;
			fightData = attackOrder.get(i);
			fightData.getUserInfo().setCurrentFightId(this.getId());
			fightData.setId(fightUID);
			if (fightData.isBrainAI() || fullStatus) {// 战斗用的血蓝  满状态
				fightData.setBlood(fightData.getUserInfo().getRole().getMaxBlood());
				fightData.setMagic(fightData.getUserInfo().getRole().getMaxMagic());
			} else {
				fightData.setBlood(fightData.getUserInfo().getRole().getBlood());
				fightData.setMagic(fightData.getUserInfo().getRole().getMagic());
			}
		}
		attackSort();
	}
	
	/** 填写此战场双飞数据 */
	private void addAttackOrder() {
		attackOrder.clear();
		for (int i = 0; i < robList.size(); i++) {
			if (robList.get(i)!=null) {
				attackOrder.add(robList.get(i));
			}
		}
		for (int i = 0; i < robberyList.size(); i++) {
			if (robberyList.get(i)!=null) {
				attackOrder.add(robberyList.get(i));
			}
		}
	}

	/** 攻击顺序排序 */
	private void attackSort() {
		Collections.sort(attackOrder, new Comparator<FightData>() {
			@Override
			public int compare(FightData o1, FightData o2) {
				int aPrice = o1.getUserInfo().getRole().getSpeed();
				int bPrice = o2.getUserInfo().getRole().getSpeed();
				if(aPrice > bPrice) {
					return -1;
				} else if(aPrice < bPrice) {
					return 1;
				} else {
					return 0;
				}
			}
		});
	}

	/**
	 * 开始当前战斗
	 */
	public void start() {
		currentRound = 0;
		nextRound();
	}
	
	/**
	 * 执行回合
	 */
	private void nextRound() {
		timerThread.clearTimer(actionPlayEnd);
		if (isGameOver) {
			fightOver();
			return;
		}
		currentRound++;
		if (currentRound > GameData.gameData.getFightMaxCount()) { // 是否大于最大回合数
			fightResult = 0;
			sendAllWiatDecision(SocketCmd.FIGHT_NOT_RESULT);
			fightOver();
			return;
		}
		userDecisionMap.clear();
		try {
			sendAllWiatDecision(SocketCmd.FIGHT_ALL_WAIT, new GameOutput().writeInt(currentRound).toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("发送战斗阶段指令报错", e);
		}
		wiatPeriod = 1;
		currentWaitOvertime = 24;
		timerThread.doLoop(1000, waitOvertime);
		
	}

	/**
	 * 等待玩家操作超时
	 */
	ActionTimerListener waitOvertime = new ActionTimerListener() {
		
		@SuppressWarnings("unused")
		public void execute() {
			currentWaitOvertime--;
			if (currentWaitOvertime == 0) {
				timerThread.clearTimer(waitOvertime);
				mockFights();
			}
		}
		
	};


	/**
	 * 玩家做了个决定
	 * @param gameInput 读取的数据
	 * @param action 决定命令
	 * @throws IOException 
	 */
	public void userDecision(GameInput gameInput, int action) throws IOException{
		long userUID = gameInput.readInt();//玩家uid
		int robUserIndex = gameInput.readInt();//被作用玩家位置
		FightData fightData;
		if (wiatPeriod != 1) {
			return;
		}
		if ((fightData=getUserInfoUid(userUID)) == null) {
			return;
		}
		if (!userDecisionMap.containsKey(userUID)) {
			ActionCmd actionCmd = new ActionCmd(action, userUID, robUserIndex, 0);
			if (actionCmd.getAction() == FightCmd.TAKE_MEDICINE || 
					actionCmd.getAction() == FightCmd.MAGIC_ATTACK) { // 如果是使用道具或技能
				
				actionCmd.setPropId(gameInput.readInt()); // 添加使用道具的id
				
			}
			userDecisionMap.put(userUID, actionCmd);
			if (!fightData.getUserInfo().getRole().isPet()) {
				sendAllWiatDecision(SocketCmd.FIGHT_EXECUTE_ACTION, fightData.getId());
			}
//			System.out.println("玩家做出决定数量="+userDecisionMap.size()+", 需要等待数量="+wiatLen+
//					", 离线数量="+offlineCount()+", 计算结果="+(wiatLen-offlineCount())
//					+" | 是否通过="+(userDecisionMap.size() >= (wiatLen-offlineCount()))
//					);
			if (userDecisionMap.size() >= (wiatLen-offlineCount())) {
				mockFights();
			}
		}
	}
	
	/**
	 * 根据敌人的位置获取敌人获取战斗对象   如果此位置敌人不存在  那么随机选择一个还活着的敌人
	 * @param fightData 自己的属性
	 * @param enemyIndex 敌人位置
	 * @param camp 是否在同阵营中找
	 * @return
	 */
	public FightData getFightData(FightData fightData, int enemyIndex, boolean camp) {
		FightData data = null;
		if (enemyIndex < 10) {
			if (checkFightDataAttacker(fightData.getId())) {//如果当前人是攻击方
				if (camp) {
					data = robList.get(enemyIndex);
				} else {
					data = robberyList.get(enemyIndex);
				}
				if (data == null) {
					data = robberyListRanderAlive();
				}
			} else {
				if (camp) {
					data = robberyList.get(enemyIndex);
				} else {
					data = robList.get(enemyIndex);
				}
				if (data == null) {
					data = robListRanderAlive();
				}
			}
		} else {
			if (checkFightDataAttacker(fightData.getId())) {//如果当前人是攻击方
				data = robberyListRanderAlive();
			} else {
				data = robListRanderAlive();
			}
		}
		return data;
	}
	
	/**
	 * 根据fightDataId or userInfo.uid获取战斗对象
	 * @param id
	 * @return
	 */
	public FightData getUserInfoUid(long fightDataId) {
		for (int i = 0; i < attackOrder.size(); i++) {
			if (attackOrder.get(i).getId() == fightDataId) {
				return attackOrder.get(i);
			}
		}
		return null;
	}
	
	
	/**
	 * 开始执行模拟战斗 
	 */
	private void mockFights() {
		if (wiatPeriod != 1) return;
		timerThread.clearTimer(waitOvertime);
		// 攻击前  重新排序    有可能  此回合有些角色速度会变  或者更换了宠物
		addAttackOrder();
		attackSort();
		castWiatLen();
		if (enemyShowHPCount > 0) {
			enemyShowHPCount--;
		}
		if (isGameOver) return;
		wiatPeriod = 2;
		rounds.clear();
		int len = attackOrder.size();
		FightData fightData;
		FightData targetFightData;
		UserInfo userInfo;
		ActionCmd actionCmd;
		ActionCmd robUserActionCmd;
		Round round;
		UserInfo targetUserInfo;// 作用者
		int damage;// 造成的伤害
		boolean isDefense;//是否处于防御状态
		Varargs luaVarargs;
		LuaValue luaValue;
		int attackCount;//技能可以攻击几个人
		int attackType;//攻击类型
		List<FightData> attactUserList;// 被攻击的对象
		Role role;
//		System.out.println("长度="+len);
		for (int i = 0; i < len; i++) {
			damage = 0;
			attackCount = 1;
			attackType = 1;
			// 当一个人模拟战斗结束后  立即判断是否已经结束战斗
			if (!isGameOver) isGameOver = checkGameEnd();
			if (isGameOver) {
				break;
			}
			fightData = attackOrder.get(i);
			userInfo = fightData.getUserInfo();
			
			actionCmd = userDecisionMap.get(fightData.getId());// 根据玩家id 获取玩家做出的决定
			if (actionCmd == null) { // 如果此人没有做出决定
				if (fightData.isBrainAI()) {//是否是电脑控制战斗
					if (fightType == 1) {// 如果是竞技场
						if (fightData.getUserInfo().getRole().isPet()) {
							// 如果此对象是宠物
							FightData tempUser = getUserInfo(fightData.getUserInfo().getId());
							if (tempUser != null && tempUser.isDeath()) {// 找主人  并且 已经死亡
								luaValue = LuaUtils.get("brainAiArenaProp").call(CoerceJavaToLua.coerce(tempUser));
								actionCmd = new ActionCmd(FightCmd.TAKE_MEDICINE, fightData.getId(), getFightDataIndex(tempUser.getId()), luaValue.toint());
							} else {
								actionCmd = getBrainAiActionCmd(fightData);
							}
						} else {
							actionCmd = getBrainAiActionCmd(fightData);
						}
					} else {
						// 判断此角色是使用什么攻击方式
						luaValue = LuaUtils.get("brainAiFight").call(CoerceJavaToLua.coerce(userInfo));
						attackType = luaValue.toint();
						if (attackType == 1) {//普通攻击
							actionCmd = randomActionCmd(FightCmd.ORDINARY_ATTACK, fightData, 0);
						} else if (attackType == 2) {//法术攻击
							int skillId = 0;
							if (userInfo.getRole().isPet()) {
								luaValue = LuaUtils.get("getAiSkillId").call(CoerceJavaToLua.coerce(userInfo));
								skillId = luaValue.toint();
							} else {
								List<Skill> skills = userInfo.getSkillLibrary().getSkills();
								skillId = (int) skills.get(RandomUtils.nextInt(0, skills.size()>3?3:skills.size())).getId();
							}
							actionCmd = randomActionCmd(FightCmd.MAGIC_ATTACK, fightData, skillId);
						}
					}
				} else {
					// 此玩家在线 
					if (userInfo.getCurrentFightId()!=0 &&  userInfo.getOnlineState() == 1) {
//						if (userInfo.getNotActionCount() < 1) {
//							userInfo.setNotActionCount(userInfo.getNotActionCount()+1);
//							actionCmd = randomActionCmd(FightCmd.ORDINARY_ATTACK, fightData, 0);
//						} else {
							actionCmd = getUserActionCmd(fightData);
//						}
					} else {
						actionCmd = getUserActionCmd(fightData);
					}
				}
			} else {
				// 添加到记忆战斗中
				if (actionCmd.getAction() != FightCmd.AUTOMATIC &&
						actionCmd.getAction() != FightCmd.FLEE &&
						actionCmd.getAction() != FightCmd.RECALL &&
						actionCmd.getAction() != FightCmd.CALL) {
					userInfo.getRole().setActionCmd(actionCmd);
				}
			}
			
			round = new Round();
			round.setUserId(fightData.getId());
			// 自动判断
			if (actionCmd.getAction() == FightCmd.AUTOMATIC) {
				actionCmd = getUserActionCmd(fightData);
			}
			
//			System.out.println("此人动作解析结束  i="+i+", fight="+fightData+"， actionCmd="+actionCmd);
			round.setAction(actionCmd.getAction());
			
			// 检查当前出手的人是否已经死亡    或者已经被捕捉
			if (fightData.isDeath() || fightData.isCall()) {
				// 死亡状态下依旧可以逃跑
				if (!fightData.isBrainAI() && !userInfo.getRole().isPet()) { //玩家操作  不是宠物  
					if (actionCmd.getAction() == FightCmd.FLEE) {
						boolean fleeSuccess = LuaUtils.get("fightFlee").call(
								CoerceJavaToLua.coerce(this), 
								CoerceJavaToLua.coerce(fightData)).toboolean();
						
						round.setSuccess(fleeSuccess);
						rounds.add(round);
					}
				}
				continue;
			}
			
			switch (actionCmd.getAction()) {
				case FightCmd.ORDINARY_ATTACK:
					// 检查作用目标是否死亡
					targetFightData = getFightData(fightData, actionCmd.getRobUserIndex(), false);
					targetUserInfo = targetFightData.getUserInfo();
					if (targetFightData.isDeath()) { // 如果此人血量为0  表示已经死亡  或被捕捉
						actionCmd = randomActionCmd(actionCmd.getAction(), fightData, actionCmd.getPropId());
						targetFightData = getFightData(fightData, actionCmd.getRobUserIndex(), false);
						targetUserInfo = targetFightData.getUserInfo();
					}
					// 获取被攻击者选择的操作
					robUserActionCmd = userDecisionMap.get(targetFightData.getId());// 获取被攻击玩家选择的状态
					if (robUserActionCmd != null) {
						isDefense = robUserActionCmd.getAction()==FightCmd.DEFEND;
					} else {
						isDefense = false;
					}
					//检查是否有人保护
					robUserActionCmd = checkProtect(actionCmd.getRobUserIndex());
					if (robUserActionCmd != null) {
						FightData tempFightData = getUserInfoUid(robUserActionCmd.getActionUserUId());
						if (!tempFightData.isDeath() && tempFightData.getId() != targetFightData.getId()) { // 如果此人没有  死亡或被捕捉
							round.setProtectUid(targetFightData.getId());
							targetFightData = tempFightData;
							targetUserInfo = tempFightData.getUserInfo();
						}
					}
					// 攻击力减去防御力  等于伤害值           非防御状态  防御减半
					
					luaVarargs = LuaUtils.get("ordinaryAttack").invoke(new LuaValue[]{
						LuaValue.valueOf(isDefense),// 被攻击者是否处于防御状态
						LuaValue.valueOf(RandomUtils.nextBoolean(20)),// 是否暴击
						LuaValue.valueOf(userInfo.getRole().getOrdinaryAttack()),// 攻击者攻击值
						LuaValue.valueOf(targetUserInfo.getRole().getDefense()),// 被攻击者防御值
					});
					
					damage = luaVarargs.toint(1);
					if (damage <= 0) { // 如果伤害低于等于0  那么最低伤害1
						damage = 1;
					}
					targetFightData.setBlood(targetFightData.getBlood()-damage);
					if (targetFightData.getBlood() <= 0 ) {
						targetFightData.setBlood(0);
						if (!targetFightData.isBrainAI() && targetUserInfo.getRole().isPet()) {
							targetUserInfo.getRole().setStrength(targetUserInfo.getRole().getStrength()-30);
							targetUserInfo.sendData(SocketCmd.PET_TAME, targetUserInfo.getRole().getUid(), targetUserInfo.getRole().getStrength());
						}
					}
					round.addObjct(targetFightData.getId(), targetFightData.getBlood(), targetFightData.getMagic());
					break;
				case FightCmd.MAGIC_ATTACK:
					// 获取使用的技能
					Skill skill = null;
					if (userInfo.getId() != 0 && !userInfo.getRole().isPet()) {// 玩家
						skill = userInfo.getSkill(actionCmd.getPropId());
					} else {
						// 此宠物等级大于20级  或是电脑控制的宠物  那么可以使用法术攻击
						if (userInfo.getRole().getLevel() >= 20 || fightData.isBrainAI()) {
							if (userInfo.getRole().checkExistSkill(actionCmd.getPropId())) {
								skill = skillConfig.getSample(actionCmd.getPropId());
								if (skill != null) {
									skill.setLevel(LuaUtils.get("getMaxUpLevel").
											call(CoerceJavaToLua.coerce(userInfo.getRole())).toint());
								}
							}
						}
					}
					if (skill == null) {
//						System.out.println(actionCmd.getPropId());
						continue;
					}
					// 计算使用此技能要消耗的蓝
					int useMp = (int) (skill.getUseMP()*skill.getLevel()*0.25);
					if (useMp <= fightData.getMagic()) {
						attackCount = LuaUtils.getCommon().get("getAttackCount").call(CoerceJavaToLua.coerce(skill)).toint();
						attactUserList = getAttactUserInfo(fightData, actionCmd, attackCount);
						for (int j = 0; j < attactUserList.size(); j++) {
							targetFightData = attactUserList.get(j);
							targetUserInfo = targetFightData.getUserInfo();
							// 获取被攻击者选择的操作
							robUserActionCmd = userDecisionMap.get(targetFightData.getId());// 获取被攻击玩家选择的状态
							if (robUserActionCmd != null) {
								isDefense = robUserActionCmd.getAction()==FightCmd.DEFEND;
							} else {
								isDefense = false;
							}
							damage = LuaUtils.get("magicAttack").invoke(new LuaValue[]{
									LuaValue.valueOf(isDefense),// 被攻击者是否处于防御状态
									CoerceJavaToLua.coerce(userInfo),// 被攻击者的对象
									CoerceJavaToLua.coerce(targetUserInfo),// 被攻击者的对象
									CoerceJavaToLua.coerce(skill),// 使用的技能
								}).toint(1);
							if (damage <= 0) { // 如果伤害低于等于0  那么最低伤害1
								damage = 1;
							}
							targetFightData.setBlood(targetFightData.getBlood()-damage);
							if (targetFightData.getBlood() <= 0 ) {
								targetFightData.setBlood(0);
								if (!targetFightData.isBrainAI() && targetUserInfo.getRole().isPet()) {
									targetUserInfo.getRole().setStrength(targetUserInfo.getRole().getStrength()-30);
									targetUserInfo.sendData(SocketCmd.PET_TAME, targetUserInfo.getRole().getUid(), targetUserInfo.getRole().getStrength());
									ResourceCell.getResource(PetServer.class).updatePet(targetUserInfo);
								}
							}
							round.addObjct(targetFightData.getId(), targetFightData.getBlood(), targetFightData.getMagic());
						}
						if (!fightData.isBrainAI()) {// 如果不是电脑控制玩家  那么扣蓝量
							fightData.setMagic(fightData.getMagic()-useMp);
						}
					} else {
						round.setAction(FightCmd.NOT_ACTION);
					}
					round.setActionMp(fightData.getMagic());
					break;
				case FightCmd.DEFEND:
					break;
				case FightCmd.FLEE:
					boolean fleeSuccess = LuaUtils.get("fightFlee").call(
							CoerceJavaToLua.coerce(this), 
							CoerceJavaToLua.coerce(fightData)).toboolean();
					round.setSuccess(fleeSuccess);
					break;
				case FightCmd.TAKE_MEDICINE:
					try {
						int propIndex = actionCmd.getPropId(); // 使用的物品位置
						UseProp prop = null;
						if (fightData.isBrainAI()) {//电脑控制的对象
							prop = (UseProp) propConfig.getSample(propIndex);
						} else {
							prop = (UseProp) userInfo.getBackpack().getPropIndex(propIndex);
						}
						if (prop != null) {
							boolean isDeleteProp = false;
							if (prop.getType() == 2 || prop.getType() == 3 || prop.getType() == 4) {
								// 如果是治疗类型道具
								targetFightData = getFightData(fightData, actionCmd.getRobUserIndex(), true);
								targetUserInfo = targetFightData.getUserInfo();
								
								targetUserInfo.getRole().setBlood(targetFightData.getBlood());
								targetUserInfo.getRole().setMagic(targetFightData.getMagic());
								
								luaValue = LuaUtils.get("useBloodAndMagicProp").call(
										CoerceJavaToLua.coerce(prop), 
										CoerceJavaToLua.coerce(targetUserInfo),
										CoerceJavaToLua.coerce(targetUserInfo.getRole())
										);
								
								isDeleteProp = luaValue.toboolean();
								targetFightData.setBlood(targetUserInfo.getRole().getBlood());
								targetFightData.setMagic(targetUserInfo.getRole().getMagic());
								round.addObjct(targetFightData.getId(), targetFightData.getBlood(), targetFightData.getMagic());
							} else if (prop.getType() == 11) {
								if (prop.getId() == 2012) {
									if (prop.getUseCount() > 0) {
										prop.setUseCount(prop.getUseCount()-1);
										enemyShowHPCount = 5;
									}
									if (prop.getUseCount() == 0) {
										isDeleteProp = true;
									}
								}
							}
							round.setMsg((int) prop.getId());
							if (!fightData.isBrainAI()) {//不是由电脑控制的对象
								if (isDeleteProp) {
									//更新此物品数量  并通知客户端更新
									userInfo.getBackpack().removePropIndexCount(propIndex, 1);
									GameOutput gameOutput = new GameOutput();
									gameOutput.write(1, propIndex, 1);
									userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
									gameOutput.close();
								}
								fightServer.updateBackpack(userInfo);
							}
						} else {
							// 不存在此物品    自动转换成普通攻击
							actionCmd = randomActionCmd(FightCmd.ORDINARY_ATTACK, fightData, 0);
						}
					} catch (Exception e) {
						Logger.getLogger(getClass()).error("用户使用道具报错", e);
					}
					break;
				case FightCmd.CALL:
					role = userInfo.getPetLibrarys().getRole(actionCmd.getRobUserIndex());
					if (role == null || role.isJoinBattlePet()) {
						continue;
					}
					if (role.getBlood() <= 0) {
						round.setSuccess(false);
						round.setMsg(CommonCmd.CALL_FAIL_BLOOD_DEFICIENCY);
						break;
					}
					if (fightData.checkUsePet(role)) {
						round.setSuccess(false);
						round.setMsg(CommonCmd.CALL_FAIL_USE);
						break;
					}
					//宠物是否愿意参加战斗
					boolean result = LuaUtils.get("checkPetWar").call(LuaValue.valueOf(role.getStrength())).toboolean();
					if (!result) {
						round.setSuccess(false);
						round.setMsg(CommonCmd.PET_UNWILLING_WAR);
						break;
					}
					userInfo.getPetLibrarys().updateJoinBattle(role.getUid());
					userInfo.sendData(SocketCmd.PET_UPDATE_JOINBATTLE, role.getUid(), true);
					// 计算宠物所在预计位置
					targetFightData = getUserPet(fightData);
					if (targetFightData == null) {
						targetFightData = UserInfo.createFightData(userInfo, role, fightData.isBrainAI());
						fightUID++;
						targetFightData.setId(fightUID);
						targetFightData.getUserInfo().setUid(targetFightData.getId());
						attackOrder.add(targetFightData);
						if(checkFightDataAttacker(fightData.getId())) {
							robList.set(getFightDataIndex(fightData.getId())+5, targetFightData);
						} else {
							robberyList.set(getFightDataIndex(fightData.getId())+5, targetFightData);
						}
					} else {
						fightData.addUsePet(targetFightData.getUserInfo().getRole());
						targetFightData.getUserInfo().getRole().setBlood(targetFightData.getBlood());
						targetFightData.getUserInfo().getRole().setMagic(targetFightData.getMagic());
					}
					targetFightData.setCall(true);
					targetFightData.setQuiltCatch(false);
					targetFightData.setBlood(role.getBlood());
					targetFightData.setMagic(role.getMagic());
					targetFightData.getUserInfo().setRole(role);
					round.setSuccess(true);
					round.addObjct(role.getId(), targetFightData.getId(), 
							targetFightData.getBlood(), targetFightData.getMagic(),
							role.getMaxBlood(), role.getMaxMagic(),
							role.getName(), role.getUid());
					break;
				case FightCmd.RECALL:
					// 不是宠物
					if (userInfo.getRole().isPet()) {
						continue;
					}
					role = userInfo.getPetLibrarys().getJoinBattlePet();
					if (role == null) {
						continue;
					}
					targetFightData = getUserInfoUid(actionCmd.getRobUserIndex());
					if (targetFightData == null || targetFightData.isDeath()) { // 如果此人血量为0  表示已经死亡  或被捕捉
						continue;
					}
					fightData.addUsePet(role);
					userInfo.getPetLibrarys().updateJoinBattle(0);
					userInfo.sendData(SocketCmd.PET_UPDATE_JOINBATTLE, role.getUid(), false);
					targetFightData.setQuiltCatch(true);
					round.addObjct(targetFightData.getId(), -1, -1);
					break;
				case FightCmd.CAPTURE:
					if (userInfo.getPortabilityPetCount() <= userInfo.getPetLibrarys().getPetSize()) {
						round.setSuccess(false);
						round.setMsg(CommonCmd.PET_COUNT_FULL);
					} else {
						targetFightData = getFightData(fightData, actionCmd.getRobUserIndex(), false);
						targetUserInfo = targetFightData.getUserInfo();
						if (targetFightData.isDeath()) {
							round.setSuccess(false);
							round.setMsg(CommonCmd.TARGET_NOT_CATCH);
							return;
						}
						if (!targetFightData.getUserInfo().getRole().isPet() && !targetFightData.isCanCatch()) {
							round.setSuccess(false);
							round.setMsg(CommonCmd.TARGET_NOT_CATCH);
						} else {
							if(targetUserInfo.getRole().getCarryLevel() > userInfo.getRole().getLevel()) {
								round.setSuccess(false);
								round.setMsg(CommonCmd.CATCH_LEVEL_HIGH);
							} else {
								boolean success = RandomUtils.nextBoolean(70);
								round.setSuccess(success);
								round.addObjct(targetFightData.getId(), -1, -1);
								if (success) {
									targetFightData.setQuiltCatch(true);
									fightData.addCatchRole(targetFightData.getUserInfo().getRole());
								}
							}
						}
					}
					break;
				case FightCmd.PROTECT:
					break;
				default:
					break;
			}
			rounds.add(round);
		}
		// 取消刚刚被召唤出来的状态
		for (int i = 0; i < attackOrder.size(); i++) {
			attackOrder.get(i).setCall(false);
		}
		// 模拟结束  再次演算等待总人数
		addAttackOrder();
		castWiatLen();
		//整理发送客户端的数据
		try {
			int timer = 1;// 播放战斗需要的时间
			for (int i = 0; i < rounds.size(); i++) {
				switch (rounds.get(i).getAction()) {
					case FightCmd.ORDINARY_ATTACK:
						timer += 3;
						break;
					case FightCmd.MAGIC_ATTACK:
						timer += 4;
						break;
					case FightCmd.FLEE:
					case FightCmd.TAKE_MEDICINE:
					case FightCmd.CALL:
					case FightCmd.RECALL:
					case FightCmd.CAPTURE:
						timer += 1;
						break;
					case FightCmd.DEFEND:
					case FightCmd.NOT_ACTION:
						timer += 0;
						break;
					default:
						timer += 2;
						break;
				}
			}
			
			userFightPlayEndList.clear();
			if (!isGameOver) isGameOver = checkGameEnd();
			GameOutput gameOutput = new GameOutput();
			writeRoundData(gameOutput);
			sendAllWiatDecision(SocketCmd.FIGHT_ROUND_PLAY_DATA, gameOutput.toByteArray());
			gameOutput.close();
			wiatPeriod = 3;
			// 回合演播计算
			timerThread.doOnce(timer*1000, actionPlayEnd);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("发送战斗阶段指令报错", e);
		}
	}
	

	/**
	 * 获取此角色所在的位置
	 * @param fightData
	 * @return
	 */
	private int getFightDataIndex(long fightDataId) {
		for (int i = 0; i < robList.size(); i++) {
			if (robList.get(i)!=null && robList.get(i).getId() == fightDataId) {
				return i;
			}
		}
		for (int i = 0; i < robberyList.size(); i++) {
			if (robberyList.get(i)!=null && robberyList.get(i).getId() == fightDataId) {
				return i;
			}
		}
		return 0;
	}

	/**
	 * 获取电脑智能战斗动作
	 * @param fightData
	 * @return
	 */
	private ActionCmd getBrainAiActionCmd(FightData fightData) {
		UserInfo userInfo = fightData.getUserInfo();
		LuaValue luaValue = LuaUtils.getCommon().get("brainAiArenaFight").call(CoerceJavaToLua.coerce(fightData));
		int attackType = luaValue.toint();
		ActionCmd actionCmd = null;
		if (attackType == 1) {
			actionCmd = randomActionCmd(FightCmd.ORDINARY_ATTACK, fightData, 0);
		} else if (attackType == 2) {
			int skillId = 0;
			if (userInfo.getRole().isPet()) {
				luaValue = LuaUtils.getCommon().get("getAiSkillId").call(CoerceJavaToLua.coerce(userInfo));
				skillId = luaValue.toint();
			} else {
				List<Skill> skills = userInfo.getSkillLibrary().getSkills();
				skillId = (int) skills.get(RandomUtils.nextInt(0, skills.size()>3?3:skills.size())).getId();
			}
			actionCmd = randomActionCmd(FightCmd.MAGIC_ATTACK, fightData, skillId);
		} else if (attackType == 5) {// 使用道具
			//使用道具的Id
			luaValue = LuaUtils.getCommon().get("brainAiArenaProp").call(CoerceJavaToLua.coerce(fightData));
			actionCmd = new ActionCmd(FightCmd.TAKE_MEDICINE, fightData.getId(), getFightDataIndex(fightData.getId()), luaValue.toint());
		}
		return actionCmd;
	}

	/**
	 * 根据玩家id获取玩家对象  非宠物
	 * @param fightData
	 * @return 
	 */
	private FightData getUserInfo(long id) {
		FightData fightData;
		for (int i = 0; i < attackOrder.size(); i++) {
			fightData = attackOrder.get(i);
			if (fightData != null) {
				// id一样   并且不是宠物
				if(fightData.getUserInfo().getId() == id && !fightData.getUserInfo().getRole().isPet()) {
					return fightData;
				}
			}
		}
		return null;
	}
	
	/**
	 * 删除一个玩家  此玩家从战场上离开
	 * @param fightData
	 */
	public void removeFightData(FightData fightData){
		if (checkFightDataAttacker(fightData.getId())) {
			for (int i = 0; i < robList.size(); i++) {
				if (robList.get(i)!=null && robList.get(i).getId() == fightData.getId()) {
					robList.set(i, null);
				}
			}
		} else {
			for (int i = 0; i < robberyList.size(); i++) {
				if (robberyList.get(i)!=null && robberyList.get(i).getId() == fightData.getId()) {
					robberyList.set(i, null);
				}
			}
		}
		
	}
	
	/**
	 * 获取此对象的宠物
	 * @param fightData
	 * @return 
	 */
	public FightData getUserPet(FightData fightData) {
		List<FightData> fightList;
		if(checkFightDataAttacker(fightData.getId())) {
			fightList = robList;
		} else {
			fightList = robberyList;
		}
		UserInfo info;
		for (int i = 0; i < fightList.size(); i++) {
			if (fightList.get(i) != null) {
				info = fightList.get(i).getUserInfo();
				if(info.getId() == fightData.getUserInfo().getId() && info.getRole().isPet()) {
					return fightList.get(i);
				}
			}
		}
		return null;
	}

	/** 计算在线玩家确认数量  包括宠物 */
	private void castWiatLen() {
		int count = 0;
		for (int i = 0; i < attackOrder.size(); i++) {
			if (!attackOrder.get(i).isBrainAI()) {
				count++;
			}
		}
		wiatLen = count;
	}

	/**
	 * 获取此次技能攻击  要攻击的角色
	 * @param fd 
	 * @param actionCmd 
	 * @param attackCount 要获取的数量
	 */
	private List<FightData> getAttactUserInfo(FightData fd, ActionCmd actionCmd, int attackCount) {
		// 选中的  攻击角色
		List<FightData> fightList = new ArrayList<>();
		// 检查主要攻击的人  是否已经死亡
		FightData fightData = getFightData(fd, actionCmd.getRobUserIndex(), false);
		if (fightData.isDeath()) { // 如果此人血量为0  表示已经死亡  或者被捕捉
			actionCmd = randomActionCmd(actionCmd.getAction(), fd, actionCmd.getPropId());
			fightData = getFightData(fd, actionCmd.getRobUserIndex(), false);
		}
		fightList.add(fightData);
		if (attackCount > fightList.size()) {
			List<FightData> aliveList = new ArrayList<>();
			if (checkFightDataAttacker(fightData.getId())) {//被攻击的人在进攻方
				for (int i = 0; i < robList.size(); i++) {
					// 次人没有死亡  没有被捕捉  并且  不再选中角色
					if (robList.get(i) != null && !robList.get(i).isDeath() && !containsUser(fightList, robList.get(i) ) ) {
						aliveList.add(robList.get(i));
					}
				}
			} else {
				for (int i = 0; i < robberyList.size(); i++) {
					// 次人没有死亡   没有被捕捉  并且  不再选中角色
					if (robberyList.get(i) != null && !robberyList.get(i).isDeath() && !containsUser(fightList, robberyList.get(i))) {
						aliveList.add(robberyList.get(i));
					}
				}
			}
			// 处理被攻击的人
			for (int i = 1; i < attackCount; i++) {
				if (aliveList.size() > 0) {
					fightList.add(aliveList.remove( RandomUtils.nextInt(aliveList.size()) ));
				} else {
					break;
				}
			}
		}
		return fightList;
	}
	
	/**
	 * 检查此id是否在进攻方
	 * @param uid
	 * @return
	 */
	public boolean checkFightDataAttacker(long uid) {
		for (int i = 0; i < robList.size(); i++) {
			if (robList.get(i)!=null && robList.get(i).getId() == uid) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 检查数组中是否存在此战斗对象
	 * @param fightDataList
	 * @param fightData
	 * @return
	 */
	private boolean containsUser(List<FightData> fightDataList, FightData fightData){
		for (int i = 0; i < fightDataList.size(); i++) {
			if(fightDataList.get(i).getId() == fightData.getId()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取此用户的战斗记录  如果没有  那么生成一个 自动攻击
	 * @param userInfo
	 * @return
	 */
	private ActionCmd getUserActionCmd(FightData fightData) {
		ActionCmd actionCmd;
		if (fightData.getUserInfo().getRole().getActionCmd() != null) {
			actionCmd = fightData.getUserInfo().getRole().getActionCmd();// 获取之前的战斗记忆
			FightData fd = getFightData(fightData, actionCmd.getRobUserIndex(), false);
			if (fd.isDeath()) {
				actionCmd = randomActionCmd(actionCmd.getAction(), fightData, actionCmd.getPropId());
			}
		} else {
			actionCmd = randomActionCmd(FightCmd.ORDINARY_ATTACK, fightData, 0);
		}
		return actionCmd;
	}

	/** 战斗演播后台演播时间到 */
	ActionTimerListener actionPlayEnd = new ActionTimerListener() {
		
		@SuppressWarnings("unused")
		public void execute() {
			nextRound();
		}
		
	};
	
	/**
	 * 当前这场战斗结束        战斗房间回收
	 */
	private synchronized void fightOver() {
//		System.out.println("结束了!");
		sendData(SocketCmd.FIGHT_OVER);
		timerThread.clearTimer(waitOvertime);
		timerThread.clearTimer(actionPlayEnd);
		userDecisionMap.clear();
		int len = attackOrder.size();
		FightData fightData;
		UserInfo userInfo;
		int blood;
		int magic;
		int tempBlood = 0;
		for (int i = 0; i < len; i++) {
			fightData = attackOrder.get(i);
			userInfo = fightData.getUserInfo();
			userInfo.setCurrentFightId(0);// 将当前战斗场景id清空
			if (userInfo.getId() != 0) {// 如果不是  电脑
				if (!fullStatus) {// 如果不是  满值作战
					tempBlood = fightData.getBlood();
					if (userInfo.getIsAutoUseSupply() == 1) { // 检查是否要自动回血
						try {
							resoveryBlood(fightData);
							resoveryMagic(fightData);
						} catch (IOException e) {
							Logger.getLogger(getClass()).error("战斗结束  恢复血蓝报错", e);
						}
					}
					blood = fightData.getBlood();
					magic = fightData.getMagic();
					userInfo.getRole().setBlood(blood<=0?1:blood);
					userInfo.getRole().setMagic(magic<=0?1:magic);
					fightData.setBlood(tempBlood);
					// 如果是宠物
					if (userInfo.getRole().isPet()) {
						// 发送当前血量变化
						userInfo.sendPetHpMp(userInfo.getRole());
						fightServer.updatePet(userInfo);
					} else {
						// 发送当前血量变化
						userInfo.sendHpMp();
						fightServer.updateUserRole(userInfo);
					}
				}
				// 如果是宠物
				if (!userInfo.getRole().isPet()) {
					// 处理被捕抓的宠物
					fightData.dealCatch();
				}
			}
		}
		attackOrder.clear();
		if (len > 0) {
			
			fightServer.gcBox(this);
		}
	}

	private void resoveryMagic(FightData fightData) throws IOException {
		Role role = fightData.getUserInfo().getRole();
		int magic = fightData.getMagic();
		UseProp useProp = fightData.getUserInfo().getBackpack().getRecoveryMagicProp();
		boolean isContinue = false;
		if (useProp != null) {
			int needMagic = role.getMaxMagic()-magic;
			if (needMagic <= 0) {
				return;
			}
			int count = 0;
			if (useProp.getId() == 1054 || useProp.getId() == 1056 || useProp.getId() == 1058) {
				if(useProp.getUseCount() > needMagic) {
					fightData.setMagic(fightData.getMagic()+needMagic);
					useProp.setUseCount(useProp.getUseCount()-needMagic);
					GameOutput gameOutput = new GameOutput();
					gameOutput.writeInt(useProp.getIndex());
					useProp.writeData(gameOutput);
					fightData.getUserInfo().sendData(SocketCmd.UPDATE_PROP_INFO, gameOutput.toByteArray());
					gameOutput.close();
				} else {
					fightData.setMagic(fightData.getMagic()+useProp.getUseCount());
					useProp.setUseCount(0);
					count = 1;
					if (fightData.getMagic() < role.getMaxMagic()) {
						isContinue = true;
					}
				}
			} else {
				double value = (double)needMagic/useProp.getValue();
				count = (int) (Math.ceil(value)-useProp.getCount());
				if (count < 0) { // 此物品满足补充血量
					count = (int) Math.ceil(value);
				} else {
					// 数量不满足
					count = useProp.getCount();
					isContinue = true;
				}
				fightData.setMagic(fightData.getMagic()+useProp.getValue()*count);
			}
			if (fightData.getMagic() > fightData.getUserInfo().getRole().getMaxMagic()) {
				fightData.setMagic(fightData.getUserInfo().getRole().getMaxMagic());
			}
			if (count > 0) {
				fightData.getUserInfo().getBackpack().removePropIndexCount(useProp.getIndex(), count);
				//更新此物品数量  并通知客户端更新
				GameOutput gameOutput = new GameOutput();
				gameOutput.writeInt(1);
				gameOutput.writeInt(useProp.getIndex());
				gameOutput.writeInt(count);
				fightData.getUserInfo().sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
				gameOutput.close();
			}
			fightServer.updateBackpack(fightData.getUserInfo());
			if (isContinue) {
				resoveryMagic(fightData);
			}
		}
	}

	/**
	 * 查看是否有恢复血剂  有恢复
	 * @param userInfo
	 * @throws IOException 
	 */
	private void resoveryBlood(FightData fightData) throws IOException {
		Role role = fightData.getUserInfo().getRole();
		int blood = fightData.getBlood();
		UseProp useProp = fightData.getUserInfo().getBackpack().getRecoveryBloodProp();
		boolean isContinue = false; // 是否继续使用药剂
		if (useProp != null) {
			int needBlood = role.getMaxBlood()-blood;
			if (needBlood <= 0) {
				return;
			}
			int count = 0;
			if (useProp.getId() == 1053 || useProp.getId() == 1055 || useProp.getId() == 1057) {
				if(useProp.getUseCount() > needBlood) {
					fightData.setBlood(fightData.getBlood()+needBlood);
					useProp.setUseCount(useProp.getUseCount()-needBlood);
					GameOutput gameOutput = new GameOutput();
					gameOutput.writeInt(useProp.getIndex());
					useProp.writeData(gameOutput);
					fightData.getUserInfo().sendData(SocketCmd.UPDATE_PROP_INFO, gameOutput.toByteArray());
					gameOutput.close();
				} else {
					fightData.setBlood(fightData.getBlood()+useProp.getUseCount());
					useProp.setUseCount(0);
					count = 1;
					if (fightData.getBlood() < role.getMaxBlood()) {
						isContinue = true;
					}
				}
			} else {
				int needPropCount = (int) Math.ceil((double)needBlood/useProp.getValue());
//				System.out.println("此物品可回复血量="+prop.getValue()+", 当前残血量="+tempBlood);
//				System.out.println("此物品恢复满血需要数量="+value+", 此物品数量="+prop.getCount());
				count = needPropCount-useProp.getCount();
//				System.out.println("需要使用数量="+count);
				if (count <= 0) { // 此物品满足补充血量
					count = needPropCount;
				} else {
					// 数量不满足
					count = useProp.getCount();
					isContinue = true;
				}
//				System.out.println("确定需要使用数量="+count);
				fightData.setBlood(fightData.getBlood()+useProp.getValue()*count);
			}
			if (fightData.getBlood() > role.getMaxBlood()) {
				fightData.setBlood(role.getMaxBlood());
			}
			if (count > 0) {
				fightData.getUserInfo().getBackpack().removePropIndexCount(useProp.getIndex(), count);
				//更新此物品数量  并通知客户端更新
				GameOutput gameOutput = new GameOutput();
				gameOutput.writeInt(1);
				gameOutput.writeInt(useProp.getIndex());
				gameOutput.writeInt(count);
				fightData.getUserInfo().sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
				gameOutput.close();
			}
			fightServer.updateBackpack(fightData.getUserInfo());
			if (isContinue) {
				resoveryBlood(fightData);
			}
		}
	}

	/**
	 * 检测此战斗位置判断 是否被人保护
	 * @param robUserIndex
	 * @return
	 */
	private ActionCmd checkProtect(long robUserIndex) {
		Collection<ActionCmd> list = userDecisionMap.values();
		Iterator<ActionCmd> iterator = list.iterator();
		ActionCmd actionCmd;
		while(iterator.hasNext()){
			actionCmd = iterator.next();
			if(actionCmd.getAction() == FightCmd.PROTECT && actionCmd.getRobUserIndex() == robUserIndex) {
				return actionCmd;
			}
		}
		return null;
	}

	/**
	 * 系统随机选择一个存活攻击目标
	 * @param action 行动指令
	 * @param fightData 行动玩家信息
	 * @param propId 使用的id
	 * @return 要被作用玩家id
	 */
	private ActionCmd randomActionCmd(int action, FightData fightData, int propId) {
		long rouUserId;
		if (checkFightDataAttacker(fightData.getId())) {//如果当前人是攻击方
			rouUserId = robberyListRanderAlive().getId();
		} else {
			rouUserId = robListRanderAlive().getId();
		}
		return new ActionCmd(action, fightData.getId(), getFightDataIndex(rouUserId), propId);
	}

	/**
	 * 从攻击者中获取一个还存活的角色
	 * @return 
	 */
	private FightData robListRanderAlive() {
		List<FightData> aliveList = new ArrayList<>();
		for (int i = 0; i < robList.size(); i++) {
			if (robList.get(i) != null && !robList.get(i).isDeath()) {
				aliveList.add(robList.get(i));
			}
		}
		return aliveList.get(RandomUtils.nextInt(aliveList.size()));
	}
	
	/**
	 * 从被攻击者中获取一个还存活的角色
	 * @return 
	 */
	private FightData robberyListRanderAlive() {
		List<FightData> aliveList = new ArrayList<>();
		for (int i = 0; i < robberyList.size(); i++) {
			if (robberyList.get(i) !=null && !robberyList.get(i).isDeath()) {
				aliveList.add(robberyList.get(i));
			}
		}
		return aliveList.get(RandomUtils.nextInt(aliveList.size()));
	}

	/**
	 * 检查游戏是否结束
	 * @return
	 */
	private boolean checkGameEnd() {
		int count = 0;// 死亡人数
		for (int i = 0; i < robList.size(); i++) {
			if(robList.get(i) == null || robList.get(i).getBlood() <= 0 || robList.get(i).isQuiltCatch()) {
				count++;
			}
		}
		if (count == robList.size()) { // 全部死亡
			fightResult = 2;
			return true;
		}
		count = 0;// 死亡人数
		for (int i = 0; i < robberyList.size(); i++) {
			if(robberyList.get(i) == null || robberyList.get(i).getBlood() <= 0 || robberyList.get(i).isQuiltCatch()) {
				count++;
			}
		}
		if (count == robberyList.size()) {// 全部死亡
			fightResult = 1;
			return true;
		}
		return false;
	}

	/**
	 * 客户端通知演播结束了
	 * @param currentRound 当前回合
	 * @param userUid 结束玩家id
	 * @param petUid 玩家宠物UID
	 */
	public void fightPlayEnd(int currentRound, long userUid, long petUid) {
		if (attackOrder.size()>1 && wiatPeriod == 3) { // 游戏没有结束
			if (this.currentRound == currentRound) { // 是当前回合返回的结束报告
				if (userUid != 0 && !userFightPlayEndList.contains(userUid)) {
					userFightPlayEndList.add(userUid);
				}
				if (petUid != 0 && !userFightPlayEndList.contains(petUid)) {
					userFightPlayEndList.add(petUid);
				}
//				System.out.println("客户端发来播放结束!"+userFightPlayEndList.size()+","+(wiatLen-offlineCount()));
				if (userFightPlayEndList.size() >= (wiatLen-offlineCount()) ) {// 总人数-1个人  播放结束  那么就可以播放下一回合了
					nextRound();
				}
			}
		}
	}

	/**
	 * 用户重连
	 * @param userInfo
	 */
	public void reconnect(UserInfo userInfo) {
		GameOutput gameOutput = new GameOutput();
		try {
			// 通知此登录玩家  当前进行到的战斗情况
			writeTeamData(gameOutput); // 写入队伍信息
			userInfo.sendData(SocketCmd.FIGHT_RECONNECT_DATA, gameOutput.toByteArray());
			// 通知房间中的人   此人离线了
			List<Long> longs = new ArrayList<>();
			for (int i = 0; i < attackOrder.size(); i++) {
				if(attackOrder.get(i).getUserInfo().getId() == userInfo.getId()) {
					attackOrder.get(i).getUserInfo().getLogin().setSession(userInfo.getSession());
					longs.add(attackOrder.get(i).getId());
				}
			}
			int len = longs.size();
			gameOutput.reset().writeInt(len);
			for (int i = 0; i < len; i++) {
				gameOutput.writeLong(longs.get(i));
			}
			sendAllWiatDecision(SocketCmd.FIGHT_USER_RECONNECT, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("用户重连群发消息报错", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("用户重连群发消息关闭流报错", e);
				}
			}
		}
	}

	/**
	 * 此玩家离线了
	 * @param userInfo
	 */
	public void userOffline(UserInfo userInfo) {
		GameOutput gameOutput = new GameOutput();
		try {
			List<Long> longs = new ArrayList<>();
			for (int i = 0; i < attackOrder.size(); i++) {
				if(attackOrder.get(i).getUserInfo().getId() == userInfo.getId()) {
					attackOrder.get(i).getUserInfo().getLogin().setSession(null);
					longs.add(attackOrder.get(i).getId());
				}
			}
			int len = longs.size();
			gameOutput.writeInt(len);
			for (int i = 0; i < len; i++) {
				gameOutput.writeLong(longs.get(i));
			}
			sendAllWiatDecision(SocketCmd.FIGHT_USER_OFFLINE, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("玩家离线群发消息报错", e);
		} finally {
			 if (gameOutput != null) {
				 try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("此玩家离线了关闭流报错", e);
				}
			}
		}
	}
	
	/**
	 * 获取房间离线的人数
	 * @return
	 */
	public int offlineCount() {
		int count = 0;
		FightData fightData;
		UserInfo userInfo;
		for (int i = 0; i < attackOrder.size(); i++) {
			fightData = attackOrder.get(i);
			if (!fightData.isBrainAI()) {
				userInfo = fightData.getUserInfo();
				if (userInfo.getRole().isPet() && fightData.isDeath()) {
					count++;
				} else if (userInfo.getSession() ==null || !userInfo.getSession().isConnected()) {
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * 写入双方队伍信息
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void writeTeamData(GameOutput gameOutput) throws IOException {
		gameOutput.writeLong(this.getId());// 战斗房间id
		gameOutput.writeInt(currentRound);// 当前回合
		gameOutput.writeInt(fightType);// 战斗类型
		gameOutput.writeInt(fightTargetType);// 0和电脑打  1和玩家打
		gameOutput.writeInt(wiatPeriod);// 当前处于什么阶段   决定还是演播
		// 发送 敌方战斗数据
		int len = robberyList.size();
		gameOutput.writeInt(len);// 敌方队伍数量
		FightData fightData;
		for (int i = 0; i < len; i++) {
			fightData = robberyList.get(i);
			if (fightData != null) {
				fightData.writeData(gameOutput);
			} else {
				gameOutput.writeLong(0);
			}
		}
		len = robList.size();
		// 发送我方战斗数据
		gameOutput.writeInt(len);// 我方队伍数量
		for (int i = 0; i < len; i++) {
			fightData = robList.get(i);
			if (fightData != null) {
				fightData.writeData(gameOutput);
			} else {
				gameOutput.writeLong(0);
			}
		}
		if (wiatPeriod == 1) {
			gameOutput.writeInt(currentWaitOvertime);// 当前等待还剩下的秒数 
		} else if (wiatPeriod == 3) {
			writeRoundData(gameOutput);
		}
	}
	
	/**
	 * 通知此房间所有玩家数据
	 * @param attackOrder2
	 */
	private void sendAllWiatDecision(int action, Object ...arge) {
		GameOutput gameOutput = new GameOutput();
		try {
			gameOutput.write(arge);
			sendAllWiatDecision(action, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("通知此房间所有玩家数据报错!", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("通知此房间所有玩家数据关闭流报错!", e);
				}
			}
		}
	}
	
	/**
	 * 通知此房间所有玩家数据
	 * @param attackOrder2
	 */
	private void sendAllWiatDecision(int action, byte[] buf) {
		UserInfo userInfo;
		ActionData<Object> actionData = new ActionData<>(action, buf);
		for (int i = 0; i < attackOrder.size(); i++) {
			userInfo = attackOrder.get(i).getUserInfo();
			if (!userInfo.getRole().isPet()) {
				userInfo.sendData(actionData);
			}
		}
	}

	/**
	 * 写入演播数据
	 * @param gameOutput
	 * @throws IOException
	 */
	private void writeRoundData(GameOutput gameOutput) throws IOException {
		Round round;
		int len = rounds.size();
		gameOutput.writeInt(len);
		for (int i = 0; i < len; i++) {
			round = rounds.get(i);
			round.writeData(gameOutput);
		}
		gameOutput.writeInt(enemyShowHPCount);
		gameOutput.writeBoolean(isGameOver);
	}
	
	

	public int getCurrentRound() {
		return currentRound;
	}

	/**
	 * 获取战斗类型1pvp2rob3pk
	 * @return fightType 战斗类型 1pvp 2rob 3pk 4pve
	 */
	public int getFightType() {
		return fightType;
	}

	/**
	 * 设置战斗类型1pvp2rob3pk
	 * @param fightType 战斗类型 1pvp 2rob 3pk 4pve
	 */
	public void setFightType(int fightType) {
		this.fightType = fightType;
	}

	/**
	 * 获取当前的状态是否属于满状态开始的游戏
	 * @return fullStatus 当前的状态是否属于满状态开始的游戏
	 */
	public boolean isFullStatus() {
		return fullStatus;
	}

	/**
	 * 获取战斗结果0没有结果1进攻方胜利2防守方胜利
	 * @return fightResult 战斗结果0没有结果1进攻方胜利2防守方胜利
	 */
	public int getFightResult() {
		return fightResult;
	}

	/**
	 * 获取被攻击方队伍数据
	 * @return robberyList 被攻击方队伍数据
	 */
	public List<FightData> getRobberyList() {
		return robberyList;
	}

	/**
	 * 获取攻击方队伍数据
	 * @return robList 攻击方队伍数据
	 */
	public List<FightData> getRobList() {
		return robList;
	}
	
	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	
	public FightServer getFightServer() {
		return fightServer;
	}

	public void setFightServer(FightServer fightServer) {
		this.fightServer = fightServer;
	}

	@Override
	public void dispose() {
	}

	public void sendData(int action, Object ...args) {
		GameOutput gameOutput = new GameOutput();
		try {
			gameOutput.write(args);
			sendData(new ActionData<>(action, gameOutput.toByteArray()));
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("队伍发送数据报错!", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("队伍发送数据关闭流报错!", e);
				}
			}
		}
	}
	
	public void sendData(int action, byte[] buf) {
		ActionData<Object> actionData = new ActionData<>(action, buf);
		sendData(actionData);
	}
	
	/**
	 * 此用户向客户端发送消息
	 * @param action 发送指令
	 */
	public synchronized void sendData(ActionData<Object> action) {
		UserInfo userInfo;
		for (int i = 0; i < attackOrder.size(); i++) {
			if (attackOrder.get(i)!=null) {
				userInfo = attackOrder.get(i).getUserInfo();
				if (!userInfo.getRole().isPet()) {
					userInfo.sendData(action);
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	
	@Override
	public String toString() {
		return "FightBox="+super.toString();
	}

	/**
	 * 获取敌人显示血量剩余次数
	 * @return enemyShowHPCount 敌人显示血量剩余次数
	 */
	public int getEnemyShowHPCount() {
		return enemyShowHPCount;
	}

	/**
	 * 设置敌人显示血量剩余次数
	 * @param enemyShowHPCount 敌人显示血量剩余次数
	 */
	public void setEnemyShowHPCount(int enemyShowHPCount) {
		this.enemyShowHPCount = enemyShowHPCount;
	}

}
