package com.server;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.UserDataManage;
import com.entity.Friend;
import com.entity.UserInfo;
import com.net.CommonCmd;
import com.net.SocketCmd;

/**
 * 好友处理
 * 
 * @author xujinbo
 */
public class FriendServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer loginServer;

	/**
	 * 请求添加好友
	 * @param buf
	 * @param session
	 */
	public void addFriendRequest(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			String userName = gameInput.readUTF().trim();// 要加的人名字
			UserInfo userTwo = userDataManage.getUserInfoData(userName);
			if (userTwo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Friend friend = new Friend();
			friend.setFriendId(userInfo.getId());
			friend.setLevel(userInfo.getRole().getLevel());
			friend.setName(userInfo.getRoleName());
			// 已经存在添加好友请求
			if (userTwo.getAddFriends().contains(friend)) return;
			
			if (userTwo.getFriends().contains(friend) ) {
				gameOutput.writeInt(CommonCmd.FRIEND_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userTwo.getOnlineState() == 1) { // 如果此用户在线 那么就通知他
				friend.writeData(gameOutput);
				userTwo.sendData(SocketCmd.ADD_FRIEND_REQUEST, gameOutput.toByteArray());
			}
			loginServer.updateAddFriendByte(userTwo, friend);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("好友处理报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("好友处理关闭流报错", e);
			}
		}
	}

	/**
	 * 加好友用户处理结果
	 * 
	 * @param buf
	 * @param session
	 */
	public void addFriendResult(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}

			int result = gameInput.readInt();// 加好友返回的 决定
			Friend friend = null;
			UserInfo userTwo;
			if (result < 3) {
				int userId = gameInput.readInt();// 同意还是拒绝人的名字
				userTwo = userDataManage.getUserInfoData(userId);
				if (userTwo == null) {
					gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				friend = userInfo.removeAddFriend(userId);
				if (friend == null) {
					gameOutput.writeInt(CommonCmd.ADD_FRIEND_NOT_EXISTENT);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				loginServer.updateAddFriendByte(userInfo);
				switch (result) {
					case 1:// 同意
						friend.setName(userTwo.getRoleName());
						friend.setSex(userTwo.getRole().getSex());
						friend.setMood(userTwo.getMood());
						friend.setOnlineState(userTwo.getOnlineState());
						loginServer.updateFriendByte(userInfo, friend);
						friend.writeData(gameOutput);
						userInfo.sendData(SocketCmd.ADD_FRIEND, gameOutput.toByteArray());
						
						gameOutput.reset();
						friend = new Friend();
						friend.setFriendId(userInfo.getId());
						friend.setLevel(userInfo.getRole().getLevel());
						friend.setName(userInfo.getRoleName());
						friend.setSex(userInfo.getRole().getSex());
						friend.setMood(userInfo.getMood());
						friend.setOnlineState(userInfo.getOnlineState());
						loginServer.updateFriendByte(userTwo, friend);
						if (userTwo.getOnlineState() == 1) { // 如果此用户在线 那么就通知他
							friend.writeData(gameOutput);
							userTwo.sendData(SocketCmd.ADD_FRIEND, gameOutput.toByteArray());
						}
						break;
					case 2:// 拒绝
						if (userTwo.getOnlineState() == 1) { // 如果此用户在线 那么就通知他
							gameOutput.writeUTF(userInfo.getRoleName());
							userTwo.sendData(SocketCmd.ADD_FRIEND_RESULT, gameOutput.toByteArray());
						}
						break;
				}

			}
			
			switch (result) {
				case 3://全部同意
					List<Friend> addFriends = userInfo.getAddFriends();
					loginServer.updateFriendByte(userInfo, addFriends);// 更新到数据库
					while(addFriends.size() > 0){
						friend = addFriends.remove(0);
						userTwo = userDataManage.getUserInfoData(friend.getFriendId());
						friend.setName(userTwo.getRoleName());
						friend.setSex(userTwo.getRole().getSex());
						friend.setMood(userTwo.getMood());
						friend.setOnlineState(userTwo.getOnlineState());
						gameOutput.reset();
						friend.writeData(gameOutput);
						userInfo.sendData(SocketCmd.ADD_FRIEND, gameOutput.toByteArray());// 通知添加好友
						
						if (userTwo != null && userTwo.getOnlineState() == 1) { // 如果此用户在线 那么就通知他
							gameOutput.reset();
							friend = new Friend();
							friend.setFriendId(userInfo.getId());
							friend.setLevel(userInfo.getRole().getLevel());
							friend.setName(userInfo.getRoleName());
							friend.setSex(userInfo.getRole().getSex());
							friend.setMood(userInfo.getMood());
							friend.setOnlineState(userInfo.getOnlineState());
							loginServer.updateFriendByte(userTwo, friend);// 更新到数据库
							friend.writeData(gameOutput);
							userTwo.sendData(SocketCmd.ADD_FRIEND, gameOutput.toByteArray());
						}
					}
					loginServer.updateAddFriendByte(userInfo);
					break;
				case 4://全部拒绝
					addFriends = userInfo.getAddFriends();
					while(addFriends.size() > 0){
						friend = addFriends.remove(0);
						userTwo = userDataManage.getCacheUserInfo(friend.getFriendId());
						if (userTwo != null && userTwo.getOnlineState() == 1) { // 如果此用户在线 那么就通知他
							gameOutput.reset().writeUTF(userInfo.getRoleName());
							userTwo.sendData(SocketCmd.ADD_FRIEND_RESULT, gameOutput.toByteArray());
						}
					}
					loginServer.updateAddFriendByte(userInfo);
					break;
			}

		} catch (IOException e) {
			Logger.getLogger(getClass()).error("好友处理报错", e);
		} finally {
			try {
				if (gameInput != null)
					gameInput.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("好友处理关闭流报错", e);
			}
		}
	}

	/**
	 * 删除好友
	 * @param buf
	 * @param session
	 */
	public void deleteFriend(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long friendId = gameInput.readInt();// 要删除人的id
			Friend friend = userInfo.removeFriend(friendId);
			if (friend == null) {
				gameOutput.writeInt(CommonCmd.FRIEND_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			loginServer.updateFriendByte(userInfo);
			gameOutput.writeLong(friend.getFriendId());
			userInfo.sendData(SocketCmd.DELETE_FRIEND, gameOutput.toByteArray());
			
			
			UserInfo userTwo = userDataManage.getUserInfoData(friend.getFriendId());
			if (userTwo == null) {
				return;
			}
			userTwo.removeFriend(userInfo.getId());
			loginServer.updateFriendByte(userTwo);
			if (userTwo.getOnlineState() == 1) {
				gameOutput.reset().writeLong(userInfo.getId());
				userTwo.sendData(SocketCmd.DELETE_FRIEND, gameOutput.toByteArray());
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("删除好友报错", e);
		} finally {
			try {
				if (gameInput != null) gameInput.close();
				if (gameOutput != null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("删除好友关闭流报错", e);
			}
		}
	}

}
