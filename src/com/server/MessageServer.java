package com.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.UserDataManage;
import com.entity.Faction;
import com.entity.FightData;
import com.entity.Friend;
import com.entity.Prop;
import com.entity.SpaceDate;
import com.entity.Team;
import com.entity.UseProp;
import com.entity.UserInfo;
import com.model.Backpack;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.server.fight.FightBox;
import com.thread.SendDataThread;
import com.utils.CommonUtils;
import com.utils.LuaUtils;

/**
 * 聊天信息处理
 * @author xujinbo
 *
 */
public class MessageServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private SendDataThread sendDataThread;
	@Resource
	private TeamServer teamServer;
	@Resource
	private FactionServer factionServer;
	@Resource
	private FightServer fightServer;
	@Resource
	private UserServer userServer;
	@Resource
	private BattlefieldServer battlefieldServer;
	
	/** 缓存的聊天记录 */
	private List<Object[]> contents = new ArrayList<>();
	
	// 0世界 1帮派 2队伍 3喇叭 4谣言 5杂项 6系统 
	
	/**
	 * 添加世界聊天消息
	 * @param buf
	 * @param session
	 */
	public void addChatInfo(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			SpaceDate spaceDate = userInfo.getSpaceDate();
			if (!spaceDate.checkType(SpaceDate.SEND_ALL_CHAT)) { // 距离上次发送消息  小于3s
				return;
			}
			spaceDate.setValue(SpaceDate.SEND_ALL_CHAT, (int) (System.currentTimeMillis()/1000));
			
			int channel = gameInput.readInt();// 频道
			int type = gameInput.readInt(); // 类型   0发送普通聊天内容   1展示物品
			String content = gameInput.readUTF().trim();// 内容
			if (content.length()==0 || content.length() > 80) {
				return;
			}
			if (channel == 1) {//帮派
				if (userInfo.getFactionId() == 0) {
					return;
				}
				Faction faction = factionServer.getFaction(userInfo.getFactionId());
				if (faction == null) {
					return;
				}
				sendChatContent(userInfo, type, channel, content, faction.getMemberList());
				return;
			}
			if (channel == 2) {//队伍
				if (userInfo.getTeamId() == 0) {
					return;
				}
				Team team = teamServer.getTeam(userInfo.getTeamId());
				if (team == null) {
					return;
				}
				sendChatContent(userInfo, type, channel, content, team.getUserList());
				return;
			}
			if (channel == 3) {//喇叭
				Backpack backpack = userInfo.getBackpack();
				UseProp prop = (UseProp) backpack.getPropId(LuaUtils.get("laba").tolong());
				if (prop == null) {
					gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				backpack.removePropIndexCount(prop.getIndex(), 1);
				userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 11, prop.getName(), 1);
				gameOutput.reset().write(1, prop.getIndex(), 1);
				userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
			}
			if (channel == -1) {//附近
				if (userInfo.getCurrentScene() != CommonCmd.SCENE_GAME) {
					return;
				}
				sendChatContent(userInfo, type, channel, content, 
						battlefieldServer.getMapUserList(userInfo.getCurrentSceneValue(), userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY()));
				return;
			}
			sendChatContent(userInfo, type, channel, content);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("发送聊天报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("发送聊天关闭流报错", e);
			}
		}
	}

	/**
	 * 发送聊天消息
	 * @param userInfo 发送信息的角色
	 * @param type 类型
	 * @param channel 频道     0世界 1帮派 2队伍 3喇叭 4谣言 5杂项 6系统 
	 * @param content 内容
	 * @param userList 指定发送对象
	 */
	private void sendChatContent(UserInfo userInfo, int type, int channel, String content, List<UserInfo> userList) {
		if (userList==null || userList.isEmpty()) 
			return;
		
		GameOutput gameOutput = new GameOutput();
		try {
			gameOutput.writeUTF(userInfo.getRoleName());
			gameOutput.writeInt(type);
			gameOutput.writeInt(channel);
			gameOutput.writeUTF(content);
			if (channel == -1) {
				gameOutput.writeLong(userInfo.getId());
			}
			ActionData<Object> action = new ActionData<>(SocketCmd.ADD_CHAT_INFO, gameOutput.toByteArray());
			for (int i = 0; i < userList.size(); i++) {
				sendDataThread.addActionData(action, userList.get(i));
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("发送聊天报错", e);
		} finally {
			try {
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("发送聊天关闭流报错", e);
			}
		}
	}

	/**
	 * 发送聊天消息
	 * @param roleName 名字
	 * @param type 类型
	 * @param channel 频道     0世界 1帮派 2队伍 3喇叭 4谣言 5杂项 6系统 
	 * @param content 内容
	 */
	public void sendChatContent(UserInfo userInfo, int type, int channel, String content) {
		Prop prop = null;
		if (type == 1) {
			prop = userInfo.getBackpack().getPropIndex(Integer.parseInt(content));
			if (prop == null) {
				return;
			}
		}
		GameOutput gameOutput = new GameOutput();
		try {
			if (channel == 0) {
//				contents.add(Arrays.asList(roleName, channel, content).toArray(new Object[0]));
			}
			gameOutput.writeUTF(userInfo.getRoleName());
			gameOutput.writeInt(type);
			gameOutput.writeInt(channel);
			gameOutput.writeUTF(content);
			if (type == 1) {
				prop.writeData(gameOutput);
			}
			ActionData<Object> actionData = new ActionData<>(SocketCmd.ADD_CHAT_INFO, gameOutput.toByteArray());
			sendDataThread.addActionData(actionData);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("发送聊天报错", e);
		} finally {
			try {
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("发送聊天关闭流报错", e);
			}
		}
	}

	/**
	 * 给此用户发送当前缓存的的世界消息
	 * @param userInfo
	 */
	public void sendLoginChat(UserInfo userInfo) {
		GameOutput gameOutput = new GameOutput();
		Object[] obj;
		try {
			for (int i = 0; i < contents.size(); i++) {
				obj = contents.get(i);
				if (obj.length == 3) {
					gameOutput.reset();
					gameOutput.writeUTF((String) obj[0]);
					gameOutput.writeInt((int) obj[1]);
					gameOutput.writeUTF((String) obj[2]);
					userInfo.sendData(SocketCmd.ADD_CHAT_INFO, gameOutput.toByteArray());
				}
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("登录发送世界消息报错", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("登录发送世界消息关闭流报错", e);
				}
			}
		}
	}

	/**
	 * 发送好友消息
	 * @param buf
	 * @param session
	 */
	public void sendFriendChat(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int friendId = gameInput.readInt();// 好友id
			String content = gameInput.readUTF().trim();// 内容
			Friend friend = userInfo.getFriend(friendId);
			if (content.length()==0 || content.length() > 80) {
				return;
			}
			// 如果是好友
			if (friend != null) {
				// TODO 如果是好友  添加亲密度
//				gameOutput.writeInt(CommonCmd.FRIEND_NOT_EXISTENT);
//				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
//				return;
			}
			UserInfo userTwo = userDataManage.getCacheUserInfo(friendId);
			if (userTwo == null || userTwo.getOnlineState() == 0) {
				gameOutput.writeInt(CommonCmd.USER_OFFLINE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			friend = new Friend();
			friend.setValue(userInfo);
			friend.writeData(gameOutput);
			gameOutput.writeUTF(content);
			userTwo.sendData(SocketCmd.SEND_FRIEND_CHAT, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("发送好友消息报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("发送好友消息关闭流报错", e);
			}
		}
	}

	/**
	 * 切磋
	 * @param buf
	 * @param session
	 */
	public void fightPk(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			String userName = gameInput.readUTF().trim();//切磋对象名字
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			Team team = teamServer.getTeam(userInfo.getTeamId());
			if (team != null) {
				// 如果此人不是队长
				if (team.getUserList().get(0).getId() != userInfo.getId()) {
					gameOutput.writeInt(CommonCmd.TEAM_NOT_LEADER);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
			}
			
			UserInfo userTwo = userDataManage.getCacheUserInfo(userName);
			if (userTwo == null || userTwo.getOnlineState() == 0) {
				gameOutput.writeInt(CommonCmd.USER_OFFLINE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Team beiTeam = teamServer.getTeam(userTwo.getTeamId());
			if (beiTeam != null) {
				userTwo = beiTeam.getUserList().get(0);
			}
			if (!userTwo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			gameOutput.writeUTF(userInfo.getRoleName());
			gameOutput.writeInt(userInfo.getRole().getLevel());
			userTwo.sendData(SocketCmd.FIGHT_PK, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("切磋报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("切磋关闭流报错", e);
			}
		}
	}

	/**
	 * 切磋决定
	 * @param buf
	 * @param session
	 */
	public void fightPkDecide(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			String userName = gameInput.readUTF().trim();//切磋对象名字
			boolean decide = gameInput.readBoolean();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Team team = teamServer.getTeam(userInfo.getTeamId());
			if (team != null) {
				// 如果此人不是队长
				if (team.getUserList().get(0).getId() != userInfo.getId()) {
					gameOutput.writeInt(CommonCmd.TEAM_NOT_LEADER);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
			}
			UserInfo userTwo = userDataManage.getCacheUserInfo(userName);
			if (userTwo == null || userTwo.getOnlineState() == 0) {
				return;
			}
			if (decide) {
				if (!userTwo.isIdle()) {
					gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				Team beiTeam = teamServer.getTeam(userTwo.getTeamId());
				if (beiTeam != null) {
					userTwo = beiTeam.getUserList().get(0);
				}
				List<FightData> robList;
				if (beiTeam != null) {
					robList = beiTeam.getFightData(false);
				} else {
					robList = CommonUtils.getFightData(userTwo, false);
				}
				List<FightData> robberyList;
				if (team != null) {
					robberyList = team.getFightData(false);
				} else {
					robberyList = CommonUtils.getFightData(userInfo, false);
				}
				
				FightBox fightBox = fightServer.createFight(robList, robberyList, true);
				fightBox.setFightType(CommonCmd.PK);
				try {
					fightBox.writeTeamData(gameOutput);
					fightBox.sendData(SocketCmd.FIGHT_DATA, gameOutput.toByteArray());
					fightServer.startFight(fightBox.getId());
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("创建一场战斗报错", e);
				} finally {
					try {
						if (gameOutput != null) gameOutput.close();
					} catch (IOException e) {
						Logger.getLogger(getClass()).error("创建一场战斗关闭流报错", e);
					}
				}
			} else {
				gameOutput.writeUTF(userInfo.getRoleName());
				userTwo.sendData(SocketCmd.FIGHT_PK_DECIDE, gameOutput.toByteArray());
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("切磋决定报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("切磋决定关闭流报错", e);
			}
		}
	}
	
	public void fightEnd(FightBox fightBox) {
		
		
		
	}

}
