package com.server;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.MapConfig;
import com.data.UserDataManage;
import com.entity.MapData;
import com.entity.SpaceDate;
import com.entity.Team;
import com.entity.UserInfo;
import com.model.GameData;
import com.model.Grid;
import com.model.MapModel;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.LuaUtils;
import com.utils.OnlyIdManage;

/**
 * 队伍处理
 * @author xujinbo
 *
 */
public class TeamServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	@Resource
	private MessageServer messageServer;
	@Resource
	private MapConfig grindConfig;
	@Resource
	private MapModel mapModel;
	@Resource
	private MapServer mapServer;
	@Resource
	private MapConfig mapConfig;
	
	/** id管理器 */
	private OnlyIdManage idManage;
	/** 已经建立的队伍   地图  此地图所拥有的队伍 */
	private List<Team> teamList = new ArrayList<>();
	
	public TeamServer() {
		idManage = new OnlyIdManage();
	}
	
	/**
	 * 根据队伍id获取队伍
	 * @param mapId 地图id
	 * @param teamId 队伍id
	 * @return
	 */
	public Team getTeam(long teamId) {
		for (int i = 0; i < teamList.size(); i++) {
			if (teamList.get(i).getId() == teamId) {
				return teamList.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 创建一个队伍
	 * @param buf
	 * @param session
	 */
	public void teamCreate(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			createTeam(userInfo, true);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("创建一个队伍报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("创建一个队伍关闭流报错", e);
			}
		}
	}

	/**
	 * 创建一个队伍
	 * @param userInfo
	 * @param isSend 是否发送通知
	 * @return 
	 */
	public synchronized Team createTeam(UserInfo userInfo, boolean isSend) {
		GameOutput gameOutput = new GameOutput();
		try {
			if (userInfo.getTeamId() != 0) {
				gameOutput.writeInt(CommonCmd.EXISTENT_TEAM);
				userInfo.sendData(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray());
				return null;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				userInfo.sendData(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray());
				return null;
			}
			Team team = new Team();
			team.setId(idManage.getGrowableId());
			team.getUserList().add(userInfo);
			userInfo.setTeamId(team.getId());
			teamList.add(team);
			if (isSend) {
				gameOutput.writeLong(team.getId());
				team.writeData(gameOutput);
				userInfo.sendData(SocketCmd.TEAM_CREATE, gameOutput.toByteArray());
			}
			return team;
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("创建一个队伍报错", e);
		} finally {
			try {
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("创建一个队伍关闭流报错", e);
			}
		}
		return null;
	}
	
	/**
	 * 解散一个队伍
	 * @param buf
	 * @param session
	 */
	public void teamDisband(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int teamId = input.readInt();
			Team team = getTeam(teamId);
			if (userInfo.getTeamId() == 0 || team == null) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 如果此人不是队长
			if (team.getUserList().get(0).getId() != userInfo.getId()) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_LEADER);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 删除队伍
			removeTeam(teamId);
			List<UserInfo> userList = team.getUserList();
			int len = userList.size();
			for (int i = 0; i < len; i++) {
				userList.get(i).setTeamId(0);
			}
			team.sendData(SocketCmd.TEAM_DISBAND);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("解散一个队伍报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("解散一个队伍关闭流报错", e);
			}
		}
	}

	/**
	 * 组队申请决定 
	 * @param buf
	 * @param session
	 */
	public void teamApplyDecide(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int teamId = input.readInt();
			boolean decide = input.readBoolean();// 决定
			long applyId = input.readInt();// 申请人id
			// 发送要删除的那条组队申请
			userInfo.sendData(SocketCmd.TEAM_APPLY_DELETE, applyId);
			Team team = getTeam(teamId);
			if (userInfo.getTeamId() == 0 || team == null) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 如果此人不是队长
			if (!team.isLeader(userInfo)) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_LEADER);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查队伍人数是否上限
			if (team.getUserList().size() >= 5) {
				gameOutput.writeInt(CommonCmd.TEAM_FULL);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			UserInfo userTwo = userDataManage.getCacheUserInfo(applyId);
			// 此玩家信息存在  在线   未组队 
			if (userTwo == null || userTwo.getOnlineState() != 1 || userTwo.getTeamId() != 0) {
				gameOutput.writeInt(CommonCmd.TEAM_APPLY_OUT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 决定
			if (decide) {
				if (!userTwo.isIdle()) {
					gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				// 判断此人是否已经在队伍中
				if (team.checkExist(userTwo)) {
					gameOutput.writeInt(CommonCmd.EXISTENT_TEAM);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				userTwo.setTeamId(team.getId());
				team.getUserList().add(userTwo);
				// 发送新成员
				team.writeUser(gameOutput, userTwo);
				team.sendData(SocketCmd.TEAM_ADD_ROLE, gameOutput.toByteArray());
				// 发送玩家进入指定地图
				enterMap(userTwo, userInfo);
				
				// 将队伍信息写入
				gameOutput.reset().writeLong(team.getId());
				team.writeData(gameOutput);
				// 发送玩家队伍信息
				userTwo.sendData(SocketCmd.TEAM_ROLE_DATA, gameOutput.toByteArray());
			} else {
				userTwo.sendData(SocketCmd.TEAM_APPLY_DECIDE, 0, userInfo.getRoleName());
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("组队申请决定 报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("组队申请决定 关闭流报错", e);
			}
		}
	}
		/**
	 * 队伍删除成员
	 * @param buf
	 * @param session
	 */
	public void teamDeleteRole(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long teamId = input.readInt();
			Team team = getTeam(teamId);
			if (userInfo.getTeamId() == 0 || team == null) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			removeTeamUser(userInfo, team);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("队伍删除成员报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("队伍删除成员关闭流报错", e);
			}
		}
	}

	/**
	 * 组队申请
	 * @param buf
	 * @param session
	 */
	public void teamApply(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getTeamId() != 0) {
				gameOutput.writeInt(CommonCmd.EXISTENT_TEAM);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int teamId = input.readInt();
			Team team = getTeam(teamId);
			if (team == null) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查队伍人数是否上限
			if (team.getUserList().size() >= 5) {
				gameOutput.writeInt(CommonCmd.TEAM_FULL);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查队伍是否处于战斗中
			if (team.isFight()) {
				gameOutput.writeInt(CommonCmd.THIS_AT_FIGHT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查此玩家是否已经在此队伍
			if (team.checkExist(userInfo)) {
				gameOutput.writeInt(CommonCmd.EXISTENT_TEAM);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 写入申请人信息
			gameOutput.writeLong(userInfo.getId());
			gameOutput.writeInt(userInfo.getRole().getLevel());
			gameOutput.writeUTF(userInfo.getRoleName());
			team.getUserList().get(0).sendData(SocketCmd.TEAM_APPLY, gameOutput.toByteArray());
			
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("组队申请报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("组队申请关闭流报错", e);
			}
		}
	}

	/**
	 * 快速组队
	 * @param buf
	 * @param session
	 */
	public void teamFast(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getTeamId() != 0) {
				gameOutput.writeInt(CommonCmd.EXISTENT_TEAM);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int len = teamList.size();
			if (len > 0) {
				List<Team> teams = getTeamRandom(5);
				len = teams.size();
				if (len > 0) {
					// 写入申请人信息
					gameOutput.writeLong(userInfo.getId());
					gameOutput.writeInt(userInfo.getRole().getLevel());
					gameOutput.writeUTF(userInfo.getRoleName());
					Team team;
					for (int i = 0; i < len; i++) {
						team = teams.get(i);
						// 新队伍人数还有空位     不再战斗中   直接不再此队伍中
						if (team.getUserList().size() < 5 && !team.isFight() && !team.checkExist(userInfo)) {
							team.getUserList().get(0).sendData(SocketCmd.TEAM_APPLY, gameOutput.toByteArray());
						}
					}
				}
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("快速组队报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("快速组队关闭流报错", e);
			}
		}
	}
	
	/** 随机获取指定数量的  未满成员未战斗的队伍 
	 * @return */
	private List<Team> getTeamRandom(int n) {
		List<Team> list = new ArrayList<>();
		int len = teamList.size();
		for (int i = 0; i < len; i++) {
			if (list.size() >= n) {
				break;
			}
			if (teamList.get(i).getUserList().size() < 5 && !teamList.get(i).isFight()) {
				list.add(teamList.get(i));
			}
		}
		return list;
	}

	/**
	 * 一键邀请
	 * @param buf
	 * @param session
	 */
	public void teamFastInvite(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			SpaceDate spaceDate = userInfo.getSpaceDate();
			if (!spaceDate.checkType(SpaceDate.FAST_INVITE)) {// 距离上次整理包裹  小于10s
				gameOutput.writeInt(CommonCmd.ACTION_COOLING);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			spaceDate.setValue(SpaceDate.FAST_INVITE, (int) (System.currentTimeMillis()/1000));
			// 如果还没有队伍
			if (userInfo.getTeamId() == 0) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long teamId = input.readInt();// 队伍id
			Team team = getTeam(teamId);
			if (team == null) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 如果此人不是队长
			if (team.getUserList().get(0).getId() != userInfo.getId()) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_LEADER);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查队伍人数是否上限
			if (team.getUserList().size() >= 5) {
				gameOutput.writeInt(CommonCmd.TEAM_FULL);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			StringBuffer content = new StringBuffer("求组");
			content.append("组队啦!有意者请<a href='event:T@"+teamId+"|"+userInfo.getRoleName()+"'><font color='#00FF00'><u>邀请组队</u></font></a>");
			messageServer.sendChatContent(userInfo, 0, 2, content.toString());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("快速组队报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("快速组队关闭流报错", e);
			}
		}
	}
	
	/**
	 * 邀请入队
	 * @param buf
	 * @param session
	 */
	public void teamInvite(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long mapId = input.readInt();// 地图id
			long teamId = input.readInt();// 队伍id
			long userId = input.readInt();// 被邀请的玩家id
			// 如果还没有队伍
			Team team = getTeam(teamId);
			if (team == null) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 如果此人不是队长
			if (team.getUserList().get(0).getId() != userInfo.getId()) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_LEADER);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查队伍人数是否上限
			if (team.getUserList().size() >= 5) {
				gameOutput.writeInt(CommonCmd.TEAM_FULL);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			UserInfo userTwo = userDataManage.getCacheUserInfo(userId);
			if (userTwo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userTwo.isIdle()) {
				gameOutput.reset().writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 写入申请人信息
			gameOutput.writeLong(userInfo.getId());
			gameOutput.writeInt(userInfo.getRole().getLevel());
			gameOutput.writeUTF(userInfo.getRoleName());
			gameOutput.writeLong(mapId);
			gameOutput.writeLong(teamId);
			userTwo.sendData(SocketCmd.TEAM_INVITE, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("快速组队报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("快速组队关闭流报错", e);
			}
		}
	}
	
	/**
	 * 邀请决定
	 * @param buf
	 * @param session
	 */
	public void teamInviteDecide(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int teamId = input.readInt();
			boolean decide = input.readBoolean();// 决定
			long applyId = input.readInt();// 申请人id
			
			UserInfo userTwo = userDataManage.getCacheUserInfo(applyId);
			if (userTwo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userTwo.isIdle()) {
				gameOutput.writeInt(CommonCmd.TEAM_APPLY_OUT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 发送要删除的那条组队申请
			userInfo.sendData(SocketCmd.TEAM_INVITE_DELETE, applyId);
			Team team = getTeam(teamId);
			if (team == null) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 检查队伍人数是否上限
			if (team.getUserList().size() >= 5) {
				gameOutput.writeInt(CommonCmd.TEAM_FULL);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 判断此人是否已经在队伍中
			if (team.checkExist(userInfo)) {
				gameOutput.writeInt(CommonCmd.EXISTENT_TEAM);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 此玩家未组队 
			if (userInfo.getTeamId() != 0) {
				gameOutput.writeInt(CommonCmd.TEAM_APPLY_OUT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 决定
			if (decide) {
				if (!userInfo.isIdle()) {
					gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
				userInfo.setTeamId(team.getId());
				team.getUserList().add(userInfo);
				// 发送新成员
				team.writeUser(gameOutput, userInfo);
				team.sendData(SocketCmd.TEAM_ADD_ROLE, gameOutput.toByteArray());
				gameOutput.reset();
				// 将队伍信息写入
				gameOutput.writeLong(team.getId());
				team.writeData(gameOutput);
				
				// 发送玩家进入指定地图
				enterMap(userInfo, userTwo);
				
				// 发送玩家队伍信息
				userInfo.sendData(SocketCmd.TEAM_ROLE_DATA, gameOutput.toByteArray());
			} else {
				userTwo.sendData(SocketCmd.TEAM_INVITE_DECIDE, 0, userInfo.getRoleName());
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("组队申请决定 报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("组队申请决定 关闭流报错", e);
			}
		}
	}
	
	/** 队伍踢出成员 */
	public void teamKickout(byte[] buf, IoSession session) {
		GameInput input = new GameInput(buf);
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int teamId = input.readInt();
			int applyId = input.readInt();
			UserInfo userTwo = userDataManage.getCacheUserInfo(applyId);
			if (userTwo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Team team = getTeam(teamId);
			if (team == null || !team.checkExist(userInfo)) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!team.checkExist(userTwo)) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 如果此人不是队长
			if (team.getUserList().get(0).getId() != userInfo.getId()) {
				gameOutput.writeInt(CommonCmd.TEAM_NOT_LEADER);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			removeTeamUser(userTwo, team);
			userTwo.sendData(SocketCmd.TEAM_KICKOUT);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("组队申请决定 报错", e);
		} finally {
			try {
				if (input != null)
					input.close();
				if (gameOutput != null)
					gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("组队申请决定 关闭流报错", e);
			}
		}
	}
	
	/**
	 * 进入界面
	 * @param userInfo
	 * @param userInfo2 队长
	 */
	private void enterMap(UserInfo userInfo, UserInfo userInfo2) {
		if (userInfo.getCurrentScene() == CommonCmd.SCENE_GAME && userInfo.getCurrentSceneValue()!=LuaUtils.get("mainMapId").tolong()) {
			MapData mapData = mapModel.getMapData(userInfo.getCurrentSceneValue());
			if (mapData != null) {
				Grid grid = mapData.getMapGridData().getGrid(userInfo2.getGridId());
				if (userInfo.getCurrentScene() == CommonCmd.SCENE_GAME && 
						userInfo.getCurrentSceneValue() == userInfo2.getCurrentSceneValue()) {
					if (Point.distance(userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY(), 
							userInfo2.getCurrentSceneStartX(), userInfo2.getCurrentSceneStartY()) > 200) {
						
						Point point = grid.randomPointRound(userInfo2.getCurrentSceneStartX(), userInfo2.getCurrentSceneStartY());
						userInfo.setCurrentSceneStartX(point.x);
						userInfo.setCurrentSceneStartY(point.y);
						userInfo.sendData(SocketCmd.MAP_POINT_CHANGE, 
								userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY()  );
						mapServer.sendMapAddUser(userInfo, mapData);
						if (mapData.getId() != LuaUtils.get("mainMapId").tolong()) {// 不是主界面
							mapData.addUser(userInfo);
						} else {
							userInfo.setGridId(mapData.getMapGridData().getUserGrid(userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY()));
						}
					}
				} else {
					// 更改此玩家的场景信息
					GameData.gameData.setupCurrentScene(userInfo, userInfo2);
					Point point = grid.randomPointRound(userInfo2.getCurrentSceneStartX(), userInfo2.getCurrentSceneStartY());
					userInfo.setCurrentSceneStartX(point.x);
					userInfo.setCurrentSceneStartY(point.y);
					userInfo.sendData(SocketCmd.MAP_POINT_CHANGE, 
							userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY()  );
					mapServer.sendMapAddUser(userInfo, mapData);
					if (mapData.getId() != LuaUtils.get("mainMapId").tolong()) {// 不是主界面
						mapData.addUser(userInfo);
					} else {
						userInfo.setGridId(mapData.getMapGridData().getUserGrid(userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY()));
					}
				}
			}
		} else {
			// 更改此玩家的场景信息
			GameData.gameData.setupCurrentScene(userInfo, userInfo2);
		}
		userInfo.setBattleMove(false);
		
		userServer.updateCurrentScene(userInfo);
		userInfo.sendData(SocketCmd.USER_ENTER_SCENE, 
				userInfo.getCurrentScene(), 
				userInfo.getCurrentSceneValue(), 
				userInfo.getCurrentSceneStartX(), 
				userInfo.getCurrentSceneStartY());
	}
	
	/**
	 * 队伍中删除一个角色
	 * @param userInfo 要被删除的角色
	 * @param team 队伍
	 * @param mapId 地图
	 */
	public void removeTeamUser(UserInfo userInfo, Team team) {
		userInfo.setTeamId(0);
		// 从此队伍中删除此人
		team.removeUser(userInfo);
		// 如果队伍中没人了   删除队伍
		if (team.getUserList().size() == 0) {
			removeTeam(team.getId());
		} else {
			// 通知此队伍中的人  有人离开
			team.sendData(SocketCmd.TEAM_DELETE_ROLE, userInfo.getId());
		}
		userInfo.sendData(SocketCmd.TEAM_DISBAND);
	}
	
	/**
	 * 删除指定队伍
	 * @param mapId 地图
	 * @param teamId 队伍id
	 */
	public void removeTeam(long teamId) {
		int len = teamList.size();
		Team team;
		for (int i = len-1; i >= 0; i--) {
			team = teamList.get(i);
			if (team.getId() == teamId) {
				teamList.remove(i);
				team.setId(0);
				idManage.gc(teamId);
				break;
			}
		}
	}


	/**
	 * 用户离线
	 * @param session
	 */
	public void userOffline(UserInfo userInfo) {
		if (userInfo != null) {
			if (userInfo.getTeamId() != 0) {
				Team team = getTeam(userInfo.getTeamId());
				// 队伍存在  并且  没有发生战斗
				if (team != null && !team.isFight() && team.checkExist(userInfo)) {
					removeTeamUser(userInfo, team);
				}
			}
		}
	}

	/**
	 * 获取已经建立的队伍地图此地图所拥有的队伍
	 * @return teamList 已经建立的队伍地图此地图所拥有的队伍
	 */
	public List<Team> getTeamList() {
		return teamList;
	}

}
