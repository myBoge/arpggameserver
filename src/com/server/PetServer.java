package com.server;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.UserDataManage;
import com.entity.Prop;
import com.entity.Role;
import com.entity.UseProp;
import com.entity.UserInfo;
import com.model.Backpack;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;
import com.utils.LuaUtils;

/**
 * 宠物处理
 * @author xujinbo
 *
 */
public class PetServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	
	/**
	 * 玩家宠物添加属性点
	 * @param buf
	 * @param session
	 */
	public void petAddAttPoint(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.YOU_AT_FIGHT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int uid = gameInput.readInt();//加点的宠物
			int tilizhi = gameInput.readInt();// 体力
			int linglizhi = gameInput.readInt();// 灵力
			int liliangzhi = gameInput.readInt();// 力量
			int suduzhi = gameInput.readInt(); // 速度
			
			Role role = userInfo.getPetLibrarys().getRole(uid);
			if (role == null) {
				gameOutput.writeInt(CommonCmd.PET_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 玩家添加的总值
			int totalCount = tilizhi+linglizhi+liliangzhi+suduzhi;
			if (totalCount == 0 || totalCount > role.getUnFenpei()) {
				gameOutput.writeInt(CommonCmd.IDLE_POINT_NOT_ENOUGH);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (tilizhi < 0 || linglizhi < 0 || liliangzhi < 0 || suduzhi < 0) {
				gameOutput.writeInt(CommonCmd.ERROR_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			role.setJiadianHp(role.getJiadianHp()+tilizhi);
			role.setJiadianMp(role.getJiadianMp()+linglizhi);
			role.setJiadianAp(role.getJiadianAp()+liliangzhi);
			role.setJiadianSp(role.getJiadianSp()+suduzhi);
			// 在更新前记录最大血蓝
			int blood = role.getMaxBlood();
			int magic = role.getMaxMagic();
			role.castAtt();
			// 在更新后 计算最大血蓝差距
			blood = role.getMaxBlood()-blood;
			magic = role.getMaxMagic()-magic;
			// 当前血蓝  加差距
			role.setBlood(role.getBlood()+blood);
			role.setMagic(role.getMagic()+magic);
			
			gameOutput.writeLong(role.getUid())
				.writeInt(role.getJiadianHp())
				.writeInt(role.getJiadianMp())
				.writeInt(role.getJiadianAp())
				.writeInt(role.getJiadianSp());
			
			userInfo.sendData(SocketCmd.PET_ADD_ATT_POINT, gameOutput.toByteArray());
			
			gameOutput.reset()
				.writeLong(role.getUid())
				.writeInt(userInfo.getRole().getBlood())
				.writeInt(userInfo.getRole().getMagic());
			userInfo.sendData(SocketCmd.PET_UPDATE_HP_MP, gameOutput.toByteArray());
			
			updatePet(userInfo);
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("玩家宠物添加属性点报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("玩家宠物添加属性点关闭流报错", e);
			}
		}
	}
	
	/** 刪除一个宠物 */
	public void deletePet(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.YOU_AT_FIGHT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long uid = gameInput.readInt();
			Role role = userInfo.getPetLibrarys().deleteRole(uid);
			if (role == null) {
				gameOutput.writeInt(CommonCmd.PET_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			updatePet(userInfo);
			userInfo.sendData(SocketCmd.DELETE_PET, role.getUid());
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("查看角色信息报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("查看角色信息关闭流报错", e);
			}
		}
	}
	
	/**
	 * 宠物参战状态更新
	 * @param buf
	 * @param session
	 */
	public void petUpdateJoinBattle(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long uid = gameInput.readInt();
			boolean state = gameInput.readBoolean();
			Role role = userInfo.getPetLibrarys().getRole(uid);
			if (role == null) {
				gameOutput.writeInt(CommonCmd.PET_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (state) {
				if (!role.isJoinBattlePet()) {
					userInfo.getPetLibrarys().updateJoinBattle(uid);
				}
			} else {
				role.setJoinBattlePet(false);
			}
			updatePet(userInfo);
			userInfo.sendData(SocketCmd.PET_UPDATE_JOINBATTLE, role.getUid(), state);
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("宠物参战状态更新报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("宠物参战状态更新关闭流报错", e);
			}
		}
	}
	
	/**
	 * 宠物参战名字更新
	 * @param buf
	 * @param session
	 */
	public void petUpdateName(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long uid = gameInput.readInt();
			String newName = gameInput.readUTF().trim();
			Role role = userInfo.getPetLibrarys().getRole(uid);
			if (role == null) {
				gameOutput.writeInt(CommonCmd.PET_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!role.getName().equals(newName)) {
				role.setName(newName);
				updatePet(userInfo);
				userInfo.sendData(SocketCmd.PET_UPDATE_NAME, role.getUid(), newName);
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("宠物参战状态更新报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("宠物参战状态更新关闭流报错", e);
			}
		}
	}

	/**
	 * 宠物驯服
	 * @param buf
	 * @param session
	 */
	public void petTame(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			long uid = gameInput.readInt();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Role role = userInfo.getPetLibrarys().getRole(uid);
			if (role == null) {
				gameOutput.writeInt(CommonCmd.PET_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (role.getStrength() < 100) {
				//如果存在训兽决
				Prop prop = userInfo.getBackpack().getPropId(LuaUtils.get("xunshoujue").tolong());
				if (prop == null) {
					prop = userInfo.getBackpack().getPropId(LuaUtils.get("gaojixunshoujue").tolong());
				}
				if (prop != null) {
					LuaUtils.get("petUseProp").invoke(new LuaValue[]{CoerceJavaToLua.coerce(userInfo), 
							CoerceJavaToLua.coerce(role), 
							CoerceJavaToLua.coerce(gameOutput),
							CoerceJavaToLua.coerce(prop)
							});
				} else {
					// 体力是否满足扣除
					if (userInfo.getRole().getStrength() < 30) {
						gameOutput.writeInt(CommonCmd.STRENGTH_LOW);
						session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
						return;
					}
					userInfo.getRole().setStrength(userInfo.getRole().getStrength()-30);
					role.setStrength(role.getStrength()+5);
					if (role.getStrength() > 100) {
						role.setStrength(100);
					}
				}
				UserInfo info = new UserInfo(userInfo.getId());
				info.setPetByte(BytesFactory.objectToByteData(userInfo.getPetLibrarys()));
				info.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
				if (prop != null) {
					info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
				}
				userServer.update(info);
				userInfo.sendData(SocketCmd.UPDATE_STRENGTH, userInfo.getRole().getStrength());
				userInfo.sendData(SocketCmd.PET_TAME, role.getUid(), role.getStrength());
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("宠物驯服报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("宠物驯服关闭流报错", e);
			}
		}
	}
	
	/**
	 * 宠物使用物品
	 * @param buf
	 * @param session
	 */
	public void petUseProp(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long uid = gameInput.readInt();
			int index = gameInput.readInt();// 物品位置
			int count = gameInput.readInt();// 使用数量
			Role role = userInfo.getPetLibrarys().getRole(uid);
			if (role == null) {
				gameOutput.writeInt(CommonCmd.PET_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Backpack backpack = userInfo.getBackpack();
			Prop prop = backpack.getPropIndex(index); // 物品
			if (prop == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (count == 0 || count > prop.getCount()) {
				gameOutput.writeInt(CommonCmd.PROP_COUNT_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!(prop instanceof UseProp)) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_USE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			UseProp useProp = (UseProp) prop;
			int useCount = count;
			int realCount = 0;//真实使用数量
			switch (useProp.getType()) {
				case 1:// 使用装备
					break;
				case 2:
				case 3:
				case 4:
					while(useCount > 0){
						useCount--;
						if (useProp.getCount() <= 0) {
							return;
						}
						switch (useProp.getType()) {
							case 2:
								if (role.getBlood()>=role.getMaxBlood()) {
									useCount = 0;
									return;
								}
								break;
							case 3:
								if (role.getMagic()>=role.getMaxMagic()) {
									useCount = 0;
									return;
								}
								break;
							case 4:
								if (role.getBlood()>=role.getMaxBlood() && role.getMagic()>=role.getMaxMagic()) {
									useCount = 0;
									return;
								}
								break;
						}
						LuaValue luaValue = LuaUtils.get("useBloodAndMagicProp").call(
								CoerceJavaToLua.coerce(useProp), 
								CoerceJavaToLua.coerce(userInfo),
								CoerceJavaToLua.coerce(role)
								);
						boolean isDeleteProp = luaValue.toboolean();
						if (isDeleteProp) {
							realCount++;
							backpack.removePropIndexCount(useProp.getIndex(), 1);
						}
					}
					if (realCount > 0) {
						userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 11, useProp.getName(), realCount);
					}
					break;
				case 11:// 使用道具
					Varargs result = LuaUtils.get("petUseProp").invoke(new LuaValue[]{
							CoerceJavaToLua.coerce(userInfo), 
							CoerceJavaToLua.coerce(role), 
							CoerceJavaToLua.coerce(gameOutput), 
							CoerceJavaToLua.coerce(useProp)
						});
					result.toboolean(1);
					break;
				default:
					break;
			}
			// 发送更新物品数量
			if (realCount > 0) {
				gameOutput.reset().write(1, useProp.getIndex(), realCount);
				userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
			}
			// 发送更新血量和蓝量
			if (useProp.getType()==2||useProp.getType()==3||useProp.getType()==4) {
				gameOutput.reset();
				gameOutput.writeLong(role.getUid());
				gameOutput.writeInt(role.getBlood());
				gameOutput.writeInt(role.getMagic());
				userInfo.sendData(SocketCmd.PET_UPDATE_HP_MP, gameOutput.toByteArray());
			}
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setBackpackByte(BytesFactory.objectToByteData(backpack));
			info.setPetByte(BytesFactory.objectToByteData(userInfo.getPetLibrarys()));
			userServer.update(info);
			
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("宠物使用物品报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("宠物使用物品关闭流报错", e);
			}
		}
	}

	/**
	 * 洗宠
	 * @param buf
	 * @param session
	 */
	public void washPet(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			long uid = gameInput.readInt();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Role role = userInfo.getPetLibrarys().getRole(uid);
			if (role == null) {
				gameOutput.writeInt(CommonCmd.PET_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Backpack backpack = userInfo.getBackpack();
			Prop prop = backpack.getPropId(LuaUtils.get("guiyuanlu").tolong());
			if (prop == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (role.getHpGrow() < role.getMaxHpGrow()) {
				role.washHp = LuaUtils.get("parseStr").call(LuaValue.valueOf(role.getBaseHpGrow())).toint();
			}
			if (role.getMpGrow() < role.getMaxMpGrow()) {
				role.washMp = LuaUtils.get("parseStr").call(LuaValue.valueOf(role.getBaseMpGrow())).toint();
			}
			if (role.getSpGrow() < role.getMaxSpGrow()) {
				role.washSp = LuaUtils.get("parseStr").call(LuaValue.valueOf(role.getBaseSpGrow())).toint();
			}
			if (role.getApGrow() < role.getMaxApGrow()) {
				role.washAp = LuaUtils.get("parseStr").call(LuaValue.valueOf(role.getBaseApGrow())).toint();
			}
			if (role.getFpGrow() < role.getMaxFpGrow()) {
				role.washFp = LuaUtils.get("parseStr").call(LuaValue.valueOf(role.getBaseFpGrow())).toint();
			}
			boolean baby = role.isBaby();
			if (!baby) {
				role.setBaby(true);
				role.setLevel(1);
				role.setStrength(100);
				if(role.washHp>0) role.setHpGrow(role.washHp);
				if(role.washMp>0) role.setMpGrow(role.washMp);
				if(role.washSp>0) role.setSpGrow(role.washSp);
				if(role.washAp>0) role.setApGrow(role.washAp);
				if(role.washFp>0) role.setFpGrow(role.washFp);
				role.setJiadianHp(0);
				role.setJiadianMp(0);
				role.setJiadianAp(0);
				role.setJiadianSp(0);
				role.castAtt();
				role.setBlood(role.getMaxBlood());
				role.setMagic(role.getMaxMagic());
				role.washHp = 0;
				role.washMp = 0;
				role.washSp = 0;
				role.washAp = 0;
				role.washFp = 0;
			}
			backpack.removePropIndexCount(prop.getIndex(), 1);
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
			info.setPetByte(BytesFactory.objectToByteData(userInfo.getPetLibrarys()));
			userServer.update(info);
			
			gameOutput.reset().write(1, prop.getIndex(), 1);
			userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
			if (baby) {
				gameOutput.reset().write(role.getUid(),
						role.washHp, role.washMp, role.washSp, role.washAp, role.washFp);
			} else {
				gameOutput.reset().write(role.getUid(),
						role.getHpGrow(), role.getMpGrow(), role.getSpGrow(), role.getApGrow(), role.getFpGrow());
			}
			userInfo.sendData(SocketCmd.WASH_PET, gameOutput.toByteArray());
			
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("洗宠报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("洗宠关闭流报错", e);
			}
		}
	}
	
	/**
	 * 洗宠替换
	 * @param buf
	 * @param session
	 */
	public void washPetReplace(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			long uid = gameInput.readInt();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Role role = userInfo.getPetLibrarys().getRole(uid);
			if (role == null) {
				gameOutput.writeInt(CommonCmd.PET_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (role.washHp==0 && role.washMp==0 && role.washSp==0 && role.washAp==0 && role.washFp==0) {
				return;
			}
			if(role.washHp>0) role.setHpGrow(role.washHp);
			if(role.washMp>0) role.setMpGrow(role.washMp);
			if(role.washSp>0) role.setSpGrow(role.washSp);
			if(role.washAp>0) role.setApGrow(role.washAp);
			if(role.washFp>0) role.setFpGrow(role.washFp);
			role.washHp = 0;
			role.washMp = 0;
			role.washSp = 0;
			role.washAp = 0;
			role.washFp = 0;
			role.castAtt();
			
			updatePet(userInfo);
			
			gameOutput.reset().write(role.getUid());
			userInfo.sendData(SocketCmd.WASH_PET_REPLACE, gameOutput.toByteArray());
			
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("洗宠替换报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("洗宠替换关闭流报错", e);
			}
		}
	}
	
	
	
	
	
	
	
	
	
	/**
	 * 更新玩家宠物
	 * @param userInfo
	 */
	public void updatePet(UserInfo userInfo) {
		UserInfo infoData = new UserInfo(userInfo.getId());
		infoData.setPetByte(BytesFactory.objectToByteData(userInfo.getPetLibrarys()));
		userServer.update(infoData);
	}

	
	
}
