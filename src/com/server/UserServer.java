package com.server;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.PropConfig;
import com.configs.RoleConfig;
import com.data.ConnectData;
import com.data.UserDataManage;
import com.entity.Escort;
import com.entity.Faction;
import com.entity.Friend;
import com.entity.Login;
import com.entity.Mail;
import com.entity.Prop;
import com.entity.Role;
import com.entity.Team;
import com.entity.UserInfo;
import com.mapper.UserMapper;
import com.model.Backpack;
import com.model.GameData;
import com.model.GiftLibrary;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.server.fight.FightBox;
import com.thread.SendDataThread;
import com.utils.BytesFactory;
import com.utils.DateUtils;
import com.utils.LuaUtils;

public class UserServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private ServerManageServer serverManageServer;
	@Resource
	private MessageServer messageServer;
	@Resource
	private RoleConfig roleConfig;
	@Resource
	private PropConfig propConfig;
	@Resource
	private EscortServer escortServer;
	@Resource
	private FightServer fightServer;
	@Resource
	private SendDataThread sendDataThread;
	@Resource
	private TeamServer teamServer;
	@Resource
	private BackpackServer backpackServer;
	@Resource
	private BattlefieldServer battlefieldServer;
	@Resource
	private FactionServer factionServer;
	@Resource
	private MapServer mapServer;
	
	/**
	 * 用户登录
	 * @param bs
	 * @param session
	 */
	public void login(byte[] buf, IoSession session) {
		SqlSession sqlSession = ConnectData.openSession();
		GameInput gameInput = null;
		try {
			gameInput = new GameInput(buf);
			String userOpenId = gameInput.readUTF();
			int serverId = gameInput.readInt();
			if (serverManageServer.serverInfo.getId() == serverId) {
				switch (serverManageServer.serverInfo.getState()) {
					case SocketCmd.SERVER_INFO_OFF:
						GameOutput gameOutput = new GameOutput();
						gameOutput.writeUTF("服务器已关闭!");
						session.write(new ActionData<Object>(SocketCmd.LOGIN_ERROR, gameOutput.toByteArray()));
						gameOutput.close();
						break;
					case SocketCmd.SERVER_INFO_NO:
						Login login = userDataManage.getLoginDataNotCreate(userOpenId, serverId, session);
						if (login != null) {
							sendUserInfo(session, login);
						}
						break;
					case SocketCmd.SERVER_INFO_INSIDE:
						// TODO 内测专线
						break;
					default:
						break;
				}
				
			} else {
				GameOutput gameOutput = new GameOutput();
				gameOutput.writeUTF("服务器不存在!");
				session.write(new ActionData<Object>(SocketCmd.LOGIN_ERROR, gameOutput.toByteArray()));
				gameOutput.close();
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("登录模块报错", e);
		} finally {
			try {
				if(sqlSession!=null)sqlSession.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("登录模块流报错", e);
			}
		}
	}
	
	/**
	 * 向刚刚登录的用户发送用户数据
	 * @param session
	 * @param requestLogin
	 */
	private void sendUserInfo(IoSession session, Login login) {
		GameOutput gameOutput = new GameOutput();
		try {
			UserInfo userInfo = userDataManage.getUserInfoData(login);
			if (userInfo != null) {
				sendUserInfoData(userInfo, gameOutput, login);
			} else {
				gameOutput.writeLong(login.getId());
				session.write(new ActionData<Object>(SocketCmd.CREATE_ROLE, gameOutput.toByteArray()));
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("发送用户数据报错", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("发送用户数据关闭流报错", e);
				}
			}
			
		}
	}
	
	/**
	 * 从数据库获取userinfo数据
	 * @param userId 用户的id
	 * @return
	 * @throws IOException
	 */
	public synchronized UserInfo queryUserInfo(long userId) throws IOException {
		UserInfo userInfo = null;
		SqlSession sqlSession = ConnectData.openSession();
		try {
			UserMapper userManage = sqlSession.getMapper(UserMapper.class);
			userInfo = userManage.queryId(userId);
			if (userInfo != null) {
				userInfo.setLogin(userDataManage.getCacheLogin(userInfo.getLoginId()));
				// 将这个新的放入储存处
				userDataManage.addNewUserInfo(userInfo);
				createUserInfo(userInfo);
			}
		} finally {
			sqlSession.close();
		}
		return userInfo;
	}
	
	/**
	 * 从数据库获取userinfo数据
	 * @param userName 根据名字从数据库查找用户
	 * @return
	 * @throws IOException
	 */
	public synchronized UserInfo queryUserInfo(String userName) throws IOException {
		UserInfo userInfo = null;
		SqlSession sqlSession = ConnectData.openSession();
		try {
			UserMapper userManage = sqlSession.getMapper(UserMapper.class);
			userInfo = userManage.queryName(userName);
			if (userInfo != null) {
				userInfo.setLogin(userDataManage.getCacheLogin(userInfo.getLoginId()));
				// 将这个新的放入储存处
				userDataManage.addNewUserInfo(userInfo);
				createUserInfo(userInfo);
			}
		} finally {
			sqlSession.close();
		}
		return userInfo;
	}
	
	/**
	 * 从数据库获取userinfo数据
	 * @param login 根据login ID 从数据库获取
	 * @return
	 * @throws IOException
	 */
	public synchronized UserInfo queryUserInfo(Login login) throws IOException {
		UserInfo userInfo = null;
		SqlSession sqlSession = ConnectData.openSession();
		try {
			UserMapper userManage = sqlSession.getMapper(UserMapper.class);
			userInfo = userManage.queryLogin(login.getId());
			if (userInfo != null) {
				userInfo.setLogin(userDataManage.getCacheLogin(userInfo.getLoginId()));
				// 将这个新的放入储存处
				userDataManage.addNewUserInfo(userInfo);
				createUserInfo(userInfo);
			}
		} finally {
			sqlSession.close();
		}
		return userInfo;
	}

	private void createUserInfo(UserInfo userInfo) {
		// 将二进制数据转换成类对象
		Role role = new Role();
		BytesFactory.createBytes(userInfo.getBytes(), role);
		role = roleConfig.getSample(1000+role.getSex());
		BytesFactory.createBytes(userInfo.getBytes(), role);
		userInfo.setRole(role);
		// 玩家属性  计算
//		System.out.println(userInfo.getRole());
		// 将穿戴的装备转换
		BytesFactory.createBytes(userInfo.getEquipsByte(), userInfo.getEquipsLibrary());
		userInfo.getRole().castAtt();
//		System.out.println("数据库新查询用户="+userInfo);
		// 将玩家包裹数据转化
		Backpack backpack = userInfo.getBackpack();
		BytesFactory.createBytes(userInfo.getBackpackByte(), backpack);
		// 将好友转化
		List<Friend> friends = userInfo.getFriends();
		friends.addAll(BytesFactory.byteToObject(userInfo.getFriendByte(), Friend.class));
		// 将好友请求转化
		List<Friend> addFriends = userInfo.getAddFriends();
		addFriends.addAll(BytesFactory.byteToObject(userInfo.getAddFriendByte(), Friend.class));
		// 将邮件转换
		List<Mail> mails = userInfo.getMails();
		mails.addAll(BytesFactory.byteToObject(userInfo.getMailByte(), Mail.class));
		for (int i = 0; i < mails.size(); i++) {
			mails.get(i).setId(userInfo.getMailId());
		}
		// 将技能转换
		BytesFactory.createBytes(userInfo.getSkillByte(), userInfo.getSkillLibrary());
		// 将宠物转换
		BytesFactory.createBytes(userInfo.getPetByte(), userInfo.getPetLibrarys());
		// 将已领取的礼包类型转换
		BytesFactory.createBytes(userInfo.getGetGiftDataByte(), userInfo.getGiftLibrary());
		// 将仓库转换
		BytesFactory.createBytes(userInfo.getWarehouseByte(), userInfo.getWarehouseLibrary());
		// 将玩家buff管理转换
		BytesFactory.createBytes(userInfo.getBuffByte(), userInfo.getBuffLibrary());
		BytesFactory.createBytes(userInfo.getTaskByte(), userInfo.getTaskLibrary());
		BytesFactory.createBytes(userInfo.getMissionRecord(), userInfo.getMissionLibrary());
		BytesFactory.createBytes(userInfo.getRechargeRecord(), userInfo.getRechargeLibrary());
		
	}

	/**
	 * 向客户端发送用户数据(用于登录和注册登录)
	 * @param session
	 * @param userInfo
	 * @param gameOutput 
	 * @throws IOException
	 */
	private void sendUserInfoData(UserInfo userInfo, GameOutput gameOutput, Login login) throws IOException {
		// 将user信息放入缓存中
		userInfo.setLogin(login);
		userInfo.setOnlineState(1);
		// 登录检查  此用户是否是第二日登录了
		Calendar calendar = Calendar.getInstance();// 上次离线时间
		calendar.setTimeInMillis(userInfo.getOfflineDate().getTime());
		Calendar current = Calendar.getInstance();// 当前时间
		current.setTimeInMillis(System.currentTimeMillis());
		if(!DateUtils.isSameMonth(calendar, current)) {// 不同月
			//重置不要属性
//			System.out.println("重置用户[ "+userInfo.getRoleName()+" ]每月属性!");
			GiftLibrary giftLibrary = userInfo.getGiftLibrary();
			giftLibrary.setLastTimeSignTimer(0);
			giftLibrary.setOnLineSignCount(0);
			updateGiftLibrary(userInfo);
			
		}
		if(!DateUtils.isSameDay(calendar, current)) {//不同天
			//重置不要属性
			System.out.println("重置用户[ "+userInfo.getRoleName()+" ]每日属性!");
			GameData.gameData.resetDayUser(userInfo);
		}
		
		userInfo.setLoginDate(new Timestamp(System.currentTimeMillis()));
		
		if(userInfo.getRole().getState() == CommonCmd.IDLE) {// 如果是空闲状态  检查是否需要升级
			if(GameData.gameData.checkUpdateLevel(userInfo.getRole())) {
				GameData.gameData.userInfoUpLevel(userInfo);
				UserInfo userData = new UserInfo(userInfo.getId());
				userData.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
				update(userData);
			}
		}
		// 检查此用户是否体力为0了 还在挂机  或者状态没有重置
		if (userInfo.getRole().getState() == CommonCmd.ESCORT) {
			if (userInfo.getRole().getStrength() <= 0) { // 如果小于等于0
				long mapId = userInfo.getCurrentSceneValue();
				if (mapId != 0) {
					Escort escort = escortServer.getEscortThread().getEscortUser(mapId, userInfo);		
					if (escort != null) {
						escortServer.removeEscort(mapId, userInfo, null);
					}
				}
				userInfo.getRole().setState(CommonCmd.IDLE);
				//保存一次玩家数据
				UserInfo userData = new UserInfo(userInfo.getId());
				userData.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
				update(userData);
			}
		}
		
//		System.out.println("登录用户:"+userInfo);
		// 登录检测完成   向玩家好友发送信息
		List<Friend> friends = userInfo.getFriends();
		// 检测好友在线状态
		int len = friends.size();
		UserInfo userTwo;
		for (int i = 0; i < len; i++) {
			userTwo = userDataManage.getCacheUserInfo(friends.get(i).getFriendId());
			if (userTwo == null || userTwo.getOnlineState() == 0) {
				friends.get(i).setOnlineState(0);
			} else {
				friends.get(i).setOnlineState(1);
				sendDataThread.addActionData(SocketCmd.FRIEND_ONLINE_STATE, userTwo, userInfo.getId(), 1);
			}
		}
		// 向玩家发送信息
		
		// 检查玩家所在场景
		if (userInfo.getCurrentScene() == CommonCmd.SCENE_ESCORT) {
			if(!escortServer.getEscortThread().containsEscortUser(userInfo.getCurrentSceneValue(), userInfo)) {
				userInfo.getRole().setState(0);
				GameData.gameData.setupCurrentScene(userInfo, CommonCmd.SCENE_GAME);
				updateUserRole(userInfo);
				updateCurrentScene(userInfo);
			}
		}
		
		// 写入用户基础数据
		gameOutput.write(
					userInfo.getId(),
					userInfo.getLoginId(),
					userInfo.getCamp(),
					userInfo.getMood(),
					userInfo.getPlayerGold(),
					userInfo.getLockGold(),
					userInfo.getLockrmb(),
					userInfo.getRmb(),
					userInfo.getCurrentScene(),
					userInfo.getCurrentSceneValue(),
					userInfo.getPotential(),
					userInfo.getFactionId(),
					userInfo.getLoginDate().getTime(),
					userInfo.getOnLineDate(),
					userInfo.getOnLineGiftCount(),
					userInfo.getGiftLibrary().getOnLineSignCount(),
					userInfo.getCurrentSceneStartX(),
					userInfo.getCurrentSceneStartY(),
					userInfo.getIsAutoUseSupply(),
					System.currentTimeMillis()
				);
		
		// 写入玩家穿戴装备
		userInfo.getEquipsLibrary().writeData(gameOutput);
		// 写入玩家角色信息
		userInfo.getRole().writeData(gameOutput);
		userInfo.updateEquipsRole();
		userInfo.getRole().castAtt();
		
		// 帮派基础信息
		if (userInfo.getFactionId() > 0) {
			factionServer.getFaction(userInfo.getFactionId()).writeBaseData(gameOutput);
		}
		
		// 如果在队伍中
		Team team = null;
		if (userInfo.getTeamId() != 0) {
			team = teamServer.getTeam(userInfo.getTeamId());
			// 此队伍不在  或  直接不再此队伍中
			if (team == null || !team.checkExist(userInfo)) {
				userInfo.setTeamId(0);
			}
		}
		gameOutput.writeLong(userInfo.getTeamId());
		if (userInfo.getTeamId() != 0) {
			team.writeData(gameOutput);
		}
		//写入技能
		userInfo.getSkillLibrary().writeData(gameOutput);
		//写入宠物
		userInfo.getPetLibrarys().writeData(gameOutput);
		userInfo.getBuffLibrary().checkOverdue();
		userInfo.getBuffLibrary().writeAllBuff(gameOutput);
		//写入用户背包数据
		Backpack backpack = userInfo.getBackpack();
		backpack.writeAllPropData(gameOutput);
		//写入好友信息
		userInfo.writeAllFriends(gameOutput);
		//写入加好友信息
		userInfo.writeAllAddFriends(gameOutput);
		userInfo.getTaskLibrary().writeAllTask(gameOutput);
		userInfo.getGiftLibrary().writeAllData(gameOutput);
		
		// 写入剧情任务
		userInfo.getMissionLibrary().writeAllData(gameOutput);
		
		userInfo.sendData(SocketCmd.USER_DATA, gameOutput.toByteArray());
		
		userDataManage.addOnLineUserInfo(userInfo);
		// 判断用户上次离线前是否在战斗
		if (userInfo.getCurrentFightId() != 0) {// 玩家离线前还在战斗
			FightBox fightBox = fightServer.getFightBox(userInfo.getCurrentFightId());
			if(fightBox != null) {
				// 战斗还在继续    施行重连机制
				fightServer.reconnect(userInfo);
				return;
			} else {
				// 战斗不再了   清除战斗数据
				userInfo.setCurrentFightId(0);
			}
		}
		// 发送聊天信息  DEBUG开启
//		if (serverManageServer.getServerConfig().isDebug()) {
//			messageServer.sendLoginChat(userInfo);
//		}
		
		if (userInfo.getCurrentScene() != CommonCmd.SCENE_GRIND 
				&& userInfo.getCurrentScene() != CommonCmd.SCENE_ESCORT) {
			boolean result = mapServer.reconnect(userInfo);
			if (result) {
				//玩家在战场场景
				gameOutput.reset().
					writeInt(userInfo.getCurrentScene()).
					writeLong(userInfo.getCurrentSceneValue()).
					writeInt(userInfo.getCurrentSceneStartX()).
					writeInt(userInfo.getCurrentSceneStartY());
				userInfo.sendData(SocketCmd.USER_ENTER_SCENE, gameOutput.toByteArray() );
				return;
			}
			GameData.gameData.setupCurrentScene(userInfo, CommonCmd.SCENE_GAME);
			updateCurrentScene(userInfo);
		}
		
		if (userInfo.getCurrentScene() == CommonCmd.SCENE_ESCORT) {
			//玩家在押镖场景
			gameOutput.reset().
				writeInt(CommonCmd.SCENE_ESCORT).
				writeLong(userInfo.getCurrentSceneValue()).
				writeInt(userInfo.getCurrentSceneStartX()).
				writeInt(userInfo.getCurrentSceneStartY());
			userInfo.sendData(SocketCmd.USER_ENTER_SCENE, gameOutput.toByteArray() );
			escortServer.sendEnterEscortData(userInfo.getSession(), userInfo.getCurrentSceneValue(), userInfo);
		} else if (userInfo.getCurrentScene() == CommonCmd.SCENE_GRIND) {
			//玩家在练级场景
			gameOutput.reset().
				writeInt(CommonCmd.SCENE_GRIND).
				writeLong(userInfo.getCurrentSceneValue()).
				writeInt(userInfo.getCurrentSceneStartX()).
				writeInt(userInfo.getCurrentSceneStartY());
			userInfo.sendData(SocketCmd.USER_ENTER_SCENE, gameOutput.toByteArray() );
		} else {
			gameOutput.reset().
				writeInt(CommonCmd.SCENE_GAME).
				writeLong(userInfo.getCurrentSceneValue()).
				writeInt(userInfo.getCurrentSceneStartX()).
				writeInt(userInfo.getCurrentSceneStartY());
			userInfo.sendData(SocketCmd.USER_ENTER_SCENE, gameOutput.toByteArray() );
		}
		
	}

	/**
	 * 创建一个角色
	 * @param bs
	 * @param session
	 */
	public void createRole(byte[] bs, IoSession session) {
		SqlSession sqlSession = ConnectData.openSession();
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		long loginId = 0;
		try {
			gameInput = new GameInput(bs);
			gameOutput = new GameOutput();
			loginId = gameInput.readInt();// 登录 id
			String userName = gameInput.readUTF().trim();//名字
			int camp = gameInput.readInt();//阵营
			int sex = gameInput.readInt();// 性别
			Login login = userDataManage.getLogin(loginId);
			if (login == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			UserInfo userInfo = userDataManage.getUserInfoData(login);
			if (userInfo != null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userName.length() < 1 || userName.length() > 6) {
				gameOutput.writeInt(CommonCmd.USER_NAME_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (GameData.gameData.checkUserName(userName)) {
				gameOutput.writeInt(CommonCmd.USER_NAME_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			UserMapper userManage = sqlSession.getMapper(UserMapper.class);
			userInfo = userManage.queryName(userName);
			if (userInfo != null) {
				gameOutput.writeInt(CommonCmd.USER_NAME_EXISTENT);
				gameOutput.writeLong(loginId);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			// 初始化角色数据
			int roleId = 1001+(sex-1)+(camp-1)*2;
			Role role = roleConfig.getSample(roleId);
			role.setName(userName);
			role.setLevel(1);
			role.castAtt();
			role.setBlood(role.getMaxBlood());
			role.setMagic(role.getMaxMagic());
			role.setMoveSpeed(1);
			
			userInfo = new UserInfo(new Login(loginId));
			userInfo.setRoleName(userName);
			userInfo.setCamp(camp);
			userInfo.setRole(role);
			userInfo.setLoginDate(new Timestamp(System.currentTimeMillis()));
			userInfo.setOfflineDate(new Timestamp(0));
			userInfo.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
			
			LuaUtils.get("newRegUserGive").invoke(new LuaValue[]{CoerceJavaToLua.coerce(userInfo)});
			
			// 保存背包数据
			userInfo.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
			// 保存好友请求数据
			userInfo.setAddFriendByte(BytesFactory.listToBytes(userInfo.getAddFriends()));
			// 好友请求数据
			userInfo.setFriendByte(BytesFactory.listToBytes(userInfo.getFriends()));
			// 保存邮件
			userInfo.setMailByte(BytesFactory.listToBytes(userInfo.getMails()));
			// 保存技能
			userInfo.setSkillByte(BytesFactory.objectToByteData(userInfo.getSkillLibrary()));
			// 保存宠物
			userInfo.setPetByte(BytesFactory.objectToByteData(userInfo.getPetLibrarys()));
			// 保存穿戴的装备
			userInfo.setEquipsByte(BytesFactory.objectToByteData(userInfo.getEquipsLibrary()));
			// 保存已领取的礼包类型
			userInfo.getGiftLibrary().setOnLineSignCount(0);
			userInfo.setGetGiftDataByte(BytesFactory.objectToByteData(userInfo.getGiftLibrary()) );
			// 保存仓库
			userInfo.setWarehouseByte(BytesFactory.objectToByteData(userInfo.getWarehouseLibrary()));
			// 保存正在使用中的buff道具
			userInfo.setBuffByte(BytesFactory.objectToByteData(userInfo.getBuffLibrary()));
			// 保存剧情任务保存记录
			userInfo.setMissionRecord(BytesFactory.objectToByteData(userInfo.getMissionLibrary()));
			// 保存充值记录
			userInfo.setRechargeRecord(BytesFactory.objectToByteData(userInfo.getRechargeLibrary()));
			
			
			userInfo.setTaskByte(BytesFactory.objectToByteData(userInfo.getTaskLibrary()));
			userManage.insert(userInfo);
			sqlSession.commit();
			userDataManage.addNewUserInfo(userInfo);
			session.write(new ActionData<>(SocketCmd.CREATE_ROLE));// 发送创建角色  
			
			gameOutput.reset();
			sendUserInfoData(userInfo, gameOutput, login);
		} catch (Exception e) {
			try {
				gameOutput.reset().writeLong(loginId);
				session.write(new ActionData<>(SocketCmd.CREATE_ROLE_ERROR, gameOutput.toByteArray()));// 创建角色失败
				Logger.getLogger(getClass()).error("创建角色报错", e);
			} catch (IOException e1) {
			}
		} finally {
			try {
				if(sqlSession!=null)sqlSession.close();
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("创建角色关闭流报错", e);
			}
		}
	}

	/**
	 * 删除一个角色
	 * @param bs
	 * @param session
	 */
	public void deleteRole(byte[] bs, IoSession session) {
//		SqlSession sqlSession = ConnectData.openSession();
//		GameInput gameInput = null;
//		try {
//			gameInput = new GameInput(bs);
//			int loginId = gameInput.readInt();
//			UserMapper userManage = sqlSession.getMapper(UserMapper.class);
//			userManage.deleteLogin(loginId);
//			userDataManage.removeUserInfo(new UserInfo(new Login(loginId)));
//			sqlSession.commit();
//		} catch (IOException e) {
//			Logger.getLogger(getClass()).error("删除一个角色报错", e);
//		} finally {
//			try {
//				if(sqlSession!=null)sqlSession.close();
//				if(gameInput!=null) gameInput.close();
//			} catch (IOException e) {
//				Logger.getLogger(getClass()).error("删除一个角色关闭流报错", e);
//			}
//		}
	}

	/**
	 * 查看角色信息
	 * @param buf
	 * @param session
	 */
	public void seeRoleInfo(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			String userName = gameInput.readUTF();
			UserInfo userTwo = userDataManage.getUserInfoData(userName);
			if (userTwo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Faction faction = factionServer.getFaction(userTwo.getFactionId());
			String factionName;
			if (faction == null) {
				factionName = "无";
			} else {
				factionName = faction.getName();
			}
			// 写入用户基础数据
			gameOutput.write(
						userTwo.getId(),
						userTwo.getMood(),
						userTwo.getOnlineState(),
						userTwo.getRole().getId(),
						userTwo.getRole().getName(),
						userTwo.getRole().getLevel(),
						userTwo.getRole().getState(),
						userTwo.getRole().getStrength(),
						factionName
					);
			userInfo.sendData(SocketCmd.SEE_ROLE_INFO, gameOutput.toByteArray());
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("查看角色信息报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("查看角色信息关闭流报错", e);
			}
		}
	}

	/**
	 * 改名
	 * @param buf
	 * @param session
	 */
	public void rename(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.YOU_AT_FIGHT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getTeamId() != 0) {
				gameOutput.reset().writeInt(CommonCmd.NOT_TEAM_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getRole().getLevel() < 10) {
				gameOutput.reset().writeInt(CommonCmd.YOU_LEVEL_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			int xiaohao = gameInput.readInt();
			String userName = gameInput.readUTF().trim();
			Prop prop = null;
			if (xiaohao == 0) {
				prop = userInfo.getBackpack().getPropId(LuaUtils.get("gaimingka").tolong());
				if (prop == null) {
					gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
			} else {
				if (userInfo.getRmb() < 2000) {
					gameOutput.writeInt(CommonCmd.RMB_NOT_LOW);
					session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
					return;
				}
			}
			if (userName.length() < 1) {
				gameOutput.writeInt(CommonCmd.USER_NAME_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (GameData.gameData.checkUserName(userName)) {
				gameOutput.writeInt(CommonCmd.USER_NAME_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (xiaohao == 0) {
				userInfo.getBackpack().removeProp(prop);
				userInfo.sendData(SocketCmd.ADD_CHAT_MESSAGE, 11, prop.getName(), 1);
			} else {
				userInfo.setRmb(userInfo.getRmb()-2000);
			}
			// 缓存旧名字
			String oldName = userInfo.getRoleName();
			userInfo.setRoleName(userName);
			userInfo.getRole().setName(userName);
			
			UserInfo info = new UserInfo(userInfo.getId());
			if (xiaohao == 0) {
				info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
			} else {
				info.setRmb(userInfo.getRmb());
			}
			info.setRoleName(userName);
			info.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
			update(info);
			
			gameOutput.writeUTF(userName);
			userInfo.sendData(SocketCmd.RENAME, gameOutput.toByteArray());
			if (xiaohao == 0) {
				gameOutput.reset().writeInt(1).writeInt(prop.getIndex()).writeInt(prop.getCount());
				userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
			} else {
				gameOutput.reset().writeLong(userInfo.getRmb()).writeLong(userInfo.getLockrmb());
				userInfo.sendData(SocketCmd.UPDATE_RMB, gameOutput.toByteArray());
			}
			
			
			//世界通知
			gameOutput.reset().writeInt(4).writeUTF(oldName).writeUTF(userName);
			sendDataThread.addActionData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput.toByteArray());
			
			// 修改好友中的名字 并邮件通知好友  改名
			List<Friend> friends = userInfo.getFriends();
			int len = friends.size();
			Friend friend;
			UserInfo userTwo;
			for (int i = 0; i < len; i++) {
				friend = friends.get(i);
				userTwo = userDataManage.getUserInfoData(friend.getFriendId());
				if (userTwo != null) {
					friend = userTwo.getFriend(userInfo.getId());
					if (friend != null) {
						gameOutput.reset();
						Mail mail = new Mail();
						mail.setId(userTwo.getMailId());
						mail.setType(0);
						mail.setSender(userInfo.getRoleName());
						mail.setTheme("好友改名");
						mail.setContent("你的好友["+friend.getName()+"]更名为["+userName+"]");
						mail.setSendData(System.currentTimeMillis());
						mail.writeData(gameOutput);
						friend.setName(userName);
						userTwo.getMails().add(mail);
						updateFriendByte(userTwo);
						updateMailByte(userTwo);
						if (userTwo.getOnlineState() == 1) {
							sendDataThread.addActionData(SocketCmd.ADD_MAIL, gameOutput.toByteArray(), userTwo);
							gameOutput.reset();
							friend.writeData(gameOutput);
							userTwo.sendData(SocketCmd.UPDATE_FRIEND, gameOutput.toByteArray());
						}
					}
				}
			}
			
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("查看角色信息报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("查看角色信息关闭流报错", e);
			}
		}
	}
	
	/**
	 * 玩家添加属性点
	 * @param buf
	 * @param session
	 */
	public void addAttPoint(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.YOU_AT_FIGHT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			int tilizhi = gameInput.readInt();
			int linglizhi = gameInput.readInt();
			int liliangzhi = gameInput.readInt();
			int suduzhi = gameInput.readInt();
			// 玩家添加的总值
			int totalCount = tilizhi+linglizhi+liliangzhi+suduzhi;
			Role role = userInfo.getRole();
			if (totalCount == 0 || totalCount > role.getUnFenpei()) {
				gameOutput.writeInt(CommonCmd.IDLE_POINT_NOT_ENOUGH);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (tilizhi < 0 || linglizhi < 0 || liliangzhi < 0 || suduzhi < 0) {
				gameOutput.writeInt(CommonCmd.ERROR_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			role.setJiadianHp(role.getJiadianHp()+tilizhi);
			role.setJiadianMp(role.getJiadianMp()+linglizhi);
			role.setJiadianAp(role.getJiadianAp()+liliangzhi);
			role.setJiadianSp(role.getJiadianSp()+suduzhi);
			
			int blood = role.getMaxBlood();
			int magic = role.getMaxMagic();
			role.castAtt();
			blood = role.getMaxBlood()-blood;
			magic = role.getMaxMagic()-magic;
			
			role.setBlood(role.getBlood()+blood);
			role.setMagic(role.getMagic()+magic);
			
			gameOutput.writeInt(role.getJiadianHp()).writeInt(role.getJiadianMp())
				.writeInt(role.getJiadianAp()).writeInt(role.getJiadianSp());
			
			userInfo.sendData(SocketCmd.UPDATE_ATT_POINT, gameOutput.toByteArray());
			userInfo.sendHpMp(gameOutput);
			updateUserRole(userInfo);
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("玩家添加属性点报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("玩家添加属性点关闭流报错", e);
			}
		}
	}
	
	

	
	
	
	
	/**
	 * 更新用户自动使用物品
	 * @param buf
	 * @param session
	 */
	public void updateAutoUseProp(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			int auto = gameInput.readInt();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getIsAutoUseSupply() != auto) {
				userInfo.setIsAutoUseSupply(auto);
				
				UserInfo info = new UserInfo(userInfo.getId());
				info.setIsAutoUseSupply(userInfo.getIsAutoUseSupply());
				update(info);
				
				userInfo.sendData(SocketCmd.UPDATE_AUTO_USE_PROP, userInfo.getIsAutoUseSupply());
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("宠物使用物品报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("宠物使用物品关闭流报错", e);
			}
		}
	}
	
	/**
	 * 更新用户数据
	 * @param userInfo
	 */
	public void update(UserInfo userInfo) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			UserMapper userManage = sqlSession.getMapper(UserMapper.class);
			userManage.update(userInfo);
			sqlSession.commit();
		} finally {
			if(sqlSession!=null)sqlSession.close();
		}
	}

	/**
	 * 玩家离线
	 * @param session
	 */
	public void userOffline(IoSession session) {
		try {
			if (session != null) {
				Login login = userDataManage.removeLogin(session);
				if (login != null) {
					UserInfo userInfo = userDataManage.getUserInfoData(login);
					if (userInfo == null) {
						userInfo = userDataManage.getCacheUserInfo(session);
					}
//					System.out.println("离线了="+userInfo);
					if (userInfo != null) {
						userDataManage.removeOnLineUserInfo(userInfo);
						userInfo.setOfflineDate(new Timestamp(System.currentTimeMillis()));
						userInfo.setOnlineState(0);
						userInfo.getLogin().setSession(null);
						
						long onLineDate = userInfo.getLoginDate().getTime();
						// 在线时间经过的时间
						long afterData = System.currentTimeMillis()-onLineDate;
						userInfo.setOnLineDate(userInfo.getOnLineDate()+afterData);
						// 告诉好友自己离线了
						List<Friend> friends = userInfo.getFriends();
						int len = friends.size();
						UserInfo userTwo;
						for (int i = 0; i < len; i++) {
							userTwo = userDataManage.getCacheUserInfo(friends.get(i).getFriendId());
							if (userTwo != null && userTwo.getOnlineState() == 1) {
								sendDataThread.addActionData(SocketCmd.FRIEND_ONLINE_STATE, userTwo, userInfo.getId(), 0);
							}
						}
						teamServer.userOffline(userInfo);
						UserInfo userData = new UserInfo(userInfo.getId());
						userData.setOfflineDate(userInfo.getOfflineDate());
						userData.setOnLineDate(userInfo.getOnLineDate());
						update(userData);
						// 检查此用户是否在战斗
						if (userInfo.getCurrentFightId() != 0 && fightServer.containsBox(userInfo.getCurrentFightId())) {
							fightServer.getFightBox(userInfo.getCurrentFightId()).userOffline(userInfo);
						}
//						System.out.println("离线场景="+userInfo.getCurrentScene());
						mapServer.userOffline(userInfo);
					}
				}
				
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("用户离线  修改数据", e);
		}
	}
	
	
	/**
	 * 更新物品背包信息
	 * @param userInfo
	 */
	public void updateBackpack(UserInfo userInfo) {
		UserInfo userData = new UserInfo(userInfo.getId());
		userData.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
		update(userData);
	}
	
	/**
	 * 更新礼包信息
	 * @param userInfo
	 */
	public void updateGiftLibrary(UserInfo userInfo) {
		UserInfo userData = new UserInfo(userInfo.getId());
		userData.setGetGiftDataByte(BytesFactory.objectToByteData(userInfo.getGiftLibrary()));
		update(userData);
	}

	/**
	 * 更新此人角色属性
	 * @param userInfo
	 */
	public void updateUserRole(UserInfo userInfo) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			UserMapper userManage = sqlSession.getMapper(UserMapper.class);
			UserInfo userData = new UserInfo(userInfo.getId());
			userData.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
			userManage.update(userData);
			sqlSession.commit();
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("更新此人角色属性报错", e);
		} finally {
			if(sqlSession!=null)sqlSession.close();
		}
	}

	/**
	 * 更新添加好友数据  （此方法是 更新现有的添加好友请求  到数据库）
	 * @param userInfo 
	 */
	public void updateAddFriendByte(UserInfo userInfo) {
		UserInfo userData = new UserInfo(userInfo.getId());
		userData.setAddFriendByte(BytesFactory.listToBytes(userInfo.getAddFriends()));
		update(userData);
		
	}
	
	/**
	 * 更新添加好友数据  (此方法是添加)
	 * @param userTwo 
	 * @param friend
	 */
	public void updateAddFriendByte(UserInfo userTwo, Friend friend) {
		if (!userTwo.getAddFriends().contains(friend)) {
			userTwo.getAddFriends().add(friend);
			updateAddFriendByte(userTwo);
		}
	}
	
	/**
	 * 更新好友数据
	 * @param userTwo 
	 * @param friend
	 */
	public void updateFriendByte(UserInfo userTwo, Friend friend) {
		updateFriendByte(userTwo, Arrays.asList(friend));
	}
	
	/**
	 * 更新好友数据
	 * @param userTwo 
	 * @param friends
	 */
	public void updateFriendByte(UserInfo userTwo, List<Friend> friends) {
		int len = friends.size();
		Friend friend;
		boolean isSave = false;
		for (int i = 0; i < len; i++) {
			friend  = friends.get(i);
			// 如果不存在 好友列表里面
			if (!userTwo.getFriends().contains(friend)) {
				userTwo.getFriends().add(friend);
				isSave = true;
			}
		}
		if (!isSave) return;
		updateFriendByte(userTwo);
	}

	/**
	 * 更新好友数据 （此方法是 更新现有的好友  到数据库）
	 * @param userTwo 
	 */
	public void updateFriendByte(UserInfo userTwo) {
		UserInfo userInfo = new UserInfo(userTwo.getId());
		userInfo.setFriendByte(BytesFactory.listToBytes(userTwo.getFriends()));
		update(userInfo);
	}
	
	/**
	 * 更新邮件数据 
	 * @param userTwo 
	 */
	public void updateMailByte(UserInfo userTwo) {
		UserInfo userInfo = new UserInfo(userTwo.getId());
		userInfo.setMailByte(BytesFactory.listToBytes(userTwo.getMails()));
		update(userInfo);
	}
	
	/**
	 * 状态更改 更改玩家当前所在场景
	 * @param userTwo 
	 */
	public void updateCurrentScene(UserInfo userTwo) {
		UserInfo infoData = new UserInfo(userTwo.getId());
		infoData.setCurrentScene(userTwo.getCurrentScene());
		infoData.setCurrentSceneValue(userTwo.getCurrentSceneValue());
		infoData.setCurrentSceneStartX(userTwo.getCurrentSceneStartX());
		infoData.setCurrentSceneStartY(userTwo.getCurrentSceneStartY());
		update(infoData);
	}
	
	/**
	 * 更新玩家技能
	 * @param userTwo 
	 */
	public void updateSkill(UserInfo userTwo) {
		UserInfo infoData = new UserInfo(userTwo.getId());
		infoData.setSkillByte(BytesFactory.objectToByteData(userTwo.getSkillLibrary()));
		update(infoData);
	}

	/**
	 * 更新玩家穿戴装备
	 * @param userInfo
	 */
	public void updateEquips(UserInfo userInfo) {
		UserInfo infoData = new UserInfo(userInfo.getId());
		infoData.setEquipsByte(BytesFactory.objectToByteData(userInfo.getEquipsLibrary()));
		update(infoData);
	}
	
	/**
	 * 更新玩家仓库
	 * @param userInfo
	 */
	public void updateWarehouse(UserInfo userInfo) {
		UserInfo infoData = new UserInfo(userInfo.getId());
		infoData.setWarehouseByte(BytesFactory.objectToByteData(userInfo.getWarehouseLibrary()));
		update(infoData);
	}

	/**
	 * 跟新此用户的 在线礼包和次数
	 * @param userInfo
	 */
	public void updateOnLine(UserInfo userInfo) {
		UserInfo infoData = new UserInfo(userInfo.getId());
		infoData.setOnLineDate(userInfo.getOnLineDate());
		infoData.setOnLineGiftCount(userInfo.getOnLineGiftCount());
		update(infoData);
	}
	
	/**
	 * 跟新此用户正在使用中的buff道具
	 * @param userInfo
	 */
	public void updateBuffByte(UserInfo userInfo) {
		UserInfo infoData = new UserInfo(userInfo.getId());
		infoData.setBuffByte(BytesFactory.objectToByteData(userInfo.getBuffLibrary()));
		update(infoData);
	}

	
	
}
