package com.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import com.entity.ServerConfig;
import com.entity.ServerInfo;
import com.model.GameData;
import com.utils.Xml2Entity;

/**
 * 服务器管理信息
 * 
 * @author xujinbo
 * 
 */
public class ServerManageServer {

	/**
	 * 服务器的配置文件 
	 */
	private ServerConfig serverConfig;
	/**
	 * 服务器信息
	 */
	public ServerInfo serverInfo;
	private Xml2Entity xml2Entity;
	
	/**
	 * 初始化配置文件
	 */
	public void init() {
		xml2Entity = new Xml2Entity();
		//注册使用了注解的VO  
		xml2Entity.parseClass(ServerConfig.class, GameData.class);
        
        serverConfig = (ServerConfig) loadXml("data/serverConfig.xml");
        
        GameData gameData = (GameData) loadXml("data/sampleConfig/gameData.xml");
        gameData.getStageHeight();
	}

	private Object loadXml(String pathname) {
		File file = new File(pathname);
		InputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
			return xml2Entity.fromXML(fileInput);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (fileInput != null) {
				try {
					fileInput.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public ServerConfig getServerConfig() {
		return serverConfig;
	}

}
