package com.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Action;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.SkillConfig;
import com.data.UserDataManage;
import com.entity.Skill;
import com.entity.UserInfo;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;
import com.utils.LuaUtils;

/**
 * 技能处理
 * @author xujinbo
 *
 */
public class SkillServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private SkillConfig skillConfig;
	@Resource
	private UserServer userServer;
	/** 缓存的可升级的技能 */
	private List<Skill> skillList = new ArrayList<>();
	
	public SkillServer() {
	}
	
	@Action(CommonCmd.INIT_SKILL_DATA)
	protected void init() {
		int len = 1016-1001+1;
		Skill skill;
		for (int i = 0; i < len; i++) {
			skill = skillConfig.getSample(i+1001);
			if (skill != null) {
				skillList.add(skill);
			}
		}
	}
	
	public Skill getSkill(long id) {
		int len = skillList.size();
		for (int i = 0; i < len; i++) {
			if (skillList.get(i).getId() == id) {
				return skillList.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 更新技能等级
	 * @param buf
	 * @param session
	 */
	public void updateSkillLevel(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (userInfo.getPotential() < 1) {
				gameOutput.writeInt(CommonCmd.POTENTIAL_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			long skillId = gameInput.readInt();// 技能id
			int count = gameInput.readInt();//升级次数
			Skill skill = getSkill(skillId);
			if (skill == null) {
				gameOutput.writeInt(CommonCmd.SKILL_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			// 获取技能  如果角色没有学习这个技能  那么立即添加学习进入
			Skill userSkill = userInfo.getSkill(skillId);
			if (userSkill == null) {
				userSkill = skillConfig.getSample(skillId);
				userInfo.getSkillLibrary().addSkill(userSkill);
			}
			// 需要消耗的潜能
			long potential = calc(userSkill);
			if (potential > userInfo.getPotential()) {
				gameOutput.writeInt(CommonCmd.POTENTIAL_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			// 获取此技能当前升级最大等级
			LuaValue revert = LuaUtils.get("checkSkillLevel")
					.call(CoerceJavaToLua.coerce(userInfo), CoerceJavaToLua.coerce(userSkill));
			
			boolean checkSkillLevel = revert.toboolean();
			if (!checkSkillLevel) {
				return;
			}
			revert = LuaUtils.getCommon().get("getMaxUpLevel").call(CoerceJavaToLua.coerce(userInfo.getRole()));
			int maxLevel = revert.toint();
			if(userSkill.getLevel() >= maxLevel) {// 大于当前角色升级的最大等级
				gameOutput.writeInt(CommonCmd.ERROR_OPERATION);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			
			for (int i = 0; i < count; i++) {
				potential = calc(userSkill);
				if (userSkill.getLevel() < maxLevel && potential <= userInfo.getPotential()) {
					userSkill.setLevel(userSkill.getLevel()+1);
					userInfo.setPotential(userInfo.getPotential()-potential);
				} else {
					break;
				}
			}
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setPotential(userInfo.getPotential());
			info.setSkillByte(BytesFactory.objectToByteData(userInfo.getSkillLibrary()));
			userServer.update(info);
			// 通知客户端技能升级
			userInfo.sendData(SocketCmd.UPDATE_POTENTIAL, userInfo.getPotential());
			userInfo.sendData(SocketCmd.UPDATE_SKILL_LEVEL, userSkill.getId(), userSkill.getLevel());
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("更新技能等级报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("更新技能等级关闭流报错", e);
			}
		}
		
	}
	
	/**
	 * 技能升级消耗计算
	 * @param skill
	 * @return
	 */
	private long calc(Skill skill) {
		int x = Math.round(skill.getLevel());
		int y = Math.round(skill.getLevel()+1);
		if( (x<0) || (y>150)){
			return 0;
		}
		int i = 1;
		long exp_value = 0;
		for (i = x+1; i <= y; i++){
			if (i==1) { exp_value=Math.round(exp_value+1);}
			if (i>1 && i<21) { exp_value=Math.round(exp_value+0.233*i*i*i); }
			if (i>20 && i<30) { exp_value=Math.round(exp_value+0.291*i*i*i); }
			if (i == 30){ exp_value=Math.round(exp_value+7329); }
			if (i>30 && i<51){ exp_value=Math.round(exp_value+0.233*i*i*i); }
			if (i>50) { exp_value=Math.round(exp_value+0.291*i*i*i); }
		}
		return exp_value;
	}
	
}
