package com.server;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.UserDataManage;
import com.entity.Prop;
import com.entity.SpaceDate;
import com.entity.UserInfo;
import com.model.Backpack;
import com.model.WarehouseLibrary;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;

/**
 * 仓库处理
 * @author xujinbo
 *
 */
public class WarehouseServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	
	/**
	 * 添加物品
	 * @param buf
	 * @param session
	 */
	public void addProp(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			int warehouseIndex = gameInput.readInt();//仓库位置
			int index = gameInput.readInt();// 位置
			int count = gameInput.readInt();// 数量
			Backpack backpack = userInfo.getBackpack();
			Prop prop = backpack.getPropIndex(index); // 此位置物品
			if (prop == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (count == 0 || count > prop.getCount()) {
				gameOutput.writeInt(CommonCmd.PROP_COUNT_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			WarehouseLibrary warehouse = userInfo.getWarehouseLibrary();
			if (!warehouse.checkEmpty()) {
				return;
			}
			backpack.removePropIndexCount(index, count);
			Prop newProp = BytesFactory.deepCopy(prop);
			newProp.setCount(count);
			warehouse.addIndexProp(warehouseIndex, newProp);
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
			info.setWarehouseByte(BytesFactory.objectToByteData(userInfo.getWarehouseLibrary()));
			
			userServer.update(info);
			newProp.writeData(gameOutput);
			userInfo.sendData(SocketCmd.WAREHOUSE_ADD_PROP, gameOutput.toByteArray());
			
			gameOutput.reset().writeInt(1).writeInt(prop.getIndex()).writeInt(count);
			userInfo.sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("添加物品报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("添加物品关闭流报错", e);
			}
		}
	}

	/**
	 * 更新物品数量
	 * @param buf
	 * @param session
	 */
	public void updatePropCount(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			int backpackIndex = gameInput.readInt();// 包裹位置
			int index = gameInput.readInt();// 位置
			int count = gameInput.readInt();// 放出数量
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			gameOutput = new GameOutput();
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			WarehouseLibrary warehouse = userInfo.getWarehouseLibrary();
			Prop prop = warehouse.getPropIndex(index); // 物品
			if (prop == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (count > prop.getCount()) {
				gameOutput.writeInt(CommonCmd.PROP_COUNT_ERROR);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			Backpack backPack = userInfo.getBackpack();
			if (!backPack.checkEmpty()) {
				gameOutput.writeInt(CommonCmd.BACKPACK_INSUFFICIENT_SPACE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			warehouse.removePropIndexCount(index, count);
			Prop newProp = BytesFactory.deepCopy(prop);
			newProp.setCount(count);
			backPack.addIndexProp(backpackIndex, newProp);
			
			UserInfo info = new UserInfo(userInfo.getId());
			info.setWarehouseByte(BytesFactory.objectToByteData(warehouse));
			info.setBackpackByte(BytesFactory.objectToByteData(backPack));
			userServer.update(info);
			
			userInfo.sendData(SocketCmd.WAREHOUSE_UPDATE_PROP_COUNT, 1, prop.getIndex(), count);
			
			gameOutput.reset().writeInt(1);
			newProp.writeData(gameOutput);
			userInfo.sendData(SocketCmd.ADD_NEW_PROP, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("使用物品报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("使用物品关闭流报错", e);
			}
		}
	}

	/**
	 * 更新物品位置
	 * @param buf
	 * @param session
	 */
	public void updatePropIndex(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			int newIndex = gameInput.readInt();// 新位置
			int index = gameInput.readInt();// 旧位置
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			WarehouseLibrary warehouse = userInfo.getWarehouseLibrary();
			
			Prop newIndexProp = warehouse.getPropIndex(newIndex);// 新位置的物品
			Prop prop = warehouse.getPropIndex(index); // 要换位置的物品
			if (prop == null) {
				gameOutput.writeInt(CommonCmd.PROP_NOT_EXISTENT);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (newIndexProp == null) { // 新位置不存在物品 
				prop.setIndex(newIndex);// 直接设置新位置
			} else {
				// 新位置存在物品 
				if (newIndexProp.getId() == prop.getId()) { // 模版一样  添加数量
					int count = newIndexProp.getMaxCount()-newIndexProp.getCount();// 还可以放多少个进去
					if (count <= 0) {
						return;
					}
					if(prop.getCount() <= count) {
						count = prop.getCount();
					}
					newIndexProp.setCount(newIndexProp.getCount()+count);
					warehouse.removePropIndexCount(prop.getIndex(), count);
				} else { // 更换位置
					newIndexProp.setIndex(index);
					prop.setIndex(newIndex);
				}
			}
			gameOutput.writeInt(index);// 原始位置
			gameOutput.writeInt(newIndex);// 新位置
			userInfo.sendData(SocketCmd.WAREHOUSE_UPDATE_PROP_INDEX, gameOutput.toByteArray());
			userServer.updateWarehouse(userInfo);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("更新物品位置报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("更新物品位置关闭流报错", e);
			}
		}
	}

	/**
	 * 整理物品
	 * @param buf
	 * @param session
	 */
	public void arrangeProp(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		try {
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			SpaceDate spaceDate = userInfo.getSpaceDate();
			if (!spaceDate.checkType(SpaceDate.WAREHOUSE_ARRANGE)) { // 距离上次整理包裹  小于10s
				gameOutput.writeInt(CommonCmd.ACTION_COOLING);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			spaceDate.setValue(SpaceDate.WAREHOUSE_ARRANGE, (int) (System.currentTimeMillis()/1000));
			WarehouseLibrary warehouse = userInfo.getWarehouseLibrary();
			warehouse.arrange();
			warehouse.writeAllPropData(gameOutput);
			userInfo.sendData(SocketCmd.WAREHOUSE_ARRANGE_PROP, gameOutput.toByteArray());
			userServer.updateWarehouse(userInfo);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("整理物品报错", e);
		} finally {
			try {
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("整理物品关闭流报错", e);
			}
		}
	}

	/**
	 * 请求数据
	 * @param buf
	 * @param session
	 */
	public void requestData(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		try {
			gameOutput = new GameOutput();
			UserInfo userInfo = userDataManage.getCacheUserInfo(session);
			if (userInfo == null) {
				gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			WarehouseLibrary warehouse = userInfo.getWarehouseLibrary();
			warehouse.writeAllPropData(gameOutput);
			userInfo.sendData(SocketCmd.WAREHOUSE_DATA, gameOutput.toByteArray());
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("请求数据报错", e);
		} finally {
			try {
				if(gameOutput!=null)gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("请求数据关闭流报错", e);
			}
		}
	}

}
