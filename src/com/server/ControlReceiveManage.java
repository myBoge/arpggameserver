package com.server;

import java.util.Scanner;

import org.apache.log4j.Logger;

import com.main.EscortMain;

/**
 * 控制台输入信息处理器
 * @author xujinbo
 *
 */
public class ControlReceiveManage extends Thread {

	/** 当前线程启动与否 */
	public boolean isOpen = true;
	private Scanner scanner;
	private EscortMain escortMain;
	
	public ControlReceiveManage(EscortMain escortMain) {
		setName("控制台输入信息处理器");
		this.escortMain = escortMain;
	}
	
	@Override
	public void run() {
		scanner = new Scanner(System.in);
		while(isOpen){
			try {
//				// 1s种检查一次
				Thread.sleep(1000);
				checkControl();
			} catch (InterruptedException e) {
				Logger.getLogger(getClass()).fatal("捕捉控制台输入报错!", e);
			}
		}
		
	}
	
	/**
	 * 检查控制台输入信息
	 */
	private void checkControl() {
		try {
			String cmd = scanner.nextLine();
			System.out.println("输入了="+cmd);
			if (cmd.length() >= 0) {
				escortMain.stop();
			}
		} catch (Exception e) {
		}
	}
	
	@Override
	public synchronized void start() {
		super.start();
	}

	/**
	 * 关闭线程
	 */
	public void close() {
		isOpen = false;
	}
	
}
