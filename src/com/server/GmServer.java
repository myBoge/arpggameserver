package com.server;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;

import com.boge.entity.GameInput;
import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.configs.PropConfig;
import com.data.UserDataManage;
import com.entity.Gift;
import com.entity.Prop;
import com.entity.RedeemCode;
import com.entity.Role;
import com.entity.UserInfo;
import com.main.EscortMain;
import com.net.AttrEnum;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.BytesFactory;
import com.utils.LuaUtils;
import com.utils.StringUtils;
import com.utils.VerificationUtils;

/**
 * gm处理
 * @author xujinbo
 *
 */
public class GmServer {

	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	@Resource
	private EscortMain escortMain;
	@Resource
	private GiftServer giftServer;
	@Resource
	private PropConfig propConfig;
	@Resource
	private RedeemCodeServer redeemCodeServer;
	/** 成功登录的号 (生成的验证值) */
	private List<String> verificationList = new ArrayList<>();
	
	public GmServer() {
	}
	
	/**
	 * gm登录
	 * @param buf
	 * @param session
	 */
	public void gmLogin(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		LuaValue luaValue;
		try {
			gameInput = new GameInput(buf);;
			String name = gameInput.readUTF().trim();
			String psd = gameInput.readUTF().trim();
			
			luaValue = LuaUtils.get("loginGm").call(LuaValue.valueOf(name), LuaValue.valueOf(psd));
			boolean result = luaValue.toboolean();
			gameOutput = new GameOutput();
			if (result) {
				gameOutput.writeInt(1);
				String cmd = getCmd();
				verificationList.add(cmd);
				session.setAttribute(AttrEnum.GM_LOGIN, cmd);
			} else {
				gameOutput.writeInt(2);
			}
			session.write(new ActionData<>(SocketCmd.GM_LOGIN, gameOutput.toByteArray()));
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("gm登录报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("gm登录关闭流报错", e);
			}
		}
	}

	/**
	 * 查询用户数据
	 * @param buf
	 * @param session
	 */
	public void gmQueryUserInfo(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			if(!checkLogin(session)) return;
			gameInput = new GameInput(buf);;
			String name = gameInput.readUTF().trim();
			UserInfo userInfo = userDataManage.getUserInfoData(name);
			gameOutput = new GameOutput();
			if (userInfo == null || name.length() == 0) {
				gameOutput.writeInt(0);
				session.write(new ActionData<>(SocketCmd.GM_QUERY_USERINFO, gameOutput.toByteArray()));
				return;
			}
			gameOutput.writeInt(1);
			gameOutput.write(
					userInfo.getId(),
					userInfo.getRoleName(),
					userInfo.getCurrentScene(),
					userInfo.getCurrentSceneValue(),
					userInfo.getTeamId(),
					userInfo.getPlayerGold(),
					userInfo.getLockGold(),
					userInfo.getRmb(),
					userInfo.getLockrmb(),
					userInfo.getMood(),
					userInfo.getOnlineState(),
					userInfo.getPotential(),
					userInfo.getRole().getBlood(),
					userInfo.getRole().getMagic(),
					userInfo.getRole().getExperience(),
					userInfo.getRole().getLevel(),
					userInfo.getRole().getState(),
					userInfo.getRole().getStrength()
				);
			session.write(new ActionData<>(SocketCmd.GM_QUERY_USERINFO, gameOutput.toByteArray()));
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("查询用户数据报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("查询用户数据关闭流报错", e);
			}
		}
	}

	/**
	 * 更改玩家数据
	 * @param buf
	 * @param session
	 */
	public void gmUpdateUserInfo(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			if(!checkLogin(session)) return;
			gameInput = new GameInput(buf);;
			int id = gameInput.readInt();
			String fileName = gameInput.readUTF().trim();
			String value = gameInput.readUTF().trim();
			
			UserInfo userInfo = userDataManage.getUserInfoData(id);
			gameOutput = new GameOutput();
			if (userInfo == null) {
//				gameOutput.writeInt(0);
//				session.write(new ActionData<>(SocketCmd.GM_QUERY_USERINFO, gameOutput.toByteArray()));
				return;
			}
			if (!userInfo.isIdle()) {
				gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
				return;
			}
			if (fileName.equals("role.level")) {
				if(Integer.parseInt(value) < userInfo.getRole().getLevel()) {
					return;
				}
			}
			if(!checkValue(userInfo, fileName, value)) {
				System.out.println("赋值失败！");
				return;
			}
			UserInfo info = new UserInfo(userInfo.getId());
			info.setRoleName(userInfo.getRoleName());
			
			userInfo.getRole().setName(userInfo.getRoleName());
			
			info.setCurrentScene(userInfo.getCurrentScene());
			info.setCurrentSceneValue(userInfo.getCurrentSceneValue());
			info.setTeamId(userInfo.getTeamId());
			info.setPlayerGold(userInfo.getPlayerGold());
			info.setLockGold(userInfo.getLockGold());
			info.setRmb(userInfo.getRmb());
			info.setLockrmb(userInfo.getLockrmb());
			info.setMood(userInfo.getMood());
			info.setOnlineState(userInfo.getOnlineState());
			info.setPotential(userInfo.getPotential());
			info.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
			userServer.update(info);
			userInfo.getRole().castAtt();
//			session.write(new ActionData<>(SocketCmd.GM_QUERY_USERINFO, gameOutput.toByteArray()));
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("更改玩家数据报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("更改玩家数据关闭流报错", e);
			}
		}
		
	}
	
	/**
	 * 添加兑换码数据
	 * @param buf
	 * @param session
	 */
	public void gmAddRedeemCode(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			if(!checkLogin(session)) return;
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			int type = gameInput.readInt();// 类型
			long giftId = gameInput.readInt();//礼包id
			Gift gift = giftServer.getGift(giftId);
			if (gift == null) {
				gameOutput.writeUTF("礼包不存在!");
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE_TXT, gameOutput.toByteArray()));
				return;
			}
			String code;
			int len = gameInput.readInt();
			for (int i = 0; i < len; i++) {
				code = gameInput.readUTF();// 兑换码
				redeemCodeServer.addRedeemCode(type, code, giftId);
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("更改玩家数据报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("更改玩家数据关闭流报错", e);
			}
		}
	}
	
	/**
	 * 添加一个礼包
	 * @param buf
	 * @param session
	 */
	public void gmAddGift(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			if(!checkLogin(session)) return;
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			int giftType = gameInput.readInt();//礼包类型
			int len = gameInput.readInt();
			if(giftServer.checkType(giftType)) {
				gameOutput.writeUTF("此类型的礼包已经存在!");
				session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE_TXT, gameOutput.toByteArray()));
				return;
			}
			Gift gift = new Gift();
			gift.setGiftType(giftType);
			long id;
			int count;
			Prop prop;
			for (int i = 0; i < len; i++) {
				id = gameInput.readInt();//物品id
				count = gameInput.readInt();//物品数量
				prop = propConfig.getSample(id);
				if (prop != null) {
					prop.setCount(count);
					prop.setTrade(false);
					gift.getGiftList().add(prop);
				}
			}
			if (!gift.getGiftList().isEmpty()) {
				giftServer.addGift(gift);
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("更改玩家数据报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("更改玩家数据关闭流报错", e);
			}
		}
	}
	
	/**
	 * 查询所有礼包
	 * @param buf
	 * @param session
	 */
	public void gmSeeGift(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			if(!checkLogin(session)) return;
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			int pageState = gameInput.readInt();//礼包开始位置
			int pageEnd = gameInput.readInt();//礼包分页
			
			List<Gift> giftList = giftServer.getGiftPage(pageState, pageEnd*1000000);
			
			gameOutput.writeInt(giftList.size());
			Gift gift;
			List<Prop> props;
			for (int i = 0; i < giftList.size(); i++) {
				gift = giftList.get(i);
				gameOutput.writeLong(gift.getId());
				gameOutput.writeInt(gift.getGiftType());
				props = gift.getGiftList();
				gameOutput.writeInt(props.size());
				for (int j = 0; j < props.size(); j++) {
					gameOutput.writeUTF(props.get(j).getName());
					gameOutput.writeInt(props.get(j).getCount());
				}
			}
			session.write(new ActionData<>(SocketCmd.GM_SEE_GIFT, gameOutput.toByteArray()));
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("更改玩家数据报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("更改玩家数据关闭流报错", e);
			}
		}
	}
	
	/**
	 * 查询所有兑换码
	 * @param buf
	 * @param session
	 */
	@SuppressWarnings("unused")
	public void gmSeeRedeemCode(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			if(!checkLogin(session)) return;
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			int pageState = gameInput.readInt();//礼包开始位置
			int pageEnd = gameInput.readInt();//礼包分页
			
			RedeemCode redeem = redeemCodeServer.getRedeem(CommonCmd.ORDINARY_REDEEM);
			if (redeem == null) {
				return;
			}
			List<String> code = redeem.getCodeList();
			
//			if (pageState >= code.size()) {
//				return;
//			}
//			
//			List<String> codeList = new ArrayList<>();
//			int end = pageState+pageEnd;
//			for (int i = pageState; i < code.size(); i++) {
//				if (i < end) {
//					codeList.add(code.get(i));
//				}
//			}
			gameOutput.writeLong(redeem.getGiftId());
			gameOutput.writeInt(redeem.getRedeemType());
			gameOutput.writeInt(code.size());
			for (int i = 0; i < code.size(); i++) {
				gameOutput.writeUTF(code.get(i));
			}
			session.write(new ActionData<>(SocketCmd.GM_SEE_REDEEM_CODE, gameOutput.toByteArray()));
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("更改玩家数据报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("更改玩家数据关闭流报错", e);
			}
		}
	}
	
	/**
	 * 清理兑换码
	 * @param buf
	 * @param session
	 */
	public void gmClearRedeemCode(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			if(!checkLogin(session)) return;
			gameInput = new GameInput(buf);
			int type = gameInput.readInt();// 类型
			redeemCodeServer.clearRedeemCode(type);
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("更改玩家数据报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("更改玩家数据关闭流报错", e);
			}
		}
	}
	
	/**
	 * 当前在线人数
	 * @param buf
	 * @param session
	 */
	public void gmOnLineCount(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		try {
			if(!checkLogin(session)) return;
			gameOutput = new GameOutput();
			gameOutput.writeInt(userDataManage.getOnLineUserInfo().size());
			session.write(new ActionData<>(SocketCmd.GM_ON_LINE_COUNT, gameOutput.toByteArray()));
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("当前在线人数报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("当前在线人数关闭流报错", e);
			}
		}
	}
	
	/**
	 * 分页查询所有在线玩家
	 * @param buf
	 * @param session
	 */
	public void gmQueryAllPage(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		GameOutput gameOutput = null;
		try {
			if(!checkLogin(session)) return;
			gameInput = new GameInput(buf);
			gameOutput = new GameOutput();
			int page = gameInput.readInt();// 类型
			int limit = gameInput.readInt();// 类型
			int start = page*limit;//起始数据
			if (start < 0) {
				return;
			}
			List<UserInfo> onList = userDataManage.getOnLineUserInfo();
			List<UserInfo> lists = new ArrayList<>();
			for (int i = start; i < onList.size(); i++) {
				if (lists.size() >= limit) {
					break;
				}
				lists.add(onList.get(i));
			}
			gameOutput.writeInt(onList.size());//总页数
			gameOutput.writeInt(lists.size());
			UserInfo userInfo;
			for (int i = 0; i < lists.size(); i++) {
				userInfo = lists.get(i);
				gameOutput.write(
						userInfo.getId(),
						userInfo.getRoleName(),
						userInfo.getCurrentScene(),
						userInfo.getCurrentSceneValue(),
						userInfo.getTeamId(),
						userInfo.getPlayerGold(),
						userInfo.getLockGold(),
						userInfo.getRmb(),
						userInfo.getLockrmb(),
						userInfo.getMood(),
						userInfo.getOnlineState(),
						userInfo.getPotential(),
						userInfo.getRole().getBlood(),
						userInfo.getRole().getMagic(),
						userInfo.getRole().getExperience(),
						userInfo.getRole().getLevel(),
						userInfo.getRole().getState(),
						userInfo.getRole().getStrength()
					);
			}
			session.write(new ActionData<>(SocketCmd.GM_QUERY_ALL_PAGE, gameOutput.toByteArray()));
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("分页查询所有在线玩家报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("分页查询所有在线玩家关闭流报错", e);
			}
		}
	}
	
	/**
	 * 查询玩家宠物数据
	 * @param buf
	 * @param session
	 */
	public void gmQueryPetData(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		GameInput gameInput = null;
		try {
			if(!checkLogin(session)) return;
			gameOutput = new GameOutput();
			gameInput = new GameInput(buf);
			String name = gameInput.readUTF().trim();
			UserInfo userInfo = userDataManage.getUserInfoData(name);
			if (userInfo == null || name.length() == 0) {
				gameOutput.writeInt(0);
				session.write(new ActionData<>(SocketCmd.GM_QUERY_USERINFO, gameOutput.toByteArray()));
				return;
			}
			userInfo.getPetLibrarys().writeData(gameOutput);
			session.write(new ActionData<>(SocketCmd.GM_QUERY_PET_DATA, gameOutput.toByteArray()));
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("查询玩家宠物数据报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("查询玩家宠物数据关闭流报错", e);
			}
		}
	}
	
	/**
	 * GM玩家更新宠物数据
	 * @param buf
	 * @param session
	 */
	public void gmUpdatePetData(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		GameInput gameInput = null;
		try {
			if(!checkLogin(session)) return;
			gameOutput = new GameOutput();
			gameInput = new GameInput(buf);
			long id = gameInput.readInt();
			long petId = gameInput.readInt();
			String type = gameInput.readUTF().trim();
			String value = gameInput.readUTF().trim();
			UserInfo userInfo = userDataManage.getUserInfoData(id);
			if (userInfo == null) {
				return;
			}
			Role role = userInfo.getPetLibrarys().getRole(petId);
			if (role == null) {
				return;
			}
			switch (type) {
				case "level":
					int level = Integer.parseInt(value);
					if (role.getLevel() >= level) {
						return;
					}
					role.setLevel(level);
					role.castAtt();
					break;
				case "name":
					role.setName(value);
					break;
				case "strength":
					int strength = Integer.parseInt(value);
					role.setStrength(strength);
					break;
			}
			UserInfo info = new UserInfo(userInfo.getId());
			info.setPetByte(BytesFactory.objectToByteData(userInfo.getPetLibrarys()));
			userServer.update(info);
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("查询玩家宠物数据报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("查询玩家宠物数据关闭流报错", e);
			}
		}
	}

	/**
	 * lua指令
	 * @param buf
	 * @param session
	 */
	public void gmLuaCmd(byte[] buf, IoSession session) {
		GameOutput gameOutput = null;
		GameInput gameInput = null;
		try {
			if(!checkLogin(session)) return;
			gameOutput = new GameOutput();
			gameInput = new GameInput(buf);
			int type = gameInput.readInt();// 类型
			String script = gameInput.readUTF().trim(); // 内容
			
			if (type == 0) { // 自定义指令
				if (script.equals("stop server")) {//关闭服务器
					escortMain.stop();
				}
			} else if (type == 1) { // lua脚本
				LuaValue c = LuaUtils.getCommon().load(script, "script");
				c.call();
			}
			
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("lua指令数据报错", e);
		} finally {
			try {
				if(gameOutput!=null) gameOutput.close();
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("lua指令数据关闭流报错", e);
			}
		}
	}
	
	/**
	 * 给指定的方法赋值 
	 * @param userInfo
	 * @param fileName
	 * @param value 
	 */
	private boolean checkValue(UserInfo userInfo, String fileName, String value) {
		String[] names = fileName.split("\\.");
		int len = names.length;
		String name;
		
		Class<?> cls = userInfo.getClass();
		String eqMethodName;
		Method[] methods;
		Method method;
		int methodLen;
		Object methodValue = userInfo;// 方法体获取的对象
		for (int i = 0; i < len; i++) {
			name = names[i];
			methods = cls.getDeclaredMethods();
			methodLen = methods.length;
			for (int j = 0; j < methodLen; j++) {
				eqMethodName = (i+1)>=len?"set":"get";
				method = methods[j];
				if (method.getName().substring(0, 3).equalsIgnoreCase(eqMethodName)) {
					if (method.getName().substring(3).equalsIgnoreCase(name)) {
						try {
							if (eqMethodName.equalsIgnoreCase("set")) {
								Parameter[] parameters = method.getParameters();
								int parametersLen = parameters.length;
								Object[] objs = null;
								if (parametersLen > 0) {
									objs = new Object[parametersLen];
								}
								for (int i2 = 0; i2 < parametersLen; i2++) {
									objs[i2] = StringUtils.changeValueType(parameters[i2].getType(), value);
								}
								method.invoke(methodValue, objs);
								return true;
							} else {
								methodValue = method.invoke(methodValue);
								if (methodValue == null) {
									return false;
								}
								cls = methodValue.getClass();
							}
							break;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			
		}
		return false;
	}

	
	/**
	 * 检查登录
	 * @param session
	 * @return
	 */
	private boolean checkLogin(IoSession session) {
		if (!session.containsAttribute(AttrEnum.GM_LOGIN)) {
			return false;
		}
		String cmd = (String) session.getAttribute(AttrEnum.GM_LOGIN);
		if (cmd == null || !verificationList.contains(cmd)) {
			return false;
		}
		return true;
	}
	
	public void userOffline(IoSession session) {
		if (session.containsAttribute(AttrEnum.GM_LOGIN)) {
			String cmd = (String) session.removeAttribute(AttrEnum.GM_LOGIN);
			if (cmd != null) {
				verificationList.remove(cmd);
			}
		}
	}
	
	/**
	 * 获取一个不会重复的验证码
	 * @return
	 */
	private String getCmd() {
		String cmd = VerificationUtils.randomString(10);
		if (verificationList.contains(cmd)) {
			return getCmd();
		}
		return cmd;
	}


	


}
