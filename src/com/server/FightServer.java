package com.server;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameInput;
import com.boge.share.Action;
import com.boge.share.Resource;
import com.entity.FightData;
import com.entity.UserInfo;
import com.net.CommonCmd;
import com.server.fight.FightBox;
import com.utils.OnlyIdManage;
import com.utils.ObjectPool;

/**
 * 战斗系统
 * @author xujinbo
 *
 */
public class FightServer {

	@Resource
	private ObjectPool objectPool;
	@Resource
	private UserServer userServer;
	@Resource
	private PetServer petServer;
	@Resource
	private BackpackServer backpackServer;
	@Resource
	private EscortServer escortServer;
	@Resource
	private MessageServer messageServer;
	@Resource
	private GrindServer grindServer;
	@Resource
	private BattlefieldServer battlefieldServer;
	@Resource
	private ArenaServer arenaServer;
	
	/** 当前可以用战斗id */
	private OnlyIdManage idManage;
	private List<FightBox> fightBoxs = new ArrayList<>();
	
	@Action({CommonCmd.INIT_FIGHT})
	protected void init() {
		idManage = new OnlyIdManage();
	}
	
	/**
	 * 创建一场玩家之间的战斗   PK专用
	 * @param robList 攻击方
	 * @param robberyList 被攻击方(防守方)
	 * @param fullStatus 是否全部满状态战斗
	 */
	public FightBox createFight(List<FightData> robList, List<FightData> robberyList, boolean fullStatus) {
		long fightId = idManage.getGrowableId();
		FightBox fightBox = objectPool.getPool(FightBox.class);
		fightBox.setId(fightId);
		fightBox.setFightServer(this);
		fightBox.setTimestamp(new Timestamp(System.currentTimeMillis()));
		fightBox.addUserData(robList, robberyList, fullStatus);
		fightBoxs.add(fightBox);
		return fightBox;
	}
	
	/**
	 * 创建一场玩家和电脑之间的战斗
	 * @param robList 攻击方
	 * @param robberyList 被攻击方(电脑方 and 防守方)
	 * @param fullStatus 是否全部满状态战斗
	 * @return
	 */
	public FightBox createAIFight(List<FightData> robList, List<FightData> robberyList, boolean fullStatus) {
		long fightId = idManage.getGrowableId();
		FightBox fightBox = objectPool.getPool(FightBox.class);
		fightBox.setId(fightId);
		fightBox.setFightServer(this);
		fightBox.setTimestamp(new Timestamp(System.currentTimeMillis()));
		fightBox.addUserAIData(robList, robberyList, fullStatus);
		fightBoxs.add(fightBox);
		return fightBox;
	}

	/**
	 * 开始战斗
	 * @param fightID 要开始战斗的id
	 */
	public void startFight(long fightID) {
		FightBox fightBox = getFightBox(fightID);
		if (fightBox != null) {
			fightBox.start();
		}
	}
	
	
	/**
	 * 玩家做了个决定
	 * @param bs 
	 * @param session 
	 */
	public void userDecision(byte[] buf, IoSession session){
		GameInput gameInput = null;
		try {
			gameInput = new GameInput(buf);
			int count = gameInput.readInt();//动作数量
			int action;
			int fightID;
			int currentRound;
			FightBox fightBox;
			for (int i = 0; i < count; i++) {
				action = gameInput.readInt();// 玩家执行的动作
				fightID = gameInput.readInt(); // 房间号
				currentRound = gameInput.readInt();
				fightBox = getFightBox(fightID);
				if (fightBox != null) {
					if (fightBox.getCurrentRound() == currentRound) {//确定回合正确
						fightBox.userDecision(gameInput, action);
					}
				}
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("玩家做了个决定报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
			}
		}
	}

	/**
	 * 客户端通知服务器  演播结束了
	 * @param buf
	 * @param session
	 */
	public void fightPlayEnd(byte[] buf, IoSession session) {
		GameInput gameInput = null;
		try {
			gameInput = new GameInput(buf);
			long fightID = gameInput.readInt(); // 房间号
			int currentRound = gameInput.readInt();
			long userUid = gameInput.readInt();// 玩家UID
			long petUid = gameInput.readInt();// 玩家宠物UID
			FightBox fightBox = getFightBox(fightID);
			if (fightBox != null) {
				fightBox.fightPlayEnd(currentRound, userUid, petUid);
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("玩家做了个决定报错", e);
		} finally {
			try {
				if(gameInput!=null) gameInput.close();
			} catch (IOException e) {
			}
		}
	}

	/**
	 * 此房间作废   回收
	 * @param fightBox
	 */
	public void gcBox(FightBox fightBox) {
		if (fightBox.getFightType() != 0) {
			switch (fightBox.getFightType()) {
				case CommonCmd.PVP:
					arenaServer.fightEnd(fightBox);
					break;
				case CommonCmd.ROB:
					escortServer.fightEnd(fightBox);
					break;
				case CommonCmd.PK:
					messageServer.fightEnd(fightBox);
					break;
				case CommonCmd.PVE:
					grindServer.fightEnd(fightBox);
					break;
				case CommonCmd.BATTLE_PVE:
					battlefieldServer.fightEnd(fightBox);
					break;
			}
		}
//		System.out.println(fightBox.getFightType());
//		System.out.println(fightBoxs.size());
		fightBoxs.remove(fightBox);
		idManage.gc(fightBox.getId());
//		System.out.println(fightBoxs.size());
		objectPool.gc(FightBox.class, fightBox);
	}

	/**
	 * 检查是否存在此战斗
	 * @param currentFightId
	 * @return 
	 */
	public boolean containsBox(long fightID) {
		return getFightBox(fightID) != null;
	}
	
	/**
	 * 根据战场id 获取战场
	 * @param fightID
	 * @return
	 */
	public FightBox getFightBox(long fightID) {
		FightBox fightBox = null;
		int len = fightBoxs.size();
		for (int i = len-1; i >= 0; i--) {
			fightBox = fightBoxs.get(i);
			if (fightBox.getId() == fightID) {
				break;
			}
			fightBox = null;
		}
		return fightBox;
	}

	/**
	 * 此用户进行重连
	 * @param userInfo
	 */
	public void reconnect(UserInfo userInfo) {
		// 战斗房间id
		long currentFightId = userInfo.getCurrentFightId();
		FightBox fightBox = getFightBox(currentFightId);
		fightBox.reconnect(userInfo);
	}

	/**
	 * 更新此用户的  包裹
	 * @param userInfo
	 */
	public void updateBackpack(UserInfo userInfo) {
		userServer.updateBackpack(userInfo);
	}

	/**
	 * 更新此人角色属性
	 * @param userInfo
	 */
	public void updateUserRole(UserInfo userInfo) {
		userServer.updateUserRole(userInfo);
	}
	
	/**
	 * 更新玩家宠物
	 * @param userInfo
	 */
	public void updatePet(UserInfo userInfo) {
		petServer.updatePet(userInfo);
	}

	/**
	 * 获取backpackServer
	 * @return backpackServer backpackServer
	 */
	public BackpackServer getBackpackServer() {
		return backpackServer;
	}

}
