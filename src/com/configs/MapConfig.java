package com.configs;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import com.entity.EscortMap;
import com.entity.GrindMap;

public class MapConfig extends BaseConfig {

	/** 练级地图 */
	private List<GrindMap> grindMaps;
	/** 押镖地图 */
	private List<EscortMap> escortMaps;
	
	public MapConfig() {
		super("data/sampleConfig/map.xml");
	}
	
	@Override
	protected void doIt() {
		grindMaps = new ArrayList<>();
		escortMaps = new ArrayList<>();
		GrindMap grindMap;
		EscortMap escortMap;
		Object obj;
		for (Element e : list) {
			obj = getXStream().fromXML(e.asXML());
			if (obj instanceof EscortMap) {
				escortMap = (EscortMap) obj;
				escortMaps.add(escortMap);
			} else if (obj instanceof GrindMap) {
				grindMap = (GrindMap) obj;
				grindMaps.add(grindMap);
			}
		}
	}
	
	/**
	 * 根据地图id返回地图对象
	 * @param id
	 * @return
	 */
	public GrindMap getCacheSample(long id) {
		int len = grindMaps.size();
		for (int i = 0; i < len; i++) {
			if(grindMaps.get(i).getId() == id) {
				return grindMaps.get(i);
			}
		}
		len = escortMaps.size();
		for (int i = 0; i < len; i++) {
			if(escortMaps.get(i).getId() == id) {
				return escortMaps.get(i);
			}
		}
		return null;
	}

	/**
	 * 获取练级地图
	 * @return grindMaps 练级地图
	 */
	public List<GrindMap> getGrindMaps() {
		return grindMaps;
	}

	/**
	 * 获取押镖地图
	 * @return escortMaps 押镖地图
	 */
	public List<EscortMap> getEscortMaps() {
		return escortMaps;
	}

}
