package com.configs;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.entity.MissionTask;

/**
 * 剧情配置文件读取
 * @author xujinbo
 *
 */
public class MissionConfig {

	/** 剧情加载缓存 */
	private Map<String, List<Element>> xmlLoad = new HashMap<>();
	
	public MissionConfig() {
		
	}
	
	/**
	 * 获取一个剧情
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MissionTask getMissionXML(String name) {
		List<Element> list = xmlLoad.get(name);
		if (list == null) {
			File file = new File(name);
			if (file.exists()) {
				//创建SAXReader对象  
				SAXReader reader = new SAXReader();  
				//读取文件 转换成Document  
				Document document;
				try {
					document = reader.read(file);
					Element root = document.getRootElement();// 获得根节点
					list = root.elements();
					xmlLoad.put(name, list);
				} catch (DocumentException e) {
					e.printStackTrace();
				}  
			}
		}
//		System.out.println(list);
		if (list != null) {
			MissionTask mission = new MissionTask();;
			mission.parse(list);
			return mission;
		}
		return null;
	}
	
	public static void main(String[] args) {
		MissionConfig missionConfig = new MissionConfig();
		
		missionConfig.getMissionXML("data/mission/1.xml");
		
	}

}
