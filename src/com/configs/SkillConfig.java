package com.configs;

import org.dom4j.Element;

import com.entity.Skill;

/**
 * 技能生成器
 * @author xujinbo
 *
 */
public class SkillConfig extends BaseConfig {

	public SkillConfig() {
		super("data/sampleConfig/skill.xml");
	}
	
	/**
	 * 根据技能id返回技能对象
	 * @param id
	 * @return
	 */
	public Skill getSample(long id) {
		long idValue;
		for (Element e : list) {
			idValue = Long.parseLong(e.attributeValue("id"));
			if (idValue == id) {
				return (Skill) getXStream().fromXML(e.asXML());
			}
		}
		return null;
	}
	
	/**
	 * 判断是否存在此技能
	 * @param id
	 * @return
	 */
	public boolean checkSkill(long id) {
		long idValue;
		for (Element e : list) {
			idValue = Long.parseLong(e.attributeValue("id"));
			if (idValue == id) {
				return true;
			}
		}
		return false;
	}

}
