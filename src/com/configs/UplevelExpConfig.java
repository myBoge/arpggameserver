package com.configs;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import com.entity.UplevelExp;

/**
 * 人物宠物升级经验
 * @author xujinbo
 *
 */
public class UplevelExpConfig extends BaseConfig {

	private List<UplevelExp> lists;
	
	public UplevelExpConfig() {
		super("data/sampleConfig/uplevelExp.xml");
	}
	
	@Override
	protected void doIt() {
		lists = new ArrayList<>();
		UplevelExp uplevelExp;
		for (Element e : list) {
			uplevelExp = (UplevelExp) getXStream().fromXML(e.asXML());
			lists.add(uplevelExp);
		}
	}
	
	/**
	 * 根据等级id返回对象
	 * @param id
	 * @return
	 */
	public UplevelExp getCacheSample(long id) {
		int index = lists.indexOf(new UplevelExp(id));
		if (index != -1) {
			return lists.get(index);
		}
		return null;
	}

}
