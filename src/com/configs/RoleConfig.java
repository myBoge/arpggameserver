package com.configs;

import org.dom4j.Element;

import com.entity.Role;

public class RoleConfig extends BaseConfig {

	public RoleConfig() {
		super("data/sampleConfig/role.xml");
	}
	
	@Override
	protected void doIt() {
	}
	
	/**
	 * 根据地图id返回地图对象
	 * @param id
	 * @return
	 */
	public Role getSample(long id) {
		long idValue;
		for (Element e : list) {
			idValue = Long.parseLong(e.attributeValue("id"));
			if (idValue == id) {
				return (Role) getXStream().fromXML(e.asXML());
			}
		}
		return null;
	}
	
}
