package com.configs;

import org.dom4j.Element;

import com.entity.Prop;

/**
 * 物品生成器
 * @author xujinbo
 *
 */
public class PropConfig extends BaseConfig {

	public PropConfig() {
		super("data/sampleConfig/prop.xml");
	}
	
	/**
	 * 根据地图id返回地图对象
	 * @param id
	 * @return
	 */
	public Prop getSample(long id) {
		long idValue;
		for (Element e : list) {
			idValue = Long.parseLong(e.attributeValue("id"));
			if (idValue == id) {
				return (Prop) getXStream().fromXML(e.asXML());
			}
		}
		return null;
	}

}
