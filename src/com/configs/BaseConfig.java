package com.configs;

import java.io.File;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.entity.Equips;
import com.entity.EscortMap;
import com.entity.GiftProp;
import com.entity.GrindMap;
import com.entity.MapData;
import com.entity.Prop;
import com.entity.Role;
import com.entity.Skill;
import com.entity.UplevelExp;
import com.entity.UseProp;
import com.utils.Xml2Entity;

@SuppressWarnings("unchecked")
public class BaseConfig {

	protected List<Element> list;
	private static Xml2Entity xml2Entity;
	
	public BaseConfig(String name) {
		File file = new File(name);
		 //创建SAXReader对象  
        SAXReader reader = new SAXReader();  
        //读取文件 转换成Document  
        Document document;
		try {
			document = reader.read(file);
			Element root = document.getRootElement();// 获得根节点
			list = root.elements();
			doIt();
		} catch (DocumentException e) {
			e.printStackTrace();
		}  
	}
	
	public static Xml2Entity getXStream() {
		if (xml2Entity == null) {
			xml2Entity = new Xml2Entity();
			//注册使用了注解的VO  
			xml2Entity.parseClass(EscortMap.class, Role.class, Equips.class, GiftProp.class,
	        		Prop.class, UseProp.class, UplevelExp.class, GrindMap.class, Skill.class, MapData.class);
		}
		return xml2Entity;
	}

	protected void doIt() {
		
	}

}
