package com.mapper;

import org.apache.ibatis.annotations.Param;

import com.entity.Report;

public interface ReportMapper extends BaseMapper<Report> {
	
	Report queryType(@Param("reportType") long reportType);
	
	
}
