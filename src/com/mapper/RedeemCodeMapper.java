package com.mapper;

import org.apache.ibatis.annotations.Param;

import com.entity.RedeemCode;

public interface RedeemCodeMapper extends BaseMapper<RedeemCode> {

	RedeemCode queryAllType(@Param("redeemType")long id);
	
}
