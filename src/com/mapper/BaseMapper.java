package com.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BaseMapper<T> {

	void insert(T t);
	
	void update(T t);
	
	T queryId(@Param("id") long id);
	
	/**
	 * 获取所有的数据
	 * @return
	 */
	List<T> queryAll();
	
	/**
	 * 获取分页数据
	 * @return
	 */
	List<T> queryPage(@Param("start") int start, @Param("limit") int limit);
	
	void delete(@Param("id")long id);
	
}
