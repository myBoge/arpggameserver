package com.mapper;

import org.apache.ibatis.annotations.Param;

import com.entity.Gift;

public interface GiftMapper extends BaseMapper<Gift> {

	Gift queryAllType(@Param("giftType")long id);
	
}
