package com.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.entity.Login;

public interface LoginMapper extends BaseMapper<Login> {
	
	List<Login> selected();
	
	Login queryName(@Param("openid") String openid, @Param("serverId") long serverId);
	
}
