package com.mapper;

import org.apache.ibatis.annotations.Param;

import com.entity.UserInfo;

public interface UserMapper extends BaseMapper<UserInfo> {
	
	UserInfo queryName(@Param("name") String name);
	/** 用于检查是否存在 */
	UserInfo checkLogin(@Param("loginId") long loginId);
	
	UserInfo queryLogin(@Param("loginId") long loginId);
	
	void deleteLogin(@Param("loginId")long loginId);
	
}
