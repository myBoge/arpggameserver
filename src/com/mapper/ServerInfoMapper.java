package com.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.entity.ServerInfo;

public interface ServerInfoMapper {

	List<ServerInfo> selected();
	
	ServerInfo queryId(@Param("id") long id);
	
	ServerInfo queryName(@Param("name") String name);
	
	void insert(ServerInfo serverInfo);

	void update(ServerInfo serverInfo);
	
}
