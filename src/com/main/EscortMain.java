package com.main;

import static com.net.SocketCmd.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import com.action.ArenaAction;
import com.action.BackpackAction;
import com.action.BattlefieldAction;
import com.action.BinCodeAction;
import com.action.EquipsAction;
import com.action.EscortAction;
import com.action.FactionAction;
import com.action.FriendAction;
import com.action.GiftAction;
import com.action.GmAction;
import com.action.GrindAction;
import com.action.MailAction;
import com.action.MapAction;
import com.action.MessageAction;
import com.action.PetAction;
import com.action.PluginsAction;
import com.action.RankingListAction;
import com.action.RechargeAction;
import com.action.RedeemCodeAction;
import com.action.ReportAction;
import com.action.ShopAction;
import com.action.SkillAction;
import com.action.TaskAction;
import com.action.TeamAction;
import com.action.UserAction;
import com.action.WarehouseAction;
import com.boge.entity.ActionCmd;
import com.boge.parse.ioByte.IoByteProtocolCodecFilter;
import com.boge.share.Inject;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ServerAcceptor;
import com.boge.socket.SessionListener;
import com.configs.BaseConfig;
import com.configs.MissionConfig;
import com.configs.UplevelExpConfig;
import com.data.UserDataManage;
import com.net.CommonCmd;
import com.server.ControlReceiveManage;
import com.server.EscortServer;
import com.server.GrindServer;
import com.server.RankingListServer;
import com.server.ServerInfoServer;
import com.server.ServerManageServer;
import com.server.fight.FightBox;
import com.thread.SendDataThread;
import com.thread.TimerManager;
import com.utils.DateManage;
import com.utils.ObjectPool;

public class EscortMain extends Inject implements SessionListener {
	
	private ServerAcceptor serverAcceptor;
	/** 控制台输入信息处理器 */
	private ControlReceiveManage controlReceiveManage;
	
	@Resource
	private ObjectPool objectPool;
	@Resource
	private TimerManager timerThread;
	@Resource
	private UserDataManage userDataManage;
	@Resource
	private DateManage timerManage;
	@Resource
	private SendDataThread sendDataThread;
	@Resource
	private ServerManageServer serverManageServer;
	@Resource
	private ServerInfoServer serverInfoServer;
	@Resource
	private EscortServer escortServer;
	@Resource
	private GrindServer grindServer;
	@Resource
	private RankingListServer rankingListServer;
	@Resource
	private UplevelExpConfig uplevelExpConfig;
	@Resource
	private MissionConfig missionConfig;
	
	@Resource
	private UserAction userAction;
	@Resource
	private EscortAction escortAction;
	@Resource
	private BackpackAction backpackAction;
	@Resource
	private ShopAction shopAction;
	@Resource
	private FriendAction friendAction;
	@Resource
	private MailAction mailAction;
	@Resource
	private MessageAction messageAction;
	@Resource
	private ReportAction reportAction;
	@Resource
	private GrindAction grindAction;
	@Resource
	private SkillAction skillAction;
	@Resource
	private EquipsAction equipsAction;
	@Resource
	private RankingListAction rankingListAction;
	@Resource
	private FactionAction factionAction;
	@Resource
	private WarehouseAction warehouseAction;
	@Resource
	private GmAction gmAction;
	@Resource
	private TeamAction teamAction;
	@Resource
	private RedeemCodeAction redeemCodeAction;
	@Resource
	private GiftAction giftAction;
	@Resource
	private ArenaAction arenaAction;
	@Resource
	private BinCodeAction binCodeAction;
	@Resource
	private TaskAction taskAction;
	@Resource
	private PetAction petAction;
	@Resource
	private MapAction mapAction;
	@Resource
	private RechargeAction rechargeAction;
	@Resource
	private PluginsAction pluginsAction;
	@Resource
	private BattlefieldAction battlefieldAction;
	
	/** 已经被出现闲置状态的isSession */
	private Map<Long, Integer> iosIdle = new HashMap<Long, Integer>();
	
	public EscortMain() throws IOException {
		super();
		controlReceiveManage = new ControlReceiveManage(this);
		controlReceiveManage.start();
		// 初始化服务器配置文件
		serverManageServer.init();
		// 配置log4j配置文件
		PropertyConfigurator.configure(serverManageServer.getServerConfig().getLog4jPath());
		// 初始化对象池
		objectPool.addPool(FightBox.class, 300);// 创建300个战斗房间
		// 更改服务器状态
		serverInfoServer.openServer();
		// 创建适配器
		serverAcceptor = new ServerAcceptor(this, new IoByteProtocolCodecFilter());
		// 注册适配器
		serverAcceptor.registAction(binCodeAction, BIND_CODE_DATA);
		serverAcceptor.registAction(userAction, LOGIN, CREATE_ROLE, DELETE_ROLE, THROB,
				SEE_ROLE_INFO, RENAME, ADD_ATT_POINT, UPDATE_AUTO_USE_PROP);
		serverAcceptor.registAction(backpackAction, UPDATE_PROP_INDEX, ARRANGE_PROP, USE_PROP, SELL_PROP);
		serverAcceptor.registAction(warehouseAction, WAREHOUSE_ADD_PROP, WAREHOUSE_DATA,
				WAREHOUSE_UPDATE_PROP_COUNT, WAREHOUSE_UPDATE_PROP_INDEX, WAREHOUSE_ARRANGE_PROP);
		serverAcceptor.registAction(messageAction, ADD_CHAT_INFO, SEND_FRIEND_CHAT, FIGHT_PK, FIGHT_PK_DECIDE);
		serverAcceptor.registAction(shopAction, BUYS_SHOP, STORE_BUYS_SHOP, SHOP_ALL_GOODS, STORE_ALL_GOODS);
		serverAcceptor.registAction(mailAction, ADD_MAIL, DELETE_MAIL, READ_MAIL, REQUEST_MAIL_DATA, EXTRACT_GOODS);
		serverAcceptor.registAction(friendAction, ADD_FRIEND_REQUEST, ADD_FRIEND_RESULT, DELETE_FRIEND);
		serverAcceptor.registAction(escortAction, ENTER_ESCORT, CREATE_ESCORT, STOP_ESCORT, MAP_ROB_ESCORT,
				FIGHT_EXECUTE_ACTION, FIGHT_PLAY_END);
		serverAcceptor.registAction(grindAction, GRIND_START, ENTER_MAP, EXIT_MAP, GRIND_STOP);
		serverAcceptor.registAction(skillAction, UPDATE_SKILL_LEVEL);
		serverAcceptor.registAction(equipsAction, EQUIPS_WEAR, EQUIPS_STRIP, EQUIPS_REFORM, JEWELRY_COMPOSE);
		serverAcceptor.registAction(rankingListAction, RANKING_REQUEST);
		serverAcceptor.registAction(factionAction, FACTION_REQUEST, FACTION_CREATE, FACTION_DISBAND, FACTION_APPLY, 
				FACTION_KICK, FACTION_QUIT, FACTION_BUILD_UP, FACTION_INFO, FACTION_APPLY_DECIDE);
		serverAcceptor.registAction(gmAction, GM_LOGIN, GM_QUERY_USERINFO, GM_UPDATE_USERINFO,
				GM_ADD_REDEEM_CODE, GM_CLEAR_REDEEM_CODE, GM_UPDATE_REDEEM_CODE, GM_ON_LINE_COUNT,
				GM_QUERY_ALL_PAGE, GM_QUERY_PET_DATA, GM_UPDATE_PET_DATA,
				GM_ADD_GIFT, GM_DELETE_GIFT, GM_UPDATE_GIFT, GM_SEE_GIFT, GM_SEE_REDEEM_CODE, GM_LUA_CMD);
		serverAcceptor.registAction(redeemCodeAction, REDEEM_CODE);
		serverAcceptor.registAction(giftAction, GIFT_GET_ON_LINE, RECEIVE_EVERYDAY_SIGN, RECEIVE_LEVEL_GIFT);
		serverAcceptor.registAction(arenaAction, ARENA_REQUEST, ARENA_CHALLENGE);
		serverAcceptor.registAction(teamAction, TEAM_APPLY, TEAM_CREATE, TEAM_FAST, TEAM_FAST_INVITE, 
				TEAM_DISBAND, TEAM_APPLY_DECIDE, TEAM_DELETE_ROLE, TEAM_INVITE, 
				TEAM_INVITE_DECIDE, TEAM_KICKOUT);
		serverAcceptor.registAction(taskAction, DELETE_BUFF_PROP, MISSION_ADD, MISSION_COMPLETE, MISSION_UPDATE);
		serverAcceptor.registAction(reportAction, REPORT_INFO);
		serverAcceptor.registAction(petAction, PET_ADD_ATT_POINT, DELETE_PET, PET_UPDATE_JOINBATTLE, PET_UPDATE_NAME,
				PET_TAME, PET_USE_PROP, WASH_PET, WASH_PET_REPLACE);
		
		serverAcceptor.registAction(rechargeAction, RECHARGE_INFO);
		
		serverAcceptor.registAction(battlefieldAction, BATTLEFIELD_FIGHT);
		
		serverAcceptor.registAction(mapAction, MAP_PLAYER_MOVE, MAP_PLAYER_MOVE_END, 
				MAP_ALL_ROLE_DATA, MAP_CHAT_MSG, MAP_PLAYER_FIGHT, MAP_GRID_CHANGE,
				MAP_ENTER);
		
		timerThread.start();

		// 发送初始化命令
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_ESCORT));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_DATA));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_FIGHT));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_BUYS_SHOP));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_STORE_BUYS_SHOP));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_GRIND));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_GRIND_DATA));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_SKILL_DATA));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_FACTION_DATA));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_GIFG_DATA));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_REDEEM_DATA));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_ARENA_DATA));
		getBaseShareBox().send(new ActionCmd<>(CommonCmd.INIT_MAP_DATA));
		serverAcceptor.getAcceptor().getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 10);
		
		
//		System.out.println(serverAcceptor.getAcceptor().getSessionConfig().getReadBufferSize());
//		System.out.println(serverAcceptor.getAcceptor().getSessionConfig().getMaxReadBufferSize());
//		System.out.println(serverAcceptor.getAcceptor().getSessionConfig().getMinReadBufferSize());
//		System.out.println(serverAcceptor.getAcceptor().getSessionConfig().getReceiveBufferSize());
//		System.out.println(serverAcceptor.getAcceptor().getSessionConfig().getSendBufferSize());
		
		BaseConfig.getXStream();
		
		// 时间管理线程
		timerManage.start();
		// 开启押镖线程
		escortServer.start();
		// 开启练级地图
		grindServer.start();
		// 开启排行榜
		rankingListServer.start();
		// 开启向客户端发送消息的线程
		sendDataThread.start();
		// 开启端口
		serverAcceptor.bind(serverManageServer.getServerConfig().getPort());
		
		Logger.getLogger(getClass()).info("服务器成功启动!");
		
//		Map<Thread, StackTraceElement[]> thread = Thread.getAllStackTraces();
//		Set<Thread> keys = thread.keySet();
//		Iterator<Thread> threads = keys.iterator();
//		while(threads.hasNext()){
//			System.out.println(threads.next());
//		}
		
		
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		new EscortMain();
		System.out.println("服务器已经启动!按任意键+回车关闭");
	}

	public void stop() {
		System.out.println("服务器已关闭");
		timerThread.close();
		controlReceiveManage.close();
		serverInfoServer.closeServer();
		escortServer.stop();
		grindServer.stop();
		timerManage.stop();
		rankingListServer.stop();
		sendDataThread.close();
		serverAcceptor.stop();
		System.exit(0);
	}

	@Override
	public void exceptionCaught(IoSession session, Throwable cause)
			throws Exception {
//		System.out.println(cause);
		if (!(cause instanceof IOException)) {
			Logger.getLogger(getClass()).error("exceptionCaught：", cause);
		} else {
//			Logger.getLogger(getClass()).info("网络Id="+session.getId()+"强制断开, 还剩玩家");
			iosIdle.remove(session.getId());
			gmAction.userOffline(session);
			userAction.userOffline(session);
		}
	}
	
	@Override
	public void messageSent(IoSession session, Object cause) throws Exception {
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void notRegAction(IoSession session, Object message)
			throws Exception {
		ActionData data = (ActionData<Object>) message;
		Logger.getLogger(getClass()).warn("动作处理器["+data.getAction()+"]未注册"+" 原始内容:"+data);
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
//		Logger.getLogger(getClass()).info("网络Id=["+session.getId()+"]断开, 还剩玩家");
		iosIdle.remove(session.getId());
		gmAction.userOffline(session);
		userAction.userOffline(session);
	}

	@Override
	public void sessionCreated(IoSession session) throws Exception {
//		Logger.getLogger(getClass()).info("新连接网络Id=["+session.getId()+"], 现有玩家");
	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus status) {
		if (iosIdle.containsKey(session.getId())) {
			Integer count = iosIdle.get(session.getId());
			if (count < 2) {
				count++;
				session.write(new ActionData<>(THROB));
				iosIdle.put(session.getId(), count);
			} else {
				iosIdle.remove(session.getId());
				session.close(false);
				Logger.getLogger(getClass()).info("有用户空闲被关闭=["+session.getId()+" , 查询的数据="+userDataManage.getCacheUserInfo(session)+"]");
			}
		} else {
			iosIdle.put(session.getId(), 1);
		}
	}

	@Override
	public void sessionOpened(IoSession session) {
	}

//	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void messageReceived(IoSession session, Object message)  {
		iosIdle.remove(session.getId());
//		ActionData data = (ActionData<Object>) message;
//		System.out.println(data.getAction());
//		if (data.getAction() != 1) {
//			System.out.println("messageReceived="+data);
//		}
	}

	/**
	 * 获取serverAcceptor
	 * @return serverAcceptor serverAcceptor
	 */
	public ServerAcceptor getServerAcceptor() {
		return serverAcceptor;
	}
	

}
