package com.core;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.entity.MapData;

public class SilzAstar {

	/**
	 * 寻路方式，8方向和4方向，有效值为8和4
	 */ 
	private static int WorkMode = 8;
	
	private Grid _grid;
	private List<Node> _path;
	private AStar astar;
	private int rows;
	private int cols;

	public SilzAstar(int[][] mapdata) {
		makeGrid(mapdata);
	}

	private void makeGrid(int[][] data) {
		rows = data.length;
		cols = data[0].length;
		_grid = new Grid(cols, rows);
		
		int px;
		int py;
		
		for(py=0;py<rows;py++)
		{
			for(px=0;px<cols;px++)
			{
				_grid.setWalkable(px,py,data[py][px]==0);
			}
		}

		if (WorkMode==4)
			_grid.calculateLinks(1);
		else
			_grid.calculateLinks();
		
		astar = new AStar(_grid);
		
	}
	
	public Grid getGrid() {
		return _grid;
	}

	public void setWORKMODE(int v) {
		if(v!=8 && v!=4) 
			throw new Error("仅支持8方向或4方向寻路");
	}
	
	/**
	 * @param		xnow	当前坐标X(世界坐标)
	 * @param		ynow	当前坐标Y(世界坐标)
	 * @param		xpos	目标点X(世界坐标)
	 * @param		ypos	目标点Y(世界坐标)
	 */ 
	public List<Node> find(int xnow, int ynow, int xpos, int ypos) {
		xpos = (int)(xpos / MapData.tileWidth);
		ypos = (int)(ypos / MapData.tileHeight);
		xpos = Math.min(xpos, _grid.getNumCols() - 1);
		ypos = Math.min(ypos, _grid.getNumRows() - 1);
		_grid.setEndNode(xpos, ypos); //1
		
		xnow = (int)(xnow / MapData.tileWidth);
		ynow = (int)(ynow / MapData.tileHeight);

		_grid.setStartNode(xnow, ynow); //2
		if( _grid.getEndNode().walkable == false ) {
			Node replacer = findReplacer(_grid.getStartNode(), _grid.getEndNode());
			if( replacer != null ) {
				xnow = replacer.x;
				ynow = replacer.y;
			}
			_grid.setEndNode(xnow, ynow);
		}

		if (astar.findPath()){ //3
			astar.floyd();
			_path = astar.getFloydPath();
			return _path;
		}
		return null;
	}
	
	/** 此点附近的点 */
	public Point findRound(int startX, int startY, int endX, int endY) {
		
		endX = (int)(endX / MapData.tileWidth);
		endY = (int)(endY / MapData.tileHeight);
		endX = Math.min(endX, _grid.getNumCols() - 1);
		endY = Math.min(endY, _grid.getNumRows() - 1);
		_grid.setEndNode(endX, endY); //1
		
		startX = (int)(startX / MapData.tileWidth);
		startY = (int)(startY / MapData.tileHeight);
		_grid.setStartNode(startX, startY); //2
		_grid.getEndNode().getDistanceTo(_grid.getStartNode());
		Node end = checkRound(_grid.getEndNode());
//		trace("SilzAstar.findRound(startX, startY, endX, endY)", _grid.endNode);
		return new Point(end.x*MapData.tileWidth, end.y*MapData.tileHeight);
		
	}
	
	private Node checkRound(Node endNode) {
		Node nodeEnd = endNode;
		Node node;
		if(endNode.x+2<_grid.getNumCols()) {
			node = _grid.getNode(endNode.x+2, endNode.y);
			node.getDistanceTo(_grid.getStartNode());
//			trace("SilzAstar.checkRound(endNode)1", nodeEnd, node, nodeEnd.distance, node.distance);
			if (nodeEnd.distance > node.distance) {
				nodeEnd = node;
			}
		}
		if(endNode.x-2<_grid.getNumCols()) {
			node = _grid.getNode(endNode.x-2, endNode.y);
			node.getDistanceTo(_grid.getStartNode());
//			trace("SilzAstar.checkRound(endNode)2", nodeEnd, node, nodeEnd.distance, node.distance);
			if (nodeEnd.distance > node.distance) {
				nodeEnd = node;
			}
		}
		if(endNode.y+2<_grid.getNumRows()) {
			node = _grid.getNode(endNode.x, endNode.y+2);
			node.getDistanceTo(_grid.getStartNode());
//			trace("SilzAstar.checkRound(endNode)3", nodeEnd, node, nodeEnd.distance, node.distance);
			if (nodeEnd.distance > node.distance) {
				nodeEnd = node;
			}
		}
		if(endNode.y-2<_grid.getNumRows()) {
			node = _grid.getNode(endNode.x, endNode.y-2);
			node.getDistanceTo(_grid.getStartNode());
//			trace("SilzAstar.checkRound(endNode)4", nodeEnd, node, nodeEnd.distance, node.distance);
			if (nodeEnd.distance > node.distance) {
				nodeEnd = node;
			}
		}
		return nodeEnd;
	}
	
	/**
	 * 当终点不可移动时寻找一个离原终点最近的可移动点来替代之
	 * @param fromNode
	 * @param toNode
	 * @return 
	 */
	public Node findReplacer(Node fromNode, Node toNode) {
		Node result = null;
		//若终点可移动则根本无需寻找替代点， 否则遍历终点周围节点以寻找离起始点最近一个可移动点作为替代点
		if(toNode.walkable) {
			result = toNode;
		} else {
			//根据节点的埋葬深度选择遍历的圈
			if( toNode.buriedDepth == -1 ) {//若该节点是第一次遍历，则计算其埋葬深度
				toNode.buriedDepth = getNodeBuriedDepth(toNode, Math.max(cols, rows));
			}
			int xFrom = toNode.x - toNode.buriedDepth < 0 ? 0 : toNode.x - toNode.buriedDepth;
			int xTo = toNode.x + toNode.buriedDepth > cols - 1 ? cols - 1 : toNode.x + toNode.buriedDepth;
			int yFrom = toNode.y - toNode.buriedDepth < 0 ? 0 : toNode.y - toNode.buriedDepth;
			int yTo = toNode.y + toNode.buriedDepth > rows - 1 ? rows - 1 : toNode.y + toNode.buriedDepth;		
			
			Node n;//当前遍历节点
			
			for(int i=xFrom; i<=xTo; i++) {
				for(int j=yFrom; j<=yTo; j++) {
					if( (i>xFrom && i<xTo) && (j>yFrom && j<yTo) ) {
						continue;
					}
					n = _grid.getNode(i, j);
					if( n.walkable ) {
						//计算此候选节点到起点的距离，记录离起点最近的候选点为替代点
						n.getDistanceTo(fromNode);
						if( result==null) {
							result = n;
						} else if( n.distance < result.distance ) {							
							result = n;
						}
					}
				}
			}
			
		}
		return result;
	}
	
	/**
	 * 计算一个节点的埋葬深度 
	 * @param node 欲计算深度的节点
	 * @param loopCount 计算深度时遍历此节点外围圈数
	 * @return 
	 * 
	 */
	private int getNodeBuriedDepth(Node node, int loopCount) {
		//如果检测节点本身是不可移动的 ，则默认它的深度为1
		int result = node.walkable ? 0 : 1;
		int l = 1;
		
		while( l <= loopCount ) {
			int startX = node.x - l < 0 ? 0 : node.x - l;
			int endX = node.x + l > cols - 1 ? cols - 1 : node.x + l;
			int startY = node.y - l < 0 ? 0 : node.y - l;
			int endY = node.y + l > rows - 1 ? rows - 1 : node.y + l;		
			
			Node n;
			//遍历一个节点周围一圈看是否周围一圈全部是不可移动点，若是，则深度加一，
			//否则返回当前累积的深度值
			for(int i = startX; i <= endX; i++) {
				for(int j = startY; j <= endY; j++) {
					n = _grid.getNode(i, j);
					if( n != node && n.walkable ) {
						return result;
					}
				}
			}
			//遍历完一圈，没发现一个可移动点，则埋葬深度加一。接着遍历下一圈
			result++;
			l++;
		}
		return result;
	}
	
}



class AStar {
	//private var _open:Array;
	private BinaryHeap _open;
	private Grid _grid;
	private Node _endNode;
	private Node _startNode;
	private List<Node> _path;
	private List<Node> _floydPath;
	private double _straightCost = 1.0;
	private double _diagCost = Math.sqrt(2);
	private int nowversion = 1;
	
	
	public AStar(Grid grid){
		this._grid = grid;
	}
	
	public Function justMin = new Function() {
		@Override
		public boolean function(Object x, Object y) {
			return ((Node)x).f < ((Node)y).f;
		}
	};
	
	public boolean findPath() {
		_endNode = _grid.getEndNode();
		nowversion++;
		_startNode = _grid.getStartNode();
		//_open = [];
		_open = new BinaryHeap(justMin);
		_startNode.g = 0;
		return search();
	}
	
	public void floyd() {
		if (_path == null)
			return;
		_floydPath = new ArrayList<>();
		_floydPath.addAll(_path);
		int len = _floydPath.size();
		if (len > 2){
			Node vector = new Node(0, 0);
			Node tempVector = new Node(0, 0);
			floydVector(vector, _floydPath.get(len-1), _floydPath.get(len-2));
			for (int i = _floydPath.size() - 3; i >= 0; i--){
				floydVector(tempVector, _floydPath.get(i+1), _floydPath.get(i));
				if (vector.x == tempVector.x && vector.y == tempVector.y){
					_floydPath.remove(i+1);
				} else {
					vector.x = tempVector.x;
					vector.y = tempVector.y;
				}
			}
		}
		len = _floydPath.size();
		for (int i = len - 1; i >= 0; i--){
			for (int j = 0; j <= i - 2; j++){
				if (floydCrossAble(_floydPath.get(i), _floydPath.get(j))){
					for (int k = i - 1; k > j; k--){
						_floydPath.remove(k);
					}
					i = j;
					len = _floydPath.size();
					break;
				}
			}
		}
	}
	
	private boolean floydCrossAble(Node n1, Node n2) {
		List<Point> ps = bresenhamNodes(new Point(n1.x, n1.y), new Point(n2.x, n2.y));
		for (int i = ps.size() - 2; i > 0; i--){
			if (!_grid.getNode(ps.get(i).x, ps.get(i).y).walkable){
				return false;
			}
		}
		return true;
	}
	
	@SuppressWarnings("unused")
	private List<Point> bresenhamNodes(Point p1, Point p2) {
		boolean steep = Math.abs(p2.y - p1.y) > Math.abs(p2.x - p1.x);
		if (steep){
			int temp = p1.x;
			p1.x = p1.y;
			p1.y = temp;
			temp = p2.x;
			p2.x = p2.y;
			p2.y = temp;
		}
		int stepX = p2.x > p1.x ? 1 : (p2.x < p1.x ? -1 : 0);
		int stepY = p2.y > p1.y ? 1 : (p2.y < p1.y ? -1 : 0);
		double deltay = (p2.y - p1.y) / Math.abs(p2.x - p1.x);
		List<Point> ret = new ArrayList<>();
		double nowX = p1.x + stepX;
		double nowY = p1.y + deltay;
		if (steep){
			ret.add(new Point(p1.y, p1.x));
		} else {
			ret.add(new Point(p1.x, p1.y));
		}
		while (nowX != p2.x){
			int fy = (int) Math.floor(nowY);
			int cy = (int) Math.ceil(nowY);
			if (steep){
				ret.add(new Point(fy, (int) nowX));
			} else {
				ret.add(new Point((int) nowX, fy));
			}
			if (fy != cy){
				if (steep){
					ret.add(new Point(cy, (int) nowX));
				} else {
					ret.add(new Point((int) nowX, cy));
				}
			}
			nowX += stepX;
			nowY += deltay;
		}
		if (steep){
			ret.add(new Point(p2.y, p2.x));
		} else {
			ret.add(new Point(p2.x, p2.y));
		}
		return ret;
	}
	
	private void floydVector(Node target, Node n1, Node n2) {
		target.x = n1.x - n2.x;
		target.y = n1.y - n2.y;
	}
	
	public boolean search() {
		Node node = _startNode;
		node.version = nowversion;
		while (node != _endNode){
			int len = node.links.size();
			for (int i = 0; i < len; i++){
				Node test = node.links.get(i).node;
				double cost = node.links.get(i).cost;
				double g = node.g + cost;
				double h = euclidian2(test);
				double f = g + h;
				if (test.version == nowversion){
					if (test.f > f){
						test.f = f;
						test.g = g;
						test.h = h;
						test.parent = node;
					}
				} else {
					test.f = f;
					test.g = g;
					test.h = h;
					test.parent = node;
					_open.ins(test);
					test.version = nowversion;
				}
				
			}
			if (_open.a.size() == 1){
				return false;
			}
			node = (Node) _open.pop();
		}
		buildPath();
		return true;
	}
	
	private void buildPath() {
		_path = new ArrayList<>();
		Node node = _endNode;
		_path.add(node);
		while (node != _startNode){
			node = node.parent;
			_path.add(0, node);
		}
	}
	
	public List<Node> getPath() {
		return _path;
	}
	
	public List<Node> getFloydPath() {
		return _floydPath;
	}
	
	public double manhattan(Node node) {
		return Math.abs(node.x - _endNode.x) + Math.abs(node.y - _endNode.y);
	}
	
	public double manhattan2(Node node) {
		double dx = Math.abs(node.x - _endNode.x);
		double dy = Math.abs(node.y - _endNode.y);
		return dx + dy + Math.abs(dx - dy) / 1000;
	}
	
	public double euclidian(Node node) {
		double dx = node.x - _endNode.x;
		double dy = node.y - _endNode.y;
		return Math.sqrt(dx * dx + dy * dy);
	}
	
	private double TwoOneTwoZero = 2 * Math.cos(Math.PI / 3);
	
	public double chineseCheckersEuclidian2(Node node) {
		int y = (int) (node.y / TwoOneTwoZero);
		int x = node.x + node.y / 2;
		double dx = x - _endNode.x - _endNode.y / 2;
		double dy = y - _endNode.y / TwoOneTwoZero;
		return sqrt(dx * dx + dy * dy);
	}
	
	private double sqrt(double x) {
		return Math.sqrt(x);
	}
	
	public double euclidian2(Node node) {
		double dx = node.x - _endNode.x;
		double dy = node.y - _endNode.y;
		return dx * dx + dy * dy;
	}
	
	public double diagonal(Node node) {
		double dx = Math.abs(node.x - _endNode.x);
		double dy = Math.abs(node.y - _endNode.y);
		double diag = Math.min(dx, dy);
		double straight = dx + dy;
		return _diagCost * diag + _straightCost * (straight - 2 * diag);
	}
}


interface Function {
	
	boolean function(Object x, Object y);
	
}

class BinaryHeap {
	public List<Object> a = new ArrayList<>();
	public Function justMinFun = new Function() {
		@Override
		public boolean function(Object x, Object y) {
			return (double)x < (double)y;
		}
	};
	
	public BinaryHeap(){
		this(null);
	}
	
	public BinaryHeap(Function justMinFun){
		a.add(-1);
		if (justMinFun != null)
			this.justMinFun = justMinFun;
	}
	
	public void ins(Object value) {
		int p = a.size();
		a.add(value);
		int pp = p >> 1;
		while (p > 1 && justMinFun.function(a.get(p), a.get(pp))){
			Object temp = a.get(p);
			a.set(p, a.get(pp));
			a.set(pp, temp);
			p = pp;
			pp = p >> 1;
		}
	}
	
	public Object pop() {
		Object min = a.get(1);
		a.set(1, a.get(a.size()-1));
		a.remove(a.size()-1);
		int p = 1;
		int l = a.size();
		int sp1 = p << 1;
		int sp2 = sp1 + 1;
		int minp;
		Object temp;
		while (sp1 < l){
			if (sp2 < l){
				minp = justMinFun.function(a.get(sp2), a.get(sp1)) ? sp2 : sp1;
			} else {
				minp = sp1;
			}
			if (justMinFun.function(a.get(minp), a.get(p))){
				temp = a.get(p);
				a.set(p, a.get(minp));
				a.set(minp, temp);
				p = minp;
				sp1 = p << 1;
				sp2 = sp1 + 1;
			} else {
				break;
			}
		}
		return min;
	}
}

class Grid {
	
	private Node _startNode;
	private Node _endNode;
	private Node[][] _nodes;
	private int _numCols;
	private int _numRows;
	
	private int type;
	
	private double _straightCost = 1.0;
	private double _diagCost = Math.sqrt(2);
	
	public Grid(int numCols, int numRows){
		_numCols = numCols;
		_numRows = numRows;
		_nodes = new Node[_numCols][_numRows];
		
		for (int i = 0; i < _numCols; i++){
			_nodes[i] = new Node[_numRows];
			for (int j = 0; j < _numRows; j++){
				_nodes[i][j] = new Node(i, j);
			}
		}
	}
	
	/**
	 *
	 * @param   type    0四方向 1八方向 2跳棋
	 */
	public void calculateLinks() {
		calculateLinks(0);
	}
	
	/**
	 *
	 * @param   type    0四方向 1八方向 2跳棋
	 */
	public void calculateLinks(int type) {
		this.type = type;
		for (int i = 0; i < _numCols; i++){
			for (int j = 0; j < _numRows; j++){
				initNodeLink(_nodes[i][j], type);
			}
		}
	}
	
	public int getType() {
		return type;
	}
	
	/**
	 *
	 * @param   node
	 * @param   type    0八方向 1四方向 2跳棋
	 */
	private void initNodeLink(Node node, int type) {
		int startX = Math.max(0, node.x - 1);
		int endX = Math.min(_numCols - 1, node.x + 1);
		int startY = Math.max(0, node.y - 1);
		int endY = Math.min(_numRows - 1, node.y + 1);
		node.links = new ArrayList<>();
		for (int i = startX; i <= endX; i++){
			for (int j = startY; j <= endY; j++){
				Node test = getNode(i, j);
				if (test == node || !test.walkable){
					continue;
				}
				if (type != 2 && i != node.x && j != node.y){
					Node test2 = getNode(node.x, j);
					if (!test2.walkable){
						continue;
					}
					test2 = getNode(i, node.y);
					if (!test2.walkable){
						continue;
					}
				}
				double cost = _straightCost;
				if (!((node.x == test.x) || (node.y == test.y))){
					if (type == 1){
						continue;
					}
					if (type == 2 && (node.x - test.x) * (node.y - test.y) == 1){
						continue;
					}
					if (type == 2){
						cost = _straightCost;
					} else {
						cost = _diagCost;
					}
				}
				node.links.add(new Link(test, cost));
			}
		}
	}
	
	public Node getNode(int x, int y) {
		return _nodes[x][y];
	}
	
	public void setEndNode(int x, int y) {
		_endNode = _nodes[x][y];
	}
	
	public void setStartNode(int x, int y) {
		_startNode = _nodes[x][y];
	}
	
	public void setWalkable(int x, int y, boolean value) {
		_nodes[x][y].walkable = value;
	}
	
	public Node getEndNode() {
		return _endNode;
	}
	
	public int getNumCols() {
		return _numCols;
	}
	
	public int getNumRows() {
		return _numRows;
	}
	
	public Node getStartNode() {
		return _startNode;
	}
	
}

class Link {
	
	public Node node;
	public double cost;
	
	public Link(Node node, double cost){
		this.node = node;
		this.cost = cost;
	}
	
}

class Node {
	public int x;
	public int y;
	public double f;
	public double g;
	public double h;
	public Boolean walkable = true;
	public Node parent;
	/** 埋葬深度 */
	public int buriedDepth = -1;
	//public Number costMultiplier = 1.0;
	public int version = 1;
	public List<Link> links;
	/** 距离 */
	public double distance;
	
	//public var index:int;
	public Node(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * 得到此节点到另一节点的网格距离
	 * @param targetNode
	 * @return 
	 * 
	 */
	public double getDistanceTo(Node targetNode) {
		double disX = targetNode.x - x;
		double disY = targetNode.y - y;
		distance = Math.sqrt( disX * disX + disY * disY );
		return distance;
	}
	
	public String toString() {
		return "x:" + x + " y:" + y;
	}
}
