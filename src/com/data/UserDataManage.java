package com.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.entity.Login;
import com.entity.UserInfo;
import com.mapper.LoginMapper;
import com.server.UserServer;

/**
 * 登录用户数据管理
 * @author xujinbo
 *
 */
public class UserDataManage {

	@Resource
	private UserServer userServer;
	
	/** 所有缓存的登录数据 */
	private List<Login> logins = new ArrayList<Login>();
	/** 所有缓存的用户数据 */
	private List<UserInfo> userInfos = new ArrayList<UserInfo>();
	/** 所有缓存的在线用户数据 */
	private List<UserInfo> onLineUserInfo = new ArrayList<UserInfo>();
	
	/**
	 * 添加一个新用户数据
	 * @param userInfo
	 */
	private void addNewLogin(Login login) {
		if (logins.contains(login)) {
			logins.remove(login);
			logins.add(login);
			Logger.getLogger(getClass()).warn("发生一次用户登录数据替换"+login);
		} else {
			logins.add(login);
		}
	}
	
	/**
	 * 根据loginID获取登录信息
	 * @param loginId
	 * @return
	 */
	public synchronized Login getLogin(long loginId) {
		Login login;
		for (int i = 0; i < logins.size(); i++) {
			login = logins.get(i);
			if (login.getId() == loginId) {
				return login;
			}
		}
		return null;
	}
	
	public synchronized Login getCacheLogin(long loginId) {
		Login login = getLogin(loginId);
		if (login == null) {
			login = queryLogin(loginId);
		}
		return login;
	}
	
	/**
	 * 删除一个用户信息
	 * @param id
	 * @return
	 */
	public synchronized Login removeLogin(long id) {
		for (int i = 0; i < logins.size(); i++) {
			if (logins.get(i).getId() == id) {
				return logins.remove(i);
			}
		}
		return null;
	}
	
	/**
	 * 删除一个用户信息
	 * @param userInfo
	 */
	public synchronized Login removeLogin(IoSession session) {
		Login login;
		for (int i = 0; i < logins.size(); i++) {
			login = logins.get(i);
			if (login.getSession() != null && login.getSession().getId() == session.getId()) {
				return logins.remove(i);
			}
		}
		return null;
	}
	
	/** 获取登录 不存在就创建 
	 * @param session */
	public synchronized Login getLoginDataNotCreate(String openid, int serverId, IoSession session) {
		Login login = getLoginServer(openid, serverId);
		if (login != null) {// 如果已经存在登录状态
			login.getSession().close(true);
			userServer.userOffline(login.getSession());
			login.setSession(null);
		}
		// 从数据库查询
		if (login == null) {
			login = queryLogin(openid, serverId);
			if (login != null) {
				addNewLogin(login);
			}
		}
		if (login == null) {
			SqlSession sqlSession = ConnectData.openSession();
			try {
				LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class);
				login = new Login(openid, serverId);
				addNewLogin(login);
				loginMapper.insert(login);
				sqlSession.commit();
			} finally {
				sqlSession.close();
			}
		}
		login.setSession(session);
		return login;
	}
	
	/**
	 * 从数据库查找登录信息
	 * @param openid
	 * @param serverId
	 * @return 
	 */
	private synchronized Login queryLogin(long id) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class);
			Login login = loginMapper.queryId(id);
			return login;
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("获取登录信息报错", e);
		} finally {
			if(sqlSession!=null)sqlSession.close();
		}
		return null;
	}
	
	/**
	 * 从数据库查找登录信息
	 * @param openid
	 * @param serverId
	 * @return 
	 */
	private synchronized Login queryLogin(String openid, int serverId) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class);
			Login login = loginMapper.queryName(openid, serverId);
			return login;
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("获取登录信息报错", e);
		} finally {
			if(sqlSession!=null)sqlSession.close();
		}
		return null;
	}

	/**
	 * 根据openId和服务器serverId 获取login信息
	 * @param openid
	 * @param serverId
	 * @return
	 */
	private synchronized Login getLoginServer(String openid, int serverId) {
		Login login;
		for (int i = 0; i < logins.size(); i++) {
			login = logins.get(i);
			if (login.getOpenid().equals(openid) && login.getServerId() == serverId) {
				return login;
			}
		}
		return null;
	}

	/**
	 * 添加一个新用户数据
	 * @param userInfo
	 */
	public void addNewUserInfo(UserInfo userInfo) {
//		System.out.println("添加新用户="+userInfo);
//		System.out.println("添加新用户="+userInfos+" | "+userInfos.size());
		int index = userInfos.indexOf(userInfo);
		if (index != -1) {
			if (userInfos.get(index) != userInfo) {
	//			System.out.println("存在数据 !却出现覆盖情况!");
				System.out.println("旧覆盖="+userInfos.get(index));
				System.out.println("新覆盖="+userInfo);
				try {
					throw new Throwable("存在数据 !却出现覆盖情况!");
				} catch (Throwable e) {
					e.printStackTrace();
				}
//				userInfos.set(index, userInfo);
			}
		} else {
			userInfos.add(userInfo);
		}
	}
	
	/**
	 * 添加一个在线用户数据
	 * @param userInfo
	 */
	public void addOnLineUserInfo(UserInfo userInfo) {
		onLineUserInfo.remove(userInfo);
		onLineUserInfo.add(userInfo);
	}
	
	/**
	 * 删除一个在线用户数据
	 * @param userInfo
	 */
	public synchronized void removeOnLineUserInfo(UserInfo userInfo) {
		onLineUserInfo.remove(userInfo);
	}
	
	/**
	 * 获取所有的在线用户
	 * @return
	 */
	public List<UserInfo> getOnLineUserInfo() {
		return onLineUserInfo;
	}
	
	/**
	 * 根据用户的名字  获取缓存中的数据
	 * @param name 用户名字
	 * @return
	 */
	public synchronized UserInfo getCacheUserInfo(String name) {
		for (int i = 0; i < userInfos.size(); i++) {
			if (userInfos.get(i).getRoleName().equals(name)) {
				return userInfos.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 根据用户的id 获取缓存中的数据
	 * @param id 用户id
	 * @return
	 */
	public synchronized UserInfo getCacheUserInfo(long id) {
		int index = userInfos.indexOf(new UserInfo(id));
		if (index != -1) {
			return userInfos.get(index);
		}
		return null;
	}
	
	/**
	 * 根据登录login id获取  用户信息
	 * @param login
	 * @return
	 */
	private synchronized UserInfo getCacheUserInfo(Login login) {
		return getCacheUserInfoLoginId(login.getId());
	}
	
	/**
	 * 根据登录login id获取  用户信息
	 * @param id
	 * @return
	 */
	private synchronized UserInfo getCacheUserInfoLoginId(long loginId) {
		UserInfo userInfo;
		for (int i = 0; i < userInfos.size(); i++) {
			userInfo = userInfos.get(i);
			if (userInfo.getLoginId() == loginId) {
				return userInfo;
			}
		}
		return null;
	}
	
	/**
	 * 根据session 获取 用户
	 * @param session
	 * @return
	 */
	public synchronized UserInfo getCacheUserInfo(IoSession session) {
		if (session != null) {
			UserInfo userInfo;
			for (int i = 0; i < userInfos.size(); i++) {
				userInfo = userInfos.get(i);
				if (userInfo.getSession() != null && userInfo.getSession().getId() == session.getId()) {
					return userInfo;
				}
			}
		}
		return null;
	}
	
	/**
	 * 删除一个用户信息
	 * @param userInfo
	 */
	public synchronized void removeUserInfo(UserInfo userInfo) {
		userInfos.remove(userInfo);
		removeOnLineUserInfo(userInfo);
	}
	
	/**
	 * 获取用户数据  如果不存在  那么就从数据库调用
	 * @param login 用户的登录id
	 * @return
	 */
	public synchronized UserInfo getUserInfoData(Login login) {
		UserInfo userInfo = getCacheUserInfo(login);
		if (userInfo == null) {
			try {
				userInfo = userServer.queryUserInfo(login);
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("数据库查询用户数据", e);
			}
		}
		return userInfo;
	}
	
	/**
	 * 获取用户id  如果不存在  那么就从数据库调用
	 * @param userId 用户的id
	 * @return
	 */
	public synchronized UserInfo getUserInfoData(long userId) {
		UserInfo userInfo = getCacheUserInfo(userId);
		if (userInfo == null) {
			try {
				userInfo = userServer.queryUserInfo(userId);
			} catch (IOException e) {
				Logger.getLogger(getClass()).error("数据库查询用户数据", e);
			}
		}
		return userInfo;
	}

	/**
	 * 根据用户名字获取用户数据  如果不存在  从数据库获取
	 * @param userName
	 * @return
	 */
	public synchronized UserInfo getUserInfoData(String userName) {
		int len = userInfos.size();
		UserInfo userInfo;
		for (int i = 0; i < len; i++) {
			userInfo = userInfos.get(i);
			if (userInfo.getRoleName().equals(userName)) {
				return userInfo;
			}
		}
		try {
			userInfo = userServer.queryUserInfo(userName);
			return userInfo;
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("数据库查询用户数据", e);
		}
		return null;
	}
	
	/**
	 * 获取所有缓存的用户数据
	 * @return userInfos 所有缓存的用户数据
	 */
	public List<UserInfo> getUserInfos() {
		return userInfos;
	}

}