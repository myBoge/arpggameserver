package com.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

import com.boge.util.ResourceCell;
import com.server.ServerManageServer;

/**
 * 连接并获取数据库
 * @author boge
 *
 */
public class ConnectData {

	private static Logger log = Logger.getLogger("ConnectData");
	
	private static Reader reader;
	
	private static SqlSessionFactory sessionFactory;

	private synchronized static void connect() {
		try {
			// 初始化 mybatis
			ServerManageServer serverManageServer = ResourceCell.getResource(ServerManageServer.class);
			File file = new File(serverManageServer.getServerConfig().getSqlResource());
			InputStream fileInput = new FileInputStream(file);
			reader = new InputStreamReader(fileInput);
			sessionFactory = new SqlSessionFactoryBuilder().build(reader);
			fileInput.close();
			log.info("成功连接到mysql数据库");
		} 
		catch (IOException e) {
			log.error("IOException", e);
		}
	}
	
	public static SqlSessionFactory getSqlSessionFactory() {
		if (sessionFactory == null) {
			connect();
		}
		return sessionFactory;
	}
	
	/**
	 * 开启一个事务
	 * @return
	 */
	public static SqlSession openSession() {
		return getSqlSessionFactory().openSession(false);
	}
	
}
