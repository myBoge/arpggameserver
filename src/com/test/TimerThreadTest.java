package com.test;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;

import com.boge.share.BaseShareBox;
import com.boge.util.ResourceCell;
import com.server.ServerManageServer;
import com.thread.TimerManager;
import com.utils.ActionTimerListener;

public class TimerThreadTest {

	private TimerManager thread;

	@SuppressWarnings("unused")
	@Test
	public void test() {
		
		ServerManageServer serverManageServer = new ServerManageServer();
		BaseShareBox baseShareBox = new BaseShareBox();
		ResourceCell.baseShareBox = new BaseShareBox();
		baseShareBox.setByType(ServerManageServer.class, serverManageServer);
		
		serverManageServer.init();
		
//		System.out.println("0="+Thread.currentThread().getName());
		
		PropertyConfigurator.configure(serverManageServer.getServerConfig().getLog4jPath());
		
		thread = new TimerManager();
		baseShareBox.associate(thread);
		thread.start();
		System.out.println("2="+Thread.currentThread().getName());
		long id;
		
//		while(true){
//			
//		}
		id = thread.doOnce(100, actionTimerListener);
//		System.out.println("计时器ID="+id);
//		id = thread.doLoop(1000, actionTimerListener, "娃哈哈"+1);
//		System.out.println(id);
		
		
	}
	
	ActionTimerListener actionTimerListener = new ActionTimerListener() {
		
		@SuppressWarnings("unused")
		public void ha(String haha){
			System.out.println(haha);
		}
		
		@SuppressWarnings("unused")
		public void fawef(){
			System.out.println("aa|"+Thread.currentThread().getName());
			System.out.println("fawef");
		}
	
		@SuppressWarnings("unused")
		public void fawef(String a, int b){
			System.out.println(a+"|"+b);
		}
		
	};
	
	public static void main(String[] args) {
		
		new TimerThreadTest().test();
		
	}
	
}
