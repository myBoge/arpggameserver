package com.test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;

import com.boge.share.BaseShareBox;
import com.boge.util.ResourceCell;
import com.configs.PropConfig;
import com.data.ConnectData;
import com.entity.UserInfo;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.mapper.UserMapper;
import com.server.ServerManageServer;
import com.utils.BytesFactory;

public class EscortTest {
	
	@Test
	public void test() throws IOException {
		
		ServerManageServer serverManageServer = new ServerManageServer();
		ResourceCell.baseShareBox = new BaseShareBox();
		ResourceCell.baseShareBox.setByType(ServerManageServer.class, serverManageServer);
		ResourceCell.baseShareBox.setByType(PropConfig.class, new PropConfig());
		
		serverManageServer.init();
		
		PropertyConfigurator.configure(serverManageServer.getServerConfig().getLog4jPath());
		
		SqlSession session = ConnectData.openSession();
		
		UserMapper userManage = session.getMapper(UserMapper.class);
		
		UserInfo userInfo = userManage.queryName("虎天禄");
		
		
//		UserInfo userInfo = userManage.queryId(1);
		System.out.println(userInfo);
//		
//		Backpack backpack = userInfo.getBackpack();
//		BytesFactory.createBytes(userInfo.getBackpackByte(), backpack);
//		
//		backpack.removePropIndexCount(0, 1);
//		
//		UserInfo userData = new UserInfo(userInfo.getId());
//		userData.setBackpackByte(BytesFactory.objectToByteData(userInfo.getBackpack()));
//		userManage.update(userData);
//		session.commit();
//		
//		userInfo = userManage.queryId(1);
//		
//		backpack = userInfo.getBackpack();
//		BytesFactory.createBytes(userInfo.getBackpackByte(), backpack);
		
		
//		ObjectPool objectPool = new ObjectPool();
//		objectPool.addPool(FightBox.class, 300);// 创建300个战斗房间
//		System.out.println(objectPool.getPoolCount(FightBox.class));
//		System.out.println(objectPool.getPool(FightBox.class));
//		FightBox fightBox = objectPool.getPool(FightBox.class);
//		System.out.println(fightBox);
//		System.out.println(objectPool.getPoolCount(FightBox.class));
//		objectPool.gc(FightBox.class, fightBox);
//		System.out.println(objectPool.getPoolCount(FightBox.class));
		
		
//		PropConfig prop = new PropConfig();
//		System.out.println(prop.getSample(1001));
		
		
//		RoleConfig config = new RoleConfig();
//		System.out.println(config.getSample(1002));
		
		
//		
//		EscortMapper escortMapper = session.getMapper(EscortMapper.class);
////		UserManage userManage = session.getMapper(UserManage.class);
//		
//		List<Escort> lists = new ArrayList<>();
//		for (int i = 0; i < 50; i++) {
//			lists.add(new Escort(0, System.currentTimeMillis()));
//		}
//		
//		byte[] bytes = BytesFactory.ListToBytes(lists);
//		
//		EscortData data = new EscortData(1001, bytes);
//		System.out.println("存入数据库长度="+data.getRoles().length);
//		escortMapper.insert(data);
////		UserInfo userInfo = new UserInfo("1234", 2000, "你猜猜看", new Role(1, 1, "你猜猜看", 1, 0, 1000));
////		userInfo.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
////		userManage.insert(userInfo);
//		
//		session.commit();
//		
////		
//		List<EscortData> list = escortMapper.queryAll();
//		System.out.println(list.size());
//		for (int i = 0; i < list.size(); i++) {
//			System.out.println(list.get(i).getRoles().length);
//			lists = byteToObject(list.get(i).getRoles(), new Escort());
//		}
//		System.out.println(lists.size());
//		System.out.println(lists);
		
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private <T> List<T> byteToObject(byte[] bytes, T escort) {
		T t = null;
		ByteArrayInputStream bi = null;
		EntityInputStream oi = null;
		List<T> list = new ArrayList<>();
		try {
			bi = new ByteArrayInputStream(bytes);
			oi = new EntityInputStream(bi);
			int size = oi.readInt();
			System.out.println("byteToObject.len="+size);
			for (int i = 0; i < size; i++) {
				t = (T) BytesFactory.createBytes(null, (IAttributDataStream) escort, oi);
				list.add(t);
			}
		} catch (Exception e) {
			System.out.println("translation" + e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if(bi!=null) bi.close();
				if(oi!=null) oi.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	
}
