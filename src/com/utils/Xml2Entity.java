package com.utils;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.annotation.XMLParse;
import com.entity.UseProp;

/**
 * xml 转 实体类
 * @author xujinbo
 *
 */
public class Xml2Entity {
	
	private Map<String, ClassEntity> maps = new HashMap<>();
	
	public static void main(String[] args) {
		Xml2Entity entity = new Xml2Entity();
		entity.parseClass(UseProp.class);
		Object obj = entity.fromXMLList("<root><UseProp id='1001'> <level>0</level> <type>2</type> <skin>res/icon/renshen.png</skin> <name>止血草</name> <des>最常见的草药,有四片嫩绿的叶子,外敷伤处,可恢复少量的气血</des> <timeline>0</timeline> <goldPrice>10</goldPrice> <maxCount>99</maxCount> <valueType>0</valueType> <value>100</value> <buysType>1</buysType> <useLevel>1</useLevel> <unmetValue>0</unmetValue> </UseProp> <UseProp id='1002'> <level>0</level> <type>2</type> <skin>res/icon/renshen.png</skin> <name>一叶草</name> <des>只有一片叶子的绿色怪草,叶面上有经脉纹路,有助于气血的恢复</des> <timeline>0</timeline> <goldPrice>25</goldPrice> <maxCount>99</maxCount> <valueType>0</valueType> <value>200</value> <buysType>1</buysType> <useLevel>1</useLevel> <unmetValue>0</unmetValue> </UseProp> </root>");
		System.out.println(obj);
	}

	private <T> List<T> fromXMLList(String value) {
		try {
			return fromXMLList(DocumentHelper.parseText(value));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public <T> List<T> fromXMLList(byte[] buf) {
		InputStream is = null;
		try {
			is = new ByteArrayInputStream(buf);
			return fromXMLList(is);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public <T> List<T> fromXMLList(InputStream fileInput) {
		try {
			SAXReader reader = new SAXReader();
			Document doc = reader.read(fileInput);
			return fromXMLList(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> fromXMLList(Document doc) {
		try {
			Element root = doc.getRootElement();
			List<Element> childList = root.elements();
			int len = childList.size();
			if (len > 0) {
				List<T> tLists = new ArrayList<>();
				Element et = null;
				for (int i = 0; i < len; i++) {
					et = (Element)childList.get(i);
					tLists.add((T) iterateElement(et));
				}
				return tLists;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 将指定的xml类容转换成entity
	 * @param value
	 */
	public <T> T fromXML(String value) {
		try {
			Document doc = DocumentHelper.parseText(value);
			return fromXML(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 将指定的xml类容转换成entity
	 * @param value
	 */
	public <T> T fromXML(byte[] buf) {
		InputStream is = null;
		try {
			is = new ByteArrayInputStream(buf);
			return fromXML(is);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public <T> T fromXML(InputStream fileInput) {
		try {
			SAXReader reader = new SAXReader();
			Document doc = reader.read(fileInput);
			return fromXML(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public <T> T fromXML(Document doc) {
		try {
			Element root = doc.getRootElement();
			return iterateElement(root);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings({ "unchecked"})
	private <T> T iterateElement(Element et) throws Exception {
		ClassEntity classEntity;
		T t = null;
		if(!maps.containsKey(et.getName())) {
			throw new InjectErrorException(et.getName()+" 没有注入解析！值="+et);
		}
		classEntity = maps.get(et.getName());
		t = (T) classEntity.getCls().newInstance();
		addValue(et, t, classEntity);
		return t;
	}

	/**
	 * 赋值
	 * @param et 要实例的节点
	 * @param t 已经被实例的类
	 * @param classEntity 缓存体
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private <T> void addValue(Element et, T t, ClassEntity classEntity) throws Exception {
		List<Attribute> list = et.attributes();
		Attribute attribute;
		// 获取所有的属性值
		int attLen = list.size();
		for (int j = 0; j < attLen; j++) {
			attribute = list.get(j);
			classEntity.voluation(t, attribute);
		}
		// 处理所有的节点值  和字节点
		if (et.getTextTrim().equals("")) {
			List<Element> lists = et.elements();
			int len = lists.size();
			if (len > 0) {
				for (int i = 0; i < len; i++) {
					addValue(lists.get(i), t, classEntity);
				}
			}
		} else {
			classEntity.voluation(t, et);
		}
	}
	
	
	
	/**
	 * 添加解析类
	 * @param args
	 */
	public void parseClass(Class<?> ...args) {
		int len = args.length;
		for (int i = 0; i < len; i++) {
			parseObject(args[i]);
		}
	}

	private void parseObject(Class<?> cls) {
//		Introspector.getBeanInfo(cls).
//		Annotation[] annotations = cls.getAnnotations();
//		System.out.println(annotations.length);
		Class<?> supCls = cls.getSuperclass();
		if (supCls != null) {
			parseObject(supCls);
		}
		if(cls.isAnnotationPresent(XMLParse.class)) {
			addAnnotation(cls.getAnnotation(XMLParse.class).value(), cls);
		}
	}

	private void addAnnotation(String value, Class<?> cls) {
		if (!maps.containsKey(value)) {
			ClassEntity classEntity = new ClassEntity(cls);
			maps.put(value, classEntity);
		}
	}

}


class ClassEntity {
	
	private Class<?> cls;
	
	private Map<String, Object> objs = new HashMap<>();
	
	public ClassEntity(Class<?> cls) {
		super();
		this.cls = cls;
		
		Field[] fields = cls.getDeclaredFields();
		int len = fields.length;
		for (int i = 0; i < len; i++) {
			if (fields[i].isAnnotationPresent(XMLParse.class)) {
				objs.put(fields[i].getAnnotation(XMLParse.class).value(), fields[i]);
			} 
		}
		
		try {
//			PropertyDescriptor[] props = Introspector.getBeanInfo(cls).getPropertyDescriptors();
//			Method writeMethod;
//			for (PropertyDescriptor pd : props) {
////				System.out.println(cls.getName()+"|"+pd.getName());
//				writeMethod = pd.getWriteMethod();//获得应该用于写入属性值的方法。 
//				if ((writeMethod != null) && (writeMethod.isAnnotationPresent(XMLParse.class))) { // 如果此可写方法  有Resource 注释
//					objs.put(writeMethod.getAnnotation(XMLParse.class).value(), writeMethod);
//				}
//			}
			// 获取所有的方法
			MethodDescriptor[] methodDescriptors = Introspector.getBeanInfo(cls).getMethodDescriptors();
			Method method;
			for (MethodDescriptor methodDescriptor : methodDescriptors) {
//				System.out.println(cls.getName()+"|"+methodDescriptor.getName());
				method = methodDescriptor.getMethod();
				if (method.getParameterTypes().length > 0 && method.isAnnotationPresent(XMLParse.class)) {
					objs.put(method.getAnnotation(XMLParse.class).value(), method);
				}
			}
			
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
	}


	public <T> void voluation(T t, Element et) throws Exception {
//		System.out.println(et.getName());
		if(this.objs.containsKey(et.getName())) {
			Object obj = this.objs.get(et.getName());
			if (obj instanceof Method) {
				Method method = (Method) obj;
				Parameter[] parameters = method.getParameters();
				int len = parameters.length;
				Object value;
				Object[] objs = null;
				if (len > 0) {
					objs = new Object[len];
				}
				for (int i = 0; i < len; i++) {
					value = StringUtils.changeValueType(parameters[i].getType(), et.getTextTrim());
					objs[i] = value;
				}
				method.setAccessible(true);
				method.invoke(t, objs);
			}
		} else {
			voluationClss(t.getClass(), t, et);
		}
	}


	private <T> void voluationClss(Class<?> class1, T t, Element et) throws Exception {
		Class<?> classt = class1.getSuperclass();
		if (classt != null) {
			voluationClss(classt, t, et);
		}
		Field[] fields = class1.getDeclaredFields();
		int len = fields.length;
		Field field;
		Object obj;
		for (int i = 0; i < len; i++) {
			field = fields[i];
			if (field.getName().equals(et.getName())) {
				field.setAccessible(true);
				obj = StringUtils.changeValueType(field.getType(), et.getTextTrim());
				field.set(t, obj);
			} 
		}
	}

	public <T> void voluation(T t, Attribute attribute) throws Exception {
		if(this.objs.containsKey(attribute.getName())) {
			Object obj = this.objs.get(attribute.getName());
			if (obj instanceof Method) {
				Method method = (Method) obj;
				method.setAccessible(true);
				method.invoke(t, attribute.getValue());
			}
		} else {
			voluationClss(t.getClass(), t, attribute);
		}
	}


	private <T> void voluationClss(Class<?> class1, T t, Attribute attribute) throws Exception {
		Class<?> classt = class1.getSuperclass();
		if (classt != null) {
			voluationClss(classt, t, attribute);
		}
		Field[] fields = class1.getDeclaredFields();
		int len = fields.length;
		Field field;
		Object obj;
		for (int i = 0; i < len; i++) {
			field = fields[i];
			if (field.getName().equals(attribute.getName())) {
				field.setAccessible(true);
				obj = StringUtils.changeValueType(field.getType(), attribute.getValue());
				field.set(t, obj);
			} 
		}
	}

	/**
	 * 获取cls
	 * @return cls cls
	 */
	public Class<?> getCls() {
		return cls;
	}

	/**
	 * 设置cls
	 * @param cls cls
	 */
	public void setCls(Class<?> cls) {
		this.cls = cls;
	}

	/**
	 * 获取objs
	 * @return objs objs
	 */
	public Map<String, Object> getObjs() {
		return objs;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ClassEntity [cls=" + cls + ", objs=" + objs + "]";
	}
	
}

/** 注入异常 */
class InjectErrorException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InjectErrorException(String msg) {
		super(msg);
	}
	
}