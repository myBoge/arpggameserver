package com.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * id增长管理器
 * @author xujinbo
 *
 */
public class OnlyIdManage {

	/** 使用的id对象池 */
	private List<Long> gcPool = new ArrayList<Long>();
	/** 当前增长的id值 */
	private long currentId;
	
	public OnlyIdManage() {
		currentId = 0;
	}
	
	public OnlyIdManage(long currentId) {
		super();
		updateUseId(currentId);
	}

	/**
	 * 更新使用id
	 */
	public void updateUseId(long id){
		if (id > currentId) {
			currentId = id;
		}
	}
	
	/**
	 * 获取唯一的id一枚
	 * @return
	 */
	public long getGrowableId() {
		if (gcPool.size() > 0) {
			return gcPool.remove(0);
		}
		currentId++;
		return currentId;
	}
	
	/**
	 * 回收这个id
	 * @param id
	 */
	public void gc(long id){
		if (!gcPool.contains(id)) {
			gcPool.add(id);
		}
	}
	
}
