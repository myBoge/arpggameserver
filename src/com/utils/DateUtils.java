package com.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具
 * @author xujinbo
 *
 */
public class DateUtils {

	 /** 
     * 获取当前时间 
     *  
     * @param date 
     * @return 
     */  
    public static Date getCurrentDate() {  
        return new Date(System.currentTimeMillis());  
    }
    
    /** 
     * 将时间后退2小时 
     *  
     * @param date 
     * @return 
     */  
    public static Date getFallBack2Hour(Date date) {  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
  
        calendar.set(Calendar.HOUR_OF_DAY,  
                calendar.get(Calendar.HOUR_OF_DAY) - 2);  
        calendar.set(Calendar.MINUTE, 0);  
        calendar.set(Calendar.SECOND, 0);  
        calendar.set(Calendar.MILLISECOND, 0);  
        return calendar.getTime();  
    }  
	
	 /** 
     * 获取两个时间间隔的天数 
     *  
     * @param date 
     * @return 
     */  
    public static long getDiffDays(Date startDate, Date endDate) {  
        long difftime = endDate.getTime() - startDate.getTime();  
        return difftime / (24L * 60L * 60L * 1000L);  
    }  
	
    /** 
     * 根据日期获取下一天起始时间 
     *  
     *  
     * @param date 
     * @return 
     */  
    public static Date getStartDateOfNextDay(Date date) {  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
        calendar.add(Calendar.DAY_OF_MONTH, 1);  
        calendar.set(Calendar.HOUR_OF_DAY, 0);  
        calendar.set(Calendar.MINUTE, 0);  
        calendar.set(Calendar.SECOND, 0);  
        calendar.set(Calendar.MILLISECOND, 0);  
        return calendar.getTime();  
    }  
  
    /** 
     * 根据日期当前日期顺延一周后的起始时间 
     * @param date 
     * @return 
     */  
    public static Date getStartDateOfNextSevenDay(Date date) {  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
        calendar.add(Calendar.DAY_OF_MONTH, 7);  
        calendar.set(Calendar.HOUR_OF_DAY, 0);  
        calendar.set(Calendar.MINUTE, 0);  
        calendar.set(Calendar.SECOND, 0);  
        calendar.set(Calendar.MILLISECOND, 0);  
        return calendar.getTime();  
    }
	
    /**
     * 判断是否是同年同月
     * @param millis
     * @param millis2
     * @return
     */
    public static boolean isSameMonth(long millis, long millis2) {  
        Calendar cal1 = Calendar.getInstance();  
        cal1.setTimeInMillis(millis);  
        Calendar cal2 = Calendar.getInstance();  
        cal2.setTimeInMillis(millis2);  
        return isSameMonth(cal1, cal2);
    }
    
    /**
     * 判断是否是同年同月
     * @param cal1
     * @param cal2
     * @return
     */
    public static boolean isSameMonth(final Calendar cal1, final Calendar cal2) {  
    	return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
    			cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
    			cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH));
    } 
    
    /**
     * 判断是否是同年同一天
     * @param millis
     * @param millis2
     * @return
     */
    public static boolean isSameDay(long millis, long millis2) {
    	Calendar cal1 = Calendar.getInstance();
    	cal1.setTimeInMillis(millis);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTimeInMillis(millis2);
        return isSameDay(cal1, cal2);
    }
    
    /**
     * 判断是否是同年同一天
     * @param cal1
     * @param cal2
     * @return
     */
    public static boolean isSameDay(final Calendar cal1, final Calendar cal2) {
    	if (cal1 == null || cal2 == null) {
    		throw new IllegalArgumentException("The date must not be null");
    	}
    	return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
    			cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
    			cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }
	
	public static void main(String[] args) {
		
	}
	
}
