package com.utils;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.JsePlatform;

public class LuaUtils {

	private static Globals common;
	
	public synchronized static Globals getCommon() {
		if (common == null) {
			common = getNewLuaState("data/lua/main.lua");
			
//			common.get("getEverydayProp").call(LuaValue.valueOf(6));
//			common.get("getEverydayProp").call(LuaValue.valueOf(13));
//			common.get("getEverydayProp").call(LuaValue.valueOf(20));
//			common.get("getEverydayProp").call(LuaValue.valueOf(26));
			
		}
		return common;
	}
	
	/**
	 * 获取key属性值
	 * @param key
	 * @return
	 */
	public static LuaValue get(String key){
		return getCommon().get(key);
	}
	
	
	/**
	 * 获取一个新的  lua  Globals
	 * @param urls 要加载的lua脚本路径
	 * @return
	 */
	private static Globals getNewLuaState(String ...urls) {
		Globals globals = JsePlatform.standardGlobals();
		LuaValue dofile = globals.get("dofile");
		for (int i = 0; i < urls.length; i++) {
			dofile.call( LuaValue.valueOf(urls[i]));
		}
		return globals;
	}

}
