package com.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.boge.util.ZlibUtil;

public class FileControl {

	/**
	 * 读取文件
	 * @param path
	 * @return 
	 */
	public static String readFile(String path) {
		return readFile(new File(path));
	}
	
	/**
	 * 读取文件
	 * @param path
	 * @return 
	 */
	public static String readFile(File file) {
		if (file.exists()) {
			int len;
			FileInputStream fileConf = null;
			try {
				len = (int) file.length();
				byte[] b = new byte[len];
				fileConf = new FileInputStream(file);
				fileConf.read(b, 0, len);
				b = ZlibUtil.decompress(b);
				String str = new String(b);
//				System.out.println(str);
				return str;
			} catch (Exception e) {
				Logger.getLogger("FileControl").error("读取文件报错", e);
			} finally {
				if (fileConf != null) {
					try {
						fileConf.close();
					} catch (IOException e) {
						Logger.getLogger("FileControl").error("读取文件关闭流报错", e);
					}
				}
			}
		}
		return null;
	}
	
	
	
	public static void main(String[] args) {
		readFile("data/map/1001/mapconf.d5");
	}
	
}
