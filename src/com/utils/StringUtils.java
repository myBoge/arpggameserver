package com.utils;

public class StringUtils {

	/**
	 * 根据类型转换值
	 * @param type
	 * @param value
	 * @return
	 */
	public static Object changeValueType(Class<?> type, String value) {
		String name = type.getName();
		Object obj;
		if (name.equals("long")) {
			obj = Long.parseLong(value);
		} else if (name.equals("boolean")) {
			obj = Boolean.parseBoolean(value);
		} else if (name.equals("double")) {
			obj = Double.parseDouble(value);
		} else if (name.equals("int")) {
			obj = Integer.parseInt(value);
		} else {
			obj = value;
		}
		return obj;
	}
	
	
	/**
	 * 将此字符串数组转换成int数组
	 * @param value
	 * @return
	 */
	public static int[] strChangeInt(String[] value) {
		int[] ints = new int[value.length];
		for (int i = 0; i < value.length; i++) {
			ints[i] = Integer.parseInt(value[i]);
		}
		return ints;
	}
	
	/**
	 * 将此字符串数组转换成int数组
	 * @param value
	 * @return
	 */
	public static long[] strChangeLong(String[] value) {
		long[] ints = new long[value.length];
		for (int i = 0; i < value.length; i++) {
			ints[i] = Long.parseLong(value[i]);
		}
		return ints;
	}
	
}
