package com.utils;

import java.util.ArrayList;
import java.util.List;

import com.boge.util.ResourceCell;
import com.entity.FightData;
import com.entity.Team;
import com.entity.UserInfo;
import com.net.CommonCmd;
import com.server.TeamServer;

/**
 * 公用类
 * @author xujinbo
 *
 */
public class CommonUtils {

	/**
	 * 根据地图id获取场景
	 * @param mapId
	 * @return
	 */
	public static int getMapScene(long mapId) {
		if (mapId > 1000 && mapId < 2000) {
			return CommonCmd.SCENE_GRIND;
		} else if (mapId > 5000 && mapId < 6000) {
			return CommonCmd.SCENE_ESCORT;
		}
		return CommonCmd.SCENE_GAME;
	}
	
	
	/**
	 * 获取战斗人数  不包括宠物
	 * @param userList
	 * @return
	 */
	public static int getFightCount(List<FightData> userList) {
		int lenRole = 0;
		for (int i = 0; i < userList.size(); i++) {
			if (userList.get(i) != null && !userList.get(i).getUserInfo().getRole().isPet()) {
				lenRole++;
			}
		}
		return lenRole;
	}
	
	
	/**
	 * 将此角色转换战斗对象
	 * @param userInfo
	 * @param isBrainAI 此战斗对象是否是电脑控制战斗
	 * @return
	 */
	public static List<FightData> getFightData(UserInfo userInfo, Boolean isBrainAI) {
		List<FightData> list;
		Team team;
		TeamServer teamServer = ResourceCell.getResource(TeamServer.class);
		if (userInfo.getTeamId() != 0) {
			team = teamServer.getTeam(userInfo.getTeamId());
			if (team != null) {
				list = team.getFightData(isBrainAI);
				return list;
			}
		}
		
		list = new ArrayList<>();
		
		int len = 1;
		FightData fightData;
		//主角数据
		for (int i = 0; i < 5; i++) {
			if (len > i) {
				fightData = new FightData();
				fightData.setBrainAI(isBrainAI);
				fightData.setUserInfo(userInfo);
				list.add(fightData);
			} else {
				list.add(null);
			}
		}
		//宠物数据
		for (int i = 0; i < 5; i++) {
			if (len > i) {
				list.add(userInfo.getPetFightData(isBrainAI));
			} else {
				list.add(null);
			}
		}
		return list;
	}
	
}
