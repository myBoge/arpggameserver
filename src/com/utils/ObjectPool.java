package com.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.interfaces.IPool;

/**
 * 对象池
 * @author xujinbo
 *
 */
public class ObjectPool {

	/** 添加到对象池中的对象类 */
	private List<Class<?>> classList = new ArrayList<>();
	/** 实体对象 */
	private Map<Class<?>, List<Object>> poolDisplay = new HashMap<>();
	/** 对象 最小保留数量 */
	private Map<Class<?>, Integer> poolMinCount = new HashMap<Class<?>, Integer>();

	public ObjectPool() {
	}
	
	/**
	 * 当前缓存还存在的实例后的对象数量
	 * @param t
	 * @return
	 */
	public int getPoolCount(Class<?> t) {
		int count = 0;
		if (classList.contains(t)) {// 存在此对象的缓存
			count = poolDisplay.get(t).size();
		}
		return count;
	}
	
	/**
	 * 添加池对象
	 * @param t 对象池类
	 * @param initCount 初始化数量  最少数量(默认10个对象)
	 */
	public void addPool(Class<?> t, int initCount) {
		if (initCount < 0) {
			Logger.getLogger(getClass()).error("initCount不能小于1");
			return;
		}
		if (!classList.contains(t)) {// 不存在此对象的缓存
			classList.add(t);
			// 初始化  默认的对象
			List<Object> list = new ArrayList<>();
			for (int i = 0; i < initCount; i++) {
				try {
					list.add(t.newInstance());
				} catch (Exception e) {
					Logger.getLogger(getClass()).error("对象池构建对象报错", e);
				}
			}
			poolMinCount.put(t, initCount);// 保存此默认对象的最小数量
			poolDisplay.put(t, list);// 保存已经实例的对象
		}
	}
	
	/**
	 * 获取池对象
	 * @param <T>
	 * @param code 对象编号
	 * @return 
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <T> T getPool(Class<T> cls) {
		if (!poolDisplay.containsKey(cls)) {
			System.out.println("获取不存在对象池中的对象:"+cls.getName());
			return null;
		}
		List<?> obj = poolDisplay.get(cls);
		T t = null;
		if (obj.size() != 0) {// 如果对象池中还有空余的对象
			t = (T) obj.remove(0);
		} else {
			try {
				t = cls.newInstance();
			} catch (Exception e) {
				Logger.getLogger(getClass()).error("对象池构建对象报错", e);
			}
		}
		return t;
	}
	
	/**
	 * 修改指定对象池  最小保留数量(如果小于当前已经存在的数量  那么会销毁多余的对象)
	 * @param code
	 * @param value
	 * 
	 */
	public void minCount(Class<?> cls, int initCount) {
		if (poolMinCount.containsKey(cls) && initCount > 0) {
			List<Object> list = poolDisplay.get(cls);
			int len = list.size();
			while(len > initCount){
				list.remove(0);
			}
			poolMinCount.put(cls, initCount);
		}
	}
	
	/**
	 * 回收一个对象到对象池
	 * @param <T>
	 * @param cls 类对象
	 * @param t 实体类
	 * 
	 */
	public <T> void gc(Class<?> cls, T t) {
		if (!poolDisplay.containsKey(cls)) {
			return;
		}
		Integer initCount = poolMinCount.get(cls);// 获取最小保留数量
		List<Object> list = poolDisplay.get(cls);// 获取对象数组
		if (list.size() >= initCount) { // 如果 数组中已经满足最小保存数量  那么直接销毁
			if (t instanceof IPool) {
				((IPool)t).dispose();
			}
			t = null;
			return;	
		}
		list.add(t);
	}
	
	/**
	 * 删除池对象
	 * @param code 对象编号
	 * 
	 */
	public void removePool(Class<?> cls) {
		if (poolDisplay.containsKey(cls)) {
			classList.remove(cls);
			poolDisplay.remove(cls);
			poolMinCount.remove(cls);
		}
	}
	
}
