package com.utils;

import java.util.Comparator;

import org.luaj.vm2.LuaValue;

import com.entity.Prop;

/**
 * 物品排序算法
 * @author xujinbo
 *
 */
public class PropComparable implements Comparator<Prop> {

	@Override
	public int compare(Prop prop, Prop prop2) {
//		物品类型  1武器2回复血3回复蓝4恢复血和蓝5头盔6衣服7鞋子8玉佩9项链10手镯11道具      技能类型1伤害类2增益类3障碍类
		long aPrice = prop.getType();
		long bPrice = prop2.getType();
		LuaValue luaValue = LuaUtils.getCommon().get("getSortType");
		aPrice = luaValue.call(LuaValue.valueOf(aPrice)).tolong();
		bPrice = luaValue.call(LuaValue.valueOf(bPrice)).tolong();
		int result = contrast(aPrice, bPrice);
		if(result == 0) {
			// 如果两个物品 id一样
			aPrice = prop.getId();
			bPrice = prop2.getId();
			// 检测id
			result = contrast(aPrice, bPrice);
			if(result == 0) { // 如果两个物品id一样
				// 如果两个物品 id一样
				aPrice = (prop.isLock()||!prop.isTrade())?0:1;
				bPrice = (prop2.isLock()||!prop2.isTrade())?0:1;
				// 检测锁定
				result = contrast(aPrice, bPrice);
				if (result==0) {
					aPrice = prop.getCount();
					bPrice = prop2.getCount();
					// 检测数量
					result = contrast(aPrice, bPrice);
					if (result == 0) {
						aPrice = prop.getIndex();
						bPrice = prop2.getIndex();
						// 检测位置
						result = contrast(aPrice, bPrice);
					}
				}
			}
		}
		return result;
	}

	private int contrast(long aPrice, long bPrice) {
		if(aPrice > bPrice) {
			return 1;
		} else if(aPrice < bPrice) {
			return -1;
		} else {
			return 0;
		}
	}

}
