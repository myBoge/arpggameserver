package com.utils;

import java.math.BigDecimal;
import java.util.Random;

public class RandomUtils {

	private static Random random = new Random();

	/**
	 * 返回一个伪随机数，它是取自此随机数生成器序列的、在 0（包括）和指定值（不包括）之间均匀分布的 int 值。
	 * 
	 * @param n
	 * @return
	 */
	public static int nextInt(int n) {
		return random.nextInt(n);
	}
	
	public static long nextLong(long n) {  
        return random.nextInt((int) n);  
    }  

	/**
	 * @param pro
	 *            为true 的概率
	 * @return
	 */
	public static boolean nextBoolean(int pro) {
		if (pro >= 100) {
			return true;
		}
		int n = nextInt(100);
		return n < pro;
	}

	/**
	 * 返回下一个伪随机数，它是取自此随机数生成器序列的均匀分布的 boolean 值。
	 * 
	 * @return
	 */
	public static boolean nextBoolean() {
		return random.nextBoolean();
	}

	public static long nextLong(long startInclusive, long endExclusive) {
		if (endExclusive < startInclusive) {
			throw new Error("结束值小于起始值!");
		}
		if (startInclusive == endExclusive) {
			return startInclusive;
		}
		return (long) nextDouble(startInclusive, endExclusive);
	}

	/**
	 * 返回下一个伪随机数，它是取自此随机数生成器序列的、在 startInclusive 和 endInclusive之间均匀分布的 double 值。
	 * 
	 * @param startInclusive
	 * @param endInclusive
	 * @return
	 */
	public static double nextDouble(double startInclusive, double endInclusive) {
		if (endInclusive < startInclusive) {
			throw new Error("结束值小于起始值!");
		}
		if (startInclusive == endInclusive) {
			return startInclusive;
		}
		return startInclusive + ((endInclusive - startInclusive) * random.nextDouble());
	}

	/**
	 * 返回下一个伪随机数，它是取自此随机数生成器序列的、在state（包括）和指定值end（不包括）之间均匀分布的 int 值。
	 * 
	 * @param startInclusive
	 * @param endInclusive
	 * @return
	 */
	public static int nextInt(int startInclusive, int endInclusive) {
		if (endInclusive < startInclusive) {
			throw new Error("结束值小于起始值!");
		}
		if (endInclusive == startInclusive) {
			return startInclusive;
		}
		return startInclusive + nextInt(endInclusive - startInclusive);
	}
	
	
	
	/** 
     * 红包生成算法 <br>
     * 理想的红包生成结果是平均值附近的红包比较多，大红包和小红包的数量比较少。<br>
     * 可以想像下，生成红包的数量的分布有点像正态分布。
     * @param total 红包总额 
     * @param count 红包个数 
     * @param max 每个小红包的最大额 
     * @param min 每个小红包的最小额 
     * @return 存放生成的每个小红包的值的数组 
     */  
    public static long[] generate(long total, int count, long max, long min) {  
        long[] result = new long[count];  
  
        long average = total / count;  
  
//        long a = average - min;  
//        long b = max - min;  
  
        //  
        //这样的随机数的概率实际改变了，产生大数的可能性要比产生小数的概率要小。  
        //这样就实现了大部分红包的值在平均数附近。大红包和小红包比较少。  
//        long range1 = sqr(average - min);  
//        long range2 = sqr(max - average);  
  
        for (int i = 0; i < result.length; i++) {  
            //因为小红包的数量通常是要比大红包的数量要多的，因为这里的概率要调换过来。  
            //当随机数>平均值，则产生小红包  
            //当随机数<平均值，则产生大红包  
            if (nextLong(min, max) > average) {  
                // 在平均线上减钱  
//              long temp = min + sqrt(nextLong(range1));  
                long temp = min + xRandom(min, average);  
                result[i] = temp;  
                total -= temp;  
            } else {  
                // 在平均线上加钱  
//              long temp = max - sqrt(nextLong(range2));  
                long temp = max - xRandom(average, max);  
                result[i] = temp;  
                total -= temp;  
            }  
        }  
        // 如果还有余钱，则尝试加到小红包里，如果加不进去，则尝试下一个。  
        while (total > 0) {  
            for (int i = 0; i < result.length; i++) {  
                if (total > 0 && result[i] < max) {  
                    result[i]++;  
                    total--;  
                }  
            }  
        }  
        // 如果钱是负数了，还得从已生成的小红包中抽取回来  
        while (total < 0) {  
            for (int i = 0; i < result.length; i++) {  
                if (total < 0 && result[i] > min) {  
                    result[i]--;  
                    total++;  
                }  
            }  
        }  
        return result;  
    }  
    
    /** 
     * 生产min和max之间的随机数，但是概率不是平均的，从min到max方向概率逐渐加大。 
     * 先平方，然后产生一个平方值范围内的随机数，再开方，这样就产生了一种“膨胀”再“收缩”的效果。 
     *  
     * @param min 
     * @param max 
     * @return 
     */  
    static long xRandom(long min, long max) {  
        return sqrt(nextLong(sqr(max - min)));  
    } 
    
    static long sqrt(long n) {  
        // 改进为查表？  
        return (long) Math.sqrt(n);  
    } 
    
    static long sqr(long n) {  
        // 查表快，还是直接算快？  
        return n * n;  
    }  

	/**
	 * 数值分割
	 * @param total 总值
	 * @param num 个数
	 * @param min 最少
	 * @param newScale 分割值   0全整数   2小数点2位  要返回的 BigDecimal 值的标度。
	 */
	public static double[] devide(double total, int num, double min, int newScale){
		double[] values = new double[num];
		BigDecimal money_bd;
		BigDecimal total_bd;
		double safe_total;
		double money;
		for(int i=1; i<num; i++){
			safe_total =(total-(num-i)*min)/(num-i);
			money = Math.random()*(safe_total-min)+min;
			money_bd = new BigDecimal(money);
			money = money_bd.setScale(newScale, BigDecimal.ROUND_HALF_UP).doubleValue();  
			total = total-money;
			total_bd = new BigDecimal(total);
			total = total_bd.setScale(newScale, BigDecimal.ROUND_HALF_UP).doubleValue();
//			System.out.println("第"+i+"个红包："+money+",余额为:"+total+"元");
			values[i-1] = money;
		}
//		System.out.println("第"+num+"个红包："+total+",余额为:0元");
		values[num-1] = total;
		return values;
	}

	public static void main(String[] args) {
		double[] values = devide(32, 4, 1, 0);
		for (int i = 0; i < values.length; i++) {
			System.out.println((int)values[i]);
		}
		
		
		// 红包测试
//		long max = 50;  
//        long min = 1;  
//  
//        long[] result = generate(100, 4, max, min);  
//        long total = 0;  
//        for (int i = 0; i < result.length; i++) {  
//            // System.out.println("result[" + i + "]:" + result[i]);  
////             System.out.println(result[i]);  
//            total += result[i];  
//        }  
//        //检查生成的红包的总额是否正确  
//        System.out.println("total:" + total);  
//  
//        //统计每个钱数的红包数量，检查是否接近正态分布  
//        int count[] = new int[(int) max + 1];  
//        for (int i = 0; i < result.length; i++) {  
//            count[(int) result[i]] += 1;  
//        }  
//  
//        for (int i = 0; i < count.length; i++) {  
//            System.out.println("" + i + "  " + count[i]);  
//        }  
		
		
		
		
		
	}

}
