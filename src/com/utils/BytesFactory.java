package com.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

/**
 * bytes 处理工厂
 * @author xujinbo
 *
 */
public class BytesFactory {

	/**
	 * 将二进制数据转换成list数组
	 * @param bytes
	 * @param clas
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> byteToObject(byte[] bytes, Class<T> clas) {
		T t = null;
		ByteArrayInputStream bi = null;
		EntityInputStream oi = null;
		List<T> list = new ArrayList<>();
		try {
			bi = new ByteArrayInputStream(bytes);
			oi = new EntityInputStream(bi);
			int size = oi.readInt();
			for (int i = 0; i < size; i++) {
				t = (T) BytesFactory.createBytes(null, (IAttributDataStream) clas.newInstance(), oi);
				list.add(t);
			}
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("list生成对象报错", e);
		} finally {
			try {
				if(bi!=null) bi.close();
				if(oi!=null) oi.close();
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("list生成对象关闭流报错", e);
			}
		}
		return list;
	}
	
	/**
	 * 将数据转换为字节
	 * @param obj 可写类型byte[] Boolean Integer Long String Object
	 * @return
	 */
	public static byte[] objectToByteDatas(Object ...arge) {
		byte[] bytes = null;
		ByteArrayOutputStream bo = null;
		EntityOutputStream oo = null;
		try {
			bo = new ByteArrayOutputStream();
			oo = new EntityOutputStream(bo);
			Object obj;
			for (int i = 0; i < arge.length; i++) {
				obj = arge[i];
				if (obj instanceof byte[]) {
					bytes = (byte[]) obj;
					oo.write(bytes, 0, bytes.length);
				} else if (obj instanceof Boolean) {
					oo.writeBoolean((boolean) obj);
				} else if (obj instanceof Integer) {
					oo.writeInt((Integer) obj);
				} else if (obj instanceof Long) {
					oo.writeLong((Long) obj);
				} else if (obj instanceof String) {
					oo.writeUTF((String) obj);
				} else {
					oo.writeObject(obj);
				}
			}
			oo.flush();
			bytes = bo.toByteArray();
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("obj 可写类型byte[] 报错", e);
		} finally {
			try {
				if(bo!=null) bo.close();
				if(oo!=null) oo.close();
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("obj 可写类型byte[] 关闭流报错", e);
			}
		}
		return bytes;
	}
	
	/**
	 * 将对象装备成字节
	 * @param entity
	 * @return
	 */
	public static byte[] objectToByteData(IAttributDataStream entity) {
		return objectToByteData(entity, null);
	}
	
	/**
	 * 将对象装备成字节
	 * @param entity
	 * @param oo2  是否是第一个流
	 * @return
	 */
	public static byte[] objectToByteData(IAttributDataStream entity, EntityOutputStream oo2 ) {
		byte[] bytes = null;
		ByteArrayOutputStream bo = null;
		EntityOutputStream oo = oo2;
		try {
			if (oo2 == null) {
				bo = new ByteArrayOutputStream();
				oo = new EntityOutputStream(bo);
			}
			entity.getAttributeData(oo);
			if (oo2 == null) {
				oo.flush();
				bytes = bo.toByteArray();
			}
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("objectToByteData 报错", e);
		} finally {
			try {
				if(bo!=null) bo.close();
				if (oo2 == null) {
					if(oo!=null) oo.close();
				}
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("objectToByteData 关闭流报错", e);
			}
		}
		return bytes;
	}
	
	/**
	 * 根据二进制 读取信息到指定类
	 * @param bytes
	 * @param attributDataStream
	 * @return
	 */
	public static IAttributDataStream createBytes(byte[] bytes,
			IAttributDataStream attributDataStream) {
		return createBytes(bytes, attributDataStream, null);
	}
	
	/**
	 * 根据二进制 读取信息到指定类
	 * @param bytes
	 * @param entity
	 * @return 
	 */
	public static IAttributDataStream createBytes(byte[] bytes, 
			IAttributDataStream attributDataStream, EntityInputStream oi2 ) {
		ByteArrayInputStream bi = null;
		EntityInputStream oi = oi2;
		try {
			if (oi2 == null) {
				bi = new ByteArrayInputStream(bytes);
				oi = new EntityInputStream(bi);
			}
			attributDataStream.setAttributeData(oi);
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("解析成实体IAttributDataStream报错", e);
		} finally {
			try {
				if(bi!=null) bi.close();
				if (oi2 == null) {
					if(oi!=null) oi.close();
				}
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("解析成实体IAttributDataStream 关闭流报错", e);
			}
		}
		return attributDataStream;
	}
	
	/**
	 * list 转 byte[]  对象必须实现接口  IAttributDataStream
	 * @param <T> 实现接口  IAttributDataStream
	 * @param list
	 * @return
	 */
	public static <T> byte[] listToBytes(List<T> list) {
		byte[] bytes = null;
		ByteArrayOutputStream bo = null;
		EntityOutputStream oo = null;
		try {
			bo = new ByteArrayOutputStream();
			oo = new EntityOutputStream(bo);
			int listLen = list.size();
			oo.writeInt(listLen);
			IAttributDataStream attributDataStream;
			for (int i = 0; i < listLen; i++) {
				attributDataStream = (IAttributDataStream) list.get(i);
				objectToByteData(attributDataStream, oo);
			}
			oo.flush();
			bytes = bo.toByteArray();
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("list 转 byte[] 报错", e);
		} finally {
			try {
				if(bo!=null) bo.close();
				if(oo!=null) oo.close();
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("list 转 byte[] 关闭流报错", e);
			}
		}
		return bytes;
	}

	/**
	 * long数组转换byte[]
	 * @param lon
	 * @return
	 */
	public static byte[] longArrayToByte(long[] lon) {
		byte[] bytes = null;
		ByteArrayOutputStream bo = null;
		EntityOutputStream oo = null;
		try {
			bo = new ByteArrayOutputStream();
			oo = new EntityOutputStream(bo);
			int listLen = lon.length;
			oo.writeInt(listLen);
			for (int i = 0; i < listLen; i++) {
				oo.writeLong(lon[i]);
			}
			oo.flush();
			bytes = bo.toByteArray();
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("long数组转换 byte[]报错", e);
		} finally {
			try {
				if(bo!=null) bo.close();
				if(oo!=null) oo.close();
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("long数组转换 byte[]关闭流报错", e);
			}
		}
		return bytes;
	}

	/**
	 * 将二进制转换成long数组
	 * @param member
	 * @return 
	 */
	public static long[] byteToLongArray(byte[] buf) {
		ByteArrayInputStream bi = null;
		EntityInputStream oi = null;
		long[] list = null;
		try {
			bi = new ByteArrayInputStream(buf);
			oi = new EntityInputStream(bi);
			int size = oi.readInt();
			list = new long[size];
			for (int i = 0; i < size; i++) {
				list[i] = oi.readLong();
			}
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("将二进制转换成long数组报错", e);
		} finally {
			try {
				if(bi!=null) bi.close();
				if(oi!=null) oi.close();
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("将二进制转换成long数组关闭流报错", e);
			}
		}
		return list;
	}
	
	/**
	 * long list数组转换byte[]
	 * @param lon
	 * @return
	 */
	public static byte[] longListToByte(List<Long> lon) {
		byte[] bytes = null;
		ByteArrayOutputStream bo = null;
		EntityOutputStream oo = null;
		try {
			bo = new ByteArrayOutputStream();
			oo = new EntityOutputStream(bo);
			int listLen = lon.size();
			oo.writeInt(listLen);
			for (int i = 0; i < listLen; i++) {
				oo.writeLong(lon.get(i));
			}
			oo.flush();
			bytes = bo.toByteArray();
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("long数组转换 byte[]报错", e);
		} finally {
			try {
				if(bo!=null) bo.close();
				if(oo!=null) oo.close();
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("long数组转换 byte[]关闭流报错", e);
			}
		}
		return bytes;
	}
	
	/**
	 * 将二进制转换成long list数组
	 * @param member
	 * @return 
	 */
	public static List<Long> byteToLongList(byte[] buf) {
		ByteArrayInputStream bi = null;
		EntityInputStream oi = null;
		List<Long> list = new ArrayList<Long>();
		try {
			bi = new ByteArrayInputStream(buf);
			oi = new EntityInputStream(bi);
			int size = oi.readInt();
			for (int i = 0; i < size; i++) {
				list.add(oi.readLong());
			}
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("将二进制转换成long数组报错", e);
		} finally {
			try {
				if(bi!=null) bi.close();
				if(oi!=null) oi.close();
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("将二进制转换成long数组关闭流报错", e);
			}
		}
		return list;
	}
	
	/**
	 * long数组转换byte[]
	 * @param lon
	 * @return
	 */
	public static byte[] intArrayToByte(Integer[] lon) {
		byte[] bytes = null;
		ByteArrayOutputStream bo = null;
		EntityOutputStream oo = null;
		try {
			bo = new ByteArrayOutputStream();
			oo = new EntityOutputStream(bo);
			int listLen = lon.length;
			oo.writeInt(listLen);
			for (int i = 0; i < listLen; i++) {
				oo.writeInt(lon[i]);
			}
			oo.flush();
			bytes = bo.toByteArray();
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("int数组转换 byte[]报错", e);
		} finally {
			try {
				if(bo!=null) bo.close();
				if(oo!=null) oo.close();
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("int数组转换 byte[]关闭流报错", e);
			}
		}
		return bytes;
	}
	
	/**
	 * 将二进制转换成int数组
	 * @param member
	 * @return 
	 */
	public static Integer[] byteToIntegerArray(byte[] buf) {
		ByteArrayInputStream bi = null;
		EntityInputStream oi = null;
		Integer[] list = null;
		try {
			bi = new ByteArrayInputStream(buf);
			oi = new EntityInputStream(bi);
			int size = oi.readInt();
			list = new Integer[size];
			for (int i = 0; i < size; i++) {
				list[i] = oi.readInt();
			}
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("将二进制转换成int数组报错", e);
		} finally {
			try {
				if(bi!=null) bi.close();
				if(oi!=null) oi.close();
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("将二进制转换成int数组关闭流报错", e);
			}
		}
		return list;
	}
	
	/**
	 * 拷贝对象
	 * @param <T>
	 * @param object
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T deepCopy(T t) {  
        // 将该对象序列化成流,因为写在流里的是对象的一个拷贝，而原对象仍然存在于JVM里面。所以利用这个特性可以实现对象的深拷贝  
        ByteArrayOutputStream bos = new ByteArrayOutputStream();  
        ObjectOutputStream oos = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
		try {
			oos = new ObjectOutputStream(bos);
			oos.writeObject(t);  
			bis = new ByteArrayInputStream(bos.toByteArray());  
			ois = new ObjectInputStream(bis);  
			// 将流序列化成对象  
			return (T) ois.readObject();
		} catch (Exception e) {
			Logger.getLogger("BytesFactory").error("拷贝对象报错", e);
		} finally {
			try {
				if(bos!=null) bos.close();
				if(oos!=null) oos.close();
				if(bis!=null) bis.close();
				if(ois!=null) ois.close();
			} catch (IOException e) {
				Logger.getLogger("BytesFactory").error("拷贝对象关闭流报错", e);
			}
		}
		return null;
    }  
	
}
