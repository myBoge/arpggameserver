package com.utils;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringUtils;

/**
 * 随机生成
 * @author xujinbo
 *
 */
public class VerificationUtils {

	private static Random strGen = new Random();;
	private static Random numGen = new Random();;
	private static char[] numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
			.toCharArray();
	private static char[] numbers = ("0123456789").toCharArray();;

	/**  产生随机字符串  */
	public static final String randomString(int length) {
		if (length < 1) {
			return null;
		}
		char[] randBuffer = new char[length];
		for (int i = 0; i < randBuffer.length; i++) {
			randBuffer[i] = numbersAndLetters[strGen.nextInt(61)];
		}
		return new String(randBuffer);
	}

	/**  产生随机数值字符串  */
	public static final String randomNumStr(int length) {
		if (length < 1) {
			return null;
		}
		char[] randBuffer = new char[length];
		for (int i = 0; i < randBuffer.length; i++) {
			randBuffer[i] = numbers[numGen.nextInt(9)];
		}
		return new String(randBuffer);
	}

	
	
	private static int maxvaluefive = 99999999;
	private static int minvaluefive = 0;
	private static AtomicInteger atomic = new AtomicInteger(minvaluefive);

	/** 生成序列号 */
	static String getSeqFive(int coverPad) {
		for (;;) {
			int current = atomic.get();
			int newValue = current >= maxvaluefive ? minvaluefive : current + 1;
			if (atomic.compareAndSet(current, newValue)) {
				return StringUtils.leftPad(String.valueOf(current), coverPad, "0");
			}
		}
	}

	
	public static void main(String[] args) {
		
//		System.out.println(randomNumStr(10));
//		System.out.println(randomString(10));
//		System.out.println(getSeqFive(10));
		
	}
	
}
