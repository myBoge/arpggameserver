package com.utils;

import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.boge.share.Resource;
import com.data.ConnectData;
import com.data.UserDataManage;
import com.entity.UserInfo;
import com.mapper.UserMapper;
import com.model.GameData;
import com.server.ServerInfoServer;
import com.server.UserServer;

/**
 * 时间处理
 * @author boge
 */
public class DateManage implements Runnable {

	@Resource
	private ServerInfoServer serverInfoServer;
	@Resource
	private UserDataManage userDataManage;
	@Resource
	private UserServer userServer;
	@Resource
	private ObjectPool objectPool;
	
	/** 当前线程启动 */
	public boolean isOpen = true;
	/** 今天的时间 */
	private long todayDate;
	
	public DateManage() {
	}
	
	/**
	 * 开启时间线程
	 */
	public void start(){
		
		todayDate = System.currentTimeMillis();
		
		//判断开服时间是否隔夜了
		checkLastNight();
		
		Thread thread = new Thread(this);
		thread.setName("运行时间监听");
		thread.start();
		
	}
	
	public void stop(){
		isOpen = false;
	}
	
	/**
	 * 检查当前开服是否是隔夜开服
	 */
	public void checkLastNight() {
		
		Calendar calendar = Calendar.getInstance();// 服务器开服时间
		calendar.setTimeInMillis(serverInfoServer.getBeforeOpenTimer().getTime());
		Calendar current = Calendar.getInstance();// 当前时间
		current.setTimeInMillis(System.currentTimeMillis());
		if(!DateUtils.isSameDay(calendar, current)) {
			System.out.println("隔日开服   重置每日数据!");
			todayDate = System.currentTimeMillis();
			serverInfoServer.updateTodayData(todayDate);
			
		}
		
		//TODO 时间不对重置属性
//		GameData.gameData.reset();
		
	}

	@Override
	public void run() {
		while(isOpen){
			try {
				Thread.sleep(1000);
				Calendar calendar = Calendar.getInstance();// 服务器开服时间
				calendar.setTimeInMillis(todayDate);
				Calendar current = Calendar.getInstance();// 当前时间
				current.setTimeInMillis(System.currentTimeMillis());
				if(DateUtils.isSameDay(calendar, current)) { // 检查是否是同一天
//					System.out.println("aaaaaaa");
					updateOnlineUserData();
				} else {
					//重置不要属性
					System.out.println("当前新时间开始   重置每日数据!");
					todayDate = System.currentTimeMillis();
					serverInfoServer.updateTodayData(todayDate);
					// 更新每日数据
					List<UserInfo> onLineUserInfo = userDataManage.getOnLineUserInfo();
					if (onLineUserInfo.isEmpty()) {
						return;
					}
					SqlSession sqlSession = ConnectData.openSession();
					try {
						UserMapper userManage = sqlSession.getMapper(UserMapper.class);
						UserInfo userInfo;
						for (int i = 0; i < onLineUserInfo.size(); i++) {
							userInfo = onLineUserInfo.get(i);
							if (userInfo != null) {
								GameData.gameData.resetDayUser(userInfo, false);
								userManage.update(userInfo);
							}
						}
						sqlSession.commit();
					} finally {
						if(sqlSession!=null)sqlSession.close();
					}
				}
			} catch (InterruptedException e) {
				Logger.getLogger(getClass()).fatal("游戏运行时间报错!", e);
			}
		}
	}

	/**
	 * 更新当前在线玩家的在线数据
	 */
	private void updateOnlineUserData() {
		
	}

}
