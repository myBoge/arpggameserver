package com.utils;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

/**
 * 图片工具
 * @author xujinbo
 *
 */
public class ImageUtil {

	/**
	 * 获取image
	 * @param path
	 * @return
	 */
	public static BufferedImage getImageByte(String path) {  
        File file = new File(path);  
        BufferedImage bi = null;  
        try {  
            bi = ImageIO.read(file);  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
		return bi;
	}
	
	/** 
     * 读取一张图片的RGB值 
     * @param bi
     * @param x
     * @param y
     * @return
     * @throws Exception
     */
    public static int getImagePixel(BufferedImage bi, int x, int y) throws Exception {  
        int width = bi.getWidth();  
        int height = bi.getHeight();  
//        int minx = bi.getMinX();  
//        int miny = bi.getMinY();  
//        System.out.println("width=" + width + ",height=" + height + ".");  
//        System.out.println("minx=" + minx + ",miniy=" + miny + ".");
        int pixel = 0;
        if (width >= x && height >= y) {
        	pixel = bi.getRGB(x, y); // 下面三行代码将一个数字转换为RGB数字
        	pixel = (pixel & 0xFFFFFF);
//	    	rgb[0] = (pixel & 0xff0000) >> 16;  
//	    	rgb[1] = (pixel & 0xff00) >> 8;  
//	    	rgb[2] = (pixel & 0xff);  
//	    	System.out.println("i=" + i + ",j=" + j + ":(" + rgb[0] + ","   + rgb[1] + "," + rgb[2] + ")");  
		}
        return pixel;
    }  
	
	/** 
     * 读取一张图片的RGB值 
     * @param path
     * @throws Exception
     */
    public static void getImagePixel(String path) throws Exception {  
        BufferedImage bi = getImageByte(path);  
        int[] rgb = new int[3];  
        int width = bi.getWidth();  
        int height = bi.getHeight();  
        int minx = bi.getMinX();  
        int miny = bi.getMinY();  
        System.out.println("width=" + width + ",height=" + height + ".");  
        System.out.println("minx=" + minx + ",miniy=" + miny + ".");  
        for (int i = minx; i < width; i++) {  
            for (int j = miny; j < height; j++) {  
                int pixel = bi.getRGB(i, j); // 下面三行代码将一个数字转换为RGB数字  
//                System.out.println(pixel);
                rgb[0] = (pixel & 0xff0000) >> 16;  
                rgb[1] = (pixel & 0xff00) >> 8;  
                rgb[2] = (pixel & 0xff);  
                System.out.println("i=" + i + ",j=" + j + ":(" + rgb[0] + ","   + rgb[1] + "," + rgb[2] + ")");  
            }  
        }  
    }  
    
    /** 
     * 返回屏幕色彩值 
     *  
     * @param x 
     * @param y 
     * @return 
     * @throws AWTException 
     */  
    public static int getScreenPixel(int x, int y) throws AWTException { // 函数返回值为颜色的RGB值。  
        Robot rb = null; // java.awt.image包中的类，可以用来抓取屏幕，即截屏。  
        rb = new Robot();  
        Toolkit tk = Toolkit.getDefaultToolkit(); // 获取缺省工具包  
        Dimension di = tk.getScreenSize(); // 屏幕尺寸规格  
        System.out.println(di.width);  
        System.out.println(di.height);  
        Rectangle rec = new Rectangle(0, 0, di.width, di.height);  
        BufferedImage bi = rb.createScreenCapture(rec);  
        int pixelColor = bi.getRGB(x, y);  
  
        return 16777216 + pixelColor; // pixelColor的值为负，经过实践得出：加上颜色最大值就是实际颜色值。  
    }  
	
}
