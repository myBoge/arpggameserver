package com.thread;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.data.UserDataManage;
import com.entity.UserInfo;

/**
 * 向客户端发送消息线程
 * @author xujinbo
 */
public class SendDataThread extends Thread {
	
	@Resource
	private UserDataManage userDataManage;
	
	/** 当前线程启动与否 */
	public boolean isOpen = true;
	
	private List<SendData> ads = new ArrayList<>();
	
	public SendDataThread() {
		setName("向客户端发送消息线程");
	}

	@Override
	public void run() {
		while(isOpen){
			try {
				Thread.sleep(200);
				sendActionData();
			} catch (InterruptedException e) {
				Logger.getLogger(getClass()).fatal("向客户端发送消息线程报错!", e);
			}
			
		}
	}
	
	private void sendActionData() {
		if (ads.size() > 0) {
			SendData sendData = ads.remove(0);
			sendAllOnLineData(sendData);
		}
	}

	public void close() {
		isOpen = false;
	}
	
	/**
	 * 添加发送消息
	 * @param action
	 * @param session 对此socket发此消息
	 * @param arge
	 */ 
	public void addActionData(int action, IoSession session, Object ...arge) {
		GameOutput gameOutput = new GameOutput();
		try {
			gameOutput.write(arge);
			addActionData(new ActionData<>(action, gameOutput.toByteArray()), session);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("写入发送数据报错!", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("写入发送数据关闭流报错!", e);
				}
			}
		}
	}
	
	/**
	 * 添加发送消息
	 * @param action
	 * @param userInfo 对此用户发此消息
	 * @param arge
	 */
	public void addActionData(int action, UserInfo userInfo, Object ...arge) {
		GameOutput gameOutput = new GameOutput();
		try {
			gameOutput.write(arge);
			addActionData(new ActionData<>(action, gameOutput.toByteArray()), userInfo);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("写入发送数据报错!", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("写入发送数据关闭流报错!", e);
				}
			}
		}
	}
	
	/**
	 * 添加发送消息（所有在线用户）
	 * @param action
	 * @param arge
	 */
	public void addActionData(int action, Object ...arge) {
		GameOutput gameOutput = new GameOutput();
		try {
			gameOutput.write(arge);
			addActionData(new ActionData<>(action, gameOutput.toByteArray()));
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("写入发送数据报错!", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("写入发送数据关闭流报错!", e);
				}
			}
		}
	}
	
	/**
	 * 添加发送消息
	 * @param action
	 * @param buf
	 * @param session 对此socket发此消息
	 */ 
	public void addActionData(int action, byte[] buf, IoSession session) {
		addActionData(new ActionData<>(action, buf), session);
	}
	
	/**
	 * 添加发送消息
	 * @param action
	 * @param buf
	 * @param userInfo 对此用户发此消息
	 */
	public void addActionData(int action, byte[] buf, UserInfo userInfo) {
		addActionData(new ActionData<>(action, buf), userInfo);
	}
	
	/**
	 * 添加发送消息（所有在线用户）
	 * @param action
	 * @param buf
	 */
	public void addActionData(int action, byte[] buf) {
		addActionData(new ActionData<>(action, buf));
	}
	
	/**
	 * 添加发送消息
	 * @param action
	 * @param session 对此socket发此消息
	 */
	public void addActionData(ActionData<Object> action, IoSession session) {
		ads.add(new SendData(session, action));
	}
	
	/**
	 * 添加发送消息
	 * @param action
	 * @param userInfo 对此用户发此消息
	 */
	public void addActionData(ActionData<Object> action, UserInfo userInfo) {
		ads.add(new SendData(userInfo, action));
	}
	
	/**
	 * 添加发送消息（所有在线用户）
	 * @param action
	 */
	public void addActionData(ActionData<Object> action) {
		ads.add(new SendData(action));
	}

	/**
	 * 给所有在线玩家发送数据
	 * @param action
	 */
	private void sendAllOnLineData(SendData sendData) {
		if (sendData.getActionData() == null) return;
		// 如果两个都是null
		if (sendData.getSession()==null && sendData.getUserInfo()==null) {
			List<UserInfo> onLineUserInfo = userDataManage.getOnLineUserInfo();
			UserInfo userInfo;
			for (int i = 0; i < onLineUserInfo.size(); i++) {
				userInfo = onLineUserInfo.get(i);
				userInfo.sendData(sendData.getActionData());
			}
		} else if (sendData.getUserInfo() == null && sendData.getSession() != null) {
			if (sendData.getSession().isConnected()) {
				sendData.getSession().write(sendData.getActionData());
			}
		} else if (sendData.getSession() == null && sendData.getUserInfo() != null) {
			sendData.getUserInfo().sendData(sendData.getActionData());
		}
	}
	
	
	class SendData {
		
		private UserInfo userInfo;
		private IoSession session;
		private ActionData<Object> actionData;
		public SendData(ActionData<Object> actionData) {
			super();
			this.actionData = actionData;
		}
		public SendData(IoSession session, ActionData<Object> actionData) {
			super();
			this.session = session;
			this.actionData = actionData;
		}
		public SendData(UserInfo userInfo, ActionData<Object> actionData) {
			super();
			this.userInfo = userInfo;
			this.actionData = actionData;
		}
		/**
		 * 获取userInfo
		 * @return userInfo userInfo
		 */
		public UserInfo getUserInfo() {
			return userInfo;
		}
		/**
		 * 设置userInfo
		 * @param userInfo userInfo
		 */
		public void setUserInfo(UserInfo userInfo) {
			this.userInfo = userInfo;
		}
		/**
		 * 获取session
		 * @return session session
		 */
		public IoSession getSession() {
			return session;
		}
		/**
		 * 设置session
		 * @param session session
		 */
		public void setSession(IoSession session) {
			this.session = session;
		}
		/**
		 * 获取actionData
		 * @return actionData actionData
		 */
		public ActionData<Object> getActionData() {
			return actionData;
		}
		/**
		 * 设置actionData
		 * @param actionData actionData
		 */
		public void setActionData(ActionData<Object> actionData) {
			this.actionData = actionData;
		}
		@Override
		public String toString() {
			return "SendData [userInfo=" + userInfo + ", session=" + session
					+ ", actionData=" + actionData + "]";
		}
		
	}
	
}