package com.thread;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.configs.MapConfig;
import com.entity.GrindMap;
import com.entity.GrindTeam;
import com.server.GrindServer;

/**
 * 副本练级地图
 * @author xujinbo
 *
 */
public class GrindThread extends Thread {

	/** 根据地图id 获取此地图上练级的队伍信息 */
	private Map<Long, List<GrindTeam>> mapGrindData = new HashMap<>();
	/** 当前线程启动与否 */
	public boolean isOpen = true;
	private MapConfig grindConfig;
	private GrindServer grindServer;
	
	public GrindThread() {
		super();
		setName("副本练级");
	}
	
	@Override
	public void run() {
		while(isOpen){
			try {
				Thread.sleep(1000);
				updateGrindData();
			} catch (InterruptedException e) {
				Logger.getLogger(getClass()).fatal("押镖地图报错!", e);
			}
		}
	}

	/**
	 * 更新当前地图中  练级的人
	 */
	private void updateGrindData() {
		Set<Long> keys = mapGrindData.keySet();
		Iterator<Long> it = keys.iterator();
		List<GrindTeam> grindTeamList;
		GrindTeam grindTeam;
		long mapId;
		GrindMap grindMap;
		while(it.hasNext()){
			mapId = it.next();
			grindTeamList = mapGrindData.get(mapId);
//			if (mapId == 1001) {
//				System.out.println("updateEscortData="+mapId);
//				System.out.println("updateEscortData="+escortList.size());
//			}
			for (int i = 0; i < grindTeamList.size(); i++) {
				grindTeam = grindTeamList.get(i);
				// 不处于战斗中  在练级场景
				if (!grindTeam.isFight()) { 
					// 队伍检查是否合法
					if(grindTeam.checkValid()) {
						grindMap = grindConfig.getCacheSample(grindTeam.getMapId());
						//  下一次遇怪时间不为0
						if (grindTeam.getNextTime()!=0 && grindTeam.getNextTime() <= System.currentTimeMillis()) {
							// 遇怪
							grindTeam.setNextTime(0);
							grindServer.createFight(grindTeam, grindMap);
						}
					} else {
						removeGrindTeam(mapId, grindTeam);
						i--;
					}
				}
			}
		}
		
	}

	/**
	 * 从练级队伍中移除
	 * @param mapId 地图id
	 * @param grindTeam 地图队伍
	 */
	public void removeGrindTeam(long mapId, GrindTeam grindTeam) {
		List<GrindTeam> grindTeamList = mapGrindData.get(mapId);
		grindTeamList.remove(grindTeam);
	}

	/**
	 * 设置grindConfig
	 * @param grindConfig grindConfig
	 */
	public void setGrindConfig(MapConfig grindConfig) {
		this.grindConfig = grindConfig;
	}

	/**
	 * 获取根据地图id获取此地图上练级的队伍信息
	 * @return mapGrindData 根据地图id获取此地图上练级的队伍信息
	 */
	public Map<Long, List<GrindTeam>> getMapGrindData() {
		return mapGrindData;
	}

	/**
	 * 设置grindServer
	 * @param grindServer grindServer
	 */
	public void setGrindServer(GrindServer grindServer) {
		this.grindServer = grindServer;
	}

	/**
	 * 根据地图id 和 队伍id  获取队伍信息
	 * @param mapId
	 * @param teamId
	 */
	public GrindTeam getGrindTeam(long mapId, long teamId) {
		List<GrindTeam> lists = mapGrindData.get(mapId);
		if (lists != null) {
			int len = lists.size();
			GrindTeam grindTeam;
			for (int i = 0; i < len; i++) {
				grindTeam = lists.get(i);
				if(grindTeam.getId() == teamId) {
					return grindTeam;
				}
			}
		}
		return null;
	}
	
}
