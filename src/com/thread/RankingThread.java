package com.thread;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.boge.util.ResourceCell;
import com.data.ConnectData;
import com.data.UserDataManage;
import com.entity.RankingData;
import com.entity.RankingList;
import com.entity.Role;
import com.entity.UserInfo;
import com.mapper.RankingMapper;
import com.utils.BytesFactory;

/**
 * 排行榜数据处理
 * @author xujinbo
 *
 */
public class RankingThread extends Thread {

	/** 当前线程启动与否 */
	public boolean isOpen = true;
	/** 排行榜数据 */
	private RankingList rankingList;
	/** 用户信息 */
	private UserDataManage userDataManage;
	
	private ComparatorRole comparatorRole;
	/** 排序时使用的数组 */
	private List<UserInfo> userInfoList = new ArrayList<UserInfo>();
	/** 宠物排序时使用的数组 */
	private List<UserInfo> petList = new ArrayList<UserInfo>();
	
	public RankingThread() {
		setName("排行榜");
		comparatorRole = new ComparatorRole();
		
		userDataManage = ResourceCell.getResource(UserDataManage.class);
	}
	
	@Override
	public void run() {
		while(isOpen){
			try {
				statistics();
//				Thread.sleep(1000*60*10);// 10分钟执行一次统计
				Thread.sleep(1000*60);// 1分钟执行一次统计
			} catch (InterruptedException e) {
				Logger.getLogger(getClass()).fatal("排行榜数据处理报错!", e);
			}
		}
	}

	/**
	 * 统计排行榜
	 */
	private void statistics() {
		if (rankingList == null) {
			initRankData();
		}
		userInfoList.clear();
		petList.clear();
		List<UserInfo> userList = userDataManage.getUserInfos();
		UserInfo userInfo;
		UserInfo petInfo;
		List<Role> roles;
		for (int i = 0; i < userList.size(); i++) {
			userInfo = userList.get(i);
//			System.out.println(userInfo);
			userInfoList.add(userInfo);
			roles = userInfo.getPetLibrarys().getRoles();
			for (int j = 0; j < roles.size(); j++) {
				if (roles.get(j).isBaby()) {
					petInfo = new UserInfo(userInfo.getId());
					petInfo.setRoleName(userInfo.getRoleName());
					petInfo.setRole(roles.get(j));
					petList.add(petInfo);
				}
			}
		}
		
		int len = 100;//要获取的数量
		// 等级排序
		comparatorRole.type = 0;
		userInfoList.sort(comparatorRole);
		List<RankingData> list = rankingList.getRoleLevel();
		addUserData(userInfoList, list, len, false);
		// 人物物伤
		comparatorRole.type = 1;
		userInfoList.sort(comparatorRole);
		list = rankingList.getRoleOrdinary();
		addUserData(userInfoList, list, len, false);
		// 人物法伤
		comparatorRole.type = 2;
		userInfoList.sort(comparatorRole);
		list = rankingList.getRoleMagic();
		addUserData(userInfoList, list, len, false);
		// 人物速度
		comparatorRole.type = 3;
		userInfoList.sort(comparatorRole);
		list = rankingList.getRoleSpeed();
		addUserData(userInfoList, list, len, false);
		// 人物防御
		comparatorRole.type = 4;
		userInfoList.sort(comparatorRole);
		list = rankingList.getRoleDefend();
		addUserData(userInfoList, list, len, false);
		
		// 宠物物伤
		comparatorRole.type = 1;
		petList.sort(comparatorRole);
		list = rankingList.getPetOrdinary();
		addUserData(petList, list, len, true);
		// 宠物法伤
		comparatorRole.type = 2;
		petList.sort(comparatorRole);
		list = rankingList.getPetMagic();
		addUserData(petList, list, len, true);
		// 宠物速度
		comparatorRole.type = 3;
		petList.sort(comparatorRole);
		list = rankingList.getPetSpeed();
		addUserData(petList, list, len, true);
		// 宠物防御
		comparatorRole.type = 4;
		petList.sort(comparatorRole);
		list = rankingList.getPetDefend();
		addUserData(petList, list, len, true);
		
		SqlSession sqlSession = ConnectData.openSession();
		try {
			RankingMapper rankingMapper = sqlSession.getMapper(RankingMapper.class);
			rankingList.setRoleLevelRank(BytesFactory.listToBytes(rankingList.getRoleLevel()));
			rankingList.setRoleOrdinaryRank(BytesFactory.listToBytes(rankingList.getRoleOrdinary()));
			rankingList.setRoleMagicRank(BytesFactory.listToBytes(rankingList.getRoleMagic()));
			rankingList.setRoleDefendRank(BytesFactory.listToBytes(rankingList.getRoleDefend()));
			rankingList.setRoleSpeedRank(BytesFactory.listToBytes(rankingList.getRoleSpeed()));
			
			rankingList.setPetOrdinaryRank(BytesFactory.listToBytes(rankingList.getPetOrdinary()));
			rankingList.setPetMagicRank(BytesFactory.listToBytes(rankingList.getPetMagic()));
			rankingList.setPetDefendRank(BytesFactory.listToBytes(rankingList.getPetDefend()));
			rankingList.setPetSpeedRank(BytesFactory.listToBytes(rankingList.getPetSpeed()));
			
			rankingMapper.update(rankingList);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}

	/**
	 * @param dataList 排序时使用的数据
	 * @param list 排行榜数据存储数组
	 * @param len 排行榜最大长度
	 * @param isPet 是否是宠物
	 */
	private void addUserData(List<UserInfo> dataList, List<RankingData> list, int len, boolean isPet) {
		UserInfo userInfo;
		int index = 0;
//		System.out.println("*****************");
		for (int i = 0; i < dataList.size(); i++) {
			if (index >= len) {
				break;
			}
			userInfo = dataList.get(i);
			// 检查之前的榜单是否已经存在此角色信息
			if(checkBefore(list, index, userInfo)){
				continue;
			}
			if (list.size() > index) {
//				System.out.println(list.get(index).getId()+", "+userInfo.getId());
				if (list.get(index).getId() != userInfo.getId()) {
					if(addRoleType(list, userInfo, index, isPet)) {
						removeIndex(list, userInfo, index);
					} else {
//						System.out.println(index+"|"+i);
						i--;
					}
				} else {
					updateRoleData(list, userInfo, index, isPet);
				}
			} else {
				addRoleType(list, userInfo, -1, isPet);
			}
			index++;
		}
		while(list.size()>len){
			list.remove(list.size());
		}
	}

	/**
	 * 从指定的位置开始删除和指定的对象一样的值
	 * @param list
	 * @param userInfo
	 * @param index
	 */
	private void removeIndex(List<RankingData> list, UserInfo userInfo, int index) {
		index+=1;
		for (int i = index; i < list.size(); i++) {
			if (list.get(i).getId() == userInfo.getId()) {
				list.remove(i);
				break;
			}
		}
	}

	/**
	 * 检查数组在指定位置之前是否已经存在此对象
	 * @param list
	 * @param index
	 * @param userInfo
	 * @return
	 */
	private boolean checkBefore(List<RankingData> list, int index, UserInfo userInfo) {
		if (list.size() > index) {
			for (int i = 0; i < index; i++) {
				if (list.get(i).getId() == userInfo.getId()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 更新此排行榜数据
	 * @param list 排行榜数据
	 * @param userInfo
	 * @param index
	 * @param isPet 是否是宠物
	 */
	private void updateRoleData(List<RankingData> list, UserInfo userInfo, int index, boolean isPet) {
		RankingData rankingData = list.get(index);
		rankingData.setName(userInfo.getRole().getName());
		if (isPet) {
			rankingData.setRankKey(userInfo.getRoleName());
		} else {
			rankingData.setRankKey("无");
		}
		switch (comparatorRole.type) {
			case 0:
				rankingData.setRankValue(userInfo.getRole().getLevel());
				break;
			case 1:
				rankingData.setRankValue(userInfo.getRole().getOrdinaryAttack());
				break;
			case 2:
				rankingData.setRankValue(userInfo.getRole().getMagicAttack());
				break;
			case 3:
				rankingData.setRankValue(userInfo.getRole().getSpeed());
				break;
			case 4:
				rankingData.setRankValue(userInfo.getRole().getDefense());
				break;
		}
	}

	/**
	 * 添加新榜排行
	 * @param list 排行榜数据
	 * @param userInfo
	 * @param index
	 * @param isPet 是否是宠物
	 * @return
	 */
	private boolean addRoleType(List<RankingData> list, UserInfo userInfo, int index, boolean isPet) {
		RankingData rankingData;
		if (index == -1) {
			rankingData = getRankingUser(userInfo, isPet);
			switch (comparatorRole.type) {
				case 0:
					rankingData.setRankValue(userInfo.getRole().getLevel());
					break;
				case 1:
					rankingData.setRankValue(userInfo.getRole().getOrdinaryAttack());
					break;
				case 2:
					rankingData.setRankValue(userInfo.getRole().getMagicAttack());
					break;
				case 3:
					rankingData.setRankValue(userInfo.getRole().getSpeed());
					break;
				case 4:
					rankingData.setRankValue(userInfo.getRole().getDefense());
					break;
			}
			list.add(rankingData);
			return true;
		}
		switch (comparatorRole.type) {
			case 0:
				if (list.get(index).getRankValue() < userInfo.getRole().getLevel()) {
					rankingData = getRankingUser(userInfo, isPet);
					rankingData.setRankValue(userInfo.getRole().getLevel());
					list.add(index, rankingData);
					return true;
				}
				break;
			case 1:
				if (list.get(index).getRankValue() < userInfo.getRole().getOrdinaryAttack()) {
					rankingData = getRankingUser(userInfo, isPet);
					rankingData.setRankValue(userInfo.getRole().getOrdinaryAttack());
					list.add(index, rankingData);
					return true;
				}
				break;
			case 2:
				if (list.get(index).getRankValue() < userInfo.getRole().getMagicAttack()) {
					rankingData = getRankingUser(userInfo, isPet);
					rankingData.setRankValue(userInfo.getRole().getMagicAttack());
					list.add(index, rankingData);
					return true;
				}
				break;
			case 3:
				if (list.get(index).getRankValue() < userInfo.getRole().getSpeed()) {
					rankingData = getRankingUser(userInfo, isPet);
					rankingData.setRankValue(userInfo.getRole().getSpeed());
					list.add(index, rankingData);
					return true;
				}
				break;
			case 4:
				if (list.get(index).getRankValue() < userInfo.getRole().getDefense()) {
					rankingData = getRankingUser(userInfo, isPet);
					rankingData.setRankValue(userInfo.getRole().getDefense());
					list.add(index, rankingData);
					return true;
				}
				break;
		}
		return false;
	}

	private RankingData getRankingUser(UserInfo userInfo, boolean isPet) {
		RankingData rankingData = new RankingData(userInfo.getId());
		rankingData.setName(userInfo.getRole().getName());
		if (isPet) {
			rankingData.setRankKey(userInfo.getRoleName());
		} else {
			rankingData.setRankKey("无");
		}
		return rankingData;
	}

	private void initRankData() {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			RankingMapper rankingMapper = sqlSession.getMapper(RankingMapper.class);
			rankingList = rankingMapper.queryId(1);
			if (rankingList == null) {
				rankingList = new RankingList();
				rankingList.setRoleLevel(new ArrayList<>());
				rankingList.setRoleOrdinary(new ArrayList<>());
				rankingList.setRoleMagic(new ArrayList<>());
				rankingList.setRoleDefend(new ArrayList<>());
				rankingList.setRoleSpeed(new ArrayList<>());
				
				rankingList.setPetOrdinary(new ArrayList<>());
				rankingList.setPetMagic(new ArrayList<>());
				rankingList.setPetDefend(new ArrayList<>());
				rankingList.setPetSpeed(new ArrayList<>());
				
				rankingList.setRoleLevelRank(BytesFactory.listToBytes(rankingList.getRoleLevel()));
				rankingList.setRoleOrdinaryRank(BytesFactory.listToBytes(rankingList.getRoleOrdinary()));
				rankingList.setRoleMagicRank(BytesFactory.listToBytes(rankingList.getRoleMagic()));
				rankingList.setRoleDefendRank(BytesFactory.listToBytes(rankingList.getRoleDefend()));
				rankingList.setRoleSpeedRank(BytesFactory.listToBytes(rankingList.getRoleSpeed()));
				
				rankingList.setPetOrdinaryRank(BytesFactory.listToBytes(rankingList.getPetOrdinary()));
				rankingList.setPetMagicRank(BytesFactory.listToBytes(rankingList.getPetMagic()));
				rankingList.setPetDefendRank(BytesFactory.listToBytes(rankingList.getPetDefend()));
				rankingList.setPetSpeedRank(BytesFactory.listToBytes(rankingList.getPetSpeed()));
				
				rankingMapper.insert(rankingList);
				sqlSession.commit();
			} else {
				rankingList.setRoleLevel(BytesFactory.byteToObject(rankingList.getRoleLevelRank(), RankingData.class));
				rankingList.setRoleOrdinary(BytesFactory.byteToObject(rankingList.getRoleOrdinaryRank(), RankingData.class));
				rankingList.setRoleMagic(BytesFactory.byteToObject(rankingList.getRoleMagicRank(), RankingData.class));
				rankingList.setRoleDefend(BytesFactory.byteToObject(rankingList.getRoleDefendRank(), RankingData.class));
				rankingList.setRoleSpeed(BytesFactory.byteToObject(rankingList.getRoleSpeedRank(), RankingData.class));
				
				rankingList.setPetOrdinary(BytesFactory.byteToObject(rankingList.getPetOrdinaryRank(), RankingData.class));
				rankingList.setPetMagic(BytesFactory.byteToObject(rankingList.getPetMagicRank(), RankingData.class));
				rankingList.setPetDefend(BytesFactory.byteToObject(rankingList.getPetDefendRank(), RankingData.class));
				rankingList.setPetSpeed(BytesFactory.byteToObject(rankingList.getPetSpeedRank(), RankingData.class));
			}
		} finally {
			sqlSession.close();
		}
	}
	
	
	class ComparatorRole implements Comparator<UserInfo>  {
		
		/** 排序类型 0等级 1 物伤 2法伤 3速度 4防御*/
		public int type;
		
		@Override
		public int compare(UserInfo o1, UserInfo o2) {
			long aPrice;
			long bPrice;
			switch (type) {
				case 0:
					aPrice = o1.getRole().getLevel();
					bPrice = o2.getRole().getLevel();
					break;
				case 1:
					aPrice = o1.getRole().getOrdinaryAttack();
					bPrice = o2.getRole().getOrdinaryAttack();
					break;
				case 2:
					aPrice = o1.getRole().getMagicAttack();
					bPrice = o2.getRole().getMagicAttack();
					break;
				case 3:
					aPrice = o1.getRole().getSpeed();
					bPrice = o2.getRole().getSpeed();
					break;
				case 4:
					aPrice = o1.getRole().getDefense();
					bPrice = o2.getRole().getDefense();
					break;
				default:
					aPrice = o1.getRole().getLevel();
					bPrice = o2.getRole().getLevel();
					break;
			}
			if(aPrice > bPrice) {
				return -1;
			} else if(aPrice < bPrice) {
				return 1;
			} else {
				return 0;
			}
		}
	}


	/**
	 * 获取排行榜数据
	 * @return rankingList 排行榜数据
	 */
	public RankingList getRankingList() {
		return rankingList;
	}
	

	
}
