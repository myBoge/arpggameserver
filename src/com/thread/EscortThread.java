package com.thread;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.boge.entity.GameOutput;
import com.boge.socket.ActionData;
import com.configs.MapConfig;
import com.entity.Escort;
import com.entity.EscortMap;
import com.entity.UserInfo;
import com.model.EscortModel;
import com.model.GameData;
import com.net.SocketCmd;
import com.server.EscortServer;
import com.utils.BytesFactory;
import com.utils.RandomUtils;

/**
 * 押镖线程
 * @author xujinbo
 *
 */
public class EscortThread extends Thread {

	/** 根据地图id 获取此地图上挂机的角色信息 */
	private Map<Long, List<Escort>> mapEscortData = new HashMap<>();
	/** 记录每张地图上次刷新角色时间 */
	private Map<Long, Long> refurbishDate = new HashMap<>();
	private MapConfig mapConfig;
	private EscortModel escortModel;
	private EscortServer escortServer;
	/** 当前线程启动与否 */
	public boolean isOpen = true;
	
	public EscortThread() {
		super();
		setName("押镖");
	}
	
	@Override
	public void run() {
		
		while(isOpen){
			try {
				Thread.sleep(1000);
				updateEscortData();
			} catch (InterruptedException e) {
				Logger.getLogger(getClass()).fatal("押镖地图报错!", e);
			}
		}
	}
	
	/**
	 * 更新挂机信息
	 */
	private void updateEscortData() {
		Set<Long> keys = mapEscortData.keySet();
		Iterator<Long> it = keys.iterator();
		List<Escort> escortList;
		Escort escort;
		int tempX = 0;
		long mapId;
		EscortMap escortMap;
		List<UserInfo> userInfoList;
		long oldTimer;
		int escortAiCount;
		UserInfo userInfo;
		while(it.hasNext()){
			mapId = it.next();
			escortAiCount = 0;
//			if (mapId == 1001) {
//				System.out.println("updateEscortData="+mapId);
//				System.out.println("updateEscortData="+escortList.size());
//			}
			escortList = mapEscortData.get(mapId);
			escortMap = (EscortMap) mapConfig.getCacheSample(mapId);
			if (mapId != 3001) { // 不是初始地图
				if (!refurbishDate.containsKey(mapId)) {
					refurbishDate.put(mapId, System.currentTimeMillis());
				}
				oldTimer = refurbishDate.get(mapId);
				if ((System.currentTimeMillis()-oldTimer)/1000 > 60*1) {// 如果过了1分钟
					refurbishDate.put(mapId, System.currentTimeMillis());
					if (RandomUtils.nextBoolean()) {
						for (int i = 0; i < escortList.size(); i++) {
							if (escortList.get(i).getTypes() != null) {
								escortAiCount++;
							}
						}
						if (escortAiCount < 10 && escortMap.getId() != 3001) {// 少于10个  创建一次
							escort = escortModel.createEscrot(escortMap);
							escortList.add(escort);
							try {
								GameOutput gameOutput = new GameOutput();
								gameOutput.writeLong(mapId);
								escort.writeData(gameOutput);
								sendAllData(mapId, SocketCmd.ADD_ESCORT, gameOutput.toByteArray());
							} catch (IOException e) {
							}
						}
					}
					
					//检查是否遇到系统打劫
					for (int i = 0; i < escortList.size(); i++) {
						escort = escortList.get(i);
						if (escort.getTypes() == null) {// 如果是玩家
							if (RandomUtils.nextBoolean()) {// 是否被电脑打劫
								if (!escort.getUserInfoList().isEmpty()) {
									userInfo = escort.getUserInfoList().get(0);
									System.out.println("战斗力="+userInfo.getForce());
									
								}
							}
						}
						
					}
				}
				
			}
			
			for (int i = 0; i < escortList.size(); i++) {
				escort = escortList.get(i);
				if (!escort.isFight()) { // 不处于战斗中
					userInfoList = escort.getUserInfoList(); // 获取此押镖信息的所有玩家
					tempX = escort.getX()+userInfoList.get(0).getRole().getMoveSpeed();
					if (tempX < escortMap.getWidth()) {//还没有走完
						escort.setX( tempX );// 移动位置
						if (escort.getTypes() == null) {
							// 每秒减1点体力
							for (int j = 0; j < userInfoList.size(); j++) {
								userInfoList.get(j).getRole().setStrength(userInfoList.get(j).getRole().getStrength()-1);
								if (userInfoList.get(j).getRole().getStrength() <= 0) {
//									System.out.println("玩家  体力归0了");
									userInfoList.get(j).getRole().setStrength(0);
									escortServer.removeEscort(escort.getMapId(), userInfoList.get(j), userInfoList.get(j).getSession());
								}
							}
							if (userInfoList.size() == 0) {
								i--;
								continue;
							}
//							if (System.currentTimeMillis()-escort.getOldRobFightDate()/1000 > 1) {
//								
//							}
						}
					} else {
						// 继续押镖决策
						if (escort.getTypes() == null) { // 如果是玩家
							// 走玩了  结算押镖奖励
							balanceEscort(escort, escortMap);
							escort.complete();
							if (mapId != 3001) {
								// 检查体力  足够下次押镖  那么继续押镖
								for (int j = 0; j < userInfoList.size(); j++) {
									// 当此玩家体力不足以继续押镖
									if (escortMap.getWidth() > userInfoList.get(j).getRole().getStrength() || escortMap.get_levels()[1] < userInfoList.get(j).getRole().getLevel()) {
										escortServer.removeEscort(escort.getMapId(), userInfoList.get(j), userInfoList.get(j).getSession());
									}
								}
								if (userInfoList.size() == 0) {
									i--;
									continue;
								}
								escort.setResidueGold(escort.getForegift()*escortMap.getExchangeRat()* userInfoList.size());
							} else {
								for (int j = 0; j < userInfoList.size(); j++) {
									escortServer.removeEscort(escort.getMapId(), userInfoList.get(j), userInfoList.get(j).getSession());
								}
								i--;
								continue;
							}
						} else {
//							escort.setResidueGold(duplicate.getForegift()*2);
							for (int j = 0; j < userInfoList.size(); j++) {
								escortServer.removeEscort(escort.getMapId(), userInfoList.get(j), userInfoList.get(j).getSession());
							}
							i--;
							continue;
						}
						escort.setTotalGold(escort.getResidueGold());
						
						escort.setX(0);
						escort.setY(escortModel.getEscortY());
						GameOutput gameOutput = new GameOutput();
						try {
							gameOutput.writeLong(mapId);
							escort.writeData(gameOutput);
							sendAllData(mapId, SocketCmd.ADD_ESCORT, gameOutput.toByteArray());
						} catch (IOException e) {
							Logger.getLogger(getClass()).error("押镖从新开启一个出错!", e);
						} finally {
							if (gameOutput != null) {
								try {
									gameOutput.close();
								} catch (IOException e) {
									Logger.getLogger(getClass()).error("押镖从新开启一个关闭流出错!", e);
								}
							}
						}
					}
				}
			}
		}
		escortServer.updateMysqlData(keys.iterator());
	}

	/**
	 * 此押镖已经完成了押送任务  开始结算此次押镖收货
	 * @param escort
	 * @param escortMap 
	 */
	private void balanceEscort(Escort escort, EscortMap escortMap) {
		// 此队伍的总人数
		int escortLen = escort.getUserInfoList().size(); // 押镖组队总人数
		long exp = escortMap.getAddExp();// 此地图押镖的经验 平分
		long totalGlod = (long)(escort.getResidueGold()*0.5); // 押镖  的钱  可以收入的总金额
		// 平分
		long glodLump = totalGlod / escortLen;// 每人可分的数量
		UserInfo userInfo;
		for (int i = 0; i < escortLen; i++) {
			userInfo = escort.getUserInfoList().get(i);
			// 处理用户所得经验和钱
			userInfo.getRole().setExperience(userInfo.getRole().getExperience()+exp);
			userInfo.setPlayerGold(userInfo.getPlayerGold()+glodLump);
			// 处理用户升级
			GameData.gameData.userInfoUpLevel(userInfo);
			// 保存一次玩家数据
			UserInfo userData = new UserInfo(userInfo.getId());
			userData.setBytes(BytesFactory.objectToByteData(userInfo.getRole()));
			userData.setPlayerGold(userInfo.getPlayerGold());
			escortServer.getUserServer().update(userData);
		}
	}

	/**
	 * 用户请求删除一个押镖数据
	 * @param userId 
	 * @return 
	 */
	public synchronized Escort removeEscort(long mapId, UserInfo userInfo) {
		List<Escort> list = mapEscortData.get(mapId);
		Escort escort;
		for (int i = 0; i < list.size(); i++) {
			escort = list.get(i);
			if (escort.getTypes() != null) {
				escort.getUserInfoList().clear();
				list.remove(i);
				escortModel.removeEscort(escort);
				return escort;
			} else if (escort.removeUser(userInfo)) {
				if (escort.getUserInfoList().isEmpty()) {
					list.remove(i);
					escortModel.removeEscort(escort);
				}
				return escort;
			}
		}
		return null;
	}

	/**
	 * 对指定的地图添加挂机数据
	 * @param mapId
	 * @param escort
	 */
	public void addEscortData(long mapId, List<Escort> escort) {
		mapEscortData.put(mapId, escort);
	}

	/**
	 * 检查挂机地图是否存在
	 * @param mapId
	 * @return
	 */
	public boolean containsEscortMap(long mapId) {
		return mapEscortData.containsKey(mapId);
	}
	
	/**
	 * 检查此角色是否存在此押镖地图
	 * @param mapId 押镖地图id
	 * @param userInfo 要查看的玩家
	 * @return
	 */
	public boolean containsEscortUser(long mapId, UserInfo userInfo) {
		List<Escort> list = getEscortList(mapId);
		int len = list.size();
		for (int i = 0; i < len; i++) {
			if (list.get(i).getUserInfoList().contains(userInfo)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 获取指定地图挂机的角色信息集合
	 * @param mapId
	 * @return
	 */
	public List<Escort> getEscortList(long mapId) {
		if(containsEscortMap(mapId)) {
			return mapEscortData.get(mapId);
		}
		return null;
	}
	
	/**
	 * 根据角色  获取所在的  押镖信息
	 * @param mapId 押镖地图id
	 * @param userInfo 要查看的玩家
	 * @return
	 */
	public Escort getEscortUser(long mapId, UserInfo userInfo) {
		List<Escort> list = getEscortList(mapId);
		int len = list.size();
		for (int i = 0; i < len; i++) {
			if (list.get(i).getUserInfoList().contains(userInfo)) {
				return list.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 根据押镖地图和  押镖信息id 获取押镖信息
	 * @param mapId
	 * @param escortId
	 * @return
	 */
	public Escort getEscortEscortId(long mapId, long escortId) {
		Escort escort = new Escort();
		escort.setId(escortId);
		List<Escort> mapEscortList = getEscortList(mapId);
		int index = mapEscortList.indexOf(escort);
		if (index != -1) {
			return mapEscortList.get(index);
		}
		return null;
	}
	
	/**
	 * 通知此地图的玩家
	 * @param mapId 
	 * @param action
	 * @param byteArray
	 */
	public void sendAllData(long mapId, int action, byte[] buf) {
		List<Escort> escortList = getEscortList(mapId);
		if (escortList != null) {
			ActionData<Object> actionData = new ActionData<>(action, buf);
			for (int i = 0; i < escortList.size(); i++) {
				escortList.get(i).sendAllData(actionData);
			}
		}
	}
	
	/**
	 * 通知此地图的玩家
	 * @param mapId 
	 * @param action 发送指令
	 * @param arge 自动判断类型包括(Boolean、Integer、Long、byte[]、其余全是String)
	 */
	public void sendAllData(long mapId, int action, Object ...arge) {
		List<Escort> escortList = getEscortList(mapId);
		if (escortList != null) {
			for (int i = 0; i < escortList.size(); i++) {
				escortList.get(i).sendAllData(action, arge);
			}
		}
	}
	
	public MapConfig getMapConfig() {
		return mapConfig;
	}

	public void setMapConfig(MapConfig mapConfig) {
		this.mapConfig = mapConfig;
	}

	public EscortModel getEscortModel() {
		return escortModel;
	}

	public void setEscortModel(EscortModel escortModel) {
		this.escortModel = escortModel;
	}

	public EscortServer getEscortServer() {
		return escortServer;
	}

	public void setEscortServer(EscortServer escortServer) {
		this.escortServer = escortServer;
	}

}
