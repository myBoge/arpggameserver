package com.thread;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.apache.log4j.Logger;

import com.utils.ActionTimerListener;

public class TimerHandler {
	
	private long id;
	/** 执行时间 */
	private long exeTime;
	/** 执行间隔 */
	private long delay;
	/** 是否重复执行 */
	private boolean repeat;
	/** 处理方法 */
	private ActionTimerListener method;
	/** 参数 */
	private Object[] args;
	
	public TimerHandler() {
		super();
	}
	
	/**
	 * 执行方法
	 */
	public static void execute(ActionTimerListener method, Object[] args) {
		try {
			Method[] ms = method.getClass().getDeclaredMethods();
			Method targetMethod = null;
			if((args==null || args.length==0) && ms.length>=1) {
	            for (Method m: ms){
	        		if(m.getParameterCount() == 0){
	        			targetMethod = m;
	        			break;
	        		}
	            }
	        } else if(args!=null && ms.length>=1) {
	        	for (Method m: ms){
	        		if(m.getParameterCount() == args.length){
	        			targetMethod = m;
	        			break;
	        		}
	            }
	        } else {
	           return;
	        }
			if (targetMethod == null) {
				return;
			}
        	targetMethod.setAccessible(true);
			targetMethod.invoke(method, args);
		} catch (Exception e) {
			Logger.getLogger("TimerHandler").error("倒计时执行方法报错！", e);
		}
	}

	/**
	 * 获取执行时间
	 * @return exeTime 执行时间
	 */
	public long getExeTime() {
		return exeTime;
	}

	/**
	 * 设置执行时间
	 * @param exeTime 执行时间
	 */
	public void setExeTime(long exeTime) {
		this.exeTime = exeTime;
	}

	/**
	 * 获取执行间隔
	 * @return delay 执行间隔
	 */
	public long getDelay() {
		return delay;
	}

	/**
	 * 设置执行间隔
	 * @param delay 执行间隔
	 */
	public void setDelay(long delay) {
		this.delay = delay;
	}

	/**
	 * 获取是否重复执行
	 * @return repeat 是否重复执行
	 */
	public boolean isRepeat() {
		return repeat;
	}

	/**
	 * 设置是否重复执行
	 * @param repeat 是否重复执行
	 */
	public void setRepeat(boolean repeat) {
		this.repeat = repeat;
	}

	/**
	 * 获取参数
	 * @return args 参数
	 */
	public Object[] getArgs() {
		return args;
	}

	/**
	 * 设置参数
	 * @param args 参数
	 */
	public void setArgs(Object[] args) {
		this.args = args;
	}

	/**
	 * 获取处理方法
	 * @return method 处理方法
	 */
	public ActionTimerListener getMethod() {
		return method;
	}

	/**
	 * 设置处理方法
	 * @param method 处理方法
	 */
	public void setMethod(ActionTimerListener method) {
		this.method = method;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TimerHandler [id=" + id + ", exeTime=" + exeTime + ", delay=" + delay + ", repeat=" + repeat
				+ ", method=" + method + ", args=" + Arrays.toString(args) + "]";
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimerHandler other = (TimerHandler) obj;
		if (method == null) {
			if (other.method != null)
				return false;
		} else if (!method.equals(other.method))
			return false;
		return true;
	}

	/**
	 * 获取id
	 * @return id id
	 */
	public long getId() {
		return id;
	}

	/**
	 * 设置id
	 * @param id id
	 */
	public void setId(long id) {
		this.id = id;
	}

}
