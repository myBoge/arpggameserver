package com.thread;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.boge.share.Resource;
import com.utils.ActionTimerListener;
import com.utils.OnlyIdManage;
import com.utils.ObjectPool;

/**
 * 计时器
 * @author xujinbo
 *
 */
public class TimerManager extends Thread {

	@Resource
	private ObjectPool objectPool;
	private OnlyIdManage idManage;
	/**
	 * 是否启动倒计时
	 */
	private boolean isRun;
	private List<TimerHandler> handlerMap = new ArrayList<TimerHandler>();
	
	public TimerManager() {
		setName("计时器");
		idManage = new OnlyIdManage();
	}
	
	/**
	 * 定时执行一次
	 * @param delay  延迟时间(单位毫秒)
	 * @param method 结束时的回调方法
	 */
	public long doOnce(long delay, ActionTimerListener method) {
		return create(false, delay, method, null);
	}
	
	/**
	 * 定时重复执行
	 * @param delay  延迟时间(单位毫秒)
	 * @param method 结束时的回调方法
	 */
	public long doLoop(long delay, ActionTimerListener method) {
		return create(true, delay, method, null);
	}
	
	/**
	 * 定时执行一次
	 * @param delay  延迟时间(单位毫秒)
	 * @param method 结束时的回调方法
	 * @param args   回调参数
	 */
	public long doOnce(long delay, ActionTimerListener method, Object ...args) {
		return create(false, delay, method, args);
	}
	
	/**
	 * 定时重复执行
	 * @param delay  延迟时间(单位毫秒)
	 * @param method 结束时的回调方法
	 * @param args   回调参数
	 */
	public long doLoop(long delay, ActionTimerListener method, Object ...args) {
		return create(true, delay, method, args);
	}
	
	/**
	 * 创建定时器
	 * @param repeat 是否重复执行
	 * @param delay  延迟时间(单位毫秒)
	 * @param method 结束时的回调方法
	 * @param args   回调参数
	 */
	private long create(boolean repeat, long delay, ActionTimerListener method, Object[] args) {
		//如果执行时间小于1，直接执行
		if (delay < 1) {
			TimerHandler.execute(method, args);
			return -1;
		}
		TimerHandler handler = objectPool.getPool(TimerHandler.class);
		handler.setId(idManage.getGrowableId());
		handler.setRepeat(repeat);
		handler.setDelay(delay);
		handler.setMethod(method);
		handler.setArgs(args);
		handler.setExeTime(delay+System.currentTimeMillis());
		handlerMap.add(handler);
		return handler.getId();
	}
	
	/**
	 * 清理定时器
	 * @param method 调用接口
	 */
	public void clearTimer(ActionTimerListener method) {
//		System.out.println("倒计时目前还剩="+cacheMap.size());
		TimerHandler handler;
		for (int i = 0; i < handlerMap.size(); i++) {
			if(handlerMap.get(i).getMethod() == method) {
				handler = handlerMap.remove(i);
				idManage.gc(handler.getId());
				objectPool.gc(TimerHandler.class, handler);
				break;
			}
		}
//		System.out.println("倒计时删后还剩="+cacheMap.size());
	}
	
	/**
	 * 清理定时器
	 * @param method 调用接口
	 */
	public void clearTimer(long id) {
//		System.out.println("倒计时目前还剩="+cacheMap.size());
		TimerHandler handler;
		for (int i = 0; i < handlerMap.size(); i++) {
			if(handlerMap.get(i).getId() == id) {
				handler = handlerMap.remove(i);
				idManage.gc(handler.getId());
				objectPool.gc(TimerHandler.class, handler);
				break;
			}
		}
//		System.out.println("倒计时删后还剩="+cacheMap.size());
	}
	
	@Override
	public void run() {
		TimerHandler handler;
		long t;
		while(isRun){
			try {
				Thread.sleep(500);
//				long curr = Global.getTimeMillis() - timer;
//				System.out.println("计时器还剩="+handlerMap.size());
				if (handlerMap.size() > 0) {
					for (int i = 0; i < handlerMap.size(); i++) {
						handler = handlerMap.get(i);
						t = System.currentTimeMillis();
//						System.out.println(handler.getExeTime()+"|"+t);
						if (handler.getExeTime() <= t) {
							if (handler.isRepeat()) {
								handler.setExeTime(handler.getDelay()+t);
							} else {
								handler = handlerMap.remove(i);
								idManage.gc(handler.getId());
								objectPool.gc(TimerHandler.class, handler);
								i--;
							}
							TimerHandler.execute(handler.getMethod(), handler.getArgs());
//							System.out.println("满足  删除一个  --"+handlerMap.size());
						}
					}
				}
//				System.out.println(curr);
			} catch (InterruptedException e) {
				Logger.getLogger(getClass()).error("计时管理程序报错:", e);
			}
		}
	}
	
	public void start(){
		isRun = true;
		// 初始化300个计时器
		objectPool.addPool(TimerHandler.class, 300);
		super.start();
	}
	
	/**
	 * 关闭线程
	 */
	public void close() {
		objectPool.removePool(TimerHandler.class);
		isRun = false;
	}

}