package com.net;

/**
 * 属性枚举
 * @author xujinbo
 *
 */
public enum AttrEnum {
	
	GM_LOGIN, // 后台登录
	THROB // 心跳保存的数据
	
}
