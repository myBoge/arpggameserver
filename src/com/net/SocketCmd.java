package com.net;

public interface SocketCmd {
	
	/** 心跳 */
	int THROB = 1;
	
	/** 服务器关闭 */
	int SERVER_INFO_OFF = 0;
	/** 服务器开启 */
	int SERVER_INFO_NO = 1;
	/** 服务器内测 */
	int SERVER_INFO_INSIDE = 2;
	/** 错误中断 */
	int ERROR_INTERRUPT = 100;
	
	/** 提示消息 */
	int PROMPT_MESSAGE = 1000;
	/** 提示消息 后台发送文字 */
	int PROMPT_MESSAGE_TXT = 1001;
	/** 绑定编号发送的数据 */
	int BIND_CODE_DATA = 1002;
	
	
	/** 用户登录 */
	int LOGIN = 10000;
	/** 用户登录错误 */
	int LOGIN_ERROR = 10001;
	/** 新建一个角色 */
	int CREATE_ROLE = 10002;
	/** 创建角色失败 */
	int CREATE_ROLE_ERROR = 10003;
	/** 删除一个角色 */
	int DELETE_ROLE = 10004;
	/** 用户数据 */
	int USER_DATA = 10005;
	/** 更新用户状态 */
	int UPDATE_USER_STATE = 10006;
	/** 更新用户金币 */
	int UPDATE_GOLD = 10007;
	/** 更新用户等级 */
	int UPDATE_UPLEVEL = 10008;
	/** 进入场景 */
	int USER_ENTER_SCENE = 10009;
	/** 删除物品指定数量 */
	int UPDATE_PROP_COUNT = 10010;
	/** 更新物品位置 */
	int UPDATE_PROP_INDEX = 10011;
	/** 整理包裹 */
	int ARRANGE_PROP = 10012;
	/** 购买商品 */
	int BUYS_SHOP = 10013;
	/** 添加新物品 */
	int ADD_NEW_PROP = 10014;
	/** 添加一条聊天信息 */
	int ADD_CHAT_INFO = 10015;
	/** 加好友请求 */
	int ADD_FRIEND_REQUEST = 10016;
	/** 加好友结果 */
	int ADD_FRIEND_RESULT = 10017;
	/** 删除好友 */
	int DELETE_FRIEND = 10018;
	/** 加好友 */
	int ADD_FRIEND = 10019;
	/** 更新用户经验 */
	int UPDATE_EXP = 10020;
	/** 更新用户HPMP */
	int UPDATE_HP_MP = 10021;
	/** 使用物品 */
	int USE_PROP = 10022;
	/** 卖出物品 */
	int SELL_PROP = 10023;
	/** 更新用户RMB */
	int UPDATE_RMB = 10024;
	/** 发送好友聊天 */
	int SEND_FRIEND_CHAT = 10025;
	/** 好友在线状态变更 */
	int FRIEND_ONLINE_STATE = 10026;
	/** 添加邮件 */
	int ADD_MAIL = 10027;
	/** 删除邮件 */
	int DELETE_MAIL = 10028;
	/** 阅读邮件 */
	int READ_MAIL = 10029;
	/** 请求邮件数据 */
	int REQUEST_MAIL_DATA = 10030;
	/** 提取物品 */
	int EXTRACT_GOODS = 10031;
	/** 发送背包所有物品 */
	int BACKPACK_ALL_PROP = 10032;
	/** 查看角色信息 */
	int SEE_ROLE_INFO = 10033;
	/** 改名 */
	int RENAME = 10034;
	/** 元宝商城购买商品 */
	int STORE_BUYS_SHOP = 10035;
	/** 金币商城可以购买的商品 */
	int SHOP_ALL_GOODS = 10036;
	/** 元宝商城可以购买的商品 */
	int STORE_ALL_GOODS = 10037;
	/** 更新好友信息 */
	int UPDATE_FRIEND = 10038;
	/** 玩家提交的举报信息 */
	int REPORT_INFO = 10039;
	/** 玩家添加属性点 */
	int ADD_ATT_POINT = 10040;
	/** 更新玩家属性点 */
	int UPDATE_ATT_POINT = 10041;
	/** 更新玩家技能等级 */
	int UPDATE_SKILL_LEVEL = 10042;
	/** 更新玩家当前的潜能数 */
	int UPDATE_POTENTIAL = 10043;
	/** 更新玩家当前所在的场景的附带值 */
	int UPDATE_SCENE_VALUE = 10044;
	/** 添加新宠物 */
	int ADD_PET = 10045;
	/** 删除宠物 */
	int DELETE_PET = 10046;
	/** 更新宠物经验 */
	int UPDATE_PET_EXP = 10047;
	/** 宠物添加属性点 */
	int PET_ADD_ATT_POINT = 10048;
	/** 宠物血蓝更新 */
	int PET_UPDATE_HP_MP = 10049;
	/** 宠物名字更新 */
	int PET_UPDATE_NAME = 10050;
	/** 宠物参战状态更新 */
	int PET_UPDATE_JOINBATTLE = 10051;
	/** 宠物驯服 */
	int PET_TAME = 10052;
	/** 宠物使用物品 */
	int PET_USE_PROP = 10053;
	/** 宠物等级更新 */
	int PET_UPDATE_UPLEVEL = 10054;
	/** 穿装备 */
	int EQUIPS_WEAR = 10055;
	/** 脱装备 */
	int EQUIPS_STRIP = 10056;
	/** 改造装备 */
	int EQUIPS_REFORM = 10057;
	/** 请求排行榜数据 */
	int RANKING_REQUEST = 10058;
	/** 聊天特殊显示信息 1经验 2得到物品 3升级  4改名 5建立帮派 6帮派解散 7竞技 8战场 9战场通告 
	 * 10潜能 11使用物品 12得到金钱 13死亡惩罚*/
	int ADD_CHAT_MESSAGE = 10059;
	/** 兑换码 */
	int REDEEM_CODE = 10060;
	/** 仓库添加物品 */
	int WAREHOUSE_ADD_PROP = 10061;
	/** 仓库删除物品数量 */
	int WAREHOUSE_UPDATE_PROP_COUNT = 10062;
	/** 仓库更新物品位置 */
	int WAREHOUSE_UPDATE_PROP_INDEX = 10063;
	/** 仓库整理 */
	int WAREHOUSE_ARRANGE_PROP = 10064;
	/** 仓库数据 */
	int WAREHOUSE_DATA = 10065;
	/** 领取每日在线礼包 */
	int GIFT_GET_ON_LINE = 10066;
	/** 更新领取每日在线礼包次数 */
	int UPDATE_GIFT_GET_ON_LINE_COUNT = 10067;
	/** 竞技场挑战数据请求 */
	int ARENA_REQUEST = 10068;
	/** 竞技场挑战 */
	int ARENA_CHALLENGE = 10069;
	/** 切磋 */
	int FIGHT_PK = 10070;
	/** 切磋决定 */
	int FIGHT_PK_DECIDE = 10071;
	/** 更新用户体力值 */
	int UPDATE_STRENGTH = 10072;
	/** 添加用户buff物品 */
	int ADD_BUFF_PROP = 10073;
	/** 删除用户buff物品 */
	int DELETE_BUFF_PROP = 10074;
	/** 更新用户buff物品 */
	int UPDATE_BUFF_PROP = 10075;
	/** 更新用户自动使用物品 */
	int UPDATE_AUTO_USE_PROP = 10076;
	/** 更新道具数据 */
	int UPDATE_PROP_INFO = 10077;
	/** 洗宠 */
	int WASH_PET = 10078;
	/** 洗宠替换 */
	int WASH_PET_REPLACE = 10079;
	/** 首饰合成 */
	int JEWELRY_COMPOSE = 10080;
	/** 领取每日签到 */
	int RECEIVE_EVERYDAY_SIGN = 10081;
	/** 领取等级礼包 */
	int RECEIVE_LEVEL_GIFT = 10082;
	/** 充值信息 */
	int RECHARGE_INFO = 10083;
	/** 新添加剧情任务 */
	int MISSION_ADD = 10084;
	/** 剧情任务更新 */
	int MISSION_UPDATE = 10085;
	/** 剧情任务完成 */
	int MISSION_COMPLETE = 10086;
	
	
	
	
	
	//战场
	/** 地图上挑战怪兽 */
	int BATTLEFIELD_FIGHT = 11001;
	/** 地图上删除怪兽 */
	int BATTLEFIELD_DELETE_MONSTER = 11002;
	/** 地图上添加怪兽 */
	int BATTLEFIELD_ADD_MONSTER = 11003;
	
	
	// 地图处理
	/** 玩家决定移动到某个位置 */
	int MAP_PLAYER_MOVE = 12001;
	/** 玩家决定移动到某个位置结束 */
	int MAP_PLAYER_MOVE_END = 12002;
	/** 地图新添加角色 */
	int MAP_ADD_PLAYER = 12003;
	/** 地图删除一个角色 */
	int MAP_DELETE_PLAYER = 12004;
	/** 地图上所有的角色 */
	int MAP_ALL_ROLE_DATA = 12005;
	/** 地图上角色聊天 */
	int MAP_CHAT_MSG = 12006;
	/** 地图上有角色发生战斗 */
	int MAP_PLAYER_FIGHT = 12007;
	/** 地图上玩家格子变动 */
	int MAP_GRID_CHANGE = 12008;
	/** 地图上玩家移动格子变动改正 */
	int MAP_CHANGE_RECTIFY = 12009;
	/** 地图进入 */
	int MAP_ENTER = 12010;
	/** 地图上玩家位置变动 */
	int MAP_POINT_CHANGE = 12011;
	
	
	// 帮派
	
	/** 请求所有帮派数据 */
	int FACTION_REQUEST = 15001;
	/** 帮派创建 */
	int FACTION_CREATE = 15002;
	/** 帮派解散 */
	int FACTION_DISBAND = 15003;
	/** 帮派申请 */
	int FACTION_APPLY = 15004;
	/** 帮派踢人 */
	int FACTION_KICK = 15005;
	/** 帮派退出 */
	int FACTION_QUIT = 15006;
	/** 帮派建筑升级 */
	int FACTION_BUILD_UP = 15007;
	/** 帮派新增成员 */
	int FACTION_ADD = 15008;
	/** 帮派减少成员 */
	int FACTION_DELETE = 15009;
	/** 帮派的完整信息 */
	int FACTION_INFO = 15010;
	/** 帮派申请决定 */
	int FACTION_APPLY_DECIDE = 15011;
	
	
	/** 进入押镖场景 */
	int ENTER_ESCORT = 20001;
	/** 创建一个新的押镖 */
	int CREATE_ESCORT = 20002;
	/** 押镖场景数据 */
	int ESCORT_MAP_DATA = 20003;
	/** 停止押镖 */
	int STOP_ESCORT = 20004;
	/** 地图上删除一个挂机角色 */
	int MAP_REMOVE_ESCORT = 20005;
	/** 打劫 */
	int MAP_ROB_ESCORT = 20006;
	/** 退出押镖场景 */
	int EXIT_ESCORT = 20007;
	/** 地图上添加一个挂机角色 */
	int ADD_ESCORT = 20008;
	/** 更改地图此人的战斗状态 */
	int MAP_UPDATE_FIGHT_STATE = 20009;
	/** 劫镖结果 */
	int MAP_ROB_RESULT = 20010;
	/** 被劫镖结果 */
	int MAP_ROBBERY_RESULT = 20011;
	
	
	
	//战斗指令
	
	/** 全体进入下达命令阶段  */
	int FIGHT_ALL_WAIT = 30001;
	/** 客户端发送确认回合动作  */
	int FIGHT_EXECUTE_ACTION = 30002;
	/** 通知客户端  当前回合演播数据  */
	int FIGHT_ROUND_PLAY_DATA = 30003;
	/** 通知服务器 演播结束  */
	int FIGHT_PLAY_END = 30004;
	/** 玩家离线  */
	int FIGHT_USER_OFFLINE = 30005;
	/** 玩家重连  */
	int FIGHT_USER_RECONNECT = 30006;
	/** 重连发送数据  */
	int FIGHT_RECONNECT_DATA = 30007;
	/** 战斗没有结果  结束战斗  */
	int FIGHT_NOT_RESULT = 30008;
	/** 战斗初始化数据 */
	int FIGHT_DATA = 30009;
	/** 战斗结束  各回各家 */
	int FIGHT_OVER = 30010;
	
	
	// 练级地图
	/** 进入地图挂机练级 */
	int ENTER_MAP = 31001;
	/** 退出地图挂机练级 */
	int EXIT_MAP = 31002;
	/** 历练开始*/
	int GRIND_START = 31003;
	/** 停止历练 */
	int GRIND_STOP = 31004;
	/** 更新历练状态 */
	int GRIND_UPDATE_STATE = 31005;
		
	
	// 团队  队伍
	/** 创建队伍 */
	int TEAM_CREATE = 32001;
	/** 解散队伍 */
	int TEAM_DISBAND = 32002;
	/** 队伍新添加一个成员 */
	int TEAM_ADD_ROLE = 32003;
	/** 队伍减少一个成员 */
	int TEAM_DELETE_ROLE = 32004;
	/** 队伍成员数据 */
	int TEAM_ROLE_DATA = 32005;
	/** 快速入队 */
	int TEAM_FAST = 32006;
	/** 组队申请 */
	int TEAM_APPLY = 32007;
	/** 一键邀请 */
	int TEAM_FAST_INVITE = 32008;
	/** 组队申请决定 */
	int TEAM_APPLY_DECIDE = 32009;
	/** 组队申请删除一个 */
	int TEAM_APPLY_DELETE = 32010;
	/** 组队邀请 */
	int TEAM_INVITE = 32011;
	/** 组队邀请决定 */
	int TEAM_INVITE_DECIDE = 32012;
	/** 组队邀请删除一个 */
	int TEAM_INVITE_DELETE = 32013;
	/** 踢出一个成员 */
	int TEAM_KICKOUT = 32014;
	
	
	// GM 工具
	/** GM登录 */
	int GM_LOGIN = 50001;
	/** GM查询用户数据*/
	int GM_QUERY_USERINFO = 50002;
	/** GM更新用户数据*/
	int GM_UPDATE_USERINFO = 50003;
	/** GM查询兑换码*/
	int GM_SEE_REDEEM_CODE = 50004;
	/** GM添加兑换码数据*/
	int GM_ADD_REDEEM_CODE = 50005;
	/** GM清理兑换码数据*/
	int GM_CLEAR_REDEEM_CODE = 50006;
	/** GM更新兑换码数据*/
	int GM_UPDATE_REDEEM_CODE = 50007;
	/** GM查询礼包*/
	int GM_SEE_GIFT = 50008;
	/** GM添加礼包数据*/
	int GM_ADD_GIFT = 50009;
	/** GM删除礼包数据*/
	int GM_DELETE_GIFT = 50010;
	/** GM更新礼包数据*/
	int GM_UPDATE_GIFT = 50011;
	/** GM在线总人数*/
	int GM_ON_LINE_COUNT = 50012;
	/** GM所有在线成员*/
	int GM_QUERY_ALL_PAGE = 50013;
	/** GM玩家宠物数据*/
	int GM_QUERY_PET_DATA = 50014;
	/** GM玩家更新宠物数据*/
	int GM_UPDATE_PET_DATA = 50015;
	/** GM向服务器发送lua指令*/
	int GM_LUA_CMD = 50016;
	
	
	
	
	
	
}