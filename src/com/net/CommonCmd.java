package com.net;

public interface CommonCmd {

	/** 初始化押镖管理数据 */
	int INIT_ESCORT = 1;
	/** 初始化战斗基础 */
	int INIT_FIGHT = 2;
	/** 初始化押镖数据 */
	int INIT_DATA = 3;
	/** 初始化金币商城数据 */
	int INIT_BUYS_SHOP = 4;
	/** 初始化元宝商城数据 */
	int INIT_STORE_BUYS_SHOP = 5;
	/** 初始化副本地图数据 */
	int INIT_GRIND = 6;
	/** 初始化副本地图数据 */
	int INIT_GRIND_DATA = 7;
	/** 初始化技能数据 */
	int INIT_SKILL_DATA = 8;
	/** 初始化所有帮派数据 */
	int INIT_FACTION_DATA = 9;
	/** 初始化礼包数据 */
	int INIT_GIFG_DATA = 10;
	/** 初始化兑换码数据 */
	int INIT_REDEEM_DATA = 11;
	/** 初始化竞技场数据 */
	int INIT_ARENA_DATA = 12;
	/** 初始化地图数据 */
	int INIT_MAP_DATA = 13;
	
	
	
	
	
	
	/** 押镖场景 */
	int SCENE_ESCORT = 1;
	/** 练级场景 */
	int SCENE_GRIND = 2;
	/** 跑动场景 */
	int SCENE_GAME = 3;
	
	
	
	
	// 战斗类型
	/** pvp 玩家竞技 */
	int PVP = 1;
	/** rob 打劫 */
	int ROB = 2;
	/** pk 玩家切磋*/
	int PK = 3;
	/** pve 与电脑发生的战斗 */
	int PVE = 4;
	/** pve 战场上与电脑发生的战斗 */
	int BATTLE_PVE = 5;
	
	
	// 角色状态
	/** 空闲 */
	int IDLE = 0;
	/** 押镖 */
	int ESCORT = 1;
	
	
	// 加好友返回结果
	/** 同意 */
	int AGREE = 1;
	/** 拒绝 */
	int REJECT = 2;
	/** 全部同意 */
	int ALL_AGREE = 3;
	/** 全部拒绝 */
	int ALL_REJECT = 4;
			
	
	
	// 排行榜值
	/** 人物等级 */
	int ROLE_LEVEL = 1;
	/** 人物物攻 */
	int ROLE_ORDINARY_ATTACK = 2;
	/** 人物法功 */
	int ROLE_MAGIC_ATTACK = 3;
	/** 人物速度 */
	int ROLE_SPEED = 4;
	/** 人物防御 */
	int ROLE_DEFENSE = 5;
	/** 宠物物攻 */
	int PET_ORDINARY_ATTACK = 6;
	/** 宠物法功 */
	int PET_MAGIC_ATTACK = 7;
	/** 宠物速度 */
	int PET_SPEED = 8;
	/** 宠物防御 */
	int PET_DEFENSE = 9;
	
	
	
	//兑换码
	/** 普通兑换 */
	int ORDINARY_REDEEM = 1;
	
	
	
	
	/** 领取类任务，直接可以完成，文字显示接受 */ 
	int GIVE = 0;
	/** 完成类任务，需要满足条件才能完成。 */ 
	int MISS = 1;
	
	/* !!! 以下内容为D5Rpg内部定义，非必要请不要修改，会影响较多代码 !!! */
	
	/** 杀死怪物 */ 
	int N_MONSTER_KILLED = 0;
	/** 拥有物品（不扣除） */ 
	int N_ITEM_TACKED = 1;
	/** 拥有物品（扣除） */ 
	int N_ITEM_NEED = 2;
	/** 拥有任务 */ 
	int N_MISSION = 3;
	/** 玩家属性 */ 
	int N_PLAYER_PROP = 4;
	/** 与NPC对话 */ 
	int N_TALK_NPC = 5;
	/** 需要技能 */ 
	int N_SKILL_LV = 6;
	/** 需要主角皮肤 */ 
	int N_SKIN = 7;
	/** 需要装备某类型道具 */ 
//	int N_EQU_TYPE = 8;
	/** 需要装备某个特定道具 */ 
	int N_EQU = 9;
	/** 需要学会技能 */ 
	int N_SKILL = 10;
	/** 需要增益 */ 
	int N_BUFF = 11;
	/** 需要游戏币 */ 
	int N_MONEY = 12;
	/** 需要标记 */ 
	int N_MARK = 13;
	/** 拥有游戏币 */ 
	int N_MONEY_KEEP = 14;
	/** 需要职业 */ 
	int N_PARTY = 15;
	/** 需要等级 */ 
	int N_LV = 16;
	
	
	/** 奖励道具 */ 
	int R_ITEM = 100;
	/** 奖励游戏币 */ 
	int R_MONEY = 101;
	/** 奖励经验 */ 
	int R_EXP = 102;
	/** 奖励任务 */ 
	int R_MISSION = 103;
	/** 奖励属性 */ 
	int R_PLAYER_PROP = 104;
	/** 奖励技能 */ 
	int R_SKILL = 105;
	
	/* !!! 以上内容为D5Rpg内部定义，非必要请不要修改，会影响较多代码 !!! */
	
	
	
	
	
	
	
	
	
	// 消息提示框番号
	/** 不存在此玩家信息 */
	int NOT_USER_INFO = 1;
	/** 不存在此地图 */
	int MAP_NON_EXISTENT = 2;
	/** 打劫玩家不在此地图 */
	int MAP_ROB_USER_NON_EXISTENT = 3;
	/** 被打劫玩家不在此地图 */
	int MAP_ROBBERY_USER_NON_EXISTENT = 4;
	/** 此镖局已处于保护状态 */
	int MAP_ESCORT_DEFEND_STATE = 5;
	/** 物品不存在 */
	int PROP_NOT_EXISTENT = 6;
	/** 此行为正在冷却中 */
	int ACTION_COOLING = 7;
	/** 你正处于战斗中 */
	int YOU_AT_FIGHT = 8;
	/** 对方正处于战斗中 */
	int THIS_AT_FIGHT = 9;
	/** 错误操作 */
	int ERROR_OPERATION = 10;
	/** 金币不足 */
	int GOLD_INSUFFICIENT = 11;
	/** 背包空间不足 */
	int BACKPACK_INSUFFICIENT_SPACE = 12;
	/** 没有此押镖地图 */
	int ESCORT_NOT_MAP = 13;
	/** 押镖信息不存在 */
	int ESCORT_INFO_NOT_MAP = 14;
	/** 体力不足 */
	int STRENGTH_LOW = 15;
	/** 该角色不处于空闲状态 */
	int USER_NOT_IDLE = 16;
	/** 添加好友请求不存在 */
	int ADD_FRIEND_NOT_EXISTENT = 17;
	/** 好友已经存在 */
	int FRIEND_EXISTENT = 18;
	/** 好友不存在 */
	int FRIEND_NOT_EXISTENT = 19;
	/** 物品数量错误 */
	int PROP_COUNT_ERROR = 20;
	/** 物品无法使用 */
	int PROP_NOT_USE = 21;
	/** 物品已被加锁 */
	int PROP_LOCK = 22;
	/** 此玩家不在线 */
	int USER_OFFLINE = 23;
	/** 输入长度错误 */
	int INPUT_LEN_ERROR = 24;
	/** 删除邮件数量错误 */
	int DELETE_MAIL_NUM_ERROR = 25;
	/** 没有可提取的附件 */
	int MAIL_NOT_EXTRACT_GOODS = 26;
	/** 此物品无法交易 */
	int PROP_NOT_TRADE = 27;
	/** 对方邮箱已经爆满了 */
	int MAIL_FULL = 28;
	/** 用户名字不合法 */
	int USER_NAME_ERROR = 29;
	/** 元宝不足 */
	int RMB_NOT_LOW = 30;
	/** 用户名已经存在 */
	int USER_NAME_EXISTENT = 31;
	/** 用户不再主场景，无法进入 */
	int USER_NOT_MAIN_SCENE = 32;
	/** 技能不存在 */
	int SKILL_NOT_EXISTENT = 33;
	/** 潜能不足够学习 */
	int POTENTIAL_ERROR = 34;
	/** 选择的目标属于不可捕抓单位 */
	int TARGET_NOT_CATCH = 35;
	/** 捕抓单位携带等级过高 */
	int CATCH_LEVEL_HIGH = 36;
	/** 已经在队伍中 */
	int EXISTENT_TEAM = 37;
	/** 队伍不存在 */
	int TEAM_NOT_EXISTENT = 38;
	/** 你不是队长,无法执行此操作 */
	int TEAM_NOT_LEADER = 39;
	/** 队伍满员 */
	int TEAM_FULL = 40;
	/** 申请过时 */
	int TEAM_APPLY_OUT = 41;
	/** 不能接受自己发出的邀请 */
	int TEAM_NOT_AUTO_APPLY = 42;
	/** 宠物不存在 */
	int PET_NOT_EXISTENT = 43;
	/** 空闲点数不足 */
	int IDLE_POINT_NOT_ENOUGH = 44;
	/** 宠物数量已经满员 */
	int PET_COUNT_FULL = 45;
	/** 召唤宠物失败，血量不足 */
	int CALL_FAIL_BLOOD_DEFICIENCY = 46;
	/** 召唤宠物失败，已经使用过 */
	int CALL_FAIL_USE = 47;
	/** 你正在历练中 */
	int YOU_GRIND_DURING = 48;
	/** 你不在队伍中 */
	int YOU_NOT_EXISTENT_TEAM = 49;
	/** 不能组队执行此操作 */
	int NOT_TEAM_OPERATION = 50;
	/** 你的等级不符合要求 */
	int YOU_LEVEL_ERROR = 51;
	/** 装备已经穿戴 */
	int EQUIPS_ALREADY_WEAR = 52;
	/** 装备改造次数上限 */
	int EQUIPS_REFORM_COUNT_ERROR = 53;
	/** 此装备无法改造 */
	int EQUIPS_NOT_REFORM = 54;
	/** 你已经在帮派中 */
	int YOU_EXISTENT_FACTION = 55;
	/** 你不存在帮派中 */
	int YOU_NOT_EXISTENT_FACTION = 56;
	/** 你不是帮主 */
	int YOU_NOT_FACTION_WANG = 57;
	/** 帮派不存在 */
	int FACTION_NOT_EXISTENT = 58;
	/** 帮派满员 */
	int FACTION_FULL = 59;
	/** 你不是帮派管理员 */
	int YOU_NOT_FACTION_ADMIN = 60;
	/** 兑换码已失效 */
	int YOU_REDEEM_CODE_ERROR = 61;
	/** 此礼包你已经领取过 */
	int YOU_GIFT_ALREADY_GET = 62;
	/** 此礼包已失效 */
	int YOU_GIFT_INVALID = 63;
	/** 你的挑战名次过低 */
	int YOU_ARENA_RANK_TOO_LOW = 64;
	/** 你的挑战名次错误 */
	int YOU_ARENA_RANK_ERROR = 65;
	/** 你的挑战对象正在战斗中 */
	int YOU_ARENA_FIGHT = 66;
	/** 地图人数上限 */
	int MAP_MAX_COUNT_LIMIT = 67;
	/** 不满足在线时间 */
	int NOT_SATISFY_ONLINE_TIME = 68;
	/** 领取次数上限 */
	int GET_COUNT_TOPLIMIT = 69;
	/** 你的宠物不愿意参加战斗 */
	int PET_UNWILLING_WAR = 70;
	/** 改造装备的材料不足 */
	int REFORM_MATERIAL_NOT_EXITS = 71;
	/** 该首饰无法继续合成 */
	int JEWELRY_COMPOSE_LEVEL_ERR = 72;
	/** 该首饰合成数量不足 */
	int JEWELRY_COMPOSE_COUNT_ERR = 73;
	/** 该宠物不是宝宝 */
	int PET_NOT_BABY = 74;
	/** 该宠物等级过低 */
	int PET_LEVEL_LOW = 75;
	/** 该位置无法到达 */
	int POINT_NOT_MOVE = 76;
	/** 今日充值上限 */
	int DAY_RECHARGE_LIMIT = 77;
	/** 你的等级过高 */
	int YOU_LEVEL_OVERTOP = 78;
	/** 任务不存在 */
	int MISSION_NOT_EXIST = 79;
	/** 条件不满足 */
	int MISSION_NOT_NEED = 80;
	

	
	
}
