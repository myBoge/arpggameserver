package com.net;

/**
 * 战斗需要用到的行为番号
 * @author Administrator
 *
 */
public interface FightCmd {

	/** 普通攻击 */
	int ORDINARY_ATTACK = 1;
	/** 法术攻击 */
	int MAGIC_ATTACK  = 2;
	/** 防守 */
	int DEFEND = 3;
	/** 逃跑 */
	int FLEE = 4;
	/** 使用道具  */
	int TAKE_MEDICINE = 5;
	/** 自动 */
	int AUTOMATIC = 6;
	/** 召唤 */
	int CALL = 7;
	/** 召回 */
	int RECALL = 8;
	/** 捕捉 */
	int CAPTURE = 9;
	/** 保护 */
	int PROTECT = 10;
	/** 无行为 */
	int NOT_ACTION = 11;
	
	
	
	
	
	
}
