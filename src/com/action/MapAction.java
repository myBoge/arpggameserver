package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.MapServer;

public class MapAction implements ActionHandler<Object> {

	@Resource
	private MapServer mapServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.MAP_PLAYER_MOVE:
				mapServer.mapPlayerMove(actionData.getBuf(), session);
				break;
			case SocketCmd.MAP_PLAYER_MOVE_END:
				mapServer.mapPlayerMoveEnd(actionData.getBuf(), session);
				break;
			case SocketCmd.MAP_ALL_ROLE_DATA:
				mapServer.mapAllPlayerData(actionData.getBuf(), session);
				break;
			case SocketCmd.MAP_CHAT_MSG:
				mapServer.mapChatMsg(actionData.getBuf(), session);
				break;
			case SocketCmd.MAP_PLAYER_FIGHT:
				mapServer.mapPlayerFight(actionData.getBuf(), session);
				break;
			case SocketCmd.MAP_GRID_CHANGE:
				mapServer.mapGridChange(actionData.getBuf(), session);
				break;
			case SocketCmd.MAP_ENTER:
				mapServer.mapEnter(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
