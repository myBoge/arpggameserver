package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.RechargeServer;

/**
 * 充值监听
 * @author xujinbo
 *
 */
public class RechargeAction implements ActionHandler<Object> {

	@Resource
	private RechargeServer rechargeServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.RECHARGE_INFO:
				rechargeServer.rechargeInfo(actionData.getBuf(), session);
				break;
	
			default:
				break;
		}
	}

}
