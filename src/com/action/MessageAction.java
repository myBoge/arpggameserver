package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.MessageServer;

/**
 * 聊天信息
 * @author xujinbo
 */
public class MessageAction implements ActionHandler<Object> {

	@Resource
	private MessageServer messageServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.ADD_CHAT_INFO:
				messageServer.addChatInfo(actionData.getBuf(), session);
				break;
			case SocketCmd.SEND_FRIEND_CHAT:
				messageServer.sendFriendChat(actionData.getBuf(), session);
				break;
			case SocketCmd.FIGHT_PK:
				messageServer.fightPk(actionData.getBuf(), session);
				break;
			case SocketCmd.FIGHT_PK_DECIDE:
				messageServer.fightPkDecide(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
