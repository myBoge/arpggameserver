package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.GmServer;

/**
 * gm 后台工具  接口
 * @author xujinbo
 *
 */
public class GmAction implements ActionHandler<Object> {

	@Resource
	private GmServer gmServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.GM_LOGIN:
				gmServer.gmLogin(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_QUERY_USERINFO:
				gmServer.gmQueryUserInfo(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_UPDATE_USERINFO:
				gmServer.gmUpdateUserInfo(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_SEE_REDEEM_CODE:
				gmServer.gmSeeRedeemCode(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_ADD_REDEEM_CODE:
				gmServer.gmAddRedeemCode(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_CLEAR_REDEEM_CODE:
				gmServer.gmClearRedeemCode(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_UPDATE_REDEEM_CODE:
				break;
			case SocketCmd.GM_SEE_GIFT:
				gmServer.gmSeeGift(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_ADD_GIFT:
				gmServer.gmAddGift(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_ON_LINE_COUNT:
				gmServer.gmOnLineCount(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_QUERY_ALL_PAGE:
				gmServer.gmQueryAllPage(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_UPDATE_GIFT:
				
				break;
			case SocketCmd.GM_DELETE_GIFT:
//				gmServer.gmDeleteGift(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_QUERY_PET_DATA:
				gmServer.gmQueryPetData(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_UPDATE_PET_DATA:
				gmServer.gmUpdatePetData(actionData.getBuf(), session);
				break;
			case SocketCmd.GM_LUA_CMD:
				gmServer.gmLuaCmd(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

	public void userOffline(IoSession session) {
		gmServer.userOffline(session);
	}

}
