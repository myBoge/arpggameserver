package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.ShopServer;
import com.server.StoreServer;

/**
 * 商品购买
 * @author xujinbo
 *
 */
public class ShopAction implements ActionHandler<Object> {

	@Resource
	private ShopServer shopServer;
	@Resource
	private StoreServer storeServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.BUYS_SHOP:
				shopServer.buysShop(actionData.getBuf(), session);
				break;
			case SocketCmd.STORE_BUYS_SHOP:
				storeServer.buysShop(actionData.getBuf(), session);
				break;
			case SocketCmd.SHOP_ALL_GOODS:
				shopServer.shopAllGoods(actionData.getBuf(), session);
				break;
			case SocketCmd.STORE_ALL_GOODS:
				storeServer.storeAllGoods(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
