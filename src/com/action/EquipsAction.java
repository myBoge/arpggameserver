package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.EquipsServer;

/**
 * 装备监听
 * @author xujinbo
 *
 */
public class EquipsAction implements ActionHandler<Object> {

	@Resource
	private EquipsServer equipsServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.EQUIPS_WEAR:
				equipsServer.equipsWear(actionData.getBuf(), session);
				break;
			case SocketCmd.EQUIPS_STRIP:
				equipsServer.equipsStrip(actionData.getBuf(), session);
				break;
			case SocketCmd.EQUIPS_REFORM:
				equipsServer.equipsReform(actionData.getBuf(), session);
				break;
			case SocketCmd.JEWELRY_COMPOSE:
				equipsServer.jewelryCompose(actionData.getBuf(), session);
				break;
			default:
				break;
		}
		
	}

}
