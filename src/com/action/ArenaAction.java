package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.ArenaServer;

/**
 * 竞技场监听
 * @author xujinbo
 */
public class ArenaAction implements ActionHandler<Object> {

	@Resource
	private ArenaServer arenaServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.ARENA_REQUEST:
				arenaServer.arenaRequest(actionData.getBuf(), session);
				break;
			case SocketCmd.ARENA_CHALLENGE:
				arenaServer.arenaChallenge(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
