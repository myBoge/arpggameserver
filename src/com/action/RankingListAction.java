package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.RankingListServer;

public class RankingListAction implements ActionHandler<Object> {

	@Resource
	private RankingListServer rankingListServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.RANKING_REQUEST:
				rankingListServer.rankingRequest(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
