package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.EscortServer;
import com.server.FightServer;

/**
 * 押镖
 * @author xujinbo
 *
 */
public class EscortAction implements ActionHandler<Object> {

	@Resource
	private EscortServer escortServer;
	@Resource
	private FightServer fightServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.CREATE_ESCORT:
				escortServer.createEscort(actionData.getBuf(), session);
				break;
			case SocketCmd.ENTER_ESCORT:
				escortServer.getEscortMapData(actionData.getBuf(), session);
				break;
			case SocketCmd.STOP_ESCORT:
				escortServer.stopEscort(actionData.getBuf(), session);
				break;
			case SocketCmd.MAP_ROB_ESCORT:
				escortServer.mapRobEscort(actionData.getBuf(), session);
				break;
			case SocketCmd.FIGHT_EXECUTE_ACTION:
				fightServer.userDecision(actionData.getBuf(), session);
				break;
			case SocketCmd.FIGHT_PLAY_END:
				fightServer.fightPlayEnd(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
