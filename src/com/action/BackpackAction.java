package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.BackpackServer;

/**
 * 背包管理
 * @author xujinbo
 *
 */
public class BackpackAction implements ActionHandler<Object> {

	@Resource
	private BackpackServer backpackServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.ARRANGE_PROP:
				backpackServer.arrangeProp(actionData.getBuf(), session);
				break;
			case SocketCmd.UPDATE_PROP_INDEX:
				backpackServer.updatePropIndex(actionData.getBuf(), session);
				break;
			case SocketCmd.USE_PROP:
				backpackServer.useProp(actionData.getBuf(), session);
				break;
			case SocketCmd.SELL_PROP:
				backpackServer.sellProp(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}


}
