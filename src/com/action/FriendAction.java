package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.FriendServer;

/**
 * 好友系统
 * @author xujinbo
 *
 */
public class FriendAction implements ActionHandler<Object> {

	@Resource
	private FriendServer friendServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.ADD_FRIEND_REQUEST:
				friendServer.addFriendRequest(actionData.getBuf(), session);
				break;
			case SocketCmd.ADD_FRIEND_RESULT:
				friendServer.addFriendResult(actionData.getBuf(), session);
				break;
			case SocketCmd.DELETE_FRIEND:
				friendServer.deleteFriend(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
