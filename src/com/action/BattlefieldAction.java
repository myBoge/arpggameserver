package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.BattlefieldServer;

/**
 * 战场监听
 * @author xujinbo
 *
 */
public class BattlefieldAction implements ActionHandler<Object> {

	@Resource
	private BattlefieldServer battlefieldServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.BATTLEFIELD_FIGHT:
				battlefieldServer.battlefieldFight(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
