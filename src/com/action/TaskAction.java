package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.TaskServer;

public class TaskAction implements ActionHandler<Object> {

	@Resource
	private TaskServer taskServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.DELETE_BUFF_PROP:
				taskServer.deleteBuffProp(actionData.getBuf(), session);
				break;
			case SocketCmd.MISSION_ADD:
				taskServer.missionAdd(actionData.getBuf(), session);
				break;
			case SocketCmd.MISSION_UPDATE:
				taskServer.missionUpdate(actionData.getBuf(), session);
				break;
			case SocketCmd.MISSION_COMPLETE:
				taskServer.missionComplete(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
