package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.GiftServer;

public class GiftAction implements ActionHandler<Object> {

	@Resource
	private GiftServer giftServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.GIFT_GET_ON_LINE:
				giftServer.giftGetOnLine(actionData.getBuf(), session);
				break;
			case SocketCmd.RECEIVE_EVERYDAY_SIGN:
				giftServer.receiceEverydaySign(actionData.getBuf(), session);
				break;
			case SocketCmd.RECEIVE_LEVEL_GIFT:
				giftServer.receiveLevelGift(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
