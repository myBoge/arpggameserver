package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.WarehouseServer;

/**
 * 仓库监听
 * @author xujinbo
 *
 */
public class WarehouseAction implements ActionHandler<Object> {

	@Resource
	private WarehouseServer warehouseServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.WAREHOUSE_ADD_PROP:
				warehouseServer.addProp(actionData.getBuf(), session);
				break;
			case SocketCmd.WAREHOUSE_UPDATE_PROP_COUNT:
				warehouseServer.updatePropCount(actionData.getBuf(), session);
				break;
			case SocketCmd.WAREHOUSE_UPDATE_PROP_INDEX:
				warehouseServer.updatePropIndex(actionData.getBuf(), session);
				break;
			case SocketCmd.WAREHOUSE_ARRANGE_PROP:
				warehouseServer.arrangeProp(actionData.getBuf(), session);
				break;
			case SocketCmd.WAREHOUSE_DATA:
				warehouseServer.requestData(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
