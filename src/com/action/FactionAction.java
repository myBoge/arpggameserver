package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.FactionServer;

/**
 * 帮派监听
 * @author xujinbo
 *
 */
public class FactionAction implements ActionHandler<Object> {

	@Resource
	private FactionServer factionServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.FACTION_REQUEST:
				factionServer.factionRequest(actionData.getBuf(), session);
				break;
			case SocketCmd.FACTION_CREATE:
				factionServer.factionCreate(actionData.getBuf(), session);
				break;
			case SocketCmd.FACTION_DISBAND:
				factionServer.factionDisband(actionData.getBuf(), session);
				break;
			case SocketCmd.FACTION_APPLY:
				factionServer.factionApply(actionData.getBuf(), session);
				break;
			case SocketCmd.FACTION_KICK:
				factionServer.factionKick(actionData.getBuf(), session);
				break;
			case SocketCmd.FACTION_QUIT:
				factionServer.factionQuit(actionData.getBuf(), session);
				break;
			case SocketCmd.FACTION_BUILD_UP:
				factionServer.factionBuildUp(actionData.getBuf(), session);
				break;
			case SocketCmd.FACTION_INFO:
				factionServer.factionInfo(actionData.getBuf(), session);
				break;
			case SocketCmd.FACTION_APPLY_DECIDE:
				factionServer.factionApplyDecide(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
