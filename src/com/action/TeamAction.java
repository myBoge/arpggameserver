package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.TeamServer;

/**
 * 队伍监听
 * @author xujinbo
 *
 */
public class TeamAction implements ActionHandler<Object> {

	@Resource
	private TeamServer teamServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.TEAM_APPLY:
				teamServer.teamApply(actionData.getBuf(), session);
				break;
			case SocketCmd.TEAM_CREATE:
				teamServer.teamCreate(actionData.getBuf(), session);
				break;
			case SocketCmd.TEAM_DISBAND:
				teamServer.teamDisband(actionData.getBuf(), session);
				break;
			case SocketCmd.TEAM_APPLY_DECIDE:
				teamServer.teamApplyDecide(actionData.getBuf(), session);
				break;
			case SocketCmd.TEAM_DELETE_ROLE:
				teamServer.teamDeleteRole(actionData.getBuf(), session);
				break;
			case SocketCmd.TEAM_FAST:
				teamServer.teamFast(actionData.getBuf(), session);
				break;
			case SocketCmd.TEAM_FAST_INVITE:
				teamServer.teamFastInvite(actionData.getBuf(), session);
				break;
			case SocketCmd.TEAM_INVITE:
				teamServer.teamInvite(actionData.getBuf(), session);
				break;
			case SocketCmd.TEAM_INVITE_DECIDE:
				teamServer.teamInviteDecide(actionData.getBuf(), session);
				break;
			case SocketCmd.TEAM_KICKOUT:
				teamServer.teamKickout(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
