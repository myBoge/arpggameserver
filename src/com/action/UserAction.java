package com.action;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.AttrEnum;
import com.net.SocketCmd;
import com.server.UserServer;


public class UserAction implements ActionHandler<Object> {

	@Resource
	private UserServer userServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.LOGIN:
				userServer.login(actionData.getBuf(), session);
				break;
			case SocketCmd.CREATE_ROLE:
				userServer.createRole(actionData.getBuf(), session);
				break;
			case SocketCmd.DELETE_ROLE:
				userServer.deleteRole(actionData.getBuf(), session);
				break;
			case SocketCmd.SEE_ROLE_INFO:
				userServer.seeRoleInfo(actionData.getBuf(), session);
				break;
			case SocketCmd.RENAME:
				userServer.rename(actionData.getBuf(), session);
				break;
			case SocketCmd.ADD_ATT_POINT:
				userServer.addAttPoint(actionData.getBuf(), session);
				break;
			case SocketCmd.UPDATE_AUTO_USE_PROP:
				userServer.updateAutoUseProp(actionData.getBuf(), session);
				break;
			case SocketCmd.THROB:
				checkThrob(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

	private void checkThrob(byte[] buf, IoSession session) {
		try {
			long timer = System.currentTimeMillis();
			if (!session.containsAttribute(AttrEnum.THROB)) {
				session.setAttribute(AttrEnum.THROB, timer);
				return;
			}
			long tempTimer = (long) session.getAttribute(AttrEnum.THROB);
			if (timer-tempTimer < 4000) {// 客户端发送的间隔时间小于4S  表示客户端    不正常
				Logger.getLogger(getClass()).fatal("客户端发送的心跳间隔时间过快  可能用了加速外挂已强制关闭!");
				session.close(true);
				return;
			}
			session.setAttribute(AttrEnum.THROB, timer);
		} catch (Exception e) {
			Logger.getLogger(getClass()).fatal("客户端发送的心跳间隔时间过快  可能用了加速外挂已强制关闭!", e);
			session.close(true);
		} 
	}

	/**
	 * 玩家离线
	 * @param session
	 */
	public void userOffline(IoSession session) {
		userServer.userOffline(session);
	}

}
