package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.MailServer;

/**
 * 邮件监听
 * @author xujinbo
 *
 */
public class MailAction implements ActionHandler<Object> {

	@Resource
	private MailServer mailServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.ADD_MAIL:
				mailServer.addMail(actionData.getBuf(), session);
				break;
			case SocketCmd.DELETE_MAIL:
				mailServer.deleteMail(actionData.getBuf(), session);
				break;
			case SocketCmd.READ_MAIL:
				mailServer.readMail(actionData.getBuf(), session);
				break;
			case SocketCmd.REQUEST_MAIL_DATA:
				mailServer.requestMailData(actionData.getBuf(), session);
				break;
			case SocketCmd.EXTRACT_GOODS:
				mailServer.extractGoods(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
