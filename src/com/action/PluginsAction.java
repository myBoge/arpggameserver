package com.action;

import java.io.IOException;

import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.boge.entity.GameOutput;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.data.UserDataManage;
import com.entity.UserInfo;
import com.main.EscortMain;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.utils.LuaUtils;

/**
 * 插件监听
 * @author xujinbo
 *
 */
public class PluginsAction implements ActionHandler<Object> {

	@Resource
	private EscortMain escortMain;
	@Resource
	private UserDataManage userDataManage;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case 0:
				break;
			default:
				GameOutput gameOutput = new GameOutput();
				try {
					UserInfo userInfo = userDataManage.getCacheUserInfo(session);
					if (userInfo == null) {
						gameOutput.writeInt(CommonCmd.NOT_USER_INFO);
						session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
						return;
					}
					if (!userInfo.isIdle()) {
						gameOutput.writeInt(CommonCmd.USER_NOT_IDLE);
						session.write(new ActionData<>(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray()));
						return;
					}
					LuaUtils.get("execute").call(
							CoerceJavaToLua.coerce(actionData), 
							CoerceJavaToLua.coerce(userInfo),
							CoerceJavaToLua.coerce(gameOutput)
							);
				} catch (IOException e) {
				} finally {
					try {
						gameOutput.close();
					} catch (IOException e) {
					}
				}
				break;
		}
	}
	
	/** 注册监听 */
	public void registAction(int action) {
		escortMain.getServerAcceptor().registAction(this, action);
	}
	/** 删除动作 */
	public void removeAction(int action) {
		escortMain.getServerAcceptor().removeAction(action);
	}
	/** 获取动作 */
	public ActionHandler<?> getAction(int action) {
		return escortMain.getServerAcceptor().getAction(action);
	}
	
	public ActionData<Object> getActionData(int action, GameOutput gameOutput) {
		return new ActionData<>(action, gameOutput.toByteArray());
	}
	
}
