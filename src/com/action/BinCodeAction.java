package com.action;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.boge.parse.Amf3Stream;
import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.main.EscortMain;
import com.net.SocketCmd;

import flex.messaging.io.amf.Amf3Input;

/**
 * 编号绑定处理
 * @author xujinbo
 */
@SuppressWarnings("unused")
public class BinCodeAction implements ActionHandler<Object> {
	
	@Resource
	private EscortMain escortMain;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		byte[] buf;
		switch (actionData.getAction()) {
			case SocketCmd.BIND_CODE_DATA:
				buf = actionData.getBuf();
				Amf3Input input = Amf3Stream.newAmf3Input(buf);
				try {
//					int code = input.readInt();// 编号
					int action = input.readInt();// 处理事件
					byte[] newBuf = new byte[input.available()];
					input.read(newBuf, 0, input.available());
//					ActionData<Object> data = new ActionData<>(action, newBuf);
//					escortMain.getServerAcceptor().getDispatcher().executeActionMapping(data, session, data);
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("解析绑定编号数据错误!", e);
				} catch (Exception e) {
					Logger.getLogger(getClass()).error("解析绑定编号数据后  执行处理错误!", e);
				} finally {
					if (input != null) {
						try {
							input.close();
						} catch (IOException e) {
							Logger.getLogger(getClass()).error("解析绑定编号数据关闭流错误!", e);
						}
					}
				}
				break;
			default:
				break;
		}
	}

}
