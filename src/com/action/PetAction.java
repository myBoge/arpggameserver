package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.PetServer;

/**
 * 宠物监听
 * @author xujinbo
 *
 */
public class PetAction implements ActionHandler<Object> {

	@Resource
	private PetServer petServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.PET_ADD_ATT_POINT:
				petServer.petAddAttPoint(actionData.getBuf(), session);
				break;
			case SocketCmd.DELETE_PET:
				petServer.deletePet(actionData.getBuf(), session);
				break;
			case SocketCmd.PET_UPDATE_JOINBATTLE:
				petServer.petUpdateJoinBattle(actionData.getBuf(), session);
				break;
			case SocketCmd.PET_UPDATE_NAME:
				petServer.petUpdateName(actionData.getBuf(), session);
				break;
			case SocketCmd.PET_TAME:
				petServer.petTame(actionData.getBuf(), session);
				break;
			case SocketCmd.PET_USE_PROP:
				petServer.petUseProp(actionData.getBuf(), session);
				break;
			case SocketCmd.WASH_PET:
				petServer.washPet(actionData.getBuf(), session);
				break;
			case SocketCmd.WASH_PET_REPLACE:
				petServer.washPetReplace(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
