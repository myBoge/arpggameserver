package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.RedeemCodeServer;

/**
 * 兑换监听
 * @author xujinbo
 *
 */
public class RedeemCodeAction implements ActionHandler<Object> {

	@Resource
	private RedeemCodeServer redeemCodeServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.REDEEM_CODE:
				redeemCodeServer.redeemCode(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
