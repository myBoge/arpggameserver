package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.GrindServer;

/**
 * 副本挂机监听
 * @author xujinbo
 *
 */
public class GrindAction implements ActionHandler<Object> {

	@Resource
	private GrindServer grindServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.ENTER_MAP:
				grindServer.enterMap(actionData.getBuf(), session);
				break;
			case SocketCmd.EXIT_MAP:
				grindServer.exitMap(actionData.getBuf(), session);
				break;
			case SocketCmd.GRIND_START:
				grindServer.grindStart(actionData.getBuf(), session);
				break;
			case SocketCmd.GRIND_STOP:
				grindServer.stopGrind(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
