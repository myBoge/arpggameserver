package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.entity.Report;
import com.net.SocketCmd;
import com.server.ReportServer;

/**
 * 举报监听
 * @author xujinbo
 *
 */
public class ReportAction implements ActionHandler<Report> {

	@Resource
	private ReportServer reportServer;
	
	@Override
	public void execute(ActionData<Report> actionData, IoSession session) {
		switch (actionData.getAction()) {
			case SocketCmd.REPORT_INFO:
				reportServer.reportInfo(actionData.getBuf(), session);
				break;
			default:
				break;
		}
	}

}
