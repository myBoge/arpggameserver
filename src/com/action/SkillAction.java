package com.action;

import org.apache.mina.core.session.IoSession;

import com.boge.share.Resource;
import com.boge.socket.ActionData;
import com.boge.socket.ActionHandler;
import com.net.SocketCmd;
import com.server.SkillServer;

/**
 * 技能监听
 * @author xujinbo
 *
 */
public class SkillAction implements ActionHandler<Object> {

	@Resource
	private SkillServer skillServer;
	
	@Override
	public void execute(ActionData<Object> actionData, IoSession session) {
		
		switch (actionData.getAction()) {
			case SocketCmd.UPDATE_SKILL_LEVEL:
				skillServer.updateSkillLevel(actionData.getBuf(), session);
				break;
			default:
				break;
		}
		
	}

}
