package com.interfaces;

public interface IPool {

	void dispose();
	
}
