package com.interfaces;

import java.io.IOException;

import com.io.EntityInputStream;
import com.io.EntityOutputStream;

public interface IAttributDataStream {

	/**
	 * 读取数据
	 * @param ois
	 * @throws IOException
	 */
	abstract void setAttributeData(EntityInputStream ois) throws IOException;
	
	/**
	 * 写入数据
	 * @param oos
	 * @throws IOException
	 */
	abstract void getAttributeData(EntityOutputStream oos) throws IOException;
	
}
