package com.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class EntityInputStream extends ObjectInputStream {

	public EntityInputStream(InputStream in) throws IOException {
		super(in);
	}
	
	@Override
	public int readInt() throws IOException {
		try {
			return super.readInt();
		} catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	public long readLong() throws IOException {
		try {
			return super.readLong();
		} catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	public String readUTF() throws IOException {
		try {
			return super.readUTF();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean readBoolean() throws IOException {
		try {
			return super.readBoolean();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Override
	public short readShort() throws IOException {
		try {
			return super.readShort();
		} catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	public double readDouble() throws IOException {
		try {
			return super.readDouble();
		} catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	public float readFloat() throws IOException {
		try {
			return super.readFloat();
		} catch (Exception e) {
			return 0;
		}
	}
	
}
