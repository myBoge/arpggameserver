package com.entity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.annotation.XMLParse;
import com.boge.entity.GameOutput;

@SuppressWarnings("serial")
@XMLParse("UseProp")
public class UseProp extends Prop {

	/** 值类型  0 数值1 百分比*/
	protected int valueType;
	/** 值 */
	protected int value;
	/** 使用等级 */
	protected int useLevel;
	/** 等级不满足时用的值 */
	protected int unmetValue;
	/** 开始使用的时间 */
	protected long startDate;
	/** 使用剩余次数  -1 永久*/
	protected int useCount = -1;
	
	public UseProp() {
	}

	@Override
	public void writeData(GameOutput gameOutput) throws IOException {
		super.writeData(gameOutput);
		gameOutput.writeLong(startDate);
		gameOutput.writeInt(useCount);
	}
	
	@Override
	public void getAttributeData(ObjectOutputStream oos) throws IOException {
		super.getAttributeData(oos);
		oos.writeLong(startDate);
		oos.writeInt(useCount);
	}
	
	@Override
	public void setAttributeData(ObjectInputStream ois) throws IOException {
		super.setAttributeData(ois);
		startDate = ois.readLong();
		useCount = ois.readInt();
	}
	
	/**
	 * 获取值类型0数值1百分比
	 * @return valueType 值类型0数值1百分比
	 */
	public int getValueType() {
		return valueType;
	}

	/**
	 * 设置值类型0数值1百分比
	 * @param valueType 值类型0数值1百分比
	 */
	public void setValueType(int valueType) {
		this.valueType = valueType;
	}

	/**
	 * 获取值
	 * @return value 值
	 */
	public int getValue() {
		return value;
	}

	/**
	 * 设置值
	 * @param value 值
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * 获取使用等级
	 * @return useLevel 使用等级
	 */
	public int getUseLevel() {
		return useLevel;
	}

	/**
	 * 设置使用等级
	 * @param useLevel 使用等级
	 */
	public void setUseLevel(int useLevel) {
		this.useLevel = useLevel;
	}

	/**
	 * 获取等级不满足时用的值
	 * @return unmetValue 等级不满足时用的值
	 */
	public int getUnmetValue() {
		return unmetValue;
	}

	/**
	 * 设置等级不满足时用的值
	 * @param unmetValue 等级不满足时用的值
	 */
	public void setUnmetValue(int unmetValue) {
		this.unmetValue = unmetValue;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UseProp [valueType=" + valueType + ", value=" + value + ", useLevel=" + useLevel + ", unmetValue="
				+ unmetValue + ", startDate=" + startDate + ", useCount=" + useCount + ", count=" + count + ", index="
				+ index + ", level=" + level + ", type=" + type + ", name=" + name + ", timeline=" + timeline
				+ ", goldPrice=" + goldPrice + ", maxCount=" + maxCount + ", isTrade=" + isTrade + ", isLock=" + isLock
				+ ", buysType=" + buysType + ", id=" + id + ", uid=" + uid + "]";
	}

	/**
	 * 获取使用剩余次数-1永久
	 * @return useCount 使用剩余次数-1永久
	 */
	public int getUseCount() {
		return useCount;
	}

	/**
	 * 设置使用剩余次数-1永久
	 * @param useCount 使用剩余次数-1永久
	 */
	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}

	/**
	 * 获取开始使用的时间
	 * @return startDate 开始使用的时间
	 */
	public long getStartDate() {
		return startDate;
	}

	/**
	 * 设置开始使用的时间
	 * @param startDate 开始使用的时间
	 */
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

}
