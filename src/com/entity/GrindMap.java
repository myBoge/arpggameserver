package com.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.annotation.XMLParse;
import com.boge.util.ResourceCell;
import com.configs.RoleConfig;
import com.utils.RandomUtils;
import com.utils.StringUtils;

@SuppressWarnings("serial")
@XMLParse("GrindMap")
public class GrindMap extends Entity {

	/** 名字 */
	private String name;
	/** 等级范围 */
	private String levels;
	/** 怪物资源可用id */
	private String res;
	
	/** 等级范围 */
	private int[] _levels;
	/** 怪物资源可用id */
	private int[] _res;
	
	public GrindMap() {
	}
	
	public GrindMap(long id) {
		super(id);
	}

	@XMLParse("levels")
	public void setLevels(String value) {
		levels = value;
		_levels = StringUtils.strChangeInt(levels.split(","));
	}
	@XMLParse("res")
	public void setRes(String value) {
		res = value;
		_res = StringUtils.strChangeInt(res.split(","));
	}
	
	/**
	 * 获取名字
	 * @return name 名字
	 */
	public String getName() {
		return name;
	}

	/**
	 * 获取等级范围
	 * @return levels 等级范围
	 */
	public String getLevels() {
		return levels;
	}

	/**
	 * 获取怪物资源可用id
	 * @return res 怪物资源可用id
	 */
	public String getRes() {
		return res;
	}

	/**
	 * 创建一波怪物
	 * @param lenRole 玩家数量
	 * @param isProp 急急如意令是否存在
	 * @return
	 */
	public List<FightData> createWaveMonster(int lenRole, boolean isProp) {
		List<FightData> lists = new ArrayList<>();
		RoleConfig roleConfig = ResourceCell.getResource(RoleConfig.class);
		UserInfo monster;
		Role role;
		int len;
		if (isProp) {
			len = lenRole*2;
		} else {
			len = RandomUtils.nextInt(lenRole, lenRole*2+1);
		}
		FightData fightData;
		int min = _levels[0];
		int max = _levels[1];
		if (min < 2) {
			min = 2;
		}
//		System.out.println("玩家数量="+userLen+"， 生成怪物数量="+len);
		for (int i = 0; i < 10; i++) {
			if (len > i) {
				fightData = new FightData();
				fightData.setBrainAI(true);
				monster = new UserInfo();
				role = roleConfig.getSample(_res[RandomUtils.nextInt(_res.length)]);
				role.setLevel(RandomUtils.nextInt(min, max));
//				role.setName(this.name+"怪物");
				role.changeValue();
				role.randomFenpei();
				role.castAtt();
				if (isProp) {
					role.setOrdinaryAttack((int) (role.getOrdinaryAttack()/1.5));
					role.setMagicAttack((int) (role.getMagicAttack()/1.5));
					role.setDefense((int) (role.getDefense()/1.5));
					role.setSpeed((int) (role.getSpeed()/1.5));
				}
				role.setBlood(role.getMaxBlood());
				role.setMagic(role.getMaxMagic());
				
				monster.setRole(role);
				fightData.setCanCatch(true);
				fightData.setUserInfo(monster);
				lists.add(fightData);
			} else {
				lists.add(null);
			}
		}
		return lists;
	}

	/**
	 * 获取等级范围
	 * @return _levels 等级范围
	 */
	public int[] get_levels() {
		return _levels;
	}

	/**
	 * 获取怪物资源可用id
	 * @return _res 怪物资源可用id
	 */
	public int[] get_res() {
		return _res;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GrindMap [name=" + name + ", _levels=" + Arrays.toString(_levels) + ", _res=" + Arrays.toString(_res)
				+ ", id=" + id + ", uid=" + uid + "]";
	}

}
