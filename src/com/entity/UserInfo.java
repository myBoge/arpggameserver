﻿package com.entity;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.luaj.vm2.LuaValue;

import com.boge.entity.GameOutput;
import com.boge.socket.ActionData;
import com.model.Backpack;
import com.model.BuffLibrary;
import com.model.EquipsLibrary;
import com.model.GiftLibrary;
import com.model.MissionLibrary;
import com.model.PetLibrary;
import com.model.RechargeLibrary;
import com.model.SkillLibrary;
import com.model.TaskLibrary;
import com.model.WarehouseLibrary;
import com.net.SocketCmd;
import com.utils.LuaUtils;
import com.utils.OnlyIdManage;

@SuppressWarnings("serial")
public class UserInfo extends Entity {

	/** 平台登录认证ID */
	private long loginId;
	/** 用户登录数据 */
	private Login login;
	/** 心情 */
	private String mood;
	/** 玩家金币 */
	private long playerGold = -1;
	/** 绑定金币 */
	private long lockGold = -1;
	/** 人民币 */
	private long rmb = -1;
	/** 绑定人民币 */
	private long lockrmb = -1;
	/** 玩家名字 */
	private String roleName;
	/** 阵营 */
	private int camp = -1;
	/** 上次登录时间 */
	private Timestamp loginDate;
	/** 上次离线时间 */
	private Timestamp offlineDate;
	/** 玩家角色信息二进制 */
	private byte[] bytes;
	/** 包裹二进制数据 */
	private byte[] backpackByte;
	/** 好友二进制数据 */
	private byte[] friendByte;
	/** 加好友请求二进制数据 */
	private byte[] addFriendByte;
	/** 加好友请求二进制数据 */
	private byte[] mailByte;
	/** 技能二进制数据 */
	private byte[] skillByte;
	/** 宠物二进制数据 */
	private byte[] petByte;
	/** 穿戴装备二进制数据 */
	private byte[] equipsByte;
	/** 已领取的礼包数据 */
	private byte[] getGiftDataByte;
	/** 仓库 */
	private byte[] warehouseByte;
	/** 正在使用中的buff道具 */
	private byte[] buffByte;
	/** 拥有的任务 */
	private byte[] taskByte;
	/** 剧情任务保存记录 */
	private byte[] missionRecord;
	/** 充值记录 */
	private byte[] rechargeRecord;
	/** 好友 */
	private List<Friend> friends;
	/** 加好友请求 */
	private List<Friend> addFriends;
	/** 邮件 */
	private List<Mail> mails;
	/** 宠物 */
	private PetLibrary petLibrarys;
	/** 礼包领取库 */
	private GiftLibrary giftLibrary;
	/** 任务集合 */
	private TaskLibrary taskLibrary;
	/** 玩家角色信息 */
	private Role role;
	/** 装备库 */
	private EquipsLibrary equipsLibrary;
	/** 玩家技能 */
	private SkillLibrary skillLibrary;
	/** 玩家背包 */
	private Backpack backpack;
	/** 玩家仓库 */
	private WarehouseLibrary warehouseLibrary;
	/** 玩家buff管理 */
	private BuffLibrary buffLibrary;
	/** 剧情任务保存记录 */
	private MissionLibrary missionLibrary;
	/** 充值记录 */
	private RechargeLibrary rechargeLibrary;
	/** 在线状态 0不在线 1在线 */
	private int onlineState;
	/** 用户当前所在场景 */
	private int currentScene = -1;
	/** 当前场景附带值 */
	private long currentSceneValue = -1;
	/** 用户当前正在战斗的boxId */
	private long currentFightId;
	/** 是否自动使用补给 1 自动 其它不不自动 */
	private int isAutoUseSupply = -1;
	/** 潜能 */
	private long potential = -1;
	/** 帮派id（创建人id） */
	private long factionId = -1;
	/** 今日在线时间 */
	private long onLineDate = -1;
	/** 在线礼包领取次数 */
	private int onLineGiftCount = -1;
	/** 当前场景起始X坐标 */
	private int currentSceneStartX = -1;
	/** 当前场景起始Y坐标 */
	private int currentSceneStartY = -1;
	
	
	/** 间隔处理时间 */
	private SpaceDate spaceDate;
	/** 历练队伍id */
	private long teamId = 0;
	/** 缓存战场上当前所在的格子id */
	private int gridId = 0;
	/** 战场上此角色是否在移动中 */
	private boolean battleMove;
	/** 战场上此角色要移动的终点 */
	private int[] movePoint = {0, 0};
	
	
	// 战斗使用参数
	/** 此玩家没有做出决定次数 */
	private int notActionCount;
	/** 物品唯一id  包括背包物品和穿戴装备和仓库 */
	private OnlyIdManage onlyIdManage;
	/** 邮件增长id */
	private long mailId;

	public UserInfo() {
		this(new Login(0));
	}

	public UserInfo(long id) {
		super(id);
	}
	
	public UserInfo(long id, byte[] backpackByte, byte[] bytes) {
		super(id);
		this.backpackByte = backpackByte;
		this.bytes = bytes;
	}

	public UserInfo(Login login) {
		super();
		this.loginId = login.getId();
		onlyIdManage = new OnlyIdManage();
		backpack = new Backpack(this);
		warehouseLibrary = new WarehouseLibrary(this);
		friends = new ArrayList<>();
		addFriends = new ArrayList<>();
		mails = new ArrayList<>();
		petLibrarys = new PetLibrary();
		giftLibrary = new GiftLibrary();
		skillLibrary = new SkillLibrary();
		buffLibrary = new BuffLibrary(this);
		taskLibrary = new TaskLibrary();
		spaceDate = new SpaceDate();
		equipsLibrary = new EquipsLibrary(this);
		missionLibrary = new MissionLibrary(this);
		rechargeLibrary = new RechargeLibrary();
	}

	/**
	 * 获取用户登录数据
	 * 
	 * @return login 用户登录数据
	 */
	public Login getLogin() {
		return login;
	}

	/**
	 * 设置用户登录数据
	 * 
	 * @param login
	 *            用户登录数据
	 */
	public void setLogin(Login login) {
		this.login = login;
	}

	/**
	 * 获取用户所拥有的session
	 * 
	 * @return session 用户所拥有的session
	 */
	public IoSession getSession() {
		return login.getSession();
	}

	/**
	 * 获取玩家金币
	 * 
	 * @return playerGold 玩家金币
	 */
	public long getPlayerGold() {
		return playerGold;
	}

	/**
	 * 设置玩家金币
	 * 
	 * @param playerGold
	 *            玩家金币
	 */
	public void setPlayerGold(long playerGold) {
		this.playerGold = playerGold;
	}

	/**
	 * 获取玩家名字
	 * 
	 * @return roleName 玩家名字
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * 设置玩家名字
	 * 
	 * @param roleName
	 *            玩家名字
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * 获取上次登录时间
	 * 
	 * @return loginDate 上次登录时间
	 */
	public Timestamp getLoginDate() {
		return loginDate;
	}

	/**
	 * 设置上次登录时间
	 * 
	 * @param loginDate
	 *            上次登录时间
	 */
	public void setLoginDate(Timestamp loginDate) {
		this.loginDate = loginDate;
	}

	/**
	 * 获取上次离线时间
	 * 
	 * @return offlineDate 上次离线时间
	 */
	public Timestamp getOfflineDate() {
		return offlineDate;
	}

	/**
	 * 设置上次离线时间
	 * 
	 * @param offlineDate
	 *            上次离线时间
	 */
	public void setOfflineDate(Timestamp offlineDate) {
		this.offlineDate = offlineDate;
	}

	/**
	 * 获取玩家角色信息二进制
	 * 
	 * @return bytes 玩家角色信息二进制
	 */
	public byte[] getBytes() {
		return bytes;
	}

	/**
	 * 设置玩家角色信息二进制
	 * 
	 * @param bytes
	 *            玩家角色信息二进制
	 */
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	/**
	 * 获取包裹二进制数据
	 * 
	 * @return backpackByte 包裹二进制数据
	 */
	public byte[] getBackpackByte() {
		return backpackByte;
	}

	/**
	 * 设置包裹二进制数据
	 * 
	 * @param backpackByte
	 *            包裹二进制数据
	 */
	public void setBackpackByte(byte[] backpackByte) {
		this.backpackByte = backpackByte;
	}

	/**
	 * 获取玩家角色信息
	 * 
	 * @return role 玩家角色信息
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * 设置玩家角色信息
	 * 
	 * @param role
	 *            玩家角色信息
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 * 获取玩家背包
	 * 
	 * @return backpack 玩家背包
	 */
	public Backpack getBackpack() {
		return backpack;
	}

	/**
	 * 获取在线状态0不在线1在线
	 * 
	 * @return onlineState 在线状态0不在线1在线
	 */
	public int getOnlineState() {
		return onlineState;
	}

	/**
	 * 设置在线状态0不在线1在线
	 * 
	 * @param onlineState
	 *            在线状态0不在线1在线
	 */
	public void setOnlineState(int onlineState) {
		this.onlineState = onlineState;
	}

	/**
	 * 获取平台登录认证ID
	 * @return loginId 平台登录认证ID
	 */
	public long getLoginId() {
		return loginId;
	}

	/**
	 * 设置平台登录认证ID
	 * @param loginId 平台登录认证ID
	 */
	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}

	/**
	 * 获取队伍id
	 * @return grindTeamId 队伍id
	 */
	public long getTeamId() {
		return teamId;
	}

	/**
	 * 设置队伍id
	 * @param grindTeamId 队伍id
	 */
	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}

	/**
	 * 获取技能二进制数据
	 * @return skillByte 技能二进制数据
	 */
	public byte[] getSkillByte() {
		return skillByte;
	}

	/**
	 * 设置技能二进制数据
	 * @param skillByte 技能二进制数据
	 */
	public void setSkillByte(byte[] skillByte) {
		this.skillByte = skillByte;
	}

	/**
	 * 获取用户当前所在场景
	 * 
	 * @return currentScene 用户当前所在场景
	 */
	public int getCurrentScene() {
		return currentScene;
	}

	/**
	 * 设置用户当前所在场景
	 * 
	 * @param currentScene
	 *            用户当前所在场景
	 */
	public void setCurrentScene(int currentScene) {
		this.currentScene = currentScene;
	}

	/**
	 * 获取当前场景附带值
	 * 
	 * @return currentSceneValue 当前场景附带值
	 */
	public long getCurrentSceneValue() {
		return currentSceneValue;
	}

	/**
	 * 设置当前场景附带值
	 * 
	 * @param currentSceneValue
	 *            当前场景附带值
	 */
	public void setCurrentSceneValue(long currentSceneValue) {
		this.currentSceneValue = currentSceneValue;
	}

	/**
	 * 获取用户当前正在战斗的boxId
	 * 
	 * @return currentFightId 用户当前正在战斗的boxId
	 */
	public long getCurrentFightId() {
		return currentFightId;
	}

	/**
	 * 设置用户当前正在战斗的boxId
	 * 
	 * @param currentFightId
	 *            用户当前正在战斗的boxId
	 */
	public void setCurrentFightId(long currentFightId) {
		this.currentFightId = currentFightId;
	}

	/**
	 * 获取是否自动使用补给
	 * 
	 * @return isAutoUseSupply 是否自动使用补给
	 */
	public int getIsAutoUseSupply() {
		return isAutoUseSupply;
	}

	/**
	 * 设置是否自动使用补给
	 * 
	 * @param isAutoUseSupply
	 *            是否自动使用补给
	 */
	public void setIsAutoUseSupply(int isAutoUseSupply) {
		this.isAutoUseSupply = isAutoUseSupply;
	}

	/**
	 * 获取此玩家没有做出决定次数
	 * @return notActionCount 此玩家没有做出决定次数
	 */
	public int getNotActionCount() {
		return notActionCount;
	}

	/**
	 * 设置此玩家没有做出决定次数
	 * @param notActionCount 此玩家没有做出决定次数
	 */
	public void setNotActionCount(int notActionCount) {
		this.notActionCount = notActionCount;
	}

	/**
	 * 获取绑定金币
	 * @return lookGold 绑定金币
	 */
	public long getLockGold() {
		return lockGold;
	}

	/**
	 * 设置绑定金币
	 * @param lookGold 绑定金币
	 */
	public void setLockGold(long lookGold) {
		this.lockGold = lookGold;
	}

	/**
	 * 获取人民币
	 * @return rmb 人民币
	 */
	public long getRmb() {
		return rmb;
	}

	/**
	 * 设置人民币
	 * @param rmb 人民币
	 */
	public void setRmb(long rmb) {
		this.rmb = rmb;
	}

	/**
	 * 获取绑定人民币
	 * @return lookrmb 绑定人民币
	 */
	public long getLockrmb() {
		return lockrmb;
	}

	/**
	 * 设置绑定人民币
	 * @param lookrmb 绑定人民币
	 */
	public void setLockrmb(long lockrmb) {
		this.lockrmb = lockrmb;
	}

	/**
	 * 获取好友二进制数据
	 * @return friendByte 好友二进制数据
	 */
	public byte[] getFriendByte() {
		return friendByte;
	}

	/**
	 * 设置好友二进制数据
	 * @param friendByte 好友二进制数据
	 */
	public void setFriendByte(byte[] friendByte) {
		this.friendByte = friendByte;
	}

	/**
	 * 获取加好友请求二进制数据
	 * @return addFriendByte 加好友请求二进制数据
	 */
	public byte[] getAddFriendByte() {
		return addFriendByte;
	}

	/**
	 * 设置加好友请求二进制数据
	 * @param addFriendByte 加好友请求二进制数据
	 */
	public void setAddFriendByte(byte[] addFriendByte) {
		this.addFriendByte = addFriendByte;
	}

	/**
	 * 获取好友
	 * @return friends 好友
	 */
	public List<Friend> getFriends() {
		return friends;
	}

	/**
	 * 获取加好友请求
	 * @return addFriends 加好友请求
	 */
	public List<Friend> getAddFriends() {
		return addFriends;
	}
	
	/**
	 * 获取加好友请求二进制数据
	 * @return mailByte 加好友请求二进制数据
	 */
	public byte[] getMailByte() {
		return mailByte;
	}

	/**
	 * 设置加好友请求二进制数据
	 * @param mailByte 加好友请求二进制数据
	 */
	public void setMailByte(byte[] mailByte) {
		this.mailByte = mailByte;
	}

	/**
	 * 获取邮件
	 * @return mails 邮件
	 */
	public List<Mail> getMails() {
		return mails;
	}

	/**
	 * 获取邮件增长id
	 * @return mailId 邮件增长id
	 */
	public long getMailId() {
		return mailId++;
	}
	
	public String getMood() {
		return mood;
	}

	public void setMood(String mood) {
		this.mood = mood;
	}

	
	/**
	 * 获取潜能
	 * @return potential 潜能
	 */
	public long getPotential() {
		return potential;
	}

	/**
	 * 设置潜能
	 * @param potential 潜能
	 */
	public void setPotential(long potential) {
		this.potential = potential;
	}
	
	/**
	 * 根据用户id  获取好友
	 * @param userId
	 * @return
	 */
	public Friend getFriend(long userId){
		int len = friends.size();
		Friend friend;
		for (int i = 0; i < len; i++) {
			friend = friends.get(i);
			if (friend.getFriendId() == userId) {
				return friend;
			}
		}
		return null;
	}
	
	/**
	 * 根据玩家id  删除加好友请求信息
	 * @param userName 玩家id
	 * @return
	 */
	public Friend removeAddFriend(long id) {
		int len = addFriends.size();
		Friend friend;
		for (int i = 0; i < len; i++) {
			friend = addFriends.get(i);
			if (friend.getFriendId() == id) {
				return addFriends.remove(i);
			}
		}
		return null;
	}
	
	/**
	 * 根据好友ID  删除好友
	 * @param userId 好友ID
	 * @return
	 */
	public Friend removeFriend(long userId) {
		int len = friends.size();
		Friend friend;
		for (int i = 0; i < len; i++) {
			friend = friends.get(i);
			if (friend.getFriendId() == userId) {
				return friends.remove(i);
			}
		}
		return null;
	}

	/**
	 * 写入所有添加好友请求数据 写到此流
	 * @param gameOutput
	 */
	public void writeAllAddFriends(GameOutput gameOutput) {
		int len = addFriends.size();
		try {
			gameOutput.writeInt(len);
			for (int i = 0; i < len; i++) {
				addFriends.get(i).writeData(gameOutput);
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("将添加好友请求数据写入流报错", e);
		}
	}
	
	/**
	 * 写入所有好友数据 写到此流
	 * @param gameOutput
	 */
	public void writeAllFriends(GameOutput gameOutput) {
		int len = friends.size();
		try {
			gameOutput.writeInt(len);
			for (int i = 0; i < len; i++) {
				friends.get(i).writeData(gameOutput);
			}
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("将好友数据写入流报错", e);
		}
	}

	/**
	 * 发送当前的血量和蓝量状态
	 * @param role
	 */
	public void sendPetHpMp(Role role) {
		GameOutput gameOutput = new GameOutput();
		try {
			sendPetHpMp(gameOutput, role);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("发送血量和蓝量状态报错", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("发送血量和蓝量状态关闭流报错", e);
				}
			}
		}
	}
	
	/**
	 * 发送当前的宠物血量和蓝量状态
	 * @param gameOutput
	 * @param role
	 * @throws IOException
	 */
	public void sendPetHpMp(GameOutput gameOutput, Role role) throws IOException {
		gameOutput.reset()
			.writeLong(role.getUid())
			.writeInt(role.getBlood())
			.writeInt(role.getMagic());
		sendData(SocketCmd.PET_UPDATE_HP_MP, gameOutput.toByteArray());
	}
	
	/**
	 * 发送当前的血量和蓝量状态
	 */
	public void sendHpMp() {
		GameOutput gameOutput = new GameOutput();
		try {
			sendHpMp(gameOutput);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("发送血量和蓝量状态报错", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("发送血量和蓝量状态关闭流报错", e);
				}
			}
		}
	}
	
	/**
	 * 发送当前的血量和蓝量状态
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void sendHpMp(GameOutput gameOutput) throws IOException {
		gameOutput.reset()
		.writeInt(this.role.getBlood())
		.writeInt(this.role.getMagic());
		sendData(SocketCmd.UPDATE_HP_MP, gameOutput.toByteArray());
	}

	/**
	 * 该角色是否处于战斗状态
	 * @return
	 */
	public boolean isIdle() {
		if (role !=null && currentFightId == 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * 根据技能id获取技能
	 * @param skillId
	 * @return
	 */
	public Skill getSkill(long skillId) {
		return skillLibrary.getSkill(skillId);
	}
	
	/**
	 * 获取当前角色  可以携带宠物数量
	 * @return
	 */
	public int getPortabilityPetCount() {
		if (role.getLevel() > 30) {
			return role.getLevel()/10;
		}
		return 3;
	}
	
	
	
	//发送
	
	/**
	 * 此用户向客户端发送消息
	 * @param action 发送指令
	 * @param arge 自动判断类型包括(Boolean、Integer、Long、byte[]、其余全是String)
	 */
	public synchronized void sendData(int action, Object ...arge) {
		GameOutput gameOutput = new GameOutput();
		try {
			gameOutput.write(arge);
			sendData(new ActionData<>(action, gameOutput.toByteArray()));
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("用户写入发送数据报错!", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("用户写入发送数据关闭流报错!", e);
				}
			}
		}
	}
	
	/**
	 * 此用户向客户端发送消息
	 * @param action 发送指令
	 * @param bs 生成的二进制
	 */
	public synchronized void sendData(int action, byte[] bs) {
		sendData(new ActionData<>(action, bs));
	}
	
	/**
	 * 此用户向客户端发送消息
	 * @param action 发送指令
	 */
	public synchronized void sendData(ActionData<Object> action) {
		if (getLogin() != null &&  getSession() != null && getSession().isConnected() && onlineState == 1) {
			getSession().write(action);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserInfo [loginId=" + loginId + ", login=" + login + ", mood=" + mood + ", playerGold=" + playerGold
				+ ", lockGold=" + lockGold + ", rmb=" + rmb + ", lockrmb=" + lockrmb + ", roleName=" + roleName
				+ ", loginDate=" + loginDate + ", offlineDate=" + offlineDate + ", bytes=" + Arrays.toString(bytes)
				+ ", friends=" + friends + ", addFriends=" + addFriends + ", mails=" + mails 
				+ ", petLibrarys=" + petLibrarys + ", role=" + role + ", equipsLibrary=" + equipsLibrary
				+ ", skillLibrary=" + skillLibrary + ", backpack=" + backpack + ", warehouseLibrary=" + warehouseLibrary
				+ ", onlineState=" + onlineState + ", currentScene=" + currentScene + ", currentSceneValue="
				+ currentSceneValue + ", currentFightId=" + currentFightId + ", isAutoUseSupply=" + isAutoUseSupply
				+ ", potential=" + potential + ", factionId=" + factionId + ", onLineDate=" + onLineDate
				+ ", onLineGiftCount=" + onLineGiftCount + ", currentSceneStartX=" + currentSceneStartX
				+ ", currentSceneStartY=" + currentSceneStartY + ", spaceDate=" + spaceDate + ", teamId=" + teamId
				+ ", notActionCount=" + notActionCount + ", onlyIdManage=" + onlyIdManage
				+ ", mailId=" + mailId + ", id=" + id + ", uid=" + uid + "]";
	}

	/**
	 * 获取玩家技能
	 * @return skillLibrary 玩家技能
	 */
	public SkillLibrary getSkillLibrary() {
		return skillLibrary;
	}

	/**
	 * 设置玩家技能
	 * @param skillLibrary 玩家技能
	 */
	public void setSkillLibrary(SkillLibrary skillLibrary) {
		this.skillLibrary = skillLibrary;
	}

	/**
	 * 获取间隔处理时间
	 * @return spaceDate 间隔处理时间
	 */
	public SpaceDate getSpaceDate() {
		return spaceDate;
	}

	/**
	 * 获取宠物二进制数据
	 * @return petByte 宠物二进制数据
	 */
	public byte[] getPetByte() {
		return petByte;
	}

	/**
	 * 设置宠物二进制数据
	 * @param petByte 宠物二进制数据
	 */
	public void setPetByte(byte[] petByte) {
		this.petByte = petByte;
	}

	/**
	 * 获取宠物
	 * @return petLibrarys 宠物
	 */
	public PetLibrary getPetLibrarys() {
		return petLibrarys;
	}

	/**
	 * 设置玩家背包
	 * @param backpack 玩家背包
	 */
	public void setBackpack(Backpack backpack) {
		this.backpack = backpack;
	}

	/**
	 * 设置宠物
	 * @param petLibrarys 宠物
	 */
	public void setPetLibrarys(PetLibrary petLibrarys) {
		this.petLibrarys = petLibrarys;
	}

	/**
	 * 获取装备库
	 * @return equipsLibrary 装备库
	 */
	public EquipsLibrary getEquipsLibrary() {
		return equipsLibrary;
	}

	/**
	 * 获取穿戴装备二进制数据
	 * @return equipsByte 穿戴装备二进制数据
	 */
	public byte[] getEquipsByte() {
		return equipsByte;
	}

	/**
	 * 设置穿戴装备二进制数据
	 * @param equipsByte 穿戴装备二进制数据
	 */
	public void setEquipsByte(byte[] equipsByte) {
		this.equipsByte = equipsByte;
	}

	/**
	 * 获取物品唯一id包括背包物品和穿戴装备和仓库
	 * @return onlyIdManage 物品唯一id包括背包物品和穿戴装备和仓库
	 */
	public OnlyIdManage getOnlyIdManage() {
		return onlyIdManage;
	}

	/** 更新角色装备加成 */
	public void updateEquipsRole() {
		role.setZhuangbeiAp(0);
		role.setZhuangbeiDp(0);
		role.setZhuangbeiHp(0);
		role.setZhuangbeiMp(0);
		role.setZhuangbeiSp(0);
		
		role.setAddHp(0);
		role.setAddMp(0);
		role.setAddAp(0);
		role.setAddSp(0);
		List<Equips> equipsList = this.equipsLibrary.getEquipsList();
		Equips equips;
		for (int i = 0; i < equipsList.size(); i++) {
			equips = equipsList.get(i);
			role.setZhuangbeiAp(role.getZhuangbeiAp()+equips.getTotalHurt());
			role.setZhuangbeiDp(role.getZhuangbeiDp()+equips.getTotalDefense());
			role.setZhuangbeiHp(role.getZhuangbeiHp()+equips.getTotalBlood());
			role.setZhuangbeiMp(role.getZhuangbeiMp()+equips.getTotalMagic());
			role.setZhuangbeiSp(role.getZhuangbeiSp()+equips.getTotalSpeed());
			
			role.setAddHp(role.getAddHp()+equips.getAttTypeValue(1));
			role.setAddMp(role.getAddMp()+equips.getAttTypeValue(2));
			role.setAddAp(role.getAddAp()+equips.getAttTypeValue(3));
			role.setAddSp(role.getAddSp()+equips.getAttTypeValue(4));
		}
	}

	/**
	 * 获取帮派id（创建人id）
	 * @return factionId 帮派id（创建人id）
	 */
	public long getFactionId() {
		return factionId;
	}

	/**
	 * 设置帮派id（创建人id）
	 * @param factionId 帮派id（创建人id）
	 */
	public void setFactionId(long factionId) {
		this.factionId = factionId;
	}

	/**
	 * 获取仓库
	 * @return warehouseByte 仓库
	 */
	public byte[] getWarehouseByte() {
		return warehouseByte;
	}

	/**
	 * 设置仓库
	 * @param warehouseByte 仓库
	 */
	public void setWarehouseByte(byte[] warehouseByte) {
		this.warehouseByte = warehouseByte;
	}

	/**
	 * 获取玩家仓库
	 * @return warehouseLibrary 玩家仓库
	 */
	public WarehouseLibrary getWarehouseLibrary() {
		return warehouseLibrary;
	}

	/**
	 * 获取在线礼包领取次数
	 * @return onLineGiftCount 在线礼包领取次数
	 */
	public int getOnLineGiftCount() {
		return onLineGiftCount;
	}

	/**
	 * 设置在线礼包领取次数
	 * @param onLineGiftCount 在线礼包领取次数
	 */
	public void setOnLineGiftCount(int onLineGiftCount) {
		this.onLineGiftCount = onLineGiftCount;
	}

	/**
	 * 获取今日在线时间
	 * @return onLineDate 今日在线时间
	 */
	public long getOnLineDate() {
		return onLineDate;
	}

	/**
	 * 设置今日在线时间
	 * @param onLineDate 今日在线时间
	 */
	public void setOnLineDate(long onLineDate) {
		this.onLineDate = onLineDate;
	}
	
	/**
	 * 获取当前上阵宠物的战斗对象
	 * @param isBrainAI 此战斗对象是否是电脑控制战斗
	 * @return
	 */
	public FightData getPetFightData(boolean isBrainAI) {
		Role petRole = petLibrarys.getJoinBattlePet();
		if (petRole != null) {
			return createFightData(this, petRole, isBrainAI);
		}
		return null;
	}
	
	/**
	 * 创建宠物战斗对象
	 * @param userInfo 宠物的主人数据对象
	 * @param petRole 战斗的宠物
	 * @param isBrainAI 此战斗对象是否是电脑控制战斗
	 * @return
	 */
	public static FightData createFightData(UserInfo userInfo, Role petRole, boolean isBrainAI) {
		boolean result = LuaUtils.get("checkPetWar").call(LuaValue.valueOf(petRole.getStrength())).toboolean();
		if (!result) {
			// 宠物不参加战斗
			return null;
		}
		FightData fightData = new FightData();
		fightData.setBrainAI(isBrainAI);
		UserInfo info = new UserInfo(userInfo.getId());
		info.setRole(petRole);
		Login login = new Login();
		login.setSession(userInfo.getSession());
		info.setLogin(login);
		info.setOnlineState(userInfo.getOnlineState());
		info.setIsAutoUseSupply(userInfo.getIsAutoUseSupply());
		info.setBackpack(userInfo.getBackpack());
		info.setPetLibrarys(userInfo.getPetLibrarys());
		info.setBuffLibrary(userInfo.getBuffLibrary());
		info.setTaskLibrary(userInfo.getTaskLibrary());
		fightData.setUserInfo(info);
		return fightData;
	}

	/**
	 * 获取当前场景起始X坐标
	 * @return currentSceneStartX 当前场景起始X坐标
	 */
	public int getCurrentSceneStartX() {
		return currentSceneStartX;
	}

	/**
	 * 设置当前场景起始X坐标
	 * @param currentSceneStartX 当前场景起始X坐标
	 */
	public void setCurrentSceneStartX(int currentSceneStartX) {
		this.currentSceneStartX = currentSceneStartX;
	}

	/**
	 * 获取当前场景起始Y坐标
	 * @return currentSceneStartY 当前场景起始Y坐标
	 */
	public int getCurrentSceneStartY() {
		return currentSceneStartY;
	}

	/**
	 * 设置当前场景起始Y坐标
	 * @param currentSceneStartY 当前场景起始Y坐标
	 */
	public void setCurrentSceneStartY(int currentSceneStartY) {
		this.currentSceneStartY = currentSceneStartY;
	}

	/**
	 * 获取缓存战场上当前所在的格子id
	 * @return gridId 缓存战场上当前所在的格子id
	 */
	public int getGridId() {
		return gridId;
	}

	/**
	 * 设置缓存战场上当前所在的格子id
	 * @param gridId 缓存战场上当前所在的格子id
	 */
	public void setGridId(int gridId) {
		this.gridId = gridId;
	}

	/**
	 * 获取战场上此角色是否在移动中
	 * @return battleMove 战场上此角色是否在移动中
	 */
	public boolean isBattleMove() {
		return battleMove;
	}

	/**
	 * 设置战场上此角色是否在移动中
	 * @param battleMove 战场上此角色是否在移动中
	 */
	public void setBattleMove(boolean battleMove) {
		this.battleMove = battleMove;
	}

	/**
	 * 获取战场上此角色要移动的终点
	 * @return movePoint 战场上此角色要移动的终点
	 */
	public int[] getMovePoint() {
		return movePoint;
	}

	/**
	 * 获取玩家buff管理
	 * @return buffLibrary 玩家buff管理
	 */
	public BuffLibrary getBuffLibrary() {
		return buffLibrary;
	}

	/**
	 * 设置玩家buff管理
	 * @param buffLibrary 玩家buff管理
	 */
	public void setBuffLibrary(BuffLibrary buffLibrary) {
		this.buffLibrary = buffLibrary;
	}

	/**
	 * 获取正在使用中的buff道具
	 * @return buffByte 正在使用中的buff道具
	 */
	public byte[] getBuffByte() {
		return buffByte;
	}

	/**
	 * 设置正在使用中的buff道具
	 * @param buffByte 正在使用中的buff道具
	 */
	public void setBuffByte(byte[] buffByte) {
		this.buffByte = buffByte;
	}

	/**
	 * 获取拥有的任务
	 * @return taskByte 拥有的任务
	 */
	public byte[] getTaskByte() {
		return taskByte;
	}

	/**
	 * 设置拥有的任务
	 * @param taskByte 拥有的任务
	 */
	public void setTaskByte(byte[] taskByte) {
		this.taskByte = taskByte;
	}

	/**
	 * 获取任务集合
	 * @return taskLibrary 任务集合
	 */
	public TaskLibrary getTaskLibrary() {
		return taskLibrary;
	}

	/**
	 * 设置任务集合
	 * @param taskLibrary 任务集合
	 */
	public void setTaskLibrary(TaskLibrary taskLibrary) {
		this.taskLibrary = taskLibrary;
	}

	/**
	 * 是否是会员
	 * @return
	 */
	public boolean isMember() {
		if (!role.isPet() && buffLibrary.getPropId(2019)!=null) {
			return true;
		}
		return false;
	}

	/**
	 * 获取礼包领取库
	 * @return giftLibrary 礼包领取库
	 */
	public GiftLibrary getGiftLibrary() {
		return giftLibrary;
	}

	/**
	 * 设置礼包领取库
	 * @param giftLibrary 礼包领取库
	 */
	public void setGiftLibrary(GiftLibrary giftLibrary) {
		this.giftLibrary = giftLibrary;
	}

	/**
	 * 获取已领取的礼包数据
	 * @return getGiftDataByte 已领取的礼包数据
	 */
	public byte[] getGetGiftDataByte() {
		return getGiftDataByte;
	}

	/**
	 * 设置已领取的礼包数据
	 * @param getGiftDataByte 已领取的礼包数据
	 */
	public void setGetGiftDataByte(byte[] getGiftDataByte) {
		this.getGiftDataByte = getGiftDataByte;
	}

	/**
	 * 获取阵营
	 * @return camp 阵营
	 */
	public int getCamp() {
		return camp;
	}

	/**
	 * 设置阵营
	 * @param camp 阵营
	 */
	public void setCamp(int camp) {
		this.camp = camp;
	}

	/**
	 * 获取剧情任务保存记录
	 * @return missionRecord 剧情任务保存记录
	 */
	public byte[] getMissionRecord() {
		return missionRecord;
	}

	/**
	 * 设置剧情任务保存记录
	 * @param missionRecord 剧情任务保存记录
	 */
	public void setMissionRecord(byte[] missionRecord) {
		this.missionRecord = missionRecord;
	}

	/**
	 * 获取充值记录 
	 * @return rechargeRecord 充值记录 
	 */
	public byte[] getRechargeRecord() {
		return rechargeRecord;
	}

	/**
	 * 设置充值记录 
	 * @param rechargeRecord 充值记录 
	 */
	public void setRechargeRecord(byte[] rechargeRecord) {
		this.rechargeRecord = rechargeRecord;
	}

	/**
	 * 获取剧情任务保存记录
	 * @return missionLibrary 剧情任务保存记录
	 */
	public MissionLibrary getMissionLibrary() {
		return missionLibrary;
	}

	/**
	 * 设置剧情任务保存记录
	 * @param missionLibrary 剧情任务保存记录
	 */
	public void setMissionLibrary(MissionLibrary missionLibrary) {
		this.missionLibrary = missionLibrary;
	}

	/**
	 * 获取充值记录
	 * @return rechargeLibrary 充值记录
	 */
	public RechargeLibrary getRechargeLibrary() {
		return rechargeLibrary;
	}

	/**
	 * 设置充值记录
	 * @param rechargeLibrary 充值记录
	 */
	public void setRechargeLibrary(RechargeLibrary rechargeLibrary) {
		this.rechargeLibrary = rechargeLibrary;
	}

	/**
	 * 获取战斗力 
	 * @return
	 */
	public int getForce() {
		int attack = role.getOrdinaryAttack()>role.getMagicAttack()?role.getOrdinaryAttack():role.getMagicAttack();
		Role tempRole = petLibrarys.getJoinBattlePet();
		if (tempRole != null) {
			attack += tempRole.getOrdinaryAttack()>tempRole.getMagicAttack()?tempRole.getOrdinaryAttack():tempRole.getMagicAttack();
		}
		return attack;
	}

}
