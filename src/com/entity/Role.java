package com.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.luaj.vm2.LuaValue;

import com.annotation.XMLParse;
import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.configs.SkillConfig;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;
import com.server.fight.ActionCmd;
import com.utils.LuaUtils;
import com.utils.RandomUtils;
import com.utils.StringUtils;

/**
 * 角色数据
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
@XMLParse("Role")
public class Role extends Entity implements IAttributDataStream {
	
	/** 玩家性别   1为男性  2为女性 */	
	private int sex;
	/** 游戏状态  0空闲 1押镖  */
	private int state;
	/** 人物移动速度  */
	private int moveSpeed;
	/** 角色名字  */	
	private String name;
	/** 玩家等级  */	
	private int level = 1;
	/** 玩家当前经验 */
	private long experience;
	/** 体力(宠物是=忠诚度) */
	private int strength = 100;
	/** 物伤 */
	private int baseOrdinaryAttack;
	/** 法伤 */
	private int baseMagicAttack;
	/** 防御 */
	private int baseDefense;
	/** 速度 */
	private int baseSpeed;
	/** 物伤 */
	private int ordinaryAttack;
	/** 法伤 */
	private int magicAttack;
	/** 防御 */
	private int defense;
	/** 速度 */
	private int speed;
	/** 当前血量 */
	private int blood;
	/** 当前法力 */
	private int magic;
	/** 最大血量 */
	private int maxBlood;
	/** 最大法力 */
	private int maxMagic;
	/** 资源 */
	private String skin;
	/** 头像 */
	private String icon;
	/** 宠物专用。技能id  */
	private List<Long> skillId = new ArrayList<>();
	
	/** 是否是宝宝 */
	private boolean isBaby;
	/** 宠物专用  是否参战 */
	private boolean joinBattlePet;
	/** 携带等级 */
	private int carryLevel = 1;
	// 宠物专用
	/** 血量成长 */
	private String baseHpGrow;
	/** 法力成长 */
	private String baseMpGrow;
	/** 速度成长 */
	private String baseSpGrow;
	/** 物攻成长 */
	private String baseApGrow;
	/** 法功成长 */
	private String baseFpGrow;
	/** 血量成长 */
	private int hpGrow;
	/** 法力成长 */
	private int mpGrow;
	/** 速度成长 */
	private int spGrow;
	/** 物攻成长 */
	private int apGrow;
	/** 法功成长 */
	private int fpGrow;
	
	/** 洗宠后的血量 */
	public int washHp;
	/** 洗宠后的法力*/
	public int washMp;
	/** 洗宠后的速度 */
	public int washSp;
	/** 洗宠后的物攻 */
	public int washAp;
	/** 洗宠后的法功 */
	public int washFp;
	
	
	// 战斗使用参数
	/** 用于播放记忆战斗 */
	private ActionCmd actionCmd;
	
	
	
	
	
	
	
	
	
	// 属性点
	/** 可带金钱(万)*/
	private int moneyNow;
	/** 可存金钱(万)*/
	private int moneyBank;
	
	/** 体质  总提升的点数   */
	private int pointHp;
	/** 灵力  总提升的点数   */
	private int pointMp;
	/** 力量  总提升的点数   */
	private int pointAp;
	/** 速度  总提升的点数   */
	private int pointSp;
	
	
	/** 体质  手动提升的点数   */
	private int jiadianHp;
	/** 灵力  手动提升的点数   */
	private int jiadianMp;
	/** 力量  手动提升的点数   */
	private int jiadianAp;
	/** 速度  手动提升的点数   */
	private int jiadianSp;
	/** 体质  装备提升的点数   */
	private int addHp;
	/** 灵力  装备提升的点数   */
	private int addMp;
	/** 力量  装备提升的点数   */
	private int addAp;
	/** 速度  装备提升的点数   */
	private int addSp;
	
	/** 还没有分配的点数   */
	private int unFenpei;
	/** 还没有分配的相性   */
	private int unXiangxing;
	
	/** 相性 金相性加点  */
	private int jiadianJin;
	/** 相性 木相性加点  */
	private int jiadianMu;
	/** 相性 水相性加点  */
	private int jiadianShui;
	/** 相性 火相性加点  */
	private int jiadianHuo;
	/** 相性 土相性加点  */
	private int jiadianTu;
	
	/** 金相性点 提升总值*/
	private int xiangxingJin;
	/** 木相性点 提升总值 */
	private int xiangxingMu;
	/** 水相性点 提升总值*/
	private int xiangxingShui;
	/** 火相性点 提升总值*/
	private int xiangxingHuo;
	/** 土相性点 提升总值*/
	private int xiangxingTu;
	
	/** 金相性点 装备提升的点数*/
	private int addJin;
	/** 木相性点 装备提升的点数*/
	private int addMu;
	/** 水相性点 装备提升的点数*/
	private int addShui;
	/** 火相性点 装备提升的点数*/
	private int addHuo;
	/** 土相性点 装备提升的点数*/
	private int addTu;
	/** 装备添加的血*/
	private int zhuangbeiHp;
	/** 装备添加的蓝*/
	private int zhuangbeiMp;
	/** 装备添加的伤害*/
	private int zhuangbeiAp;
	/** 装备添加的速度*/
	private int zhuangbeiSp;
	/** 装备添加的防御*/
	private int zhuangbeiDp;
	
	
	public Role() {
		super();
	}
	
	public Role(long id) {
		super(id);
	}

	@XMLParse("skillId")
	public void addSkillId(String value) {
		long[] values = StringUtils.strChangeLong(value.split(","));
		SkillConfig skillConfig = ResourceCell.getResource(SkillConfig.class);
		for (int i = 0; i < values.length; i++) {
			if (skillConfig.checkSkill(values[i])) {
				skillId.add(values[i]);
			}
		}
	}
	
	@XMLParse("hpGrow")
	public void textHpGrow(String value) {
		baseHpGrow = value;
		hpGrow = LuaUtils.get("parseStr").call(LuaValue.valueOf(value)).toint();
	}
	
	@XMLParse("mpGrow")
	public void textMpGrow(String value){
		baseMpGrow = value;
		mpGrow = LuaUtils.get("parseStr").call(LuaValue.valueOf(value)).toint();
	}
	
	@XMLParse("spGrow")
	public void textSpGrow(String value){
		baseSpGrow = value;
		spGrow = LuaUtils.get("parseStr").call(LuaValue.valueOf(value)).toint();
	}
	
	@XMLParse("apGrow")
	public void textApGrow(String value){
		baseApGrow = value;
		apGrow = LuaUtils.get("parseStr").call(LuaValue.valueOf(value)).toint();
	}
	
	@XMLParse("fpGrow")
	public void textFpGrow(String value){
		baseFpGrow = value;
		fpGrow = LuaUtils.get("parseStr").call(LuaValue.valueOf(value)).toint();
	}
	
	/**
	 * 获取玩家性别1为男性2为女性
	 * @return sex 玩家性别1为男性2为女性
	 */
	public int getSex() {
		return sex;
	}

	/**
	 * 设置玩家性别1为男性2为女性
	 * @param sex 玩家性别1为男性2为女性
	 */
	public void setSex(int sex) {
		this.sex = sex;
	}

	/**
	 * 获取游戏状态0空闲1押镖 2练级
	 * @return state 游戏状态0空闲1押镖 2练级
	 */
	public int getState() {
		return state;
	}

	/**
	 * 设置游戏状态0空闲1押镖
	 * @param state 游戏状态0空闲1押镖
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * 获取人物移动速度
	 * @return moveSpeed 人物移动速度
	 */
	public int getMoveSpeed() {
		return moveSpeed;
	}

	/**
	 * 设置人物移动速度
	 * @param moveSpeed 人物移动速度
	 */
	public void setMoveSpeed(int moveSpeed) {
		this.moveSpeed = moveSpeed;
	}

	/**
	 * 获取角色名字
	 * @return name 角色名字
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置角色名字
	 * @param name 角色名字
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取玩家等级
	 * @return level 玩家等级
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * 设置玩家等级
	 * @param level 玩家等级
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * 获取玩家当前经验
	 * @return experience 玩家当前经验
	 */
	public long getExperience() {
		return experience;
	}

	/**
	 * 设置玩家当前经验
	 * @param experience 玩家当前经验
	 */
	public void setExperience(long experience) {
		this.experience = experience;
	}

	/**
	 * 获取体力(宠物是=忠诚度)
	 * @return strength 体力(宠物是=忠诚度)
	 */
	public int getStrength() {
		return strength;
	}

	/**
	 * 设置体力(宠物是=忠诚度)
	 * @param strength 体力(宠物是=忠诚度)
	 */
	public void setStrength(int strength) {
		if (strength < 0) {
			strength = 0;
		}
		this.strength = strength;
	}

	/**
	 * 获取物伤
	 * @return ordinaryAttack 物伤
	 */
	public int getOrdinaryAttack() {
		return ordinaryAttack;
	}

	/**
	 * 设置物伤
	 * @param ordinaryAttack 物伤
	 */
	public void setOrdinaryAttack(int ordinaryAttack) {
		this.ordinaryAttack = ordinaryAttack;
	}

	/**
	 * 获取法伤
	 * @return magicAttack 法伤
	 */
	public int getMagicAttack() {
		return magicAttack;
	}

	/**
	 * 设置法伤
	 * @param magicAttack 法伤
	 */
	public void setMagicAttack(int magicAttack) {
		this.magicAttack = magicAttack;
	}

	/**
	 * 获取防御
	 * @return defense 防御
	 */
	public int getDefense() {
		return defense;
	}

	/**
	 * 设置防御
	 * @param defense 防御
	 */
	public void setDefense(int defense) {
		this.defense = defense;
	}

	/**
	 * 获取速度
	 * @return speed 速度
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * 设置速度
	 * @param speed 速度
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	/**
	 * 获取当前血量
	 * @return blood 当前血量
	 */
	public int getBlood() {
		return blood;
	}

	/**
	 * 设置当前血量
	 * @param blood 当前血量
	 */
	public void setBlood(int blood) {
		this.blood = blood;
	}

	/**
	 * 获取当前法力
	 * @return magic 当前法力
	 */
	public int getMagic() {
		return magic;
	}

	/**
	 * 设置当前法力
	 * @param magic 当前法力
	 */
	public void setMagic(int magic) {
		this.magic = magic;
	}

	/**
	 * 获取最大血量
	 * @return maxBlood 最大血量
	 */
	public int getMaxBlood() {
		return maxBlood;
	}

	/**
	 * 设置最大血量
	 * @param maxBlood 最大血量
	 */
	public void setMaxBlood(int maxBlood) {
		this.maxBlood = maxBlood;
	}

	/**
	 * 获取最大法力
	 * @return maxMagic 最大法力
	 */
	public int getMaxMagic() {
		return maxMagic;
	}

	/**
	 * 设置最大法力
	 * @param maxMagic 最大法力
	 */
	public void setMaxMagic(int maxMagic) {
		this.maxMagic = maxMagic;
	}

	/**
	 * 获取资源
	 * @return skin 资源
	 */
	public String getSkin() {
		return skin;
	}

	/**
	 * 设置资源
	 * @param skin 资源
	 */
	public void setSkin(String skin) {
		this.skin = skin;
	}

	/**
	 * 获取头像
	 * @return icon 头像
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * 设置头像
	 * @param icon 头像
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * 获取属性点
	 * @return moneyNow 属性点
	 */
	public int getMoneyNow() {
		return moneyNow;
	}

	/**
	 * 设置属性点
	 * @param moneyNow 属性点
	 */
	public void setMoneyNow(int moneyNow) {
		this.moneyNow = moneyNow;
	}

	/**
	 * 获取可存金钱(万)
	 * @return moneyBank 可存金钱(万)
	 */
	public int getMoneyBank() {
		return moneyBank;
	}

	/**
	 * 设置可存金钱(万)
	 * @param moneyBank 可存金钱(万)
	 */
	public void setMoneyBank(int moneyBank) {
		this.moneyBank = moneyBank;
	}

	/**
	 * 获取体质总提升的点数
	 * @return pointHp 体质总提升的点数
	 */
	public int getPointHp() {
		return pointHp;
	}

	/**
	 * 设置体质总提升的点数
	 * @param pointHp 体质总提升的点数
	 */
	public void setPointHp(int pointHp) {
		this.pointHp = pointHp;
	}

	/**
	 * 获取灵力总提升的点数
	 * @return pointMp 灵力总提升的点数
	 */
	public int getPointMp() {
		return pointMp;
	}

	/**
	 * 设置灵力总提升的点数
	 * @param pointMp 灵力总提升的点数
	 */
	public void setPointMp(int pointMp) {
		this.pointMp = pointMp;
	}

	/**
	 * 获取力量总提升的点数
	 * @return pointAp 力量总提升的点数
	 */
	public int getPointAp() {
		return pointAp;
	}

	/**
	 * 设置力量总提升的点数
	 * @param pointAp 力量总提升的点数
	 */
	public void setPointAp(int pointAp) {
		this.pointAp = pointAp;
	}

	/**
	 * 获取速度总提升的点数
	 * @return pointSp 速度总提升的点数
	 */
	public int getPointSp() {
		return pointSp;
	}

	/**
	 * 设置速度总提升的点数
	 * @param pointSp 速度总提升的点数
	 */
	public void setPointSp(int pointSp) {
		this.pointSp = pointSp;
	}

	/**
	 * 获取体质手动提升的点数
	 * @return jiadianHp 体质手动提升的点数
	 */
	public int getJiadianHp() {
		return jiadianHp;
	}

	/**
	 * 设置体质手动提升的点数
	 * @param jiadianHp 体质手动提升的点数
	 */
	public void setJiadianHp(int jiadianHp) {
		this.jiadianHp = jiadianHp;
	}

	/**
	 * 获取灵力手动提升的点数
	 * @return jiadianMp 灵力手动提升的点数
	 */
	public int getJiadianMp() {
		return jiadianMp;
	}

	/**
	 * 设置灵力手动提升的点数
	 * @param jiadianMp 灵力手动提升的点数
	 */
	public void setJiadianMp(int jiadianMp) {
		this.jiadianMp = jiadianMp;
	}

	/**
	 * 获取力量手动提升的点数
	 * @return jiadianAp 力量手动提升的点数
	 */
	public int getJiadianAp() {
		return jiadianAp;
	}

	/**
	 * 设置力量手动提升的点数
	 * @param jiadianAp 力量手动提升的点数
	 */
	public void setJiadianAp(int jiadianAp) {
		this.jiadianAp = jiadianAp;
	}

	/**
	 * 获取速度手动提升的点数
	 * @return jiadianSp 速度手动提升的点数
	 */
	public int getJiadianSp() {
		return jiadianSp;
	}

	/**
	 * 设置速度手动提升的点数
	 * @param jiadianSp 速度手动提升的点数
	 */
	public void setJiadianSp(int jiadianSp) {
		this.jiadianSp = jiadianSp;
	}

	/**
	 * 获取体质装备提升的点数
	 * @return addHp 体质装备提升的点数
	 */
	public int getAddHp() {
		return addHp;
	}

	/**
	 * 设置体质装备提升的点数
	 * @param addHp 体质装备提升的点数
	 */
	public void setAddHp(int addHp) {
		this.addHp = addHp;
	}

	/**
	 * 获取灵力装备提升的点数
	 * @return addMp 灵力装备提升的点数
	 */
	public int getAddMp() {
		return addMp;
	}

	/**
	 * 设置灵力装备提升的点数
	 * @param addMp 灵力装备提升的点数
	 */
	public void setAddMp(int addMp) {
		this.addMp = addMp;
	}

	/**
	 * 获取力量装备提升的点数
	 * @return addAp 力量装备提升的点数
	 */
	public int getAddAp() {
		return addAp;
	}

	/**
	 * 设置力量装备提升的点数
	 * @param addAp 力量装备提升的点数
	 */
	public void setAddAp(int addAp) {
		this.addAp = addAp;
	}

	/**
	 * 获取速度装备提升的点数
	 * @return addSp 速度装备提升的点数
	 */
	public int getAddSp() {
		return addSp;
	}

	/**
	 * 设置速度装备提升的点数
	 * @param addSp 速度装备提升的点数
	 */
	public void setAddSp(int addSp) {
		this.addSp = addSp;
	}

	/**
	 * 获取还没有分配的点数
	 * @return unFenpei 还没有分配的点数
	 */
	public int getUnFenpei() {
		return unFenpei;
	}

	/**
	 * 设置还没有分配的点数
	 * @param unFenpei 还没有分配的点数
	 */
	public void setUnFenpei(int unFenpei) {
		this.unFenpei = unFenpei;
	}

	/**
	 * 获取还没有分配的相性
	 * @return unXiangxing 还没有分配的相性
	 */
	public int getUnXiangxing() {
		return unXiangxing;
	}

	/**
	 * 设置还没有分配的相性
	 * @param unXiangxing 还没有分配的相性
	 */
	public void setUnXiangxing(int unXiangxing) {
		this.unXiangxing = unXiangxing;
	}

	/**
	 * 获取相性金相性加点
	 * @return jiadianJin 相性金相性加点
	 */
	public int getJiadianJin() {
		return jiadianJin;
	}

	/**
	 * 设置相性金相性加点
	 * @param jiadianJin 相性金相性加点
	 */
	public void setJiadianJin(int jiadianJin) {
		this.jiadianJin = jiadianJin;
	}

	/**
	 * 获取相性木相性加点
	 * @return jiadianMu 相性木相性加点
	 */
	public int getJiadianMu() {
		return jiadianMu;
	}

	/**
	 * 设置相性木相性加点
	 * @param jiadianMu 相性木相性加点
	 */
	public void setJiadianMu(int jiadianMu) {
		this.jiadianMu = jiadianMu;
	}

	/**
	 * 获取相性水相性加点
	 * @return jiadianShui 相性水相性加点
	 */
	public int getJiadianShui() {
		return jiadianShui;
	}

	/**
	 * 设置相性水相性加点
	 * @param jiadianShui 相性水相性加点
	 */
	public void setJiadianShui(int jiadianShui) {
		this.jiadianShui = jiadianShui;
	}

	/**
	 * 获取相性火相性加点
	 * @return jiadianHuo 相性火相性加点
	 */
	public int getJiadianHuo() {
		return jiadianHuo;
	}

	/**
	 * 设置相性火相性加点
	 * @param jiadianHuo 相性火相性加点
	 */
	public void setJiadianHuo(int jiadianHuo) {
		this.jiadianHuo = jiadianHuo;
	}

	/**
	 * 获取相性土相性加点
	 * @return jiadianTu 相性土相性加点
	 */
	public int getJiadianTu() {
		return jiadianTu;
	}

	/**
	 * 设置相性土相性加点
	 * @param jiadianTu 相性土相性加点
	 */
	public void setJiadianTu(int jiadianTu) {
		this.jiadianTu = jiadianTu;
	}

	/**
	 * 获取金相性点提升总值
	 * @return xiangxingJin 金相性点提升总值
	 */
	public int getXiangxingJin() {
		return xiangxingJin;
	}

	/**
	 * 设置金相性点提升总值
	 * @param xiangxingJin 金相性点提升总值
	 */
	public void setXiangxingJin(int xiangxingJin) {
		this.xiangxingJin = xiangxingJin;
	}

	/**
	 * 获取木相性点提升总值
	 * @return xiangxingMu 木相性点提升总值
	 */
	public int getXiangxingMu() {
		return xiangxingMu;
	}

	/**
	 * 设置木相性点提升总值
	 * @param xiangxingMu 木相性点提升总值
	 */
	public void setXiangxingMu(int xiangxingMu) {
		this.xiangxingMu = xiangxingMu;
	}

	/**
	 * 获取水相性点提升总值
	 * @return xiangxingShui 水相性点提升总值
	 */
	public int getXiangxingShui() {
		return xiangxingShui;
	}

	/**
	 * 设置水相性点提升总值
	 * @param xiangxingShui 水相性点提升总值
	 */
	public void setXiangxingShui(int xiangxingShui) {
		this.xiangxingShui = xiangxingShui;
	}

	/**
	 * 获取火相性点提升总值
	 * @return xiangxingHuo 火相性点提升总值
	 */
	public int getXiangxingHuo() {
		return xiangxingHuo;
	}

	/**
	 * 设置火相性点提升总值
	 * @param xiangxingHuo 火相性点提升总值
	 */
	public void setXiangxingHuo(int xiangxingHuo) {
		this.xiangxingHuo = xiangxingHuo;
	}

	/**
	 * 获取土相性点提升总值
	 * @return xiangxingTu 土相性点提升总值
	 */
	public int getXiangxingTu() {
		return xiangxingTu;
	}

	/**
	 * 设置土相性点提升总值
	 * @param xiangxingTu 土相性点提升总值
	 */
	public void setXiangxingTu(int xiangxingTu) {
		this.xiangxingTu = xiangxingTu;
	}

	/**
	 * 获取金相性点装备提升的点数
	 * @return addJin 金相性点装备提升的点数
	 */
	public int getAddJin() {
		return addJin;
	}

	/**
	 * 设置金相性点装备提升的点数
	 * @param addJin 金相性点装备提升的点数
	 */
	public void setAddJin(int addJin) {
		this.addJin = addJin;
	}

	/**
	 * 获取木相性点装备提升的点数
	 * @return addMu 木相性点装备提升的点数
	 */
	public int getAddMu() {
		return addMu;
	}

	/**
	 * 设置木相性点装备提升的点数
	 * @param addMu 木相性点装备提升的点数
	 */
	public void setAddMu(int addMu) {
		this.addMu = addMu;
	}

	/**
	 * 获取水相性点装备提升的点数
	 * @return addShui 水相性点装备提升的点数
	 */
	public int getAddShui() {
		return addShui;
	}

	/**
	 * 设置水相性点装备提升的点数
	 * @param addShui 水相性点装备提升的点数
	 */
	public void setAddShui(int addShui) {
		this.addShui = addShui;
	}

	/**
	 * 获取火相性点装备提升的点数
	 * @return addHuo 火相性点装备提升的点数
	 */
	public int getAddHuo() {
		return addHuo;
	}

	/**
	 * 设置火相性点装备提升的点数
	 * @param addHuo 火相性点装备提升的点数
	 */
	public void setAddHuo(int addHuo) {
		this.addHuo = addHuo;
	}

	/**
	 * 获取土相性点装备提升的点数
	 * @return addTu 土相性点装备提升的点数
	 */
	public int getAddTu() {
		return addTu;
	}

	/**
	 * 设置土相性点装备提升的点数
	 * @param addTu 土相性点装备提升的点数
	 */
	public void setAddTu(int addTu) {
		this.addTu = addTu;
	}

	/**
	 * 获取装备添加的血
	 * @return zhuangbeiHp 装备添加的血
	 */
	public int getZhuangbeiHp() {
		return zhuangbeiHp;
	}

	/**
	 * 设置装备添加的血
	 * @param zhuangbeiHp 装备添加的血
	 */
	public void setZhuangbeiHp(int zhuangbeiHp) {
		this.zhuangbeiHp = zhuangbeiHp;
	}

	/**
	 * 获取装备添加的蓝
	 * @return zhuangbeiMp 装备添加的蓝
	 */
	public int getZhuangbeiMp() {
		return zhuangbeiMp;
	}

	/**
	 * 设置装备添加的蓝
	 * @param zhuangbeiMp 装备添加的蓝
	 */
	public void setZhuangbeiMp(int zhuangbeiMp) {
		this.zhuangbeiMp = zhuangbeiMp;
	}

	/**
	 * 获取装备添加的伤害
	 * @return zhuangbeiAp 装备添加的伤害
	 */
	public int getZhuangbeiAp() {
		return zhuangbeiAp;
	}

	/**
	 * 设置装备添加的伤害
	 * @param zhuangbeiAp 装备添加的伤害
	 */
	public void setZhuangbeiAp(int zhuangbeiAp) {
		this.zhuangbeiAp = zhuangbeiAp;
	}

	/**
	 * 获取装备添加的速度
	 * @return zhuangbeiSp 装备添加的速度
	 */
	public int getZhuangbeiSp() {
		return zhuangbeiSp;
	}

	/**
	 * 设置装备添加的速度
	 * @param zhuangbeiSp 装备添加的速度
	 */
	public void setZhuangbeiSp(int zhuangbeiSp) {
		this.zhuangbeiSp = zhuangbeiSp;
	}

	/**
	 * 获取装备添加的防御
	 * @return zhuangbeiDp 装备添加的防御
	 */
	public int getZhuangbeiDp() {
		return zhuangbeiDp;
	}

	/**
	 * 设置装备添加的防御
	 * @param zhuangbeiDp 装备添加的防御
	 */
	public void setZhuangbeiDp(int zhuangbeiDp) {
		this.zhuangbeiDp = zhuangbeiDp;
	}
	
	/**
	 * 获取宠物专用是否参战
	 * @return joinBattlePet 宠物专用是否参战
	 */
	public boolean isJoinBattlePet() {
		return joinBattlePet;
	}

	/**
	 * 设置宠物专用是否参战
	 * @param joinBattlePet 宠物专用是否参战
	 */
	public void setJoinBattlePet(boolean joinBattlePet) {
		this.joinBattlePet = joinBattlePet;
	}
	
	/**
	 * 是否是宠物或怪兽
	 * @return
	 */
	public boolean isPet() {
		if (id > 2000) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Role [sex=" + sex + ", state=" + state + ", moveSpeed="
				+ moveSpeed + ", name=" + name + ", level=" + level
				+ ", experience=" + experience + ", strength=" + strength
				+ ", ordinaryAttack=" + ordinaryAttack + ", magicAttack="
				+ magicAttack + ", defense=" + defense + ", speed=" + speed
				+ ", blood=" + blood + ", magic=" + magic + ", maxBlood="
				+ maxBlood + ", maxMagic=" + maxMagic + ", skin=" + skin
				+ ", icon=" + icon + ", moneyNow=" + moneyNow + ", moneyBank="
				+ moneyBank + ", pointHp=" + pointHp + ", pointMp=" + pointMp
				+ ", pointAp=" + pointAp + ", pointSp=" + pointSp
				+ ", jiadianHp=" + jiadianHp + ", jiadianMp=" + jiadianMp
				+ ", jiadianAp=" + jiadianAp + ", jiadianSp=" + jiadianSp
				+ ", addHp=" + addHp + ", addMp=" + addMp + ", addAp=" + addAp
				+ ", addSp=" + addSp + ", unFenpei=" + unFenpei
				+ ", unXiangxing=" + unXiangxing + ", jiadianJin=" + jiadianJin
				+ ", jiadianMu=" + jiadianMu + ", jiadianShui=" + jiadianShui
				+ ", jiadianHuo=" + jiadianHuo + ", jiadianTu=" + jiadianTu
				+ ", xiangxingJin=" + xiangxingJin + ", xiangxingMu="
				+ xiangxingMu + ", xiangxingShui=" + xiangxingShui
				+ ", xiangxingHuo=" + xiangxingHuo + ", xiangxingTu="
				+ xiangxingTu + ", addJin=" + addJin + ", addMu=" + addMu
				+ ", addShui=" + addShui + ", addHuo=" + addHuo + ", addTu="
				+ addTu + ", zhuangbeiHp=" + zhuangbeiHp + ", zhuangbeiMp="
				+ zhuangbeiMp + ", zhuangbeiAp=" + zhuangbeiAp
				+ ", zhuangbeiSp=" + zhuangbeiSp + ", zhuangbeiDp="
				+ zhuangbeiDp + ", id=" + id + ", uid=" + uid + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if(super.equals(obj))
			return true;
		Role other = (Role) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sex != other.sex)
			return false;
		return true;
	}
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		id = ois.readLong();
		sex = ois.readInt();
		state = ois.readInt();
		icon = ois.readUTF();
		moveSpeed = ois.readInt();
		name = ois.readUTF();
		level = ois.readInt();
		experience = ois.readLong();
		strength = ois.readInt();
		blood = ois.readInt();
		magic = ois.readInt();
		jiadianHp = ois.readInt();
		jiadianMp = ois.readInt();
		jiadianAp = ois.readInt();
		jiadianSp = ois.readInt();
		if (isPet()) {
			isBaby = ois.readBoolean();
			joinBattlePet = ois.readBoolean();
			hpGrow = ois.readInt();
			mpGrow = ois.readInt();
			spGrow = ois.readInt();
			apGrow = ois.readInt();
			fpGrow = ois.readInt();
			
			washHp = ois.readInt();
			washMp = ois.readInt();
			washSp = ois.readInt();
			washAp = ois.readInt();
			washFp = ois.readInt();
		}
//		System.out.println("ois="+blood+" | "+magic+" | "+name);
	}
	
	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		oos.writeLong(id);
		oos.writeInt(sex);
		oos.writeInt(state);
		oos.writeUTF(icon==null?"":icon);
		oos.writeInt(moveSpeed);
		oos.writeUTF(name==null?"":name);
		oos.writeInt(level);
		oos.writeLong(experience);
		oos.writeInt(strength);
		oos.writeInt(blood);
		oos.writeInt(magic);
		oos.writeInt(jiadianHp);
		oos.writeInt(jiadianMp);
		oos.writeInt(jiadianAp);
		oos.writeInt(jiadianSp);
		if (isPet()) {
			oos.writeBoolean(isBaby);
			oos.writeBoolean(joinBattlePet);
			oos.writeInt(hpGrow);
			oos.writeInt(mpGrow);
			oos.writeInt(spGrow);
			oos.writeInt(apGrow);
			oos.writeInt(fpGrow);
			
			oos.writeInt(washHp);
			oos.writeInt(washMp);
			oos.writeInt(washSp);
			oos.writeInt(washAp);
			oos.writeInt(washFp);
		}
	}

	/**
	 * 将基础信息  写入到发送流
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void writeData(GameOutput gameOutput) throws IOException {
		gameOutput.writeLong(id);
		gameOutput.writeLong(uid);
		gameOutput.writeUTF(name);
		gameOutput.writeInt(level);
		gameOutput.writeLong(experience);
		gameOutput.writeInt(state);
		gameOutput.writeInt(strength);
		gameOutput.writeInt(blood);
		gameOutput.writeInt(magic);
		gameOutput.writeInt(jiadianHp);
		gameOutput.writeInt(jiadianMp);
		gameOutput.writeInt(jiadianAp);
		gameOutput.writeInt(jiadianSp);
		if (isPet()) {
			gameOutput.writeBoolean(isBaby);
			gameOutput.writeBoolean(joinBattlePet);
			gameOutput.writeInt(hpGrow);
			gameOutput.writeInt(mpGrow);
			gameOutput.writeInt(spGrow);
			gameOutput.writeInt(apGrow);
			gameOutput.writeInt(fpGrow);
			
			gameOutput.writeInt(washHp);
			gameOutput.writeInt(washMp);
			gameOutput.writeInt(washSp);
			gameOutput.writeInt(washAp);
			gameOutput.writeInt(washFp);
		} else {
			gameOutput.writeInt(baseOrdinaryAttack);
			gameOutput.writeInt(baseMagicAttack);
			gameOutput.writeInt(baseSpeed);
			gameOutput.writeInt(baseDefense);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	/** 属性值改变 */
	public void changeValue() {
		this.unFenpei = this.level*4 - this.jiadianHp - this.jiadianMp - this.jiadianAp - this.jiadianSp-4;
		this.pointHp = this.addHp + this.jiadianHp + this.level;
		this.pointMp = this.addMp + this.jiadianMp + this.level;
		this.pointAp = this.addAp + this.jiadianAp + this.level;
		this.pointSp = this.addSp + this.jiadianSp + this.level;
		if (isPet()) {
			return;
		}
		if (this.level<60)
			unXiangxing = (this.level-1)/2 - this.jiadianJin-this.jiadianMu-this.jiadianShui-this.jiadianHuo-this.jiadianTu;
		else
			unXiangxing = this.level-30 - this.jiadianJin-this.jiadianMu-this.jiadianShui-this.jiadianHuo-this.jiadianTu;
		this.xiangxingJin = this.jiadianJin + this.addJin;
		this.xiangxingMu = this.jiadianMu + this.addMu;
		this.xiangxingShui = this.jiadianShui + this.addShui;
		this.xiangxingHuo = this.jiadianHuo + this.addHuo;
		this.xiangxingTu = this.jiadianTu + this.addTu;
		if (this.level > 9){
			this.moneyNow = (int) (this.level*this.level*this.level*0.0015+this.level*this.level*0.1+2);
			this.moneyBank = (int) (this.level*this.level*this.level*0.0045+this.level*this.level*0.28+2);
		} else {
			this.moneyNow = (int) (this.level*this.level*this.level*0.0015+this.level*this.level*0.1+2);
			this.moneyBank = 0;
		}
	}
	
	/**
	 * 计算
	 */
	@SuppressWarnings("unused")
	public void castAtt() {
		changeValue();
		int level = this.level;
		int un_fenpei = this.unFenpei;
		int point_hp = this.pointHp;
		int point_mp = this.pointMp;
		int point_ap = this.pointAp;	
		int point_sp = this.pointSp;
		int hp,mp,sp,dp,ap,fp;
		int zhuangbei_hp = this.zhuangbeiHp;
		int zhuangbei_mp = this.zhuangbeiMp;
		int zhuangbei_ap = this.zhuangbeiAp;
		int zhuangbei_sp = this.zhuangbeiSp;
		int zhuangbei_dp = this.zhuangbeiDp;
		if (isPet()) {
			point_hp = point_hp-level;
			point_mp = point_mp-level;
			point_ap = point_ap-level;
			point_sp = point_sp-level;
			hp = (int) ((1.397*level*Math.round(1.285*level+59.148)+Math.round( level/3.5+4)*point_hp*1.6 )*(0.00625*hpGrow+0.375 ));
			mp = (int) ((Math.round(1.1876*level+56.6079)*level + Math.round(level/5.3+4)*point_mp*1.6 )  * (0.00625*mpGrow+0.375));
			ap = (int) (((60.255+4.789*level+0.286*level*level)+point_ap*8)*(0.00625*apGrow+0.375));
			fp = (int) (((60.255+4.789*level+0.286*level*level)+point_mp*8)*(0.00625*fpGrow+0.375));
			sp = (int) (((point_sp+level)*4.8+77)*(0.00625*spGrow+0.375));
			dp = (int) (20 + (level+point_hp)*5 +0.214*(level+point_hp));
			this.maxBlood = hp+zhuangbei_hp;
			this.maxMagic = mp+zhuangbei_mp;
			this.ordinaryAttack = ap+zhuangbei_ap;
			this.magicAttack = fp+zhuangbei_ap;
			this.speed = sp+zhuangbei_sp;
			this.defense = dp+zhuangbei_dp;
			return;
		}
		int add_jin = this.addJin;
		int add_mu = this.addMu;
		int add_shui = this.addShui;
		int add_huo = this.addHuo;
		int add_tu = this.addTu;
		int xiangxing_jin = this.xiangxingJin;
		int xiangxing_mu = this.xiangxingMu;
		int xiangxing_shui = this.xiangxingShui;
		int xiangxing_huo = this.xiangxingHuo;
		int xiangxing_tu = this.xiangxingTu;
		point_hp=point_hp-level;
		point_mp=point_mp-level;
		point_ap=point_ap-level;
		point_sp=point_sp-level;
		
		hp= (int) (Math.round(0.645*level*(1.084*level+0.66276)+28.824*level+74.2808)+((level-1)/3.536+5)*point_hp+xiangxing_mu*(level+point_hp)/1.03);
		mp= (int) (0.4070*level*(1.1823*level)+18.3623*level+63.5348+((level-1)/5.304+4)*point_mp);
		fp= (int) (baseMagicAttack + (level+point_mp)*5 +0.476075*(level+point_mp)*xiangxing_jin);
		ap= (int) (baseOrdinaryAttack + (level+point_ap)*5 +0.476075*(level+point_ap)*xiangxing_tu);
		sp= (int) (baseSpeed + (level+point_sp)*2 +0.02367*(level+point_sp)*xiangxing_huo);
		dp= (int) (baseDefense + (level+point_hp)*5 +0.214*(level+point_hp)*xiangxing_shui);
		this.maxBlood = hp+zhuangbei_hp;
		this.maxMagic = mp+zhuangbei_mp;
		this.ordinaryAttack = ap+zhuangbei_ap;
		this.magicAttack = fp+zhuangbei_ap;
		this.speed = sp+zhuangbei_sp;
		this.defense = dp+zhuangbei_dp;
	}

	/**
	 * 获取宠物专用。技能id
	 * @return skillId 宠物专用。技能id
	 */
	public List<Long> getSkillId() {
		return skillId;
	}
	
	/**
	 * 检查此宠物是否存在此技能
	 * @param skill
	 * @return
	 */
	public boolean checkExistSkill(long skillId) {
		return this.skillId.indexOf(skillId) != -1;
	}
	
	/**
	 * 检查此宠物是否可以使用此技能
	 * @param skill
	 * @return
	 */
	public boolean checkUseSkill(long skillId) {
		if (level >= 20) {
			int index = this.skillId.indexOf(skillId);
			if (index == 0 && level >= 20) {
				return true;
			} else if (index == 1 && level >= 40) {
				return true;
			} else if (index == 2 && level >= 60) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 设置宠物专用。技能id
	 * @param skillId 宠物专用。技能id
	 */
	public void setSkillId(List<Long> skillId) {
		this.skillId = skillId;
	}

	/**
	 * 获取宠物专用
	 * @return hpGrow 宠物专用
	 */
	public int getHpGrow() {
		return hpGrow;
	}

	/**
	 * 设置宠物专用
	 * @param hpGrow 宠物专用
	 */
	public void setHpGrow(int hpGrow) {
		this.hpGrow = hpGrow;
	}

	/**
	 * 获取法力成长
	 * @return mpGrow 法力成长
	 */
	public int getMpGrow() {
		return mpGrow;
	}

	/**
	 * 设置法力成长
	 * @param mpGrow 法力成长
	 */
	public void setMpGrow(int mpGrow) {
		this.mpGrow = mpGrow;
	}

	/**
	 * 获取速度成长
	 * @return spGrow 速度成长
	 */
	public int getSpGrow() {
		return spGrow;
	}

	/**
	 * 设置速度成长
	 * @param spGrow 速度成长
	 */
	public void setSpGrow(int spGrow) {
		this.spGrow = spGrow;
	}

	/**
	 * 获取物攻成长
	 * @return apGrow 物攻成长
	 */
	public int getApGrow() {
		return apGrow;
	}

	/**
	 * 设置物攻成长
	 * @param apGrow 物攻成长
	 */
	public void setApGrow(int apGrow) {
		this.apGrow = apGrow;
	}

	/**
	 * 获取法功成长
	 * @return fpGrow 法功成长
	 */
	public int getFpGrow() {
		return fpGrow;
	}

	/**
	 * 设置法功成长
	 * @param fpGrow 法功成长
	 */
	public void setFpGrow(int fpGrow) {
		this.fpGrow = fpGrow;
	}

	/**
	 * 获取携带等级
	 * @return carryLevel 携带等级
	 */
	public int getCarryLevel() {
		return carryLevel;
	}

	/**
	 * 设置携带等级
	 * @param carryLevel 携带等级
	 */
	public void setCarryLevel(int carryLevel) {
		this.carryLevel = carryLevel;
	}

	/**
	 * 剩余的未分配点数  随机分配
	 */
	public void randomFenpei() {
		if (unFenpei >= 4) {
			double[] fenpei = RandomUtils.devide(unFenpei, 4, 0, 0);
			setJiadianHp((int)fenpei[0]);
			setJiadianMp((int)fenpei[1]);
			setJiadianAp((int)fenpei[2]);
			setJiadianSp((int)fenpei[3]);
		}
	}

	/**
	 * 获取战斗使用参数
	 * @return actionCmd 战斗使用参数
	 */
	public ActionCmd getActionCmd() {
		return actionCmd;
	}

	/**
	 * 设置战斗使用参数
	 * @param actionCmd 战斗使用参数
	 */
	public void setActionCmd(ActionCmd actionCmd) {
		this.actionCmd = actionCmd;
	}

	/**
	 * 获取宠物专用
	 * @return baseHpGrow 宠物专用
	 */
	public String getBaseHpGrow() {
		return baseHpGrow;
	}

	/**
	 * 获取法力成长
	 * @return baseMpGrow 法力成长
	 */
	public String getBaseMpGrow() {
		return baseMpGrow;
	}

	/**
	 * 获取速度成长
	 * @return baseSpGrow 速度成长
	 */
	public String getBaseSpGrow() {
		return baseSpGrow;
	}

	/**
	 * 获取物攻成长
	 * @return baseApGrow 物攻成长
	 */
	public String getBaseApGrow() {
		return baseApGrow;
	}

	/**
	 * 获取法功成长
	 * @return baseFpGrow 法功成长
	 */
	public String getBaseFpGrow() {
		return baseFpGrow;
	}

	/**
	 * 获取是否是宝宝
	 * @return isBaby 是否是宝宝
	 */
	public boolean isBaby() {
		return isBaby;
	}

	/**
	 * 设置是否是宝宝
	 * @param isBaby 是否是宝宝
	 */
	public void setBaby(boolean isBaby) {
		this.isBaby = isBaby;
	}

	public int getMaxHpGrow() {
		return Integer.parseInt(baseHpGrow.split("～")[1]);
	}
	public int getMaxMpGrow() {
		return Integer.parseInt(baseMpGrow.split("～")[1]);
	}
	public int getMaxSpGrow() {
		return Integer.parseInt(baseSpGrow.split("～")[1]);
	}
	public int getMaxApGrow() {
		return Integer.parseInt(baseApGrow.split("～")[1]);
	}
	public int getMaxFpGrow() {
		return Integer.parseInt(baseFpGrow.split("～")[1]);
	}

}
