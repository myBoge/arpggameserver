package com.entity;

import java.io.Serializable;
import java.sql.Timestamp;

@SuppressWarnings("serial")
public class ServerInfo extends Entity implements Serializable {

	/** 服务器名字 */
	private String name;
	/** 服务器状态 */
	private long state;
	/** 首次开服时间 */
	private Timestamp openDate;
	/** 本次开服时间 */
	private Timestamp currentlyOpenDate;
	/** 今天时间 */
	private Timestamp todayData;
	
	public ServerInfo() {
		super();
	}

	public ServerInfo(long id, Timestamp todayData) {
		super(id);
		this.todayData = todayData;
	}

	public ServerInfo(long id, int state) {
		super();
		this.id = id;
		this.state = state;
	}
	
	public ServerInfo(long id, int state, Timestamp openDate,
			Timestamp currentlyOpenDate) {
		super();
		this.id = id;
		this.state = state;
		this.openDate = openDate;
		this.currentlyOpenDate = currentlyOpenDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getState() {
		return (int) state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Timestamp getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Timestamp openDate) {
		this.openDate = openDate;
	}

	public Timestamp getCurrentlyOpenDate() {
		return currentlyOpenDate;
	}

	public void setCurrentlyOpenDate(Timestamp currentlyOpenDate) {
		this.currentlyOpenDate = currentlyOpenDate;
	}

	@Override
	public String toString() {
		return "ServerInfo [name=" + name + ", state=" + state + ", openDate="
				+ openDate + ", currentlyOpenDate=" + currentlyOpenDate
				+ ", todayData=" + todayData + ", id=" + id + ", uid=" + uid
				+ "]";
	}

	public Timestamp getTodayData() {
		return todayData;
	}

	public void setTodayData(Timestamp todayData) {
		this.todayData = todayData;
	}

	public void setState(long state) {
		this.state = state;
	}
	
}
