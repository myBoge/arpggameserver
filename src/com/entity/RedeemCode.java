package com.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

/**
 * 兑换码
 * @author xujinbo
 */
@SuppressWarnings("serial")
public class RedeemCode extends Entity implements IAttributDataStream {

	/** 兑换码类型 */
	private int redeemType = -1;
	/** 兑换码数据 */
	private byte[] bytes;
	/** 礼包id */
	private long giftId = -1;
	/** 兑换码 */
	private List<String> codeList = new ArrayList<>();
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		int len = ois.readInt();
		for (int i = 0; i < len; i++) {
			codeList.add(ois.readUTF());
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		int len = codeList.size();
		oos.writeInt(len);
		for (int i = 0; i < len; i++) {
			oos.writeUTF(codeList.get(i));
		}
	}

	/**
	 * 获取兑换码
	 * @return codeList 兑换码
	 */
	public List<String> getCodeList() {
		return codeList;
	}

	/**
	 * 获取兑换码类型
	 * @return redeemType 兑换码类型
	 */
	public int getRedeemType() {
		return redeemType;
	}

	/**
	 * 设置兑换码类型
	 * @param redeemType 兑换码类型
	 */
	public void setRedeemType(int redeemType) {
		this.redeemType = redeemType;
	}

	/**
	 * 获取兑换码数据
	 * @return bytes 兑换码数据
	 */
	public byte[] getBytes() {
		return bytes;
	}

	/**
	 * 设置兑换码数据
	 * @param bytes 兑换码数据
	 */
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	/**
	 * 获取礼包id
	 * @return giftId 礼包id
	 */
	public long getGiftId() {
		return giftId;
	}

	/**
	 * 设置礼包id
	 * @param giftId 礼包id
	 */
	public void setGiftId(long giftId) {
		this.giftId = giftId;
	}

}
