package com.entity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.annotation.XMLParse;
import com.boge.entity.GameOutput;

@SuppressWarnings("serial")
@XMLParse("Equips")
public class Equips extends Prop {

	/** 伤害 */
	protected int hurt;
	/** 防御 */
	protected int defense;
	/** 气血 */
	protected int blood;
	/** 法力 */
	protected int magic;
	/** 速度 */
	protected int speed;
	/** 闪避 */
	protected int dodge;
	/** 需要等级 */
	protected int useLevel;
	/** 耐久度 */
	protected int durability;
	/** 当前耐久度 */
	protected int currentDurability;
	/** 改造次数 */
	protected int reformCount;
	/** 此装备附带的属性 */
	private List<Attribute> attributesList;
	
	public Equips() {
		super();
		attributesList = new ArrayList<>();
	}
	
	@Override
	public void writeData(GameOutput gameOutput) throws IOException {
		super.writeData(gameOutput);
		gameOutput.writeInt(currentDurability);
		gameOutput.writeInt(reformCount);
		writeAttributeData(gameOutput);
	}
	
	@Override
	public void getAttributeData(ObjectOutputStream oos) throws IOException {
		super.getAttributeData(oos);
		oos.writeInt(currentDurability);
		oos.writeInt(reformCount);
		oos.writeInt(attributesList.size());
		for (int i = 0; i < attributesList.size(); i++) {
			oos.writeInt(attributesList.get(i).getType());
			oos.writeInt(attributesList.get(i).getValue());
		}
	}
	
	@Override
	public void setAttributeData(ObjectInputStream ois) throws IOException {
		super.setAttributeData(ois);
		currentDurability = ois.readInt();
		reformCount = ois.readInt();
		int len = ois.readInt();
		Attribute attribute;
		for (int i = 0; i < len; i++) {
			attribute = new Attribute();
			attribute.setType(ois.readInt());
			attribute.setValue(ois.readInt());
			attributesList.add(attribute);
		}
	}

	/**
	 * 获取当前耐久度
	 * @return currentDurability 当前耐久度
	 */
	public int getCurrentDurability() {
		return currentDurability;
	}

	/**
	 * 设置当前耐久度
	 * @param currentDurability 当前耐久度
	 */
	public void setCurrentDurability(int currentDurability) {
		this.currentDurability = currentDurability;
	}

	/**
	 * 获取需要等级
	 * @return useLevel 需要等级
	 */
	public int getUseLevel() {
		return useLevel;
	}

	/**
	 * 设置需要等级
	 * @param useLevel 需要等级
	 */
	public void setUseLevel(int useLevel) {
		this.useLevel = useLevel;
	}

	/**
	 * 获取耐久度
	 * @return durability 耐久度
	 */
	public int getDurability() {
		return durability;
	}

	/**
	 * 设置耐久度
	 * @param durability 耐久度
	 */
	@XMLParse("durability")
	public void setDurability(int durability) {
		this.durability = durability;
		currentDurability = durability;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Equips [hurt=" + hurt + ", defense=" + defense + ", blood=" + blood + ", magic=" + magic + ", speed="
				+ speed + ", dodge=" + dodge + ", useLevel=" + useLevel + ", durability=" + durability
				+ ", currentDurability=" + currentDurability + ", reformCount=" + reformCount + ", attributesList="
				+ attributesList + ", count=" + count + ", index=" + index + ", level=" + level + ", type=" + type
				+ ", name=" + name + ", timeline=" + timeline + ", goldPrice=" + goldPrice + ", maxCount=" + maxCount
				+ ", isTrade=" + isTrade + ", isLock=" + isLock + ", buysType=" + buysType + ", id=" + id + ", uid="
				+ uid + "]";
	}

	/**
	 * 获取伤害
	 * @return hurt 伤害
	 */
	public int getHurt() {
		return hurt;
	}

	/**
	 * 设置伤害
	 * @param hurt 伤害
	 */
	public void setHurt(int hurt) {
		this.hurt = hurt;
	}

	/**
	 * 获取防御
	 * @return defense 防御
	 */
	public int getDefense() {
		return defense;
	}

	/**
	 * 设置防御
	 * @param defense 防御
	 */
	public void setDefense(int defense) {
		this.defense = defense;
	}

	/**
	 * 获取气血
	 * @return blood 气血
	 */
	public int getBlood() {
		return blood;
	}

	/**
	 * 设置气血
	 * @param blood 气血
	 */
	public void setBlood(int blood) {
		this.blood = blood;
	}

	/**
	 * 获取法力
	 * @return magic 法力
	 */
	public int getMagic() {
		return magic;
	}

	/**
	 * 设置法力
	 * @param magic 法力
	 */
	public void setMagic(int magic) {
		this.magic = magic;
	}

	/**
	 * 获取速度
	 * @return speed 速度
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * 设置速度
	 * @param speed 速度
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	/**
	 * 获取闪避
	 * @return dodge 闪避
	 */
	public int getDodge() {
		return dodge;
	}

	/**
	 * 设置闪避
	 * @param dodge 闪避
	 */
	public void setDodge(int dodge) {
		this.dodge = dodge;
	}

	/**
	 * 获取此装备附带的属性
	 * @return attributesList 此装备附带的属性
	 */
	public List<Attribute> getAttributesList() {
		return attributesList;
	}

	/**
	 * 获取改造次数
	 * @return reformCount 改造次数
	 */
	public int getReformCount() {
		return reformCount;
	}

	/**
	 * 设置改造次数
	 * @param reformCount 改造次数
	 */
	public void setReformCount(int reformCount) {
		this.reformCount = reformCount;
	}

	/** 获取总伤害 */
	public int getTotalHurt() {
		int tempValue = hurt;
		if (type == 1) {
			tempValue += this.reformCount*(useLevel*3)+getAttTypeValue(5);
		}
		return tempValue;
	}

	/**
	 * 从属性中获取一个类型值
	 * @param type 1 体力 2灵力 3力量 4速度 5 伤害
	 * @return
	 */
	public int getAttTypeValue(int type) {
		int value = 0;
		for (int i = 0; i < attributesList.size(); i++) {
			if (attributesList.get(i).getType() == type) {
				value = attributesList.get(i).getValue();
			}
		}
		return value;
	}

	/** 获取总防御 */
	public int getTotalDefense() {
		int tempValue = defense;
		if (type == 5 || type == 6 || type == 7) {
			tempValue += this.reformCount*(useLevel*3);
		}
		return tempValue;
	}

	public int getTotalBlood() {
		return blood;
	}

	public int getTotalMagic() {
		return magic;
	}

	public int getTotalSpeed() {
		return speed;
	}

	/**
	 * 写入所有的附加属性数据
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void writeAttributeData(GameOutput gameOutput) throws IOException {
		gameOutput.writeInt(attributesList.size());
		for (int i = 0; i < attributesList.size(); i++) {
			attributesList.get(i).writeData(gameOutput);
		}
	}

}
