package com.entity;

import java.util.ArrayList;
import java.util.List;

import com.boge.util.ResourceCell;
import com.configs.RoleConfig;
import com.utils.RandomUtils;

@SuppressWarnings("serial")
public class Monster extends Entity {

	/** 怪物数据 */
	private Role role;
	/** 怪物类型  0妖王1巡游使 2天君 */
	private int type;
	/** 是否是在战斗中 */
	private boolean isFight;
	/** 怪物类型x位置 */
	private int x;
	/** 怪物类型y位置 */
	private int y;
	/** 缓存战场上当前所在的格子id */
	private int gridId = 0;
	
	private static String[] lichKingStr = {"妖王", "巡游使", "天君"};
	/** 妖王 */
	public static int[] lichKings = {4001, 4004, 4007};
	/** 巡游使 */
	public static int[] cruisings = {4002, 4003, 4005};
	/** 天君 */
	public static int[] tianjuns = {4006, 4008, 4009};
	
	/**
	 * 获取怪物数据
	 * @return role 怪物数据
	 */
	public Role getRole() {
		return role;
	}
	/**
	 * 设置怪物数据
	 * @param role 怪物数据
	 */
	public void setRole(Role role) {
		this.role = role;
	}
	/**
	 * 获取怪物类型x位置
	 * @return x 怪物类型x位置
	 */
	public int getX() {
		return x;
	}
	/**
	 * 设置怪物类型x位置
	 * @param x 怪物类型x位置
	 */
	public void setX(int x) {
		this.x = x;
	}
	/**
	 * 获取怪物类型y位置
	 * @return y 怪物类型y位置
	 */
	public int getY() {
		return y;
	}
	/**
	 * 设置怪物类型y位置
	 * @param y 怪物类型y位置
	 */
	public void setY(int y) {
		this.y = y;
	}
	/**
	 * 获取怪物类型0妖王1巡游使2天君
	 * @return type 怪物类型0妖王1巡游使2天君
	 */
	public int getType() {
		return type;
	}
	/**
	 * 设置怪物类型0妖王1巡游使2天君
	 * @param type 怪物类型0妖王1巡游使2天君
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * 获取是否是在战斗中
	 * @return isFight 是否是在战斗中
	 */
	public boolean isFight() {
		return isFight;
	}
	/**
	 * 设置是否是在战斗中
	 * @param isFight 是否是在战斗中
	 */
	public void setFight(boolean isFight) {
		this.isFight = isFight;
	}
	
	/**
	 * 创建一波怪物
	 * @param userLen 玩家数量
	 * @param userInfo 
	 * @return
	 */
	public List<FightData> createWaveMonster(int userLen, UserInfo userInfo) {
		List<FightData> lists = new ArrayList<>();
		int len;
		if (type == 1) {
			len = userLen*2;
		} else if (type == 2) {
			len = 10;
		} else {
			len = userLen*2;
		}
		UserInfo monster;
		FightData fightData;
//		System.out.println("玩家数量="+userLen+"， 生成怪物数量="+len);
		RoleConfig roleConfig = ResourceCell.getResource(RoleConfig.class);
		Role role;
		for (int i = 0; i < 10; i++) {
			if (len > i) {
				fightData = new FightData();
				fightData.setBrainAI(true);
				monster = new UserInfo();
				role = getRoleConfig(roleConfig, type);
				role.setLevel(userInfo.getRole().getLevel());
				monster.setRoleName(role.getName());
				role.changeValue();
				role.randomFenpei();
				role.castAtt();
				role.setBlood(role.getMaxBlood());
				role.setMagic(role.getMaxMagic());
				
				monster.setRole(role);
				fightData.setCanCatch(true);
				fightData.setUserInfo(monster);
				lists.add(fightData);
			} else {
				lists.add(null);
			}
		}
		return lists;
	}
	
	/**
	 * 获取角色形象
	 * @param roleConfig
	 * @param type
	 * @return
	 */
	public static Role getRoleConfig(RoleConfig roleConfig, int type) {
		Role role = null;
		if (type == 1) {
			role = roleConfig.getSample(Monster.cruisings[RandomUtils.nextInt(Monster.cruisings.length)]);
		} else if (type == 2) {
			role = roleConfig.getSample(Monster.tianjuns[RandomUtils.nextInt(Monster.tianjuns.length)]);
		} else {
			role = roleConfig.getSample(Monster.lichKings[RandomUtils.nextInt(Monster.lichKings.length)]);
		}
		role.setName(lichKingStr[type]);
		return role;
	}
	public void getFightData(boolean b) {
		
	}
	/**
	 * 获取缓存战场上当前所在的格子id
	 * @return gridId 缓存战场上当前所在的格子id
	 */
	public int getGridId() {
		return gridId;
	}
	/**
	 * 设置缓存战场上当前所在的格子id
	 * @param gridId 缓存战场上当前所在的格子id
	 */
	public void setGridId(int gridId) {
		this.gridId = gridId;
	}
	
}
