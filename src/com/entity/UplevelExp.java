package com.entity;

import com.annotation.XMLParse;

/**
 * 人物宠物升级经验
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
@XMLParse("UplevelExp")
public class UplevelExp extends Entity {
	
	/** 人物升级经验 */
	private int exp;
	/** 宠物升级经验 */
	private int bbExp;

	public UplevelExp() {
	}

	public UplevelExp(long id) {
		super(id);
	}

	/**
	 * 获取人物升级经验
	 * @return exp 人物升级经验
	 */
	public int getExp() {
		return exp;
	}

	/**
	 * 设置人物升级经验
	 * @param exp 人物升级经验
	 */
	public void setExp(int exp) {
		this.exp = exp;
	}

	/**
	 * 获取宠物升级经验
	 * @return bbExp 宠物升级经验
	 */
	public int getBbExp() {
		return bbExp;
	}

	/**
	 * 设置宠物升级经验
	 * @param bbExp 宠物升级经验
	 */
	public void setBbExp(int bbExp) {
		this.bbExp = bbExp;
	}
	
	@Override
	public String toString() {
		return "UplevelExp [exp=" + exp + ", bbExp="
				+ bbExp + ", id=" + id + ", uid=" + uid + "]";
	}

}
