package com.entity;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Element;

import com.boge.entity.GameOutput;
import com.net.CommonCmd;
import com.utils.StringUtils;

/**
 * 任务剧情
 * @author xujinbo
 */
@SuppressWarnings("serial")
public class MissionTask extends Task {

	/** 0承接类  1交付类 */
	private int type;
	/** 关连的npc */
	private int npc;
	/** 是否已经完成 */
	private boolean complete;
	/** 条件 */
	private List<MissionBlock> need;
	/** 奖励 */
	private List<MissionBlock> give;
	
	public MissionTask() {
		need = new ArrayList<>();
		give = new ArrayList<>();
	}
	
	@Override
	public void writeData(GameOutput gameOutput) throws IOException {
		super.writeData(gameOutput);
		gameOutput.writeLong(id);
		gameOutput.writeBoolean(complete);
	}
	
	@SuppressWarnings("unchecked")
	public void parse(List<Element> list) {
		if (list == null || list.isEmpty()) return;
		PropertyDescriptor[] propertys;
		try {
			propertys = Introspector.getBeanInfo(getClass()).getPropertyDescriptors();
			PropertyDescriptor propertyDescriptor;
			MissionBlock missionBlock;
			List<Attribute> attList;
			Field[] fields;
			for (Element element : list) {
				propertyDescriptor = getProType(propertys, element.getName());
				if(propertyDescriptor != null && element.getTextTrim() != "") {
					if (propertyDescriptor.getPropertyType().getName().equals("java.util.List")) {
						attList = element.attributes();
						if (element.getName().trim().equals("need")) {
							missionBlock = new MissionBlock();
							fields = missionBlock.getClass().getDeclaredFields();
							for (int i = 0; i < attList.size(); i++) {
								voluationClss(fields, missionBlock, attList.get(i));
							}
							this.need.add(missionBlock);
						} else if(element.getName().trim().equals("give")){
							missionBlock = new MissionBlock();
							fields = missionBlock.getClass().getDeclaredFields();
							for (int i = 0; i < attList.size(); i++) {
								voluationClss(fields, missionBlock, attList.get(i));
							}
							this.give.add(missionBlock);
						}
					} else {
						Method write = propertyDescriptor.getWriteMethod();
						write.setAccessible(true);
						write.invoke(this, StringUtils.changeValueType(propertyDescriptor.getPropertyType(), element.getTextTrim()));
					}
				}
//				System.out.println(element.getName());
			}
//			System.out.println("Mission |"+this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private <T> void voluationClss(Field[] fields, T t, Attribute attribute) throws Exception {
		if (attribute.getValue().equals("")) return;
		Field field;
		Object obj;
		for (int i = 0; i < fields.length; i++) {
			field = fields[i];
			if (field.getName().equals(attribute.getName())) {
				field.setAccessible(true);
				obj = StringUtils.changeValueType(field.getType(), attribute.getValue());
				field.set(t, obj);
			} 
		}
	}

	private PropertyDescriptor getProType(PropertyDescriptor[] propertys, String name) {
		for (PropertyDescriptor propertyDescriptor : propertys) {
			if (propertyDescriptor.getName().equals(name)) {
				return propertyDescriptor;
			}
		}
		return null;
	}

	
	@Override
	public String toString() {
		return "Mission [type=" + type + ", npc=" + npc + ", need=" + need + ", give=" + give + ", id=" + id + ", uid="
				+ uid + "]";
	}

	/**
	 * 获取关连的npc
	 * @return npc 关连的npc
	 */
	public int getNpc() {
		return npc;
	}

	/**
	 * 设置关连的npc
	 * @param npc 关连的npc
	 */
	public void setNpc(int npc) {
		this.npc = npc;
	}

	/**
	 * 获取条件
	 * @return need 条件
	 */
	public List<MissionBlock> getNeed() {
		return need;
	}

	/**
	 * 设置条件
	 * @param need 条件
	 */
	public void setNeed(List<MissionBlock> need) {
		this.need = need;
	}

	/**
	 * 获取奖励
	 * @return give 奖励
	 */
	public List<MissionBlock> getGive() {
		return give;
	}

	/**
	 * 设置奖励
	 * @param give 奖励
	 */
	public void setGive(List<MissionBlock> give) {
		this.give = give;
	}

	/**
	 * 获取是否已经完成
	 * @return complete 是否已经完成
	 */
	public boolean isComplete() {
		return complete;
	}

	/**
	 * 设置是否已经完成
	 * @param complete 是否已经完成
	 */
	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	/** 检查此角色是否满足剧情任务条件 
	 * @return */
	public boolean checkNeed(UserInfo userInfo) {
		if(this.type == CommonCmd.GIVE) return true;
		if (!complete) {
			MissionBlock missionBlock;
			for (int i = 0; i < need.size(); i++) {
				missionBlock = need.get(i);
				switch (missionBlock.getType()) {
					case CommonCmd.N_ITEM_NEED:
					case CommonCmd.N_ITEM_TACKED:
						complete = false;
						break;
					case CommonCmd.N_MONEY:
					case CommonCmd.N_MONEY_KEEP:
						complete = false;
						break;
					case CommonCmd.N_MARK:
						complete = false;
						break;
					case CommonCmd.N_MONSTER_KILLED:
						complete = false;
						break;
					case CommonCmd.N_PLAYER_PROP:
						complete = false;
						break;
					case CommonCmd.N_MISSION:
						complete = userInfo.getMissionLibrary().hasMission(missionBlock.getValue());
						break;
					case CommonCmd.N_TALK_NPC:
						complete = true;
						break;
					default:
//						System.out.println(missionBlock.isComplete());
						complete = missionBlock.isComplete();
						break;
				}
				if (!complete) {// 又一个条件不满足  就不通过
					break;
				}
			}
		}
		return complete;
	}

	/** 添加一个交谈对话NPC */
	public void addTalkedNpcId(long npcId) {
		for (int i = 0; i < need.size(); i++) {
			// 判断是否需要和此npc对话 
			if (need.get(i).getType() == CommonCmd.N_TALK_NPC && need.get(i).getValue() == npcId) {
				need.get(i).setComplete(true);
			}
		}
	}

	/**
	 * 获取0承接类1交付类
	 * @return type 0承接类1交付类
	 */
	public int getType() {
		return type;
	}

	/**
	 * 设置0承接类1交付类
	 * @param type 0承接类1交付类
	 */
	public void setType(int type) {
		this.type = type;
	}

	/** 完成押镖地图任务 */
	public void completeEsocrtMap(long mapId) {
		for (int i = 0; i < need.size(); i++) {
			// 判断是否需要和此npc对话 
			if (need.get(i).getType() == 202 && need.get(i).getValue() == mapId) {
				need.get(i).setComplete(true);
			}
		}
	}

	/** 地图战斗完成检查 
	 * @param userInfo */
	public void completeGrind(long mapId, UserInfo userInfo) {
		for (int i = 0; i < need.size(); i++) {
			// 判断是否拥有宠物
			if (need.get(i).getType() == 204) {
				need.get(i).setComplete(userInfo.getPetLibrarys().getPetSize()>0);
			}
		}
	}

	/** 满足玩家升级 
	 * @param userInfo */
	public void completeLevel(UserInfo userInfo) {
		for (int i = 0; i < need.size(); i++) {
			// 判断是否拥有宠物
			if (need.get(i).getType() == 205) {
				need.get(i).setComplete(userInfo.getRole().getLevel()>=need.get(i).getValue());
			}
		}
	}

}


/**
 * 条件
 * @author xujinbo
 *
 */
class MissionBlock {
	
	/** 类型 */
	private int type;
	/** 值 */ 
	private int value;
	/** 数量 */ 
	private int num;
	/** 是否已经满足 */
	private boolean complete;
	
	public MissionBlock() {
	}

	/**
	 * 获取类型
	 * @return type 类型
	 */
	public int getType() {
		return type;
	}

	/**
	 * 设置类型
	 * @param type 类型
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * 获取值
	 * @return value 值
	 */
	public int getValue() {
		return value;
	}

	/**
	 * 设置值
	 * @param value 值
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * 获取数量
	 * @return num 数量
	 */
	public int getNum() {
		return num;
	}

	/**
	 * 设置数量
	 * @param num 数量
	 */
	public void setNum(int num) {
		this.num = num;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MissionBlock [type=" + type + ", value=" + value + ", num=" + num + ", complete=" + complete + "]";
	}

	/**
	 * 获取是否已经满足
	 * @return complete 是否已经满足
	 */
	public boolean isComplete() {
		return complete;
	}

	/**
	 * 设置是否已经满足
	 * @param complete 是否已经满足
	 */
	public void setComplete(boolean complete) {
		this.complete = complete;
	}
	
}