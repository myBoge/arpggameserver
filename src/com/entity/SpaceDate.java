package com.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 间隔时间
 * @author xujinbo
 *
 */
public class SpaceDate {

	/** 整理背包 */
	public static final int BACKPACK_ARRANGE = 1;
	/** 快速邀请 */
	public static final int FAST_INVITE = 2;
	/** 仓库整理 */
	public static final int WAREHOUSE_ARRANGE = 3;
	/** 发送世界消息 */
	public static final int SEND_ALL_CHAT = 4;
	/** 记录     类型  间隔时间   上次处理时间 */
	private List<Integer> spaceList = new ArrayList<>();
	
	public SpaceDate() {
		addTypeSpace(BACKPACK_ARRANGE, 10);// 间隔时间
		addTypeSpace(FAST_INVITE, 10);// 间隔时间
		addTypeSpace(WAREHOUSE_ARRANGE, 10);// 间隔时间
		addTypeSpace(SEND_ALL_CHAT, 3);// 间隔时间
	}
	
	/**
	 * 初始化设置类型和间隔时间
	 * @param type 类型
	 * @param space 处理间隔时间（秒）
	 */
	public void addTypeSpace(int type, int space) {
		spaceList.add(type);
		spaceList.add(space);
		spaceList.add(0);
	}
	
	/**
	 * 更新新的间隔时间
	 * @param type
	 * @param space
	 */
	public void updateSpace(int type, int space) {
		int len = spaceList.size();
		for (int i = 0; i < len; i+=3) {
			if(spaceList.get(i) == type) {
				spaceList.set(i+1, space);
				break;
			}
		}
	}
	
	/**
	 * 设置最新的使用时间
	 * @param type 类型
	 * @param value 秒
	 */
	public void setValue(int type, int value) {
		int len = spaceList.size();
		for (int i = 0; i < len; i+=3) {
			if(spaceList.get(i) == type) {
				spaceList.set(i+2, value);
				break;
			}
		}
	}
	
	/**
	 * 检查此类型  是否已经满足再次使用
	 * @param type
	 * @return
	 */
	public boolean checkType(int type) {
		int len = spaceList.size();
		for (int i = 0; i < len; i+=3) {
			if(spaceList.get(i) == type) {
				long space = System.currentTimeMillis()/1000 - spaceList.get(i+2);//计算已经过了的时间
				if (space >= spaceList.get(i+1)) {
					return true;
				}
				break;
			}
		}
		return false;
	}
	
}
