package com.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.net.SocketCmd;
import com.server.PetServer;

/**
 * 战斗对象
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
public class FightData extends Entity {
	
	/** 此战斗对象是否是电脑控制战斗 */
	private boolean isBrainAI;
	/** 角色数据 */
	private UserInfo userInfo;
	/** 是否可以被捕捉 */
	private boolean isCanCatch;
	/** 是否已经被捕捉 */
	private boolean isQuiltCatch;
	/** 是否刚被召唤出来 */
	private boolean isCall;
	/** 战斗的时候使用的血 */
	private int blood;
	/** 战斗的时候使用的蓝 */
	private int magic;
	/** 战斗中使用过的宠物 */
	private List<Role> usePet;
	/** 战斗中捕捉到的宠物 */
	private List<Role> catchRole;
	
	public FightData() {
		super();
		usePet = new ArrayList<>();
		catchRole = new ArrayList<>();
	}

	public FightData(long id) {
		super(id);
	}

	@Deprecated
	@Override
	public long getUid() {
		return super.getUid();
	}
	
	@Deprecated
	@Override
	public void setUid(long uid) {
		super.setUid(uid);
	}
	/**
	 * 获取角色数据
	 * @return userInfo 角色数据
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	/**
	 * 设置角色数据
	 * @param userInfo 角色数据
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	/**
	 * 获取战斗的时候使用的血
	 * @return blood 战斗的时候使用的血
	 */
	public int getBlood() {
		return blood;
	}
	/**
	 * 设置战斗的时候使用的血
	 * @param blood 战斗的时候使用的血
	 */
	public void setBlood(int blood) {
		this.blood = blood;
	}
	/**
	 * 获取战斗的时候使用的蓝
	 * @return magic 战斗的时候使用的蓝
	 */
	public int getMagic() {
		return magic;
	}
	/**
	 * 设置战斗的时候使用的蓝
	 * @param magic 战斗的时候使用的蓝
	 */
	public void setMagic(int magic) {
		this.magic = magic;
	}

	/**
	 * 获取是否可以被捕捉
	 * @return isCanCatch 是否可以被捕捉
	 */
	public boolean isCanCatch() {
		return isCanCatch;
	}

	/**
	 * 设置是否可以被捕捉
	 * @param isCanCatch 是否可以被捕捉
	 */
	public void setCanCatch(boolean isCanCatch) {
		this.isCanCatch = isCanCatch;
	}

	/**
	 * 获取是否已经被捕捉
	 * @return isQuiltCatch 是否已经被捕捉
	 */
	public boolean isQuiltCatch() {
		return isQuiltCatch;
	}

	/**
	 * 设置是否已经被捕捉
	 * @param isQuiltCatch 是否已经被捕捉
	 */
	public void setQuiltCatch(boolean isQuiltCatch) {
		this.isQuiltCatch = isQuiltCatch;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FightData [isBrainAI=" + isBrainAI + ", userInfo=" + userInfo + ", isCanCatch=" + isCanCatch
				+ ", isQuiltCatch=" + isQuiltCatch + ", isCall=" + isCall + ", blood=" + blood + ", magic=" + magic
				+ ", usePet=" + usePet + ", catchRole=" + catchRole + ", id=" + id + ", uid=" + uid + "]";
	}

	/**
	 * 将数据写入发送流
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void writeData(GameOutput gameOutput) throws IOException {
		gameOutput.writeLong(userInfo.getRole().getId());
		gameOutput.writeLong(id);
		gameOutput.writeLong(userInfo.getRole().getUid());
		gameOutput.writeLong(userInfo.getId());// 玩家的id
		gameOutput.writeUTF(userInfo.getRole().getName());// 名字
		gameOutput.writeInt(userInfo.getRole().getLevel());// 等级
		gameOutput.writeInt(blood); // 剩余血量
		gameOutput.writeInt(magic); // 剩余蓝量
		gameOutput.writeInt(userInfo.getRole().getMaxBlood()); // 最大血量
		gameOutput.writeInt(userInfo.getRole().getMaxMagic()); // 最大蓝量
		gameOutput.writeBoolean(isCanCatch);
	}

	/**
	 * 添加一个新捕抓到的宠物
	 * @param role
	 */
	public void addCatchRole(Role role) {
		catchRole.add(role);
	}

	/**
	 * 处理被捕抓的宠物
	 */
	public void dealCatch() {
		PetServer petServer = ResourceCell.getResource(PetServer.class);
		int len = catchRole.size();
		Role role;
		GameOutput gameOutput = new GameOutput();
		try {
			// 保存到数据库
			for (int i = 0; i < len; i++) {
				role = catchRole.get(i);
				role.setStrength(100);
				role.castAtt();
				userInfo.getPetLibrarys().addRole(role);
			}
			petServer.updatePet(userInfo);
			// 发送给玩家
			for (int i = 0; i < len; i++) {
				role = catchRole.get(i);
				gameOutput.reset();
				role.writeData(gameOutput);
				userInfo.sendData(SocketCmd.ADD_PET, gameOutput.toByteArray());
			}
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("处理被捕抓的宠物报错!", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("处理被捕抓的宠物关闭流报错!", e);
				}
			}
		}
	}

	/**
	 * 判断是否死亡或被捕抓
	 * @return
	 */
	public boolean isDeath() {
		if (this.blood <= 0 || this.isQuiltCatch) {
			return true;
		}
		return false;
	}

	/**
	 * 获取是否刚被召唤出来
	 * @return isCall 是否刚被召唤出来
	 */
	public boolean isCall() {
		return isCall;
	}

	/**
	 * 设置是否刚被召唤出来
	 * @param isCall 是否刚被召唤出来
	 */
	public void setCall(boolean isCall) {
		this.isCall = isCall;
	}

	/**
	 * 添加一个使用过的宠物
	 * @param role
	 */
	public void addUsePet(Role role) {
		if (!usePet.contains(role)) {
			usePet.add(role);
		}
	}
	
	/**
	 * 这个宠物  是否已经使用过
	 * @param role
	 * @return
	 */
	public boolean checkUsePet(Role role) {
		for (int i = 0; i < usePet.size(); i++) {
			if (usePet.get(i).getUid() == role.getUid()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取此战斗对象是否是电脑控制战斗
	 * @return isBrainAI 此战斗对象是否是电脑控制战斗
	 */
	public boolean isBrainAI() {
		return isBrainAI;
	}

	/**
	 * 设置此战斗对象是否是电脑控制战斗
	 * @param isBrainAI 此战斗对象是否是电脑控制战斗
	 */
	public void setBrainAI(boolean isBrainAI) {
		this.isBrainAI = isBrainAI;
	}

}
