package com.entity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.annotation.XMLParse;
import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.configs.PropConfig;

/**
 * 礼包
 * @author xujinbo
 */
@SuppressWarnings("serial")
@XMLParse("GiftProp")
public class GiftProp extends UseProp {

	/** 礼包ID */
	private long giftId;
	
	private List<Prop> giftList = new ArrayList<>();
	
	public GiftProp() {
	}
	
	@XMLParse("giftList")
	public void addProp(String propStr){
		String[] strs = propStr.split(",");
		PropConfig propConfig = ResourceCell.getResource(PropConfig.class);
		Prop prop;
		for (int i = 0; i < strs.length; i++) {
			prop = propConfig.getSample(Long.parseLong(strs[i].trim()));
			if (prop != null) {
				prop.setTrade(false);
				giftList.add(prop);
			}
		}
	}
	
	@Override
	public void writeData(GameOutput gameOutput) throws IOException {
		super.writeData(gameOutput);
		gameOutput.writeLong(giftId);
	}
	
	@Override
	public void getAttributeData(ObjectOutputStream oos) throws IOException {
		super.getAttributeData(oos);
		oos.writeLong(giftId);
	}

	@Override
	public void setAttributeData(ObjectInputStream ois) throws IOException {
		super.setAttributeData(ois);
		giftId = ois.readLong();
	}

	/**
	 * 获取礼包ID
	 * @return giftId 礼包ID
	 */
	public long getGiftId() {
		return giftId;
	}

	/**
	 * 设置礼包ID
	 * @param giftId 礼包ID
	 */
	public void setGiftId(long giftId) {
		this.giftId = giftId;
	}

	@Override
	public String toString() {
		return "GiftProp [giftId=" + giftId + ", toString()=" + super.toString() + "]";
	}

	/**
	 * 获取giftList
	 * @return giftList giftList
	 */
	public List<Prop> getGiftList() {
		return giftList;
	}
	
}
