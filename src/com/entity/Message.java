package com.entity;

public class Message {

	private Object msg;
	private int type;

	public Message() {
		super();
	}

	public Message(Object msg, int type) {
		super();
		this.msg = msg;
		this.type = type;
	}

	public Message(Object msg) {
		super();
		this.msg = msg;
	}

	public Object getMsg() {
		return msg;
	}

	public void setMsg(Object msg) {
		this.msg = msg;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Message [msg=" + msg + ", type=" + type + "]";
	}

}
