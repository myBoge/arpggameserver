package com.entity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.annotation.XMLParse;
import com.boge.entity.GameOutput;

/**
 * 物品
 * 
 * @author xujinbo
 * 
 */
@SuppressWarnings("serial")
@XMLParse("Prop")
public class Prop extends Entity {

	/** 数量 */
	protected int count = 1;
	/** 索引 */
	protected int index;
	/** 等级 */
	protected int level;
	/** 物品类型 1 武器 2 回复血 3 回复蓝 4 恢复血和蓝 5 头盔 6 衣服 7 鞋子 8 玉佩 9 项链 10手镯  11道具 12礼包  技能类型   1伤害类 2增益类 3障碍类*/
	protected int type;
	/** 名字 */
	protected String name;
	/** 时限 分钟 */
	protected long timeline;
	/** 商城售价 */
	protected long goldPrice;
	/** 最大堆叠数 */
	protected int maxCount;
	/** 是否可交易 */
	protected boolean isTrade = true;
	/** 是否被加锁  锁定后无法交易卖出 */
	protected boolean isLock;
	/** 购买方式 1 金币商店2 宝盒3 商城 */
	protected int buysType;
	/** 是否可以用绑定货币购买*/
	protected boolean isLockBuy = true;

	public Prop() {
		super();
	}

	public Prop(long id) {
		super(id);
	}

	@Override
	public String toString() {
		return "Prop [count=" + count + ", index=" + index + ", level=" + level
				+ ", type=" + type + ", name=" + name
				+ ", timeline=" + timeline + ", goldPrice="
				+ goldPrice + ", maxCount=" + maxCount + ", isTrade=" + isTrade
				+ ", isLock=" + isLock + ", buysType="
				+ buysType + ", id=" + id + ", uid=" + uid + "]";
	}

	/**
	 * 获取数量
	 * 
	 * @return count 数量
	 */
	public synchronized int getCount() {
		return count;
	}

	/**
	 * 设置数量
	 * 
	 * @param count
	 *            数量
	 */
	public synchronized void setCount(int count) {
		if (this.maxCount < count) {
			this.count = maxCount;
		} else {
			this.count = count;
		}
	}

	/**
	 * 获取索引
	 * 
	 * @return index 索引
	 */
	public synchronized int getIndex() {
		return index;
	}

	/**
	 * 设置索引
	 * 
	 * @param index
	 *            索引
	 */
	public synchronized void setIndex(int index) {
		this.index = index;
	}

	/**
	 * 获取等级
	 * 
	 * @return level 等级
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * 设置等级
	 * 
	 * @param level
	 *            等级
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * 获取名字
	 * 
	 * @return name 名字
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名字
	 * 
	 * @param name
	 *            名字
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取时限
	 * 
	 * @return timeline 时限
	 */
	public long getTimeline() {
		return timeline;
	}

	/**
	 * 设置时限
	 * 
	 * @param timeline
	 *            时限
	 */
	public void setTimeline(long timeline) {
		this.timeline = timeline;
	}

	/**
	 * 获取商城售价
	 * 
	 * @return goldPrice 商城售价
	 */
	public long getGoldPrice() {
		return goldPrice;
	}

	/**
	 * 设置商城售价
	 * 
	 * @param goldPrice
	 *            商城售价
	 */
	public void setGoldPrice(long goldPrice) {
		this.goldPrice = goldPrice;
	}

	/**
	 * 获取最大堆叠数
	 * 
	 * @return maxCount 最大堆叠数
	 */
	public int getMaxCount() {
		return maxCount;
	}

	/**
	 * 设置最大堆叠数
	 * 
	 * @param maxCount
	 *            最大堆叠数
	 */
	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}

	/**
	 * 获取是否可交易
	 * 
	 * @return isTrade 是否可交易
	 */
	public boolean isTrade() {
		return isTrade;
	}

	/**
	 * 设置是否可交易
	 * 
	 * @param isTrade
	 *            是否可交易
	 */
	public void setTrade(boolean isTrade) {
		this.isTrade = isTrade;
	}

	/**
	 * 获取购买方式1金币商店2宝盒3商城
	 * 
	 * @return buysType 购买方式1金币商店2宝盒3商城
	 */
	public int getBuysType() {
		return buysType;
	}

	/**
	 * 设置购买方式1金币商店2宝盒3商城
	 * 
	 * @param buysType
	 *            购买方式1金币商店2宝盒3商城
	 */
	public void setBuysType(int buysType) {
		this.buysType = buysType;
	}

	/**
	 * 获取是否被加锁锁定后无法交易卖出
	 * @return isLock 是否被加锁锁定后无法交易卖出
	 */
	public boolean isLock() {
		return isLock;
	}

	/**
	 * 设置是否被加锁锁定后无法交易卖出
	 * @param isLock 是否被加锁锁定后无法交易卖出
	 */
	public void setLock(boolean isLock) {
		this.isLock = isLock;
	}
	
	/**
	 * 获取物品类型1武器2回复血3回复蓝4恢复血和蓝5头盔6衣服7鞋子8玉佩9项链10手镯11道具 12礼包 技能类型1伤害类2增益类3障碍类
	 * @return type 物品类型1武器2回复血3回复蓝4恢复血和蓝5头盔6衣服7鞋子8玉佩9项链10手镯11道具 12礼包 技能类型1伤害类2增益类3障碍类
	 */
	public int getType() {
		return type;
	}

	/**
	 * 设置物品类型1武器2回复血3回复蓝4恢复血和蓝5头盔6衣服7鞋子8玉佩9项链10手镯11道具 12礼包 技能类型1伤害类2增益类3障碍类
	 * @param type 物品类型1武器2回复血3回复蓝4恢复血和蓝5头盔6衣服7鞋子8玉佩9项链10手镯11道具 12礼包 技能类型1伤害类2增益类3障碍类
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * 获取是否可以用绑定货币购买
	 * @return isLockBuy 是否可以用绑定货币购买
	 */
	public boolean isLockBuy() {
		return isLockBuy;
	}

	/**
	 * 设置是否可以用绑定货币购买
	 * @param isLockBuy 是否可以用绑定货币购买
	 */
	public void setLockBuy(boolean isLockBuy) {
		this.isLockBuy = isLockBuy;
	}
	
	public void writeData(GameOutput gameOutput) throws IOException {
		gameOutput.writeLong(id)
			.writeLong(uid)
			.writeInt(index)
			.writeInt(count)
			.writeLong(timeline)
			.writeInt(level)
			.writeBoolean(isLock)
			.writeBoolean(isTrade);
	}

	public void getAttributeData(ObjectOutputStream oos) throws IOException {
		oos.writeInt(count);
		oos.writeInt(index);
		oos.writeInt(level);
		oos.writeLong(timeline);
		oos.writeBoolean(isTrade);
		oos.writeBoolean(isLock);
	}

	public void setAttributeData(ObjectInputStream ois) throws IOException {
		count = ois.readInt();
		index = ois.readInt();
		level = ois.readInt();
		timeline = ois.readLong();
		isTrade = ois.readBoolean();
		isLock = ois.readBoolean();
	}
	
}
