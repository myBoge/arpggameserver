package com.entity;

/**
 * 练级队伍
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
public class GrindTeam extends Entity {

	/** 队伍信息 */
	private Team team;
	/** 练级地图id */
	private long mapId = 0;
	/** 在这个地图押镖开始时间 */
	private long startDate = 0;
	/** 下次遇到怪物时间 毫秒 */
	private long nextTime = 0;
	
	public GrindTeam() {
	}

	/**
	 * 获取在这个地图押镖开始时间
	 * @return startDate 在这个地图押镖开始时间
	 */
	public long getStartDate() {
		return startDate;
	}

	/**
	 * 设置在这个地图押镖开始时间
	 * @param startDate 在这个地图押镖开始时间
	 */
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	/**
	 * 获取练级地图id
	 * @return mapId 练级地图id
	 */
	public long getMapId() {
		return mapId;
	}

	/**
	 * 设置练级地图id
	 * @param mapId 练级地图id
	 */
	public void setMapId(long mapId) {
		this.mapId = mapId;
	}

	/**
	 * 获取下次遇到怪物时间毫秒
	 * @return nextTime 下次遇到怪物时间毫秒
	 */
	public long getNextTime() {
		return nextTime;
	}

	/**
	 * 设置下次遇到怪物时间毫秒
	 * @param nextTime 下次遇到怪物时间毫秒
	 */
	public void setNextTime(long nextTime) {
		this.nextTime = nextTime;
	}
	
	/**
	 * 获取队伍信息
	 * @return team 队伍信息
	 */
	public Team getTeam() {
		return team;
	}

	/**
	 * 设置队伍信息
	 * @param team 队伍信息
	 */
	public void setTeam(Team team) {
		this.team = team;
	}

	/**
	 * 检查此队伍信息是否合法  合法继续挂机 
	 * @return
	 */
	public boolean checkValid() {
		return team.checkValid();
	}

	/**
	 * 获取是否发生战斗
	 * @return isFight 是否发生战斗
	 */
	public boolean isFight() {
		return team.isFight();
	}
	
	/**
	 * 设置是否发生战斗
	 * @param fight 是否发生战斗
	 */
	public void setFight(boolean fight) {
		team.setFight(fight);
	}

//	/**
//	 * 更新状态
//	 * @param state
//	 * @param userServer 
//	 */
//	public void updateState(int state, UserServer userServer) {
//		List<UserInfo> usetList = team.getUserList();
//		int len = usetList.size();
//		UserInfo userInfo;
//		for (int i = 0; i < len; i++) {
//			userInfo = usetList.get(i);
//			if (userInfo.getRole().getState() != state) {
//				userInfo.getRole().setState(state);
//				// 保存一次玩家数据
//				userServer.updateUserRole(userInfo);
//				// 向客户端发送用户状态变更
//				userInfo.sendData(SocketCmd.UPDATE_USER_STATE, userInfo.getRole().getState());
//			}
//		}
//	}

}
