package com.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.boge.entity.GameOutput;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

/**
 * 邮件
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
public class Mail extends Entity implements IAttributDataStream {

	/** 0系统 1私人 2帮派  */
	private int type;
	/** 是否阅读过 */
	private boolean isRead;
	/** 发件人 */
	private String sender;
	/** 主题 */
	private String theme;
	/** 内容 */
	private String content;
	/** 发送时间 */
	private long sendData;
	/** 附件是否领取 */
	private boolean isReceive;
	/** 附近内容   模版id,数量 */
	private List<Object> lists;
	
	public Mail() {
		super();
		lists = new ArrayList<>();
	}
	
	public Mail(long id) {
		super(id);
	}

	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		type = ois.readInt();
		isRead = ois.readBoolean();
		sender = ois.readUTF();
		theme = ois.readUTF();
		content = ois.readUTF();
		isReceive = ois.readBoolean();
		int len = ois.readInt();
		lists.clear();
		for (int i = 0; i < len; i+=3) {
			lists.add(ois.readLong());
			lists.add(ois.readInt());
			lists.add(ois.readBoolean());
		}
		sendData = ois.readLong();
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		oos.writeInt(type);
		oos.writeBoolean(isRead);
		oos.writeUTF(sender);
		oos.writeUTF(theme);
		oos.writeUTF(content);
		oos.writeBoolean(isReceive);
		int len = lists.size();
		oos.writeInt(len);
		for (int i = 0; i < len; i+=3) {
			oos.writeLong(Long.valueOf(lists.get(i).toString()));
			oos.writeInt((int) lists.get(i+1));
			oos.writeBoolean((boolean) lists.get(i+2));
		}
		oos.writeLong(sendData);
	}

	/**
	 * 获取发件人
	 * @return sender 发件人
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * 设置发件人
	 * @param sender 发件人
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * 获取主题
	 * @return theme 主题
	 */
	public String getTheme() {
		return theme;
	}

	/**
	 * 设置主题
	 * @param theme 主题
	 */
	public void setTheme(String theme) {
		this.theme = theme;
	}

	/**
	 * 获取内容
	 * @return content 内容
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 设置内容
	 * @param content 内容
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * 获取发送时间
	 * @return sendData 发送时间
	 */
	public long getSendData() {
		return sendData;
	}

	/**
	 * 设置发送时间
	 * @param sendData 发送时间
	 */
	public void setSendData(long sendData) {
		this.sendData = sendData;
	}

	/**
	 * 获取附近内容模版id数量
	 * @return lists 附近内容模版id数量
	 */
	public List<Object> getLists() {
		return lists;
	}

	/**
	 * 设置附近内容模版id数量
	 * @param lists 附近内容模版id数量
	 */
	public void setLists(List<Object> lists) {
		this.lists = lists;
	}

	@Override
	public String toString() {
		return "Mail [type=" + type + ", isRead=" + isRead + ", sender="
				+ sender + ", theme=" + theme + ", content=" + content
				+ ", sendData=" + sendData + ", isReceive=" + isReceive
				+ ", lists=" + lists + ", id=" + id + ", uid=" + uid + "]";
	}

	public void writeData(GameOutput gameOutput) throws IOException {
		gameOutput.writeLong(id);
		gameOutput.writeInt(type);
		gameOutput.writeBoolean(isRead);
		gameOutput.writeUTF(sender);
		gameOutput.writeUTF(theme);
		gameOutput.writeUTF(content);
		gameOutput.writeBoolean(isReceive);
		int len = lists.size();
		gameOutput.writeInt(len);
		for (int i = 0; i < len; i+=3) {
			gameOutput.writeLong(Long.valueOf(lists.get(i).toString()));
			gameOutput.writeInt((int) lists.get(i+1));
			gameOutput.writeBoolean((boolean) lists.get(i+2));
		}
		gameOutput.writeLong(sendData);
	}

	/**
	 * 获取0系统1私人2帮派
	 * @return type 0系统1私人2帮派
	 */
	public int getType() {
		return type;
	}

	/**
	 * 设置0系统1私人2帮派
	 * @param type 0系统1私人2帮派
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * 获取是否阅读过
	 * @return isRead 是否阅读过
	 */
	public boolean isRead() {
		return isRead;
	}

	/**
	 * 设置是否阅读过
	 * @param isRead 是否阅读过
	 */
	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	/**
	 * 获取附件是否领取
	 * @return isReceive 附件是否领取
	 */
	public boolean isReceive() {
		return isReceive;
	}

	/**
	 * 设置附件是否领取
	 * @param isReceive 附件是否领取
	 */
	public void setReceive(boolean isReceive) {
		this.isReceive = isReceive;
	}
	
}
