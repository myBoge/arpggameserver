package com.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.boge.entity.GameOutput;
import com.boge.socket.ActionData;
import com.boge.util.ResourceCell;
import com.data.ConnectData;
import com.mapper.UserMapper;
import com.model.GameData;
import com.model.MapModel;
import com.net.CommonCmd;
import com.net.SocketCmd;
import com.server.MapServer;
import com.server.TeamServer;
import com.server.UserServer;
import com.utils.LuaUtils;

/**
 * 团队
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
public class Team extends Entity {

	/** 是否发生战斗 */
	private boolean fight;
	/** 当前团队的玩家 */
	private List<UserInfo> userList;
	
	UserServer userServer;
	
	TeamServer teamServer;
	
	public Team() {
		userList = new ArrayList<>();
		userServer = ResourceCell.getResource(UserServer.class);
		teamServer = ResourceCell.getResource(TeamServer.class);
	}

	/**
	 * 获取当前团队的玩家
	 * @return userList 当前团队的玩家
	 */
	public List<UserInfo> getUserList() {
		return userList;
	}

	/**
	 * 获取是否发生战斗
	 * @return isFight 是否发生战斗
	 */
	public boolean isFight() {
		return fight;
	}

	/**
	 * 设置是否发生战斗
	 * @param fight 是否发生战斗
	 */
	public void setFight(boolean fight) {
		this.fight = fight;
	}
	
	/**
	 * 检查是否存在此玩家
	 * @param userInfo
	 * @return
	 */
	public boolean checkExist(UserInfo userInfo) {
		int len = userList.size();
		for (int i = 0; i < len; i++) {
			if (userList.get(i).getId() == userInfo.getId()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 删除此用户
	 * @param userInfo
	 */
	public void removeUser(UserInfo userInfo) {
		int len = userList.size();
		for (int i = 0; i < len; i++) {
			if (userList.get(i).getId() == userInfo.getId()) {
				userList.remove(i);
				break;
			}
		}
	}

	/**
	 * 检查此队伍队长是否还属于合法队伍    既  没有离线的人 
	 * @return
	 */
	public boolean checkValid() {
		// 检查队长在线状态   如果离线   踢出队伍 
		UserInfo userInfo;
		for (int i = 0; i < userList.size(); i++) {
			userInfo = userList.get(i);
			if (userInfo.getOnlineState() == 0) {// 该角色是否已经离线
				userList.remove(i);
				userInfo.getRole().setState(CommonCmd.IDLE);
				userInfo.setTeamId(0);
				// 更新状态
				userServer.updateUserRole(userInfo);
				sendData(SocketCmd.TEAM_DELETE_ROLE, userInfo.getId());
				i--;
			}
		}
		if (userList.size() > 0) {
			return true;
		}
		teamServer.removeTeam(id);
		return false;
	}
	
	
	public void sendData(int action, Object ...args) {
		GameOutput gameOutput = new GameOutput();
		try {
			gameOutput.write(args);
			sendData(new ActionData<>(action, gameOutput.toByteArray()));
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("队伍发送数据报错!", e);
		} finally {
			if (gameOutput != null) {
				try {
					gameOutput.close();
				} catch (IOException e) {
					Logger.getLogger(getClass()).error("队伍发送数据关闭流报错!", e);
				}
			}
		}
	}
	
	public void sendData(int action, byte[] buf) {
		ActionData<Object> actionData = new ActionData<>(action, buf);
		sendData(actionData);
	}
	
	/**
	 * 此用户向客户端发送消息
	 * @param action 发送指令
	 */
	public synchronized void sendData(ActionData<Object> action) {
		for (int i = 0; i < userList.size(); i++) {
			userList.get(i).sendData(action);
		}
	}

	/**
	 * 写入队伍成员所有信息
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void writeData(GameOutput gameOutput) throws IOException {
		int len = userList.size();
		gameOutput.writeInt(len);
		for (int i = 0; i < len; i++) {
			writeUser(gameOutput, userList.get(i));
		}
	}
	
	/**
	 * 将指定成员信息写入
	 * @param gameOutput
	 * @param userTwo
	 * @throws IOException
	 */
	public void writeUser(GameOutput gameOutput, UserInfo userTwo) throws IOException {
		gameOutput.writeLong(userTwo.getId());
		gameOutput.writeUTF(userTwo.getRoleName());
		gameOutput.writeLong(userTwo.getRole().getId());
		gameOutput.writeInt(userTwo.getRole().getLevel());
	}

	/**
	 * 获取战斗对象
	 * @param isBrainAI 此战斗对象是否是电脑控制战斗
	 * @return
	 */
	public List<FightData> getFightData(boolean isBrainAI) {
		List<FightData> list = new ArrayList<>();
		int len = userList.size();
		FightData fightData;
		UserInfo userInfo;
		//主角数据
		for (int i = 0; i < 5; i++) {
			if (len > i) {
				userInfo = userList.get(i);
				fightData = new FightData();
				fightData.setBrainAI(isBrainAI);
				fightData.setUserInfo(userInfo);
				list.add(fightData);
			} else {
				list.add(null);
			}
		}
		//宠物数据
		for (int i = 0; i < 5; i++) {
			if (len > i) {
				userInfo = userList.get(i);
				list.add(userInfo.getPetFightData(isBrainAI));
			} else {
				list.add(null);
			}
		}
		return list;
	}

	/**
	 * 检查此角色是否可以执行命令
	 * @param info
	 * @return
	 */
	public boolean checkCmdExecute(UserInfo info){
		if (!fight && !userList.isEmpty() && userList.get(0).getId()==info.getId()) {
			// 不再战斗  成员存在  此玩家是队长
			return true;
		}
		return false;
	}
	
	/**
	 * 检查此角色是否可以执行命令  如果不可执行会反馈客户端
	 * @param info
	 * @return 
	 * @return
	 * @throws IOException 
	 */
	public boolean checkCmdExecute(UserInfo userInfo, GameOutput gameOutput) throws IOException{
		if (userList.isEmpty() || !checkExist(userInfo)) {
			gameOutput.writeInt(CommonCmd.YOU_NOT_EXISTENT_TEAM);
			userInfo.sendData(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray());
			return false;
		}
		if (fight) {
			gameOutput.writeInt(CommonCmd.YOU_AT_FIGHT);
			userInfo.sendData(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray());
			return false;
		}
		// 如果此人不是队长
		if (userList.get(0).getId() != userInfo.getId()) {
			gameOutput.writeInt(CommonCmd.TEAM_NOT_LEADER);
			userInfo.sendData(SocketCmd.PROMPT_MESSAGE, gameOutput.toByteArray());
			return false;
		}
		return true;
	}
	
	/**
	 * 通知该队伍所有成员  改变所在场景 和 地图  会自动发送和保存数据
	 * @param currentScene 用户当前所在场景
	 * @param currentSceneValue 当前场景附带值
	 */
	public void updateCurrentScene(int currentScene, long currentSceneValue) {
		updateCurrentScene(currentScene, currentSceneValue, 0, 0);
	}
	
	/**
	 * 通知该队伍所有成员  改变所在场景 和 地图  会自动发送和保存数据
	 * @param currentScene 用户当前所在场景
	 * @param currentSceneValue 当前场景附带值
	 * @param startX 
	 * @param startY 
	 */
	public void updateCurrentScene(int currentScene, long currentSceneValue, int startX, int startY) {
		SqlSession sqlSession = ConnectData.openSession();
		try {
			UserMapper userManage = sqlSession.getMapper(UserMapper.class);
			UserInfo userInfo;
			UserInfo info;
			for (int j = 0; j < userList.size(); j++) {
				userInfo = userList.get(j);
				GameData.gameData.setupCurrentScene(userInfo, currentScene, currentSceneValue, startX, startY);
				
				userInfo.setBattleMove(false);
				
				info = new UserInfo(userList.get(j).getId());
				GameData.gameData.setupCurrentScene(info, userInfo);
				userManage.update(info);
				
				userInfo.sendData(SocketCmd.USER_ENTER_SCENE, 
						userInfo.getCurrentScene(), 
						userInfo.getCurrentSceneValue(), 
						userInfo.getCurrentSceneStartX(), 
						userInfo.getCurrentSceneStartY());
				
				
				if (currentScene == CommonCmd.SCENE_GAME) {
					MapModel mapModel = ResourceCell.getResource(MapModel.class);
					MapServer mapServer = ResourceCell.getResource(MapServer.class);
					MapData mapData = mapModel.getMapData(currentSceneValue);
					if (mapData != null) {
						mapServer.sendMapAddUser(userInfo, mapData);
						if (mapData.getId() != LuaUtils.get("mainMapId").tolong()) {// 不是主界面
							mapData.addUser(userInfo);
						} else {
							userInfo.setGridId(mapData.getMapGridData().getUserGrid(userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY()));
						}
					}
				}
				
			}
			sqlSession.commit();
		} finally {
			if(sqlSession!=null)sqlSession.close();
		}
		
	}

	/**
	 * 判断此用户是否是队长
	 * @param userInfo
	 * @return 
	 */
	public boolean isLeader(UserInfo userInfo) {
		if (userInfo != null && userList.size() > 0 && userList.get(0).getId()==userInfo.getId()) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Team [fight=" + fight + ", id=" + id + ", uid=" + uid + "]";
	}

}
