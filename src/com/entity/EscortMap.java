package com.entity;

import com.annotation.XMLParse;

/**
 * 挂机地图
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
@XMLParse("EscortMap")
public class EscortMap extends GrindMap {
	
	/** 宽度 */
	private int width;
	/** 押金 */
	private int foregift;
	/** 最大押金 */
	private int maxForegift;
	/** 最低保证金(押镖总额*百分比) */
	private int minLivingGold;
	/** 遇系统打劫概率 */
	private int robRat;
	/** 押金兑换押镖金额(押金*值) */
	private int exchangeRat;
	/** 押镖完一次增加经验 */
	private int addExp;
	
	public EscortMap() {
	}
	
	public EscortMap(long id) {
		super(id);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EscortMap [width=" + width + ", foregift=" + foregift + ", maxForegift=" + maxForegift
				+ ", minLivingGold=" + minLivingGold + ", robRat=" + robRat + ", exchangeRat=" + exchangeRat
				+ ", addExp=" + addExp + ", toString()=" + super.toString() + "]";
	}

	/**
	 * 获取宽度
	 * @return width 宽度
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * 设置宽度
	 * @param width 宽度
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * 获取押金
	 * @return foregift 押金
	 */
	public int getForegift() {
		return foregift;
	}

	/**
	 * 设置押金
	 * @param foregift 押金
	 */
	public void setForegift(int foregift) {
		this.foregift = foregift;
	}

	/**
	 * 获取最大押金
	 * @return maxForegift 最大押金
	 */
	public int getMaxForegift() {
		return maxForegift;
	}

	/**
	 * 设置最大押金
	 * @param maxForegift 最大押金
	 */
	public void setMaxForegift(int maxForegift) {
		this.maxForegift = maxForegift;
	}

	/**
	 * 获取最低保证金(押镖总额百分比)
	 * @return minLivingGold 最低保证金(押镖总额百分比)
	 */
	public int getMinLivingGold() {
		return minLivingGold;
	}

	/**
	 * 设置最低保证金(押镖总额百分比)
	 * @param minLivingGold 最低保证金(押镖总额百分比)
	 */
	public void setMinLivingGold(int minLivingGold) {
		this.minLivingGold = minLivingGold;
	}

	/**
	 * 获取遇系统打劫概率
	 * @return robRat 遇系统打劫概率
	 */
	public int getRobRat() {
		return robRat;
	}

	/**
	 * 设置遇系统打劫概率
	 * @param robRat 遇系统打劫概率
	 */
	public void setRobRat(int robRat) {
		this.robRat = robRat;
	}

	/**
	 * 获取押金兑换押镖金额(押金值)
	 * @return exchangeRat 押金兑换押镖金额(押金值)
	 */
	public int getExchangeRat() {
		return exchangeRat;
	}

	/**
	 * 设置押金兑换押镖金额(押金值)
	 * @param exchangeRat 押金兑换押镖金额(押金值)
	 */
	public void setExchangeRat(int exchangeRat) {
		this.exchangeRat = exchangeRat;
	}

	/**
	 * 获取押镖一次增加经验
	 * @return addExp 押镖每秒增加经验
	 */
	public int getAddExp() {
		return addExp;
	}

	/**
	 * 设置押镖一次增加经验
	 * @param addExp 押镖每秒增加经验
	 */
	public void setAddExp(int addExp) {
		this.addExp = addExp;
	}

}
