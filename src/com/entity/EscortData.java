package com.entity;

import java.util.Arrays;


/**
 * 用于存储挂机数据
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
public class EscortData extends Entity {

	/** 押镖地图id */
	private long mapId;
	/** List<Escort>缓存的二进制文件(玩家) */
	private byte[] roles;
	
	public EscortData() {
		super();
	}
	public EscortData(long mapId, byte[] roles) {
		super();
		this.mapId = mapId;
		this.roles = roles;
	}
	public long getMapId() {
		return mapId;
	}
	public void setMapId(long mapId) {
		this.mapId = mapId;
	}
	public byte[] getRoles() {
		return roles;
	}
	public void setRoles(byte[] roles) {
		this.roles = roles;
	}
	@Override
	public String toString() {
		return "EscortData [mapId=" + mapId + ", roles="
				+ Arrays.toString(roles) + "]";
	}
}
