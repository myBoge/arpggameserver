package com.entity;

import java.io.Serializable;

import org.apache.mina.core.session.IoSession;

@SuppressWarnings("serial")
public class Login extends Entity implements Serializable {
	
	/** 用户所拥有的session */
	private IoSession session;
	
	private String openid;
	
	private int serverId;
	
	public Login() {
		super();
	}

	public Login(long id) {
		super(id);
	}

	public Login(String openid, int serverId) {
		super();
		this.openid = openid;
		this.serverId = serverId;
	}

	public Login(long id, String openid, int serverId) {
		super();
		this.id = id;
		this.openid = openid;
		this.serverId = serverId;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public int getServerId() {
		return serverId;
	}

	public void setServerId(int serverId) {
		this.serverId = serverId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Login [session=" + session + ", openid=" + openid + ", serverId=" + serverId + ", id=" + id + ", uid="
				+ uid + "]";
	}

	/**
	 * 获取用户所拥有的session
	 * @return session 用户所拥有的session
	 */
	public IoSession getSession() {
		return session;
	}

	/**
	 * 设置用户所拥有的session
	 * @param session 用户所拥有的session
	 */
	public void setSession(IoSession session) {
		this.session = session;
	}
	
}
