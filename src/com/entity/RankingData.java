package com.entity;

import java.io.IOException;

import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

/**
 * 排行榜数据
 * @author xujinbo
 *
 */
public class RankingData implements IAttributDataStream {
	
	/** 角色id */
	private long id;
	/** 角色名字 */
	private String name;
	/** 排行值 */
	private long rankValue;
	/** 排行key*/
	private String rankKey;
	
	public RankingData() {
		this(0);
	}
	public RankingData(long id) {
		super();
		this.id = id;
	}
	/**
	 * 获取角色id
	 * @return id 角色id
	 */
	public long getId() {
		return id;
	}
	/**
	 * 设置角色id
	 * @param id 角色id
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * 获取角色名字
	 * @return name 角色名字
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置角色名字
	 * @param name 角色名字
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取排行值
	 * @return rankValue 排行值
	 */
	public long getRankValue() {
		return rankValue;
	}
	/**
	 * 设置排行值
	 * @param rankValue 排行值
	 */
	public void setRankValue(long rankValue) {
		this.rankValue = rankValue;
	}
	/**
	 * 获取排行key
	 * @return rankKey 排行key
	 */
	public String getRankKey() {
		return rankKey;
	}
	/**
	 * 设置排行key
	 * @param rankKey 排行key
	 */
	public void setRankKey(String rankKey) {
		this.rankKey = rankKey;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RankingData [id=" + id + ", name=" + name + ", rankValue=" + rankValue + ", rankKey=" + rankKey + "]";
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RankingData other = (RankingData) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		id = ois.readLong();
		name = ois.readUTF();
		rankKey = ois.readUTF();
		rankValue = ois.readLong();
	}
	
	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		oos.writeLong(id);
		oos.writeUTF(name);
		oos.writeUTF(rankKey);
		oos.writeLong(rankValue);
	}
	
}
