package com.entity;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.annotation.XMLParse;
import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.configs.RoleConfig;
import com.core.SilzAstar;
import com.model.Grid;
import com.model.MapGridData;
import com.utils.ImageUtil;
import com.utils.OnlyIdManage;
import com.utils.RandomUtils;

/**
 * 地图数据
 * @author xujinbo
 */
@SuppressWarnings("serial")
@XMLParse("map")
public class MapData extends Entity {

	private int hasTile;
	private String tileFormat;
	
	/**
	 * 常量 寻路格子宽度
	 */
	public static int tileWidth = 64;
	/**
	 * 常量 寻路格子高度
	 */ 
	public static int tileHeight = 32;
	
	private int tileX;
	private int tileY;
	private int mapW;
	private int mapH;
	private String mapName;
	private int type;
	/** 起始X坐标 */
	private int startX;
	/** 起始Y坐标 */
	private int startY;
	/** 地图中可移动的全局坐标 */
	private List<Point> pointList = new ArrayList<>();
	/** 地图数组 */
	private int[][] _arry;
	
	/** 怪物id */
	private OnlyIdManage onlyIdManage;
	private RoleConfig roleConfig;
	
	private MapGridData mapGridData;
	private SilzAstar silzAstar;
	
	public MapData() {
		roleConfig = ResourceCell.getResource(RoleConfig.class);
		onlyIdManage = new OnlyIdManage();
	}
	
	/** 更新此地图可行走坐标 */
	public void updateAstar() throws Exception {
		int w = mapW/tileWidth;
		int h = mapH/tileHeight;
		mapGridData = new MapGridData(id, mapW, mapH, 500, 400);
		_arry = new int[h][w];
		int[] arr;
		for(int y = 0; y<h; y++) {
			arr = new int[w];
			for(int x = 0; x<w; x++) {
				arr[x] = 0;
			}
			_arry[y] = arr;
		}
		BufferedImage imageImage = ImageUtil.getImageByte("data/map/"+id+"/roadmap.png");
		double road = (double)imageImage.getWidth()/mapW;
		for(int y = 0; y<h; y++) {
			for(int x = 0; x<w; x++) {
				_arry[y][x] = ImageUtil.getImagePixel(imageImage, (int)(tileWidth*x*road), (int)(tileHeight*y*road))==0?1:0;
			}
		}
		int index;
		Grid grid;
		Point point;
		// 可移动的点处理
		for (int i = 0; i < _arry.length; i++) {
			for (int j = 0; j < _arry[i].length; j++) {
				if (_arry[i][j] == 0) {
					point = new Point(j*tileWidth, i*tileHeight);
					index = mapGridData.getUserGrid(point.x, point.y);
					grid = mapGridData.getGrid(index);
					grid.addMovePoint(point);
					pointList.add(point);
				}
			}
		}
		silzAstar = new SilzAstar(_arry);
	}
	
	/** 创建新怪物 
	 * @return */
	public List<Monster> createRandomMonster() {
		// 当前场景上的怪物少于200个
		int len = 0;
		if (mapGridData.getMonsterLength() < 5) {
			len = RandomUtils.nextInt(30, 70);
		} else if (mapGridData.getMonsterLength() < 200) {
			len = RandomUtils.nextInt(1, 10);
		}
		List<Monster> monsterList = new ArrayList<>();
		Monster monster;
		for (int i = 0; i < len; i++) {
			monster = createMonster();
			monsterList.add(monster);
		}
		return monsterList;
	}
	
	/** 创建一个新怪物 
	 * @return */
	public Monster createMonster() {
		int type = RandomUtils.nextInt(3);
		Monster monster = new Monster();
		monster.setId(onlyIdManage.getGrowableId());
		monster.setType(type);
		Role role = Monster.getRoleConfig(roleConfig, type);
		role.setLevel(RandomUtils.nextInt(20, 61));
		Point point = pointList.get(RandomUtils.nextInt(pointList.size()));
		monster.setRole(role);
		monster.setX(point.x);
		monster.setY(point.y);
		mapGridData.addMonster(monster.getX(), monster.getY(), monster);
//		monsterList.add(monster);
		return monster;
	}
	
	/**
	 * 当前地图的路径地图数组
	 * @param is 使用 Postion2Tile（）得到的坐标值
	 * @return 0可走  1不可走
	 */
	public int getRoadMap(int[] is) {
		if (is.length != 2) {
			return 0;
		}
		return getRoadMap(is[0], is[1]);
	}
	
	/**
	 * 当前地图的路径地图数组
	 * @param is 使用 Postion2Tile（）得到的坐标值
	 * @return 0可走  1不可走
	 */
	public int getRoadMap(int x, int y) {
		return _arry[y][x];	
	}
	
	/**
	 * 世界地图点到路点的转换
	 * @param px
	 * @param py
	 * @return
	 */
	public int[] Postion2Tile(int px, int py) {
		int[] _turnResult = new int[2];
		_turnResult[0] = px/tileWidth;
		_turnResult[1] = py/tileHeight;
		return _turnResult;
	}
	
	/**
	 * 获取hasTile
	 * @return hasTile hasTile
	 */
	public int getHasTile() {
		return hasTile;
	}
	/**
	 * 设置hasTile
	 * @param hasTile hasTile
	 */
	public void setHasTile(int hasTile) {
		this.hasTile = hasTile;
	}
	/**
	 * 获取tileFormat
	 * @return tileFormat tileFormat
	 */
	public String getTileFormat() {
		return tileFormat;
	}
	/**
	 * 设置tileFormat
	 * @param tileFormat tileFormat
	 */
	public void setTileFormat(String tileFormat) {
		this.tileFormat = tileFormat;
	}
	/**
	 * 获取tileX
	 * @return tileX tileX
	 */
	public int getTileX() {
		return tileX;
	}
	/**
	 * 设置tileX
	 * @param tileX tileX
	 */
	public void setTileX(int tileX) {
		this.tileX = tileX;
	}
	/**
	 * 获取tileY
	 * @return tileY tileY
	 */
	public int getTileY() {
		return tileY;
	}
	/**
	 * 设置tileY
	 * @param tileY tileY
	 */
	public void setTileY(int tileY) {
		this.tileY = tileY;
	}
	/**
	 * 获取mapW
	 * @return mapW mapW
	 */
	public int getMapW() {
		return mapW;
	}
	/**
	 * 设置mapW
	 * @param mapW mapW
	 */
	public void setMapW(int mapW) {
		this.mapW = mapW;
	}
	/**
	 * 获取mapH
	 * @return mapH mapH
	 */
	public int getMapH() {
		return mapH;
	}
	/**
	 * 设置mapH
	 * @param mapH mapH
	 */
	public void setMapH(int mapH) {
		this.mapH = mapH;
	}
	/**
	 * 获取mapName
	 * @return mapName mapName
	 */
	public String getMapName() {
		return mapName;
	}
	/**
	 * 设置mapName
	 * @param mapName mapName
	 */
	public void setMapName(String mapName) {
		this.mapName = mapName;
	}
	/**
	 * 获取type
	 * @return type type
	 */
	public int getType() {
		return type;
	}
	/**
	 * 设置type
	 * @param type type
	 */
	public void setType(int type) {
		this.type = type;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MapData [hasTile=" + hasTile + ", tileFormat=" + tileFormat + ", tileX=" + tileX + ", tileY=" + tileY
				+ ", mapW=" + mapW + ", mapH=" + mapH + ", mapName=" + mapName + ", type=" + type + ", id=" + id
				+ ", uid=" + uid + "]";
	}

	/**
	 * 获取起始X坐标
	 * @return startX 起始X坐标
	 */
	public int getStartX() {
		return startX;
	}

	/**
	 * 设置起始X坐标
	 * @param startX 起始X坐标
	 */
	public void setStartX(int startX) {
		this.startX = startX;
	}

	/**
	 * 获取起始Y坐标
	 * @return startY 起始Y坐标
	 */
	public int getStartY() {
		return startY;
	}

	/**
	 * 设置起始Y坐标
	 * @param startY 起始Y坐标
	 */
	public void setStartY(int startY) {
		this.startY = startY;
	}

	/**
	 * 获取地图中的玩家数量
	 * @return int 地图中的玩家数量
	 */
	public int getUserLength() {
		return mapGridData.getUserLength();
	}
	
	/**
	 * 获取地图中的玩家
	 * @return userList 地图中的玩家
	 */
	public List<UserInfo> getUserList(int x, int y) {
		return mapGridData.getNearbyUser(x, y);
	}

	/**
	 * 添加新玩家到此地图
	 * @param userInfo
	 */
	public void addUser(UserInfo userInfo) {
		mapGridData.addUserInfo(userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY(), userInfo);
	}

	public boolean removeUser(UserInfo userInfo) {
		return mapGridData.removeUserInfo(userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY(), userInfo);
	}

	/**
	 * 给此地图中所有的玩家发送消息
	 * @param action 发送指令
	 * @param buf 生成的二进制
	 */
	public void sendData(Monster monster, int action, byte[] buf) {
		List<UserInfo> userList = mapGridData.getNearbyUser(monster.getX(), monster.getY());
		for (int i = 0; i < userList.size(); i++) {
			userList.get(i).sendData(action, buf);
		}
	}
	
	/**
	 * 给此地图中所有的玩家发送消息
	 * @param action 发送指令
	 * @param arge 自动判断类型包括(Boolean、Integer、Long、byte[]、其余全是String)
	 */
	public void sendData(Monster monster, int action, Object ...arge) {
		List<UserInfo> userList = mapGridData.getNearbyUser(monster.getX(), monster.getY());
		for (int i = 0; i < userList.size(); i++) {
			userList.get(i).sendData(action, arge);
		}
	}
	
	/**
	 * 给此地图中所有的玩家发送消息
	 * @param action 发送指令
	 * @param buf 生成的二进制
	 */
	public void sendData(UserInfo userInfo, int action, byte[] buf) {
		List<UserInfo> userList = mapGridData.getNearbyUser(userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY());
		for (int i = 0; i < userList.size(); i++) {
			userList.get(i).sendData(action, buf);
		}
	}
	
	/**
	 * 给此地图中所有的玩家发送消息
	 * @param action 发送指令
	 * @param buf 生成的二进制
	 */
	public void sendData(UserInfo userInfo, int action, byte[] buf, boolean shieldAuto) {
		List<UserInfo> userList = mapGridData.getNearbyUser(userInfo.getCurrentSceneStartX(), userInfo.getCurrentSceneStartY());
		UserInfo info;
		for (int i = 0; i < userList.size(); i++) {
			info = userList.get(i);
			if (shieldAuto && info.getId()==userInfo.getId()) {
				continue;
			}
			info.sendData(action, buf);
		}
	}
	
	/**
	 * 给此地图中所有的玩家发送消息
	 * @param action 发送指令
	 * @param buf 生成的二进制
	 */
	public void sendData(UserInfo userInfo, int action, byte[] buf, boolean shieldAuto, List<Integer> gridIds) {
		Grid grid;
		List<UserInfo> userList = new ArrayList<>();
		for (int i = 0; i < gridIds.size(); i++) {
			grid = mapGridData.getGrid(gridIds.get(i));
			if (grid != null) {
				userList.addAll(grid.getUserInfoList());
			}
		}
		UserInfo info;
		for (int i = 0; i < userList.size(); i++) {
			info = userList.get(i);
			if (shieldAuto && info.getId()==userInfo.getId()) {
				continue;
			}
			info.sendData(action, buf);
		}
	}
	
	/**
	 * 给此地图中所有的玩家发送消息
	 * @param action 发送指令
	 * @param arge 自动判断类型包括(Boolean、Integer、Long、byte[]、其余全是String)
	 */
	public void sendData(UserInfo userInfo, int action, Object ...arge) {
		List<UserInfo> userList = mapGridData.getNearbyUser(userInfo.getCurrentSceneStartX(), 
				userInfo.getCurrentSceneStartY());
		for (int i = 0; i < userList.size(); i++) {
			userList.get(i).sendData(action, arge);
		}
	}

	/**
	 * 根据格子  写入其中的玩家和怪物数据
	 * @param gameOutput
	 * @param userInfo 
	 * @param gridIds 格子数组
	 * @throws IOException 
	 */
	public void writeGridData(GameOutput gameOutput, UserInfo userInfo, List<Integer> gridIds) throws IOException {
		Grid grid;
		List<UserInfo> userList = new ArrayList<>();
		for (int i = 0; i < gridIds.size(); i++) {
			grid = mapGridData.getGrid(gridIds.get(i));
			if (grid != null) {
				userList.addAll(grid.getUserInfoList());
			}
		}
		writeUserList(gameOutput, userList, userInfo);
		List<Monster> monsterList = new ArrayList<>();
		for (int i = 0; i < gridIds.size(); i++) {
			grid = mapGridData.getGrid(gridIds.get(i));
			if (grid != null) {
				monsterList.addAll(grid.getMonsterList());
			}
		}
		writeMonsterList(gameOutput, monsterList);
	}
	
	/**
	 * 写入所有的数据
	 * @param gameOutput
	 * @param userInfo 
	 * @throws IOException 
	 */
	public void writeAllData(GameOutput gameOutput, UserInfo userInfo) throws IOException {
		List<UserInfo> userList = mapGridData.getNearbyUser(userInfo.getCurrentSceneStartX(), 
				userInfo.getCurrentSceneStartY());
		writeUserList(gameOutput, userList, userInfo);
		List<Monster> monsterList = mapGridData.getNearbyMonster(userInfo.getCurrentSceneStartX(), 
				userInfo.getCurrentSceneStartY());
		writeMonsterList(gameOutput, monsterList);
	}

	/**
	 * 写入怪物数据
	 * @param gameOutput
	 * @param monsterList
	 * @throws IOException
	 */
	private void writeMonsterList(GameOutput gameOutput, List<Monster> monsterList) throws IOException {
		gameOutput.writeInt(monsterList.size());
		Monster monster;
		for (int i = 0; i < monsterList.size(); i++) {
			monster = monsterList.get(i);
			writeMonster(gameOutput, monster);
		}
	}

	/**
	 * 将指定的玩家列表数据写入数据
	 * @param gameOutput
	 * @param userList
	 * @param userInfo
	 * @throws IOException
	 */
	private void writeUserList(GameOutput gameOutput, List<UserInfo> userList, UserInfo userInfo) throws IOException {
		gameOutput.writeInt(userList.size());
		UserInfo info;
		for (int i = 0; i < userList.size(); i++) {
			info = userList.get(i);
			gameOutput.writeLong(info.getId());
			if (info.getId() != userInfo.getId()) {
				writeUserNotId(gameOutput, info);
			}
		}
	}

	public void writeUser(GameOutput gameOutput, UserInfo userInfo) throws IOException {
		gameOutput.writeLong(userInfo.getId());
		writeUserNotId(gameOutput, userInfo);
	}
	
	/**
	 * 写入发送流 不发送User 否 Id
	 * @param gameOutput
	 * @param userInfo
	 * @throws IOException
	 */
	public void writeUserNotId(GameOutput gameOutput, UserInfo userInfo) throws IOException {
		gameOutput.write(
				userInfo.getRole().getId(),
				userInfo.getRoleName(),
				userInfo.getRole().getLevel(),
				userInfo.getGridId()
				);
		gameOutput.writeBoolean(userInfo.isBattleMove());
		if (userInfo.isBattleMove()) {
			gameOutput.writeInt(userInfo.getMovePoint()[0]);
			gameOutput.writeInt(userInfo.getMovePoint()[1]);
		}
		gameOutput.writeInt(userInfo.getCurrentSceneStartX());
		gameOutput.writeInt(userInfo.getCurrentSceneStartY());
	}

	public void writeMonster(GameOutput gameOutput, Monster monster) throws IOException {
		gameOutput.write(
				monster.getId(),
				monster.getType(),
				monster.getRole().getId(),
				monster.getRole().getName(),
				monster.getRole().getLevel(),
				monster.getGridId()
				);
		gameOutput.writeInt(monster.getX());
		gameOutput.writeInt(monster.getY());
	}

	/**
	 * 根据怪物id删除怪物
	 * @param monsterId
	 * @return
	 */
	public synchronized Monster removeMonster(long monsterId) {
		Monster monster = mapGridData.removeMonster(monsterId);
		if (monster != null) {
			onlyIdManage.gc(monster.getId());
		}
		return monster;
	}
	
	/**
	 * 根据怪物id获取怪物
	 * @param monsterId
	 * @return
	 */
	public Monster getMonster(long monsterId) {
		return mapGridData.getMonster(monsterId);
	}

	/**
	 * 获取地图中的怪物数量
	 * @return monsterList 地图中的怪物数量
	 */
	public int getMonsterLength() {
		return mapGridData.getMonsterLength();
	}

	/**
	 * 获取mapGridData
	 * @return mapGridData mapGridData
	 */
	public MapGridData getMapGridData() {
		return mapGridData;
	}

	/**
	 * 获取silzAstar
	 * @return silzAstar silzAstar
	 */
	public SilzAstar getSilzAstar() {
		return silzAstar;
	}

}
