package com.entity;

import java.io.IOException;
import java.util.List;

import com.boge.entity.GameOutput;

/**
 * 排行榜数据
 * @author xujinbo
 */
@SuppressWarnings("serial")
public class RankingList extends Entity {
	
	/** 人物等级排行 */
	private byte[] roleLevelRank;
	/** 人物物伤排行 */
	private byte[] roleOrdinaryRank;
	/** 人物法伤排行 */
	private byte[] roleMagicRank;
	/** 人物速度排行 */
	private byte[] roleSpeedRank;
	/** 人物防御排行 */
	private byte[] roleDefendRank;
	/** 宠物物伤排行 */
	private byte[] petOrdinaryRank;
	/** 宠物法伤排行 */
	private byte[] petMagicRank;
	/** 宠物速度排行 */
	private byte[] petSpeedRank;
	/** 宠物防御排行 */
	private byte[] petDefendRank;
	
	/** 人物等级排行 */
	private List<RankingData> roleLevel;
	/** 人物物伤排行 */
	private List<RankingData> roleOrdinary;
	/** 人物法伤排行 */
	private List<RankingData> roleMagic;
	/** 人物速度排行 */
	private List<RankingData> roleSpeed;
	/** 人物防御排行 */
	private List<RankingData> roleDefend;
	/** 人物物伤排行 */
	private List<RankingData> petOrdinary;
	/** 人物法伤排行 */
	private List<RankingData> petMagic;
	/** 人物速度排行 */
	private List<RankingData> petSpeed;
	/** 人物防御排行 */
	private List<RankingData> petDefend;
	
	public RankingList() {
		
	}
	
	/**
	 * 获取人物等级排行
	 * @return roleLevelRank 人物等级排行
	 */
	public byte[] getRoleLevelRank() {
		return roleLevelRank;
	}
	/**
	 * 设置人物等级排行
	 * @param roleLevelRank 人物等级排行
	 */
	public void setRoleLevelRank(byte[] roleLevelRank) {
		this.roleLevelRank = roleLevelRank;
	}
	/**
	 * 获取人物物伤排行
	 * @return roleOrdinaryRank 人物物伤排行
	 */
	public byte[] getRoleOrdinaryRank() {
		return roleOrdinaryRank;
	}
	/**
	 * 设置人物物伤排行
	 * @param roleOrdinaryRank 人物物伤排行
	 */
	public void setRoleOrdinaryRank(byte[] roleOrdinaryRank) {
		this.roleOrdinaryRank = roleOrdinaryRank;
	}
	/**
	 * 获取人物法伤排行
	 * @return roleMagicRank 人物法伤排行
	 */
	public byte[] getRoleMagicRank() {
		return roleMagicRank;
	}
	/**
	 * 设置人物法伤排行
	 * @param roleMagicRank 人物法伤排行
	 */
	public void setRoleMagicRank(byte[] roleMagicRank) {
		this.roleMagicRank = roleMagicRank;
	}
	/**
	 * 获取人物速度排行
	 * @return roleSpeedRank 人物速度排行
	 */
	public byte[] getRoleSpeedRank() {
		return roleSpeedRank;
	}
	/**
	 * 设置人物速度排行
	 * @param roleSpeedRank 人物速度排行
	 */
	public void setRoleSpeedRank(byte[] roleSpeedRank) {
		this.roleSpeedRank = roleSpeedRank;
	}
	/**
	 * 获取人物防御排行
	 * @return roleDefendRank 人物防御排行
	 */
	public byte[] getRoleDefendRank() {
		return roleDefendRank;
	}
	/**
	 * 设置人物防御排行
	 * @param roleDefendRank 人物防御排行
	 */
	public void setRoleDefendRank(byte[] roleDefendRank) {
		this.roleDefendRank = roleDefendRank;
	}
	/**
	 * 获取宠物物伤排行
	 * @return petOrdinaryRank 宠物物伤排行
	 */
	public byte[] getPetOrdinaryRank() {
		return petOrdinaryRank;
	}
	/**
	 * 设置宠物物伤排行
	 * @param petOrdinaryRank 宠物物伤排行
	 */
	public void setPetOrdinaryRank(byte[] petOrdinaryRank) {
		this.petOrdinaryRank = petOrdinaryRank;
	}
	/**
	 * 获取宠物法伤排行
	 * @return petMagicRank 宠物法伤排行
	 */
	public byte[] getPetMagicRank() {
		return petMagicRank;
	}
	/**
	 * 设置宠物法伤排行
	 * @param petMagicRank 宠物法伤排行
	 */
	public void setPetMagicRank(byte[] petMagicRank) {
		this.petMagicRank = petMagicRank;
	}
	/**
	 * 获取宠物速度排行
	 * @return petSpeedRank 宠物速度排行
	 */
	public byte[] getPetSpeedRank() {
		return petSpeedRank;
	}
	/**
	 * 设置宠物速度排行
	 * @param petSpeedRank 宠物速度排行
	 */
	public void setPetSpeedRank(byte[] petSpeedRank) {
		this.petSpeedRank = petSpeedRank;
	}
	/**
	 * 获取宠物防御排行
	 * @return petDefendRank 宠物防御排行
	 */
	public byte[] getPetDefendRank() {
		return petDefendRank;
	}
	/**
	 * 设置宠物防御排行
	 * @param petDefendRank 宠物防御排行
	 */
	public void setPetDefendRank(byte[] petDefendRank) {
		this.petDefendRank = petDefendRank;
	}
	/**
	 * 获取人物等级排行
	 * @return roleLevel 人物等级排行
	 */
	public List<RankingData> getRoleLevel() {
		return roleLevel;
	}
	/**
	 * 设置人物等级排行
	 * @param roleLevel 人物等级排行
	 */
	public void setRoleLevel(List<RankingData> roleLevel) {
		this.roleLevel = roleLevel;
	}
	/**
	 * 获取人物物伤排行
	 * @return roleOrdinary 人物物伤排行
	 */
	public List<RankingData> getRoleOrdinary() {
		return roleOrdinary;
	}
	/**
	 * 设置人物物伤排行
	 * @param roleOrdinary 人物物伤排行
	 */
	public void setRoleOrdinary(List<RankingData> roleOrdinary) {
		this.roleOrdinary = roleOrdinary;
	}
	/**
	 * 获取人物法伤排行
	 * @return roleMagic 人物法伤排行
	 */
	public List<RankingData> getRoleMagic() {
		return roleMagic;
	}
	/**
	 * 设置人物法伤排行
	 * @param roleMagic 人物法伤排行
	 */
	public void setRoleMagic(List<RankingData> roleMagic) {
		this.roleMagic = roleMagic;
	}
	/**
	 * 获取人物速度排行
	 * @return roleSpeed 人物速度排行
	 */
	public List<RankingData> getRoleSpeed() {
		return roleSpeed;
	}
	/**
	 * 设置人物速度排行
	 * @param roleSpeed 人物速度排行
	 */
	public void setRoleSpeed(List<RankingData> roleSpeed) {
		this.roleSpeed = roleSpeed;
	}
	/**
	 * 获取人物防御排行
	 * @return roleDefend 人物防御排行
	 */
	public List<RankingData> getRoleDefend() {
		return roleDefend;
	}
	/**
	 * 设置人物防御排行
	 * @param roleDefend 人物防御排行
	 */
	public void setRoleDefend(List<RankingData> roleDefend) {
		this.roleDefend = roleDefend;
	}
	/**
	 * 获取人物物伤排行
	 * @return petOrdinary 人物物伤排行
	 */
	public List<RankingData> getPetOrdinary() {
		return petOrdinary;
	}
	/**
	 * 设置人物物伤排行
	 * @param petOrdinary 人物物伤排行
	 */
	public void setPetOrdinary(List<RankingData> petOrdinary) {
		this.petOrdinary = petOrdinary;
	}
	/**
	 * 获取人物法伤排行
	 * @return petMagic 人物法伤排行
	 */
	public List<RankingData> getPetMagic() {
		return petMagic;
	}
	/**
	 * 设置人物法伤排行
	 * @param petMagic 人物法伤排行
	 */
	public void setPetMagic(List<RankingData> petMagic) {
		this.petMagic = petMagic;
	}
	/**
	 * 获取人物速度排行
	 * @return petSpeed 人物速度排行
	 */
	public List<RankingData> getPetSpeed() {
		return petSpeed;
	}
	/**
	 * 设置人物速度排行
	 * @param petSpeed 人物速度排行
	 */
	public void setPetSpeed(List<RankingData> petSpeed) {
		this.petSpeed = petSpeed;
	}
	/**
	 * 获取人物防御排行
	 * @return petDefend 人物防御排行
	 */
	public List<RankingData> getPetDefend() {
		return petDefend;
	}
	/**
	 * 设置人物防御排行
	 * @param petDefend 人物防御排行
	 */
	public void setPetDefend(List<RankingData> petDefend) {
		this.petDefend = petDefend;
	}

	/** 将人物等级排行写入发送流 
	 * @throws IOException */
	public void writeDataRoleLevel(GameOutput gameOutput) throws IOException {
		writeData(roleLevel, gameOutput);
	}
	
	/** 将人物物伤排行写入发送流 */
	public void writeDataRoleOrdinaryAttack(GameOutput gameOutput) throws IOException {
		writeData(roleOrdinary, gameOutput);
	}
	
	/** 将人物法伤排行写入发送流 */
	public void writeDataRoleMagic(GameOutput gameOutput) throws IOException {
		writeData(roleMagic, gameOutput);
	}
	
	/** 将人物速度排行写入发送流 */
	public void writeDataRoleSpeed(GameOutput gameOutput) throws IOException {
		writeData(roleSpeed, gameOutput);
	}
	
	/** 将人物防御排行写入发送流 */
	public void writeDataRoleDefend(GameOutput gameOutput) throws IOException {
		writeData(roleDefend, gameOutput);
	}
	
	/** 将宠物物伤排行写入发送流 */
	public void writeDataPetOrdinary(GameOutput gameOutput) throws IOException {
		writeData(petOrdinary, gameOutput);
	}
	/** 将宠物法伤排行写入发送流 */
	public void writeDataPetMagic(GameOutput gameOutput) throws IOException {
		writeData(petMagic, gameOutput);
	}
	/** 将宠物速度排行写入发送流 */
	public void writeDataPetSpeed(GameOutput gameOutput) throws IOException {
		writeData(petSpeed, gameOutput);
	}
	/** 将宠物防御排行写入发送流 */
	public void writeDataPetDefend(GameOutput gameOutput) throws IOException {
		writeData(petDefend, gameOutput);
	}

	private void writeData(List<RankingData> list, GameOutput gameOutput) throws IOException {
		RankingData rankingData;
		int len = list.size();
		gameOutput.writeInt(len);
		for (int i = 0; i < len; i++) {
			rankingData = list.get(i);
			if (rankingData != null) {
				gameOutput.writeLong(rankingData.getId());// 此角色的id
				gameOutput.writeUTF(rankingData.getName());//角色名字
				gameOutput.writeUTF(rankingData.getRankKey());//排行key
				gameOutput.writeLong(rankingData.getRankValue());//排行值
			}
		}
	}
	
}
