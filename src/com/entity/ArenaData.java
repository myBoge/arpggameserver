package com.entity;

import java.io.IOException;
import java.util.List;

import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.configs.PropConfig;
import com.configs.RoleConfig;
import com.configs.SkillConfig;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;
import com.model.EquipsLibrary;
import com.model.PetLibrary;
import com.utils.BytesFactory;

/**
 * 竞技场数据
 * @author xujinbo
 */
@SuppressWarnings("serial")
public class ArenaData extends Entity implements IAttributDataStream {

	/** 玩家挑战时候的数据 */
	private byte[] roleBytes;
	/** 竞技场中的角色数据 */
	private UserInfo userInfo;
	/** 是否正在战斗 */
	private boolean isFight;
	
	public ArenaData() {
		
	}

	/**
	 * 获取玩家挑战时候的数据
	 * @return roleBytes 玩家挑战时候的数据
	 */
	public byte[] getRoleBytes() {
		return roleBytes;
	}

	/**
	 * 设置玩家挑战时候的数据
	 * @param roleBytes 玩家挑战时候的数据
	 */
	public void setRoleBytes(byte[] roleBytes) {
		this.roleBytes = roleBytes;
	}

//	/**
//	 * 创建一个模拟数据
//	 * @param rank 排位名次
//	 */
//	public void createUserInfo(int rank) {
//		userInfo = new UserInfo();
//		userInfo.setLogin(new Login());
//		userInfo.setRoleName("竞技小英雄");
//		LuaUtils.get("createAIUserInfo");
//		RoleConfig roleConfig = ResourceCell.getResource(RoleConfig.class);
//		Role role = roleConfig.getSample(1000+RandomUtils.nextInt(1, 3));
//		if (rank >= 0 && rank < 10) {
//			role.setLevel(RandomUtils.nextInt(35, 40));
//		} else if (rank >= 10 && rank < 100) {
//			role.setLevel(RandomUtils.nextInt(30, 35));
//		} else if (rank >= 100 && rank < 500) {
//			role.setLevel(RandomUtils.nextInt(25, 30));
//		} else {
//			role.setLevel(RandomUtils.nextInt(1, 25));
//		}
//		role.setName(userInfo.getRoleName());
//		role.changeValue();
//		role.randomFenpei();
//		userInfo.setRole(role);
//		// 穿装备
//		EquipsLibrary equips = userInfo.getEquipsLibrary();
//		PropConfig propConfig = ResourceCell.getResource(PropConfig.class);
//		if (role.getLevel() >= 30) {
//			equips.addEquips((Equips) propConfig.getSample(3004));
//			equips.addEquips((Equips) propConfig.getSample(3104));
//			equips.addEquips((Equips) propConfig.getSample(3204));
//			equips.addEquips((Equips) propConfig.getSample(3304));
//			if (role.getLevel() >= 35) {
//				equips.addEquips((Equips) propConfig.getSample(3402));
//				equips.addEquips((Equips) propConfig.getSample(3502));
//				equips.addEquips((Equips) propConfig.getSample(3602));
//				equips.addEquips((Equips) propConfig.getSample(3602));
//			}
//		} else if (role.getLevel() >= 20 && role.getLevel() < 30) {
//			equips.addEquips((Equips) propConfig.getSample(3003));
//			equips.addEquips((Equips) propConfig.getSample(3103));
//			equips.addEquips((Equips) propConfig.getSample(3203));
//			equips.addEquips((Equips) propConfig.getSample(3303));
//			
//			equips.addEquips((Equips) propConfig.getSample(3401));
//			equips.addEquips((Equips) propConfig.getSample(3501));
//			equips.addEquips((Equips) propConfig.getSample(3601));
//			equips.addEquips((Equips) propConfig.getSample(3601));
//		} else if (role.getLevel() >= 10 && role.getLevel() < 20) {
//			equips.addEquips((Equips) propConfig.getSample(3002));
//			equips.addEquips((Equips) propConfig.getSample(3102));
//			equips.addEquips((Equips) propConfig.getSample(3202));
//			equips.addEquips((Equips) propConfig.getSample(3302));
//		} else if (role.getLevel() >= 1 && role.getLevel() < 10) {
//			equips.addEquips((Equips) propConfig.getSample(3001));
//			equips.addEquips((Equips) propConfig.getSample(3101));
//			equips.addEquips((Equips) propConfig.getSample(3201));
//			equips.addEquips((Equips) propConfig.getSample(3301));
//		}
//		userInfo.updateEquipsRole();
//		role.castAtt();
//		role.setBlood(role.getMaxBlood());
//		role.setMagic(role.getMaxMagic());
//		// 宠物
//		PetLibrary pet = userInfo.getPetLibrarys();
//		role = roleConfig.getSample(RandomUtils.nextLong(2010, 2045));
//		role.setJoinBattlePet(true);
//		role.setName("竞技小宠物");
//		if (rank >= 0 && rank < 10) {
//			role.setLevel(RandomUtils.nextInt(35, 40));
//		} else if (rank >= 10 && rank < 100) {
//			role.setLevel(RandomUtils.nextInt(30, 35));
//		} else if (rank >= 100 && rank < 500) {
//			role.setLevel(RandomUtils.nextInt(25, 30));
//		} else {
//			role.setLevel(RandomUtils.nextInt(1, 25));
//		}
//		role.castAtt();
//		role.setBlood(role.getMaxBlood());
//		role.setMagic(role.getMaxMagic());
//		pet.addRole(role);
//		// 技能
//		SkillLibrary skillLib = userInfo.getSkillLibrary();
//		SkillConfig skillConfig = ResourceCell.getResource(SkillConfig.class);
//		Skill skill = skillConfig.getSample(1001);
//		skill.setLevel((int) (role.getLevel()*1.6));
//		skillLib.addSkill(skill);
//		skill = skillConfig.getSample(1002);
//		skill.setLevel((int) (role.getLevel()*1.6));
//		skillLib.addSkill(skill);
//		skill = skillConfig.getSample(1003);
//		skill.setLevel((int) (role.getLevel()*1.6));
//		skillLib.addSkill(skill);
//	}
	
	public void convertUserInfo() {
		BytesFactory.createBytes(roleBytes, this);
	}

	public void convertBytes() {
		roleBytes = BytesFactory.objectToByteData(this);
	}

	/**
	 * 获取竞技场中的角色数据
	 * @return userInfo 竞技场中的角色数据
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * 设置竞技场中的角色数据
	 * @param userInfo 竞技场中的角色数据
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		userInfo = new UserInfo();
		userInfo.setId(ois.readLong());
		userInfo.setLogin(new Login());
		userInfo.setRoleName(ois.readUTF());
		RoleConfig roleConfig = ResourceCell.getResource(RoleConfig.class);
		Role role = roleConfig.getSample(ois.readLong());
		role.setLevel(ois.readInt());
		role.setSex(ois.readInt());
		role.setName(userInfo.getRoleName());
		role.setJiadianHp(ois.readInt());
		role.setJiadianMp(ois.readInt());
		role.setJiadianAp(ois.readInt());
		role.setJiadianSp(ois.readInt());
		userInfo.setRole(role);
		
		// 穿装备
		EquipsLibrary equips = userInfo.getEquipsLibrary();
		PropConfig propConfig = ResourceCell.getResource(PropConfig.class);
		int len = ois.readInt();
		for (int i = 0; i < len; i++) {
			equips.addEquips((Equips) propConfig.getSample(ois.readLong()));
		}
		userInfo.updateEquipsRole();
		role.castAtt();
		role.setBlood(role.getMaxBlood());
		role.setMagic(role.getMaxMagic());
		// 宠物
		long id = ois.readLong();
		if (id != 0) {
			PetLibrary pet = userInfo.getPetLibrarys();
			role = roleConfig.getSample(id);
			role.setJoinBattlePet(true);
			role.setLevel(ois.readInt());
			role.setName(ois.readUTF());
			role.setJiadianHp(ois.readInt());
			role.setJiadianMp(ois.readInt());
			role.setJiadianAp(ois.readInt());
			role.setJiadianSp(ois.readInt());
			role.castAtt();
			role.setBlood(role.getMaxBlood());
			role.setMagic(role.getMaxMagic());
			pet.addRole(role);
		}
		// 技能
		List<Skill> skills = userInfo.getSkillLibrary().getSkills();
		SkillConfig skillConfig = ResourceCell.getResource(SkillConfig.class);
		len = ois.readInt();
		Skill skill;
		for (int i = 0; i < len; i++) {
			skill = skillConfig.getSample(ois.readLong());
			skill.setLevel(ois.readInt());
			skills.add(skill);
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		oos.writeLong(userInfo.getId());
		oos.writeUTF(userInfo.getRoleName());
		Role role = userInfo.getRole();
		oos.writeLong(role.getId());
		oos.writeInt(role.getLevel());
		oos.writeInt(role.getSex());
		oos.writeInt(role.getJiadianHp());
		oos.writeInt(role.getJiadianMp());
		oos.writeInt(role.getJiadianAp());
		oos.writeInt(role.getJiadianSp());
		// 穿装备
		EquipsLibrary equips = userInfo.getEquipsLibrary();
		List<Equips> equipsList = equips.getEquipsList();
		oos.writeInt(equipsList.size());
		for (int i = 0; i < equipsList.size(); i++) {
			oos.writeLong(equipsList.get(i).getId());
		}
		// 宠物
		PetLibrary pet = userInfo.getPetLibrarys();
		role = pet.getJoinBattlePet();
		if (role != null) {
			oos.writeLong(role.getId());
			oos.writeInt(role.getLevel());
			oos.writeUTF(role.getName());
			oos.writeInt(role.getJiadianHp());
			oos.writeInt(role.getJiadianMp());
			oos.writeInt(role.getJiadianAp());
			oos.writeInt(role.getJiadianSp());
		} else {
			oos.writeLong(0);
		}
		// 技能
		List<Skill> skills = userInfo.getSkillLibrary().getSkills();
		oos.writeInt(skills.size());
		Skill skill;
		for (int i = 0; i < skills.size(); i++) {
			skill = skills.get(i);
			oos.writeLong(skill.getId());
			oos.writeInt(skill.getLevel());
		}
		
	}

	public void writeData(GameOutput gameOutput) throws IOException {
		gameOutput.writeLong(this.getId());
		gameOutput.writeLong(userInfo.getRole().getId());
		gameOutput.writeUTF(userInfo.getRoleName());
		gameOutput.writeInt(userInfo.getRole().getLevel());
	}

	/**
	 * 获取是否正在战斗
	 * @return isFight 是否正在战斗
	 */
	public boolean isFight() {
		return isFight;
	}

	/**
	 * 设置是否正在战斗
	 * @param isFight 是否正在战斗
	 */
	public void setFight(boolean isFight) {
		this.isFight = isFight;
	}
	
}