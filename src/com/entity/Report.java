package com.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

@SuppressWarnings("serial")
public class Report extends Entity implements IAttributDataStream {

	/** 提交类型 0bug 1投诉 2建议 3其他 */
	private int reportType = -1;
	/** 标题 */
	private String title;
	/** 内容 */
	private String content;
	/** 提交人 */
	private long userId;
	/** 保存二进制数据 */
	private byte[] bytes;
	
	private List<Report> reports;
	
	public Report() {
		super();
		reports = new ArrayList<>();
	}
	public Report(int reportType, String title, String content, long userId) {
		super();
		this.reportType = reportType;
		this.title = title;
		this.content = content;
		this.userId = userId;
	}
	/**
	 * 获取提交类型0bug1投诉2建议3其他
	 * @return reportType 提交类型0bug1投诉2建议3其他
	 */
	public int getReportType() {
		return reportType;
	}
	/**
	 * 设置提交类型0bug1投诉2建议3其他
	 * @param reportType 提交类型0bug1投诉2建议3其他
	 */
	public void setReportType(int reportType) {
		this.reportType = reportType;
	}
	/**
	 * 获取标题
	 * @return title 标题
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置标题
	 * @param title 标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取内容
	 * @return content 内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置内容
	 * @param content 内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * 获取提交人
	 * @return userId 提交人
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * 设置提交人
	 * @param userId 提交人
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		reportType = ois.readInt();
		title = ois.readUTF();
		content = ois.readUTF();
		userId = ois.readLong();
			
	}
	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		oos.writeInt(reportType);
		oos.writeUTF(title);
		oos.writeUTF(content);
		oos.writeLong(userId);
	}
	
	@Override
	public String toString() {
		return "Report [reportType=" + reportType + ", title=" + title
				+ ", content=" + content + ", userId=" + userId + ", id=" + id
				+ ", uid=" + uid + "]";
	}
	/**
	 * 获取保存二进制数据
	 * @return bytes 保存二进制数据
	 */
	public byte[] getBytes() {
		return bytes;
	}
	/**
	 * 设置保存二进制数据
	 * @param bytes 保存二进制数据
	 */
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
	/**
	 * 获取reports
	 * @return reports reports
	 */
	public List<Report> getReports() {
		return reports;
	}
	
}
