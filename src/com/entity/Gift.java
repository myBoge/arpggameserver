package com.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.boge.util.ResourceCell;
import com.configs.PropConfig;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

@SuppressWarnings("serial")
public class Gift extends Entity implements IAttributDataStream {

	/** 兑换码类型 */
	private int giftType = -1;
	/** 兑换码数据 */
	private byte[] bytes;
	/** 奖励礼包 */
	private List<Prop> giftList = new ArrayList<>();
	/** 已经领取玩家的id */
	private List<Long> userIDList = new ArrayList<>();
	
	public Gift() {
	}
	
	/**
	 * 获取兑换码类型
	 * @return giftType 兑换码类型
	 */
	public int getGiftType() {
		return giftType;
	}
	/**
	 * 设置兑换码类型
	 * @param giftType 兑换码类型
	 */
	public void setGiftType(int giftType) {
		this.giftType = giftType;
	}
	/**
	 * 获取兑换码数据
	 * @return bytes 兑换码数据
	 */
	public byte[] getBytes() {
		return bytes;
	}
	/**
	 * 设置兑换码数据
	 * @param bytes 兑换码数据
	 */
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
	/**
	 * 获取奖励礼包
	 * @return giftList 奖励礼包
	 */
	public List<Prop> getGiftList() {
		return giftList;
	}

	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		int len = ois.readInt();
		PropConfig propConfig = ResourceCell.getResource(PropConfig.class);
		Prop prop;
		for (int j = 0; j < len; j++) {
			prop = propConfig.getSample(ois.readLong());
			prop.setCount(ois.readInt());
			giftList.add(prop);
		}
		len = ois.readInt();
		for (int i = 0; i < len; i++) {
			userIDList.add(ois.readLong());
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		int len = giftList.size();
		oos.writeInt(len);
		for (int j = 0; j < len; j++) {
			oos.writeLong(giftList.get(j).getId());
			oos.writeInt(giftList.get(j).getCount());
		}
		len = userIDList.size();
		oos.writeInt(len);
		for (int i = 0; i < len; i++) {
			oos.writeLong(userIDList.get(i));
		}
	}
	
	@Override
	public String toString() {
		return "Gift [giftType=" + giftType + ", giftList=" + giftList + ", id=" + id + ", uid=" + uid + "]";
	}

	/**
	 * 获取已经领取玩家的id
	 * @return userIDList 已经领取玩家的id
	 */
	public List<Long> getUserIDList() {
		return userIDList;
	}

	/**
	 * 设置已经领取玩家的id
	 * @param userIDList 已经领取玩家的id
	 */
	public void setUserIDList(List<Long> userIDList) {
		this.userIDList = userIDList;
	}

}
