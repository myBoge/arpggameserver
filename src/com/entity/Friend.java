package com.entity;

import java.io.IOException;

import com.boge.entity.GameOutput;
import com.boge.util.ResourceCell;
import com.data.UserDataManage;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

@SuppressWarnings("serial")
public class Friend extends Entity implements IAttributDataStream {

	/** 玩家id */
	private long friendId;
	/** 玩家名字 */
	private String name;
	/** 玩家等级 */
	private int level;
	/** 心情 */
	private String mood;
	/** 玩家性别   1为男性  2为女性 */	
	private int sex;
	/** 在线状态 0不在线 1在线 */
	private int onlineState;
	
	public Friend() {
	}

	public Friend(long friendId) {
		super();
		this.friendId = friendId;
	}

	public Friend(String name) {
		super();
		this.name = name;
	}

	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		friendId = ois.readLong();
		UserDataManage dataManage = ResourceCell.getResource(UserDataManage.class);
		UserInfo userInfo = dataManage.getUserInfoData(friendId);
		if (userInfo != null) {
			setValue(userInfo);
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		oos.writeLong(friendId);
	}

	/**
	 * 获取玩家id
	 * 
	 * @return friendId 玩家id
	 */
	public long getFriendId() {
		return friendId;
	}

	/**
	 * 设置玩家id
	 * 
	 * @param friendId
	 *            玩家id
	 */
	public void setFriendId(long friendId) {
		this.friendId = friendId;
	}

	/**
	 * 获取玩家名字
	 * 
	 * @return name 玩家名字
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置玩家名字
	 * 
	 * @param name
	 *            玩家名字
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取玩家等级
	 * 
	 * @return level 玩家等级
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * 设置玩家等级
	 * 
	 * @param level
	 *            玩家等级
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return "Friend [friendId=" + friendId + ", name=" + name + ", level="
				+ level + ", mood=" + mood + ", sex=" + sex + ", onlineState="
				+ onlineState + ", id=" + id + ", uid=" + uid + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Friend other = (Friend) obj;
		if (friendId != other.friendId)
			return false;
		return true;
	}

	/**
	 * 获取心情
	 * @return mood 心情
	 */
	public String getMood() {
		return mood;
	}

	/**
	 * 设置心情
	 * @param mood 心情
	 */
	public void setMood(String mood) {
		this.mood = mood;
	}

	/**
	 * 获取玩家性别1为男性2为女性
	 * @return sex 玩家性别1为男性2为女性
	 */
	public int getSex() {
		return sex;
	}

	/**
	 * 设置玩家性别1为男性2为女性
	 * @param sex 玩家性别1为男性2为女性
	 */
	public void setSex(int sex) {
		this.sex = sex;
	}
	
	/**
	 * 好友数据 写到此流
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void writeData(GameOutput gameOutput) throws IOException {
		gameOutput.writeLong(friendId);
		gameOutput.writeInt(level);
		gameOutput.writeUTF(name);
		gameOutput.writeUTF(mood);
		gameOutput.writeInt(sex);
		gameOutput.writeInt(onlineState);
	}

	/**
	 * 赋值
	 * @param userInfo
	 */
	public void setValue(UserInfo userInfo) {
		friendId = userInfo.getId();
		level = userInfo.getRole().getLevel();
		name = userInfo.getRoleName();
		mood = userInfo.getMood();
		sex = userInfo.getRole().getSex();
		onlineState = userInfo.getOnlineState();
	}

	/**
	 * 获取在线状态0不在线1在线
	 * @return onlineState 在线状态0不在线1在线
	 */
	public int getOnlineState() {
		return onlineState;
	}

	/**
	 * 设置在线状态0不在线1在线
	 * @param onlineState 在线状态0不在线1在线
	 */
	public void setOnlineState(int onlineState) {
		this.onlineState = onlineState;
	}

}
