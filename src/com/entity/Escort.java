package com.entity;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.boge.entity.GameOutput;
import com.boge.socket.ActionData;
import com.boge.util.ResourceCell;
import com.data.UserDataManage;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;

/**
 * 用于存数据库
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
public class Escort extends Entity implements Serializable, IAttributDataStream {

	/** 用户数据 */
	private List<UserInfo> userInfoList = new ArrayList<>();
	/** 类型  null玩家 其他属于电脑押镖模型id */
	private List<Long> types;
	/** 押镖地图id */
	private long mapId = 0;
	/** 在这个地图押镖开始时间 */
	private long startDate = 0;
	/** 当前位置x */
	private int x = 0;
	/** 当前位置y */
	private int y = 0;
	/** 玩家用的押金 */
	private long foregift = 0;
	/** 押送的金币总金额 */
	private long totalGold = 0;
	/** 押送的金币剩余数量 */
	private long residueGold = 0;
	/** 打劫别人次数 */
	private int robCount = 0;
	/** 被别人打劫次数 */
	private int robberyCount = 0;
	/** 打劫得到的金币 */
	private long robGold = 0;
	/** 是否发生战斗 */
	private boolean isFight;
	/** 上一次发生战斗时间 */
	private long oldRobFightDate;
	
	public Escort() {
		super();
	}

	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		int len = ois.readInt();
		if (len == 0) {
			types = null;
		} else {
			types = new ArrayList<>();
			for (int i = 0; i < len; i++) {
				types.add(ois.readLong());
			}
		}
		mapId = ois.readLong();
		startDate = ois.readLong();
		x = ois.readInt();
		y = ois.readInt();
		foregift = ois.readLong();
		totalGold = ois.readLong();
		residueGold = ois.readLong();
		robCount = ois.readInt();
		robberyCount = ois.readInt();
		robGold = ois.readLong();
		
		len = ois.readInt();
		UserDataManage userDataManage = ResourceCell.getResource(UserDataManage.class);
		UserInfo userInfo;
		long userId;
		for (int i = 0; i < len; i++) {
			userId = ois.readLong();
			if (userId != 0) {
				userInfo = userDataManage.getUserInfoData(userId);
				userInfoList.add(userInfo);
			}
		}
	}
	
	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		int len = 0;
		if (types == null) {
			oos.writeInt(len);
		} else {
			len = types.size();
			oos.writeInt(len);
			for (int i = 0; i < len; i++) {
				oos.writeLong(types.get(i));
			}
		}
		oos.writeLong(mapId);
		oos.writeLong(startDate);
		oos.writeInt(x);
		oos.writeInt(y);
		oos.writeLong(foregift);
		oos.writeLong(totalGold);
		oos.writeLong(residueGold);
		oos.writeInt(robCount);
		oos.writeInt(robberyCount);
		oos.writeLong(robGold);
		
		len = userInfoList.size();
		oos.writeInt(len);
		for (int i = 0; i < len; i++) {
			oos.writeLong(userInfoList.get(i).getId());
		}
	}

	/**
	 * 获取押镖地图id
	 * @return mapId 押镖地图id
	 */
	public long getMapId() {
		return mapId;
	}
	/**
	 * 设置押镖地图id
	 * @param mapId 押镖地图id
	 */
	public void setMapId(long mapId) {
		this.mapId = mapId;
	}
	/**
	 * 获取在这个地图押镖开始时间
	 * @return startDate 在这个地图押镖开始时间
	 */
	public long getStartDate() {
		return startDate;
	}
	/**
	 * 设置在这个地图押镖开始时间
	 * @param startDate 在这个地图押镖开始时间
	 */
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}
	/**
	 * 获取当前位置x
	 * @return x 当前位置x
	 */
	public int getX() {
		return x;
	}
	/**
	 * 设置当前位置x
	 * @param x 当前位置x
	 */
	public void setX(int x) {
		this.x = x;
	}
	/**
	 * 获取当前位置y
	 * @return y 当前位置y
	 */
	public int getY() {
		return y;
	}
	/**
	 * 设置当前位置y
	 * @param y 当前位置y
	 */
	public void setY(int y) {
		this.y = y;
	}
	/**
	 * 获取打劫别人次数
	 * @return robCount 打劫别人次数
	 */
	public int getRobCount() {
		return robCount;
	}
	/**
	 * 设置打劫别人次数
	 * @param robCount 打劫别人次数
	 */
	public void setRobCount(int robCount) {
		this.robCount = robCount;
	}
	/**
	 * 获取被别人打劫次数
	 * @return robberyCount 被别人打劫次数
	 */
	public int getRobberyCount() {
		return robberyCount;
	}
	/**
	 * 设置被别人打劫次数
	 * @param robberyCount 被别人打劫次数
	 */
	public void setRobberyCount(int robberyCount) {
		this.robberyCount = robberyCount;
	}
	/**
	 * 获取打劫得到的金币
	 * @return robGold 打劫得到的金币
	 */
	public long getRobGold() {
		return robGold;
	}
	/**
	 * 设置打劫得到的金币
	 * @param robGold 打劫得到的金币
	 */
	public void setRobGold(long robGold) {
		this.robGold = robGold;
	}
	/**
	 * 获取是否发生战斗
	 * @return isFight 是否发生战斗
	 */
	public boolean isFight() {
		return isFight;
	}
	/**
	 * 设置是否发生战斗
	 * @param isFight 是否发生战斗
	 */
	public void setFight(boolean isFight) {
		this.isFight = isFight;
	}
	/**
	 * 获取押送的金币总金额
	 * @return totalGold 押送的金币总金额
	 */
	public long getTotalGold() {
		return totalGold;
	}
	/**
	 * 设置押送的金币总金额
	 * @param totalGold 押送的金币总金额
	 */
	public void setTotalGold(long totalGold) {
		this.totalGold = totalGold;
	}
	
	/**
	 * 获取押送的金币剩余数量
	 * @return residueGold 押送的金币剩余数量
	 */
	public long getResidueGold() {
		return residueGold;
	}
	/**
	 * 设置押送的金币剩余数量
	 * @param residueGold 押送的金币剩余数量
	 */
	public void setResidueGold(long residueGold) {
		this.residueGold = residueGold;
	}
	/**
	 * 获取玩家用的押金
	 * @return foregift 玩家用的押金
	 */
	public long getForegift() {
		return foregift;
	}
	/**
	 * 设置玩家用的押金
	 * @param foregift 玩家用的押金
	 */
	public void setForegift(long foregift) {
		this.foregift = foregift;
	}

	/**
	 * 获取用户数据
	 * @return userInfoList 用户数据
	 */
	public List<UserInfo> getUserInfoList() {
		return userInfoList;
	}

	@Override
	public String toString() {
		return "Escort [userInfoList=" + userInfoList + ", types=" + types
				+ ", mapId=" + mapId + ", startDate=" + startDate + ", x=" + x
				+ ", y=" + y + ", foregift=" + foregift
				+ ", totalGold=" + totalGold
				+ ", residueGold=" + residueGold + ", robCount=" + robCount
				+ ", robberyCount=" + robberyCount + ", robGold=" + robGold
				+ ", isFight=" + isFight + ", id=" + id + ", uid=" + uid + "]";
	}

	/**
	 * 获取类型null玩家其他属于电脑押镖模型id
	 * @return types 类型null玩家其他属于电脑押镖模型id
	 */
	public List<Long> getTypes() {
		return types;
	}

	/**
	 * 设置类型null玩家其他属于电脑押镖模型id
	 * @param types 类型null玩家其他属于电脑押镖模型id
	 */
	public void setTypes(List<Long> types) {
		this.types = types;
	}

	/**
	 * 给此队伍所有人发送消息
	 * @param action 发送指令
	 * @param buf
	 */
	public void sendAllData(int action, byte[] buf) {
		sendAllData(new ActionData<>(action, buf));
	}
	
	/**
	 * 给此队伍所有人发送消息
	 * @param action 发送指令
	 * @param arge 自动判断类型包括(Boolean、Integer、Long、byte[]、其余全是String)
	 */
	public void sendAllData(int action, Object ...arge) {
		for (int i = 0; i < userInfoList.size(); i++) {
			userInfoList.get(i).sendData(action, arge);
		}
	}
	
	/**
	 * 给此队伍所有人发送消息
	 * @param action
	 */
	public void sendAllData(ActionData<Object> action) {
		for (int i = 0; i < userInfoList.size(); i++) {
			userInfoList.get(i).sendData(action);
		}
	}

	/**
	 * 获取此押镖的战斗对象
	 * @param isBrainAI 此战斗对象是否是电脑控制战斗
	 * @return
	 */
	public List<FightData> getFightList(boolean isBrainAI) {
		List<FightData> list = new ArrayList<>();
		int len = userInfoList.size();
		FightData fightData;
		UserInfo userInfo;
		for (int i = 0; i < 5; i++) {
			if (len > i) {
				userInfo = userInfoList.get(i);
				fightData = new FightData();
				fightData.setBrainAI(isBrainAI);
				fightData.setUserInfo(userInfo);
				list.add(fightData);
			} else {
				list.add(null);
			}
		}
		//宠物数据
		for (int i = 0; i < 5; i++) {
			if (len > i) {
				userInfo = userInfoList.get(i);
				list.add(userInfo.getPetFightData(isBrainAI));
			} else {
				list.add(null);
			}
		}
		return list;
	}

	/**
	 * 将此押镖押镖信息写入  发送流
	 * @param gameOutput 
	 * @throws IOException 
	 */
	public void writeData(GameOutput gameOutput) throws IOException {
		gameOutput.writeLong(id);
		gameOutput.writeLong(mapId);
		gameOutput.writeBoolean(types== null);
		gameOutput.writeLong(startDate);
		gameOutput.writeInt(x);
		gameOutput.writeInt(y);
		gameOutput.writeLong(residueGold);
		gameOutput.writeLong(totalGold);
		gameOutput.writeInt(robCount);
		gameOutput.writeInt(robberyCount);
		gameOutput.writeLong(robGold);
		gameOutput.writeBoolean(isFight);

		int i = 0;
//		int len = userInfoList.size();
		// gameOutput.writeInt(len);
		// for (int i = 0; i < len; i++) {
		gameOutput.writeUTF(userInfoList.get(i).getRoleName());
		gameOutput.writeLong(userInfoList.get(i).getId());
		gameOutput.writeInt(userInfoList.get(i).getRole().getSex());
		gameOutput.writeInt(userInfoList.get(i).getRole().getMoveSpeed());
		gameOutput.writeInt(userInfoList.get(i).getRole().getStrength());
		// }
	}

	/**
	 * 删除一个玩家
	 * @param userInfo
	 * @return
	 */
	public boolean removeUser(UserInfo userInfo) {
		boolean result = userInfoList.remove(userInfo);
		if (result) {
			
		}
		return result;
	}

	/**
	 * 获取上一次发生战斗时间
	 * @return oldRobFightDate 上一次发生战斗时间
	 */
	public long getOldRobFightDate() {
		return oldRobFightDate;
	}

	/**
	 * 设置上一次发生战斗时间
	 * @param oldRobFightDate 上一次发生战斗时间
	 */
	public void setOldRobFightDate(long oldRobFightDate) {
		this.oldRobFightDate = oldRobFightDate;
	}

	/** 押镖路程结束 */
	public void complete() {
		for (int i = 0; i < userInfoList.size(); i++) {
			userInfoList.get(i).getMissionLibrary().completeEscortMap(mapId);
		}
	}

}
