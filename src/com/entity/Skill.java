package com.entity;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.annotation.XMLParse;
import com.boge.entity.GameOutput;

@SuppressWarnings("serial")
@XMLParse("Skill")
public class Skill extends Prop {
	
	/** 最小目标 */
	private int minTarget;
	/** 最大目标 */
	private int maxTarget;
	/** 耗蓝 */
	private int useMP;
	
	/**
	 * 将数据写入发送流
	 * @param gameOutput
	 */
	public void writeData(GameOutput gameOutput) {
		try {
			gameOutput.writeLong(id);
			gameOutput.writeInt(level);
		} catch (IOException e) {
			Logger.getLogger(getClass()).error("将数据写入发送流报错", e);
		}
	}

	/**
	 * 获取耗蓝
	 * @return useMP 耗蓝
	 */
	public int getUseMP() {
		return useMP;
	}

	/**
	 * 设置耗蓝
	 * @param useMP 耗蓝
	 */
	public void setUseMP(int useMP) {
		this.useMP = useMP;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Skill [minTarget=" + minTarget + ", maxTarget=" + maxTarget + ", useMP=" + useMP + ", count=" + count
				+ ", index=" + index + ", level=" + level + ", type=" + type + ", name=" + name
				+ ", timeline=" + timeline + ", goldPrice=" + goldPrice + ", maxCount=" + maxCount
				+ ", isTrade=" + isTrade + ", isLock=" + isLock + ", buysType=" + buysType + ", id=" + id + ", uid="
				+ uid + "]";
	}

	/**
	 * 获取最小目标
	 * @return minTarget 最小目标
	 */
	public int getMinTarget() {
		return minTarget;
	}

	/**
	 * 获取最大目标
	 * @return maxTarget 最大目标
	 */
	public int getMaxTarget() {
		return maxTarget;
	}


}
