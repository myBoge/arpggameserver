package com.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.boge.entity.GameOutput;

/**
 * 战斗回合数据
 * @author xujinbo
 *
 */
@SuppressWarnings("serial")
public class Round extends Entity {

	/** 行动玩家id */
	private long userId;
	/** 行为 */
	private int action;
	/** 被保护对象uid */
	private long protectUid;
	/** 行动玩家血量行动后值 */
	private int actionHp = -1;
	/** 行动玩家蓝量行动后值 */
	private int actionMp = -1;
	/** 被攻击玩家   id,血量,蓝量 */
	private List<Object> list;
	/** 执行的某些行为是否成功 */
	private boolean success;
	/** 反馈的信息 */
	private int msg;
	
	public Round() {
		list = new ArrayList<Object>();
	}
	
	/**
	 * 添加一个行动
	 * @param args 作用角色id 作用角色血量 作用角色蓝量    一般情况下都是这3个
	 */
	public void addObjct(Object ...args) {
		for (int i = 0; i < args.length; i++) {
			list.add(args[i]);
		}
	}
	
	/**
	 * 获取行动玩家id
	 * @return userId 行动玩家id
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * 设置行动玩家id
	 * @param userId 行动玩家id
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * 获取行为
	 * @return action 行为
	 */
	public int getAction() {
		return action;
	}
	/**
	 * 设置行为
	 * @param action 行为
	 */
	public void setAction(int action) {
		this.action = action;
	}
	/**
	 * 获取行动玩家血量行动后变化
	 * @return actionHp 行动玩家血量行动后变化
	 */
	public int getActionHp() {
		return actionHp;
	}
	/**
	 * 设置行动玩家血量行动后变化
	 * @param actionHp 行动玩家血量行动后变化
	 */
	public void setActionHp(int actionHp) {
		this.actionHp = actionHp;
	}
	/**
	 * 获取行动玩家蓝量行动后变化
	 * @return actionMp 行动玩家蓝量行动后变化
	 */
	public int getActionMp() {
		return actionMp;
	}
	/**
	 * 设置行动玩家蓝量行动后变化
	 * @param actionMp 行动玩家蓝量行动后变化
	 */
	public void setActionMp(int actionMp) {
		this.actionMp = actionMp;
	}
	
	/**
	 * 获取执行的某些行为是否成功
	 * @return success 执行的某些行为是否成功
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * 设置执行的某些行为是否成功
	 * @param success 执行的某些行为是否成功
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	

	/**
	 * 获取反馈的信息
	 * @return msg 反馈的信息
	 */
	public int getMsg() {
		return msg;
	}

	/**
	 * 设置反馈的信息
	 * @param msg 反馈的信息
	 */
	public void setMsg(int msg) {
		this.msg = msg;
	}

	/**
	 * 获取被保护对象uid
	 * @return protectUid 被保护对象uid
	 */
	public long getProtectUid() {
		return protectUid;
	}

	/**
	 * 设置被保护对象uid
	 * @param protectUid 被保护对象uid
	 */
	public void setProtectUid(long protectUid) {
		this.protectUid = protectUid;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Round [userId=" + userId + ", action=" + action + ", protectUid=" + protectUid + ", actionHp="
				+ actionHp + ", actionMp=" + actionMp + ", list=" + list + ", success=" + success + ", msg=" + msg
				+ ", id=" + id + ", uid=" + uid + "]";
	}

	/**
	 * 写入此回合信息
	 * @param gameOutput
	 * @throws IOException
	 */
	public void writeData(GameOutput gameOutput) throws IOException {
		gameOutput.writeInt(action);
		gameOutput.writeLong(userId);
		gameOutput.writeLong(protectUid);
		gameOutput.writeInt(actionHp);
		gameOutput.writeInt(actionMp);
		gameOutput.writeInt(msg);
		gameOutput.writeBoolean(success);
		gameOutput.writeList(list);
	}
	
}
