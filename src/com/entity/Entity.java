package com.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Entity implements Serializable {

	/**
	 * 唯一标识符ID 
	 */
	protected long id;
	/** 根据需要变动的id  不能随意更改 */
	protected long uid;

	public Entity() {
		super();
	}

	public Entity(long id) {
		super();
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entity other = (Entity) obj;
		if (id != other.id)
			return false;
		return true;
	}

	/**
	 * 获取唯一标识符ID
	 * @return id 唯一标识符ID
	 */
	public long getId() {
		return id;
	}

	/**
	 * 设置唯一标识符ID
	 * @param id 唯一标识符ID
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * 获取根据需要变动的id不能随意更改
	 * @return uid 根据需要变动的id不能随意更改
	 */
	public long getUid() {
		return uid;
	}

	/**
	 * 设置根据需要变动的id不能随意更改
	 * @param uid 根据需要变动的id不能随意更改
	 */
	public void setUid(long uid) {
		this.uid = uid;
	}

	@Override
	public String toString() {
		return "Entity [id=" + id + ", uid=" + uid + "]";
	}
	
}
