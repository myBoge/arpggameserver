package com.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.boge.entity.GameOutput;
import com.boge.socket.ActionData;
import com.boge.util.ResourceCell;
import com.data.UserDataManage;
import com.interfaces.IAttributDataStream;
import com.io.EntityInputStream;
import com.io.EntityOutputStream;
import com.thread.SendDataThread;

/**
 * 帮派
 * @author xujinbo
 */
public class Faction implements IAttributDataStream {

	/** 创建人id */
	private long userId;
	/** 家族名字 */
	private String name;
	/** 家族等级 */
	private int level;
	/** 资金 */
	private int capital;
	/** 繁荣度 */
	private int boom;
	/** 成员 */
	private byte[] member;
	/** 聚宝盆等级 */
	private int treasure;
	/** 镇妖塔等级 */
	private int demonTower;
	/** 奇士府等级 */
	private int skill;
	/** 公告 */
	private String notice;
	/** 职位 */
	private byte[] job;
	/** 创建者信息 */
	private UserInfo userInfo;
	/** 帮派成员信息 */
	private List<UserInfo> memberList;
	
	public Faction(long userId) {
		this.userId = userId;
	}
	
	public Faction() {
		super();
		memberList = new ArrayList<>();
	}

	/**
	 * 获取创建人id
	 * @return userId 创建人id
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * 设置创建人id
	 * @param userId 创建人id
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * 获取家族名字
	 * @return name 家族名字
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置家族名字
	 * @param name 家族名字
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取家族等级
	 * @return level 家族等级
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * 设置家族等级
	 * @param level 家族等级
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * 获取资金
	 * @return capital 资金
	 */
	public int getCapital() {
		return capital;
	}

	/**
	 * 设置资金
	 * @param capital 资金
	 */
	public void setCapital(int capital) {
		this.capital = capital;
	}

	/**
	 * 获取繁荣度
	 * @return boom 繁荣度
	 */
	public int getBoom() {
		return boom;
	}

	/**
	 * 设置繁荣度
	 * @param boom 繁荣度
	 */
	public void setBoom(int boom) {
		this.boom = boom;
	}

	/**
	 * 获取成员
	 * @return member 成员
	 */
	public byte[] getMember() {
		return member;
	}

	/**
	 * 设置成员
	 * @param member 成员
	 */
	public void setMember(byte[] member) {
		this.member = member;
	}

	/**
	 * 获取聚宝盆等级
	 * @return treasure 聚宝盆等级
	 */
	public int getTreasure() {
		return treasure;
	}

	/**
	 * 设置聚宝盆等级
	 * @param treasure 聚宝盆等级
	 */
	public void setTreasure(int treasure) {
		this.treasure = treasure;
	}

	/**
	 * 获取镇妖塔等级
	 * @return demonTower 镇妖塔等级
	 */
	public int getDemonTower() {
		return demonTower;
	}

	/**
	 * 设置镇妖塔等级
	 * @param demonTower 镇妖塔等级
	 */
	public void setDemonTower(int demonTower) {
		this.demonTower = demonTower;
	}

	/**
	 * 获取奇士府等级
	 * @return skill 奇士府等级
	 */
	public int getSkill() {
		return skill;
	}

	/**
	 * 设置奇士府等级
	 * @param skill 奇士府等级
	 */
	public void setSkill(int skill) {
		this.skill = skill;
	}

	/**
	 * 获取公告
	 * @return notice 公告
	 */
	public String getNotice() {
		return notice;
	}

	/**
	 * 设置公告
	 * @param notice 公告
	 */
	public void setNotice(String notice) {
		this.notice = notice;
	}

	/**
	 * 获取职位
	 * @return job 职位
	 */
	public byte[] getJob() {
		return job;
	}

	/**
	 * 设置职位
	 * @param job 职位
	 */
	public void setJob(byte[] job) {
		this.job = job;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Faction [userId=" + userId + ", name=" + name + ", level=" + level + ", capital=" + capital + ", boom="
				+ boom + ", member=" + Arrays.toString(member) + ", treasure=" + treasure + ", demonTower=" + demonTower
				+ ", skill=" + skill + ", notice=" + notice + ", job=" + Arrays.toString(job) + ", userInfo=" + userInfo
				+ ", memberList=" + memberList + "]";
	}

	/**
	 * 获取创建者信息
	 * @return userInfo 创建者信息
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * 设置创建者信息
	 * @param userInfo 创建者信息
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * 获取帮派成员信息
	 * @return memberList 帮派成员信息
	 */
	public List<UserInfo> getMemberList() {
		return memberList;
	}

	/**
	 * 将此帮派基础信息写入发送流
	 * @param gameOutput
	 * @throws IOException 
	 */
	public void writeBaseData(GameOutput gameOutput) throws IOException {
		gameOutput.writeLong(userId);
		gameOutput.writeUTF(name);
		gameOutput.writeInt(level);
		gameOutput.writeUTF(notice);
		gameOutput.writeUTF(userInfo.getRoleName());
		gameOutput.writeInt(userInfo.getRole().getLevel());
		gameOutput.writeInt(userInfo.getRole().getSex());
		gameOutput.writeInt(userInfo.getOnlineState());
		gameOutput.writeInt(memberList.size());
	}
	
	/** 写入完整信息 
	 * @throws IOException */
	public void writeData(GameOutput gameOutput) throws IOException {
		writeBaseData(gameOutput);
		gameOutput.writeInt(capital);
		gameOutput.writeInt(boom);
		gameOutput.writeInt(treasure);
		gameOutput.writeInt(demonTower);
		gameOutput.writeInt(skill);
		UserInfo info;
		for (int i = 0; i < memberList.size(); i++) {
			info = memberList.get(i);
			writeUser(info, gameOutput);
		}
	}
	
	/**
	 * 将此用户写入发送流
	 * @param info
	 * @param gameOutput
	 * @throws IOException
	 */
	public void writeUser(UserInfo info, GameOutput gameOutput) throws IOException {
		gameOutput.writeLong(info.getId());
		gameOutput.writeUTF(info.getRoleName());
		gameOutput.writeInt(info.getRole().getLevel());
		gameOutput.writeInt(info.getOnlineState());
	}

	@Override
	public void setAttributeData(EntityInputStream ois) throws IOException {
		UserDataManage dataManage = ResourceCell.getResource(UserDataManage.class);
		int len = ois.readInt();
		UserInfo info;
		for (int i = 0; i < len; i++) {
			info = dataManage.getUserInfoData(ois.readLong());
			if (info != null) {
				memberList.add(info);
			}
		}
	}

	@Override
	public void getAttributeData(EntityOutputStream oos) throws IOException {
		int len = memberList.size();
		oos.writeInt(len);
		UserInfo info;
		for (int i = 0; i < len; i++) {
			info = memberList.get(i);
			if (info != null) {
				oos.writeLong(info.getId());
			}
		}
	}

	/**
	 * 给帮派中所有成员发送消息
	 * @param factionAdd
	 * @param byteArray
	 */
	public void sendData(int action) {
		SendDataThread dataThread = ResourceCell.getResource(SendDataThread.class);
		ActionData<Object> actionData = new ActionData<>(action);
		UserInfo info;
		for (int i = 0; i < memberList.size(); i++) {
			info = memberList.get(i);
			if (info != null) {
				dataThread.addActionData(actionData, info);
			}
		}
	}
	
	/**
	 * 给帮派中所有成员发送消息
	 * @param factionAdd
	 * @param byteArray
	 */
	public void sendData(int action, byte[] buf) {
		SendDataThread dataThread = ResourceCell.getResource(SendDataThread.class);
		ActionData<Object> actionData = new ActionData<>(action, buf);
		UserInfo info;
		for (int i = 0; i < memberList.size(); i++) {
			info = memberList.get(i);
			if (info != null) {
				dataThread.addActionData(actionData, info);
			}
		}
	}

	/**
	 * 从帮派中删除此成员
	 * @param id
	 * @return 
	 */
	public UserInfo removeUser(long id) {
		for (int i = 0; i < memberList.size(); i++) {
			if (memberList.get(i).getId() == id) {
				return memberList.remove(i);
			}
		}
		return null;
	}
	
	/**
	 * 检查帮派中是否存在此成员
	 * @param user
	 * @return 
	 */
	public boolean checkExist(UserInfo user) {
		for (int i = 0; i < memberList.size(); i++) {
			if (memberList.get(i).getId() == user.getId()) {
				return true;
			}
		}
		return false;
	}

}
