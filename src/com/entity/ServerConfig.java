package com.entity;

import com.annotation.XMLParse;

@XMLParse("serverConfig")
public class ServerConfig {

	/** 当前是否是debug模式 */
	private boolean debug;
	/** 服务器端口 */
	private int port;
	/** log4j配置路径 */
	private String log4jPath;
	/** 连接并获取数据库配置路径 */
	private String sqlResource;
	
	public ServerConfig() {
		super();
	}

	/**
	 * 获取当前是否是debug模式
	 * @return debug 当前是否是debug模式
	 */
	public boolean isDebug() {
		return debug;
	}

	/**
	 * 设置当前是否是debug模式
	 * @param debug 当前是否是debug模式
	 */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	/**
	 * 获取服务器端口
	 * @return port 服务器端口
	 */
	public int getPort() {
		return port;
	}

	/**
	 * 设置服务器端口
	 * @param port 服务器端口
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * 获取log4j配置路径
	 * @return log4jPath log4j配置路径
	 */
	public String getLog4jPath() {
		return log4jPath;
	}

	/**
	 * 设置log4j配置路径
	 * @param log4jPath log4j配置路径
	 */
	public void setLog4jPath(String log4jPath) {
		this.log4jPath = log4jPath;
	}

	/**
	 * 获取连接并获取数据库配置路径
	 * @return sqlResource 连接并获取数据库配置路径
	 */
	public String getSqlResource() {
		return sqlResource;
	}

	/**
	 * 设置连接并获取数据库配置路径
	 * @param sqlResource 连接并获取数据库配置路径
	 */
	public void setSqlResource(String sqlResource) {
		this.sqlResource = sqlResource;
	}

	@Override
	public String toString() {
		return "ServerConfig [debug=" + debug + ", port=" + port
				+ ", log4jPath=" + log4jPath + ", sqlResource=" + sqlResource
				+ "]";
	}
	
}
