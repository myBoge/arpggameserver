package com.entity;

import java.io.IOException;

import com.boge.entity.GameOutput;

/**
 * 装备属性块
 * @author xujinbo
 *
 */
public class Attribute {

	/** 武器可生成的类型 */
	public static int[] weaponType = {1,2,3,4,5};
	/** 防具可生成的类型 */
	public static int[] armorType = {1,2,3,4};
	
	/** 属性类型 1 体力 2灵力  3力量  4速度 5 伤害        */
	private int type;
	/** 属性值 */
	private int value;
	/**
	 * 获取属性类型1 体力 2灵力  3力量  4速度 5 伤害  
	 * @return type 属性类型1 体力 2灵力  3力量  4速度 5 伤害  
	 */
	public int getType() {
		return type;
	}
	/**
	 * 设置属性类型1 体力 2灵力  3力量  4速度 5 伤害  
	 * @param type 属性类型1 体力 2灵力  3力量  4速度 5 伤害  
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * 获取属性值
	 * @return value 属性值
	 */
	public int getValue() {
		return value;
	}
	/**
	 * 设置属性值
	 * @param value 属性值
	 */
	public void setValue(int value) {
		this.value = value;
	}
	
	public void writeData(GameOutput gameOutput) throws IOException {
		gameOutput.writeInt(type);
		gameOutput.writeInt(value);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Attribute [type=" + type + ", value=" + value + "]";
	}
	
}
