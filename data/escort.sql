DROP DATABASE IF EXISTS escort;
CREATE DATABASE escort;
USE escort;

CREATE TABLE test (
  id int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (Id)
) DEFAULT CHARSET=utf8;

CREATE TABLE server_info (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(20) DEFAULT NULL unique COMMENT '服务器名字',
  state int(1) DEFAULT 0 COMMENT '服务器状态 0关闭 1开启 2内测',
  openDate datetime default '0000-00-00 00:00:00' COMMENT '首次开服时间',
  currentlyOpenDate datetime default '0000-00-00 00:00:00' COMMENT '本次开服时间',
  todayData datetime default '0000-00-00 00:00:00' COMMENT '今天时间',
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

INSERT INTO server_info(name,state) VALUES ('测试一服',0);

CREATE TABLE login (
  id int(11) NOT NULL AUTO_INCREMENT,
  openid varchar(33) DEFAULT NULL unique COMMENT '角色OpenId',
  serverId int(11) NOT NULL COMMENT '服务器ID',
  PRIMARY KEY (id),
  FOREIGN KEY(serverId) REFERENCES server_info(id)
) DEFAULT CHARSET=utf8;


CREATE TABLE user_info (
  id int(11) NOT NULL AUTO_INCREMENT,
  loginId int(11) unique COMMENT '登录表ID',
  roleName varchar(6) NOT NULL unique COMMENT '昵称',
  camp int(11) DEFAULT 1 COMMENT '阵营',
  mood varchar(255) NOT NULL DEFAULT '天下之大，可有吾容身之处' COMMENT '心情',
  playerGold int(11) DEFAULT 0 COMMENT '金币',
  lockGold int(11) DEFAULT 2000 COMMENT '绑定金币',
  rmb int(11) DEFAULT 0 COMMENT '人民币',
  lockrmb int(11) DEFAULT 2000 COMMENT '绑定人民币',
  loginDate datetime default '0000-00-00 00:00:00' COMMENT '上次登录时间',
  offlineDate datetime default '0000-00-00 00:00:00' COMMENT '上次离线时间',
  bytes blob COMMENT '角色信息',
  backpackByte blob COMMENT '包裹',
  currentScene int(11) NOT NULL COMMENT '用户当前所在场景',
  currentSceneValue int(11) NOT NULL COMMENT '当前场景附带值',
  isAutoUseSupply int(11) DEFAULT 1 COMMENT '是否自动使用补给',
  friendByte blob COMMENT '好友数据',
  addFriendByte blob COMMENT '加好友请求',
  mailByte blob COMMENT '邮件',
  skillByte blob COMMENT '技能',
  potential bigint(11) COMMENT '潜能',
  petByte blob COMMENT '宠物',
  equipsByte blob COMMENT '装备',
  factionId int(11) DEFAULT '0' COMMENT '帮派创建人id',
  getGiftDataByte blob COMMENT '已领取的礼包数据',
  warehouseByte blob COMMENT '仓库',
  buffByte blob COMMENT '正在使用中的buff道具',
  taskByte blob COMMENT '拥有的任务',
  onLineDate int(11) DEFAULT 0 COMMENT '今日在线时间 秒',
  onLineGiftCount int(11) DEFAULT 0 COMMENT '在线礼包领取次数',
  currentSceneStartX int(11) DEFAULT 0 COMMENT '当前场景起始X坐标',
  currentSceneStartY int(11) DEFAULT 0 COMMENT '当前场景起始Y坐标',
  missionRecord blob COMMENT '剧情任务保存记录',
  rechargeRecord blob COMMENT '充值记录',
  PRIMARY KEY (id),
  FOREIGN KEY(loginId) REFERENCES login(id)
) DEFAULT CHARSET=utf8;

CREATE TABLE escort (
  id int(11) NOT NULL AUTO_INCREMENT,
  mapid varchar(255) NOT NULL COMMENT '押镖地图id',
  roles blob COMMENT '押镖用户数据',
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE report (
  id int(11) NOT NULL AUTO_INCREMENT,
  bytes mediumblob NOT NULL COMMENT '提交的问题二进制数据',
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE ranking (
  id int(11) NOT NULL AUTO_INCREMENT,
  roleLevelRank blob,
  roleOrdinaryRank blob,
  roleMagicRank blob,
  roleSpeedRank blob,
  roleDefendRank blob,
  petOrdinaryRank blob,
  petMagicRank blob,
  petSpeedRank blob,
  petDefendRank blob,
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;


CREATE TABLE faction (
  userId int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  name varchar(10) DEFAULT NULL COMMENT '家族名字',
  level int(11) NOT NULL DEFAULT '1' COMMENT '家族等级',
  capital int(11) NOT NULL DEFAULT '0' COMMENT '资金',
  boom int(11) NOT NULL DEFAULT '0' COMMENT '繁荣度',
  member blob COMMENT '成员',
  treasure int(11) NOT NULL DEFAULT '0' COMMENT '聚宝盆等级',
  demonTower int(11) NOT NULL DEFAULT '0' COMMENT '镇妖塔等级',
  skill int(11) NOT NULL DEFAULT '0' COMMENT '奇士府等级',
  notice varchar(255) DEFAULT NULL COMMENT '公告',
  job blob COMMENT '职位',
  FOREIGN KEY(userId) REFERENCES user_info(id)
) DEFAULT CHARSET=utf8;

-- 礼包
CREATE TABLE gift (
  id int(11) NOT NULL AUTO_INCREMENT,
  giftType int(11) DEFAULT '0' unique COMMENT '礼包类型',
  bytes blob COMMENT '礼包数据',
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

-- 兑换码
CREATE TABLE redeem_code (
  id int(11) NOT NULL AUTO_INCREMENT,
  redeemType int(11) DEFAULT '0' unique COMMENT '兑换码类型',
  giftId int(11) unique COMMENT '礼包Id',
  bytes blob COMMENT '兑换码数据',
  PRIMARY KEY (id),
  FOREIGN KEY(giftId) REFERENCES gift(id)
) DEFAULT CHARSET=utf8;

-- 竞技场
CREATE TABLE arena (
  id int(11) NOT NULL AUTO_INCREMENT,
  roleBytes blob COMMENT '玩家挑战时候的数据',
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;