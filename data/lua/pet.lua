Logger:info("宠物脚本已启动!")

--检查此宠物是否参加战斗
function checkPetWar(strength)
	if strength < 70 then
		return RandomUtils:nextBoolean(strength+10)
	end
	return true
end

--给此宠物使用道具
function petUseProp(userInfo, role, gameOutput, useProp)
	if useProp:getId() == 2007 or useProp:getId() == 2011 then
		local value = 100-role:getStrength()
		if value > 0 then
			if useProp:getUseCount() > value then
				role:setStrength(100)
				useProp:setUseCount(useProp:getUseCount()-value)
			else
				role:setStrength(role:getStrength()+useProp:getUseCount())
				useProp:setUseCount(0)
			end
			if useProp:getUseCount() ==0 then
				--删除物品
				userInfo:getBackpack():removePropIndexCount(useProp:getIndex(), 1)
				gameOutput:reset():writeInt(1):writeInt(useProp:getIndex()):writeInt(1)
				userInfo:sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput:toByteArray())
			else
				--更新物品信息
				gameOutput:reset()
				gameOutput:writeInt(useProp:getIndex())
				useProp:writeData(gameOutput)
				userInfo:sendData(SocketCmd.UPDATE_PROP_INFO, gameOutput:toByteArray())
			end
			--更新宠物忠诚度
			gameOutput:reset():writeLong(role:getUid()):writeInt(role:getStrength())
			userInfo:sendData(SocketCmd.PET_TAME, gameOutput:toByteArray())
			return true
		end
	elseif useProp:getId() == 2018 then
		if role:isBaby() then
			if (role:getLevel()+15)>=useProp:getLevel() then
				local value = useProp:getLevel()*2*100
				role:setExperience(role:getExperience()+value)
				GameData:userPetUpLevel(userInfo, role)

				userInfo:getBackpack():removePropIndexCount(useProp:getIndex(), 1)
				gameOutput:reset():writeInt(1):writeInt(useProp:getIndex()):writeInt(1)
				userInfo:sendData(SocketCmd.UPDATE_PROP_COUNT, gameOutput:toByteArray())
			else
				gameOutput:reset():writeInt(CommonCmd.PET_LEVEL_LOW)
				userInfo:sendData(SocketCmd.PROMPT_MESSAGE, gameOutput:toByteArray())
			end
		else
			gameOutput:reset():writeInt(CommonCmd.PET_NOT_BABY)
			userInfo:sendData(SocketCmd.PROMPT_MESSAGE, gameOutput:toByteArray())
		end
		return true
	end
	return false
end