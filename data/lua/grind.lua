Logger:info("练级地图脚本已启动!")

-- 计算此地图经验
function castGrindExp(grindMap, robList)
	local get_levels = grindMap:get_levels()
	local userInfo
	local exp = 0
	for i=0,robList:size()-1,1 do
		if robList:get(i)~=null and robList:get(i):isQuiltCatch()==false then
			userInfo = robList:get(i):getUserInfo()
			-- 经验 怪物等级*5
			exp = exp+userInfo:getRole():getLevel()*5
			--print("等级"..userInfo:getRole():getLevel())
		end
	end
	return exp
end

-- 战斗结束  经验获得计算
function grindFightEnd(userInfo, grindMap, totalExp)
	if totalExp<1 then
		return 0
	end
	local get_levels = grindMap:get_levels()
	local min = get_levels[1]
	local max = get_levels[2]
	local level = userInfo:getRole():getLevel()
	--等级低于最低等级
	if level<min then
		return 1
	end
	--等级高于最大等级5级
	if (level-5)>max then
		return 1
	end

	local cha
	if level>=min and level<=max then
		return totalExp
	else
		cha = level-max
		cha = totalExp/cha
		if cha<1 then
			cha = 1
		end
		return cha
	end
end