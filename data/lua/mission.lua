Logger:info("剧情脚本已启动!")

--完成一个剧情
function missionComplete(userInfo, missionTask)
	local id = missionTask:getId()
	local giveList = missionTask:getGive()
	local gameOutput = getGameOutput()
	local backpack = userInfo:getBackpack()
	local prop
	local giveType = 0


	--发送完成剧情任务
	userInfo:getMissionLibrary():deleteMission(id)

	gameOutput:reset():writeLong(id)
	userInfo:sendData(SocketCmd.MISSION_COMPLETE, gameOutput:toByteArray())
	--发放奖励
	if id==1 then
		--创建新手物品
		for i=1,2,1 do
			prop = createPropAndBackpack(backpack, 1001, 50)
			gameOutput:reset():writeInt(1)
			prop:writeData(gameOutput)
			userInfo:sendData(SocketCmd.ADD_NEW_PROP, gameOutput:toByteArray())
		end

		gameOutput:reset():writeInt(2):writeLong(1001):writeInt(100)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())

		for i=1,2,1 do
			prop = createPropAndBackpack(backpack, 1006, 30)
			gameOutput:reset():writeInt(1)
			prop:writeData(gameOutput)
			userInfo:sendData(SocketCmd.ADD_NEW_PROP, gameOutput:toByteArray())
		end

		gameOutput:reset():writeInt(2):writeLong(1006):writeInt(60)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())

		prop = createPropAndBackpack(backpack, 1029, 50)
		gameOutput:reset():writeInt(1)
		prop:writeData(gameOutput)
		userInfo:sendData(SocketCmd.ADD_NEW_PROP, gameOutput:toByteArray())

		gameOutput:reset():writeInt(2):writeLong(1029):writeInt(50)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())

	elseif id==3 then
		--赠送装备
		-- 送一件新手武器装备
		prop = createPropAndBackpack(backpack, 3001, 1)
		gameOutput:reset():writeInt(1)
		prop:writeData(gameOutput)
		userInfo:sendData(SocketCmd.ADD_NEW_PROP, gameOutput:toByteArray())
		-- 送一件新手头盔装备
		prop = createPropAndBackpack(backpack, 3101, 1)
		gameOutput:reset():writeInt(1)
		prop:writeData(gameOutput)
		userInfo:sendData(SocketCmd.ADD_NEW_PROP, gameOutput:toByteArray())
		-- 送一件新手衣服装备
		prop = createPropAndBackpack(backpack, 3201, 1)
		gameOutput:reset():writeInt(1)
		prop:writeData(gameOutput)
		userInfo:sendData(SocketCmd.ADD_NEW_PROP, gameOutput:toByteArray())
		-- 送一件新手鞋子装备
		prop = createPropAndBackpack(backpack, 3301, 1)
		gameOutput:reset():writeInt(1)
		prop:writeData(gameOutput)
		userInfo:sendData(SocketCmd.ADD_NEW_PROP, gameOutput:toByteArray())

		gameOutput:reset():writeInt(2):writeLong(3001):writeInt(1)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
		gameOutput:reset():writeInt(2):writeLong(3101):writeInt(1)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
		gameOutput:reset():writeInt(2):writeLong(3201):writeInt(1)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
		gameOutput:reset():writeInt(2):writeLong(3301):writeInt(1)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
	end

	local missionBlock
	for i=0,giveList:size()-1 do
		missionBlock = giveList:get(i)
		giveType = missionBlock:getType()
		if giveType==100 then--奖励道具

		elseif giveType==101 then--奖励游戏币
			userInfo:setPlayerGold(userInfo:getPlayerGold()+missionBlock:getValue())
			gameOutput:reset():writeLong(userInfo:getPlayerGold()):writeLong(userInfo:getLockGold())
			userInfo:sendData(SocketCmd.UPDATE_GOLD, gameOutput:toByteArray())
		elseif giveType==102 then--奖励经验
			userInfo:getRole():setExperience(userInfo:getRole():getExperience()+missionBlock:getValue())
			GameData:userInfoUpLevel(userInfo)
		elseif giveType==103 then--奖励任务
			local mission = userInfo:getMissionLibrary():addMission(missionBlock:getValue())
			if mission~=null then
				gameOutput:reset()
				mission:writeData(gameOutput)
				userInfo:sendData(SocketCmd.MISSION_ADD, gameOutput:toByteArray())
			end
		elseif giveType==104 then--奖励属性

		elseif giveType==105 then--奖励技能

		end
	end

	--奖励发放完成

end


