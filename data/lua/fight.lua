Logger:info("战斗脚本已启动!")


-- 竞技场电脑攻击自定义判断
function brainAiArenaFight(fightData)
	-- 计算危险血量
	local minBlood = fightData:getUserInfo():getRole():getMaxBlood()*0.3
	--print("危险血量"..minBlood)
	if fightData:getBlood()<=minBlood then
		return 5
	end
	local role = fightData:getUserInfo():getRole()
	--如果是宠物  并且 等级小于20级
	if role:isPet() and role:getLevel()<20 then
		return 1
	end
	local len = role:getSkillId():size()
	local pre = 20
	if role:getJiadianMp()>role:getJiadianAp() then
		pre = 80
	end
	local boo = RandomUtils:nextBoolean(pre)
	if len>0 and boo then
		return 2
	end
	return 1
end

-- 竞技场电脑使用加血道具
function brainAiArenaProp(fightData)
	local level = fightData:getUserInfo():getRole():getLevel()
	local id = 1003
	if level<10 then
		id=1003
	elseif level>=10 and level<20 then
		id=1004
	elseif level>=20 and level<30 then
		id=1005
	elseif level>=30 and level<40 then
		id=1006
	elseif level>=40 and level<50 then
		id=1007
	elseif level>=50 and level<60 then
		id=1008
	elseif level>=60 and level<70 then
		id=1009
	elseif level>=70 and level<80 then
		id=1010
	elseif level>=80 and level<90 then
		id=1013
	elseif level>=90 and level<100 then
		id=1015
	end
	return id
end





function brainAiFight(userInfo)
	local role = userInfo:getRole()
	local len = role:getSkillId():size()
	local pre = 20
	if role:getJiadianMp()>role:getJiadianAp() then
		pre = 80
	end
	local boo = RandomUtils:nextBoolean(pre)
	if len>0 and boo then
		return 2
	end
	return 1
end

-- 电脑技能攻击获取技能id
function getAiSkillId(userInfo)
	local role = userInfo:getRole()
	local skillList = role:getSkillId()
	if role:getLevel()<40 then
		return skillList:get(0)
	elseif role:getLevel()>=40 and role:getLevel()<=60 then
		return skillList:get(1)
	end
	return skillList:get(2)
end

-- 计算物理伤害值  被攻击者是否处于防御状态    是否暴击   攻击者攻击值  防御者防御值
function ordinaryAttack(isDefense, isCrit, attackValue, defenseValue)
	local value
	if isCrit then
		if isDefense then
			value = attackValue*2-defenseValue
		else
			value = attackValue*2-defenseValue/2
		end
	else
		if isDefense then
			value = attackValue*1.5-defenseValue
		else
			value = attackValue*1.5-defenseValue/2
		end
	end
	if value<=0 then
		value = 1;
	end
	return value;
end

-- 计算法术伤害值  被攻击者是否处于防御状态    攻击者  被攻击者
function magicAttack(isDefense, userInfo, passiveUserInfo, skill)
	local role = userInfo:getRole()
	
	--print(role:getPointMp())
	--print(role:getXiangxingJin())
	--print(role:getZhuangbeiAp())
	local ratio = 1+role:getXiangxingJin()/80
	--print("系数="..ratio)
	
	local value = (role:getPointMp()*5*ratio+role:getZhuangbeiAp())+skill:getLevel()*10
	--print("计算结果="..value)
	--print("是否防御="..tostring(isDefense))
	--print("防御值="..(passiveUserInfo:getRole():getDefense()/2))
	if isDefense then
		value = value-passiveUserInfo:getRole():getDefense()
	else
		value = value-passiveUserInfo:getRole():getDefense()/2
	end
	return value;
end


--战斗动作


--逃跑
function fightFlee(fightBox, fightData)
	local userInfo = fightData:getUserInfo()
	local fleeSuccess = RandomUtils:nextBoolean(60)
	if fleeSuccess then
		userInfo:setCurrentFightId(0)
		--获取宠物
		local petFightData = fightBox:getUserPet(fightData)
		if petFightData~=null then
			fightBox:removeFightData(petFightData)
		end
		fightBox:removeFightData(fightData)
		local team = teamServer:getTeam(userInfo:getTeamId())
		if team~=null then
			teamServer:removeTeamUser(userInfo, team)
		end
		userInfo:sendData(SocketCmd.FIGHT_NOT_RESULT, null)
		local gameOutput = getGameOutput()
		gameOutput:writeBoolean(false):writeUTF(userInfo:getRoleName())
		userInfo:sendData(SocketCmd.GRIND_UPDATE_STATE, gameOutput:toByteArray())
	end
	return fleeSuccess
end








--战斗结束  死亡玩家惩罚
function fightUserDeath(userInfo)
	local prop = userInfo:getBuffLibrary():getPropId(tianshenbaoyou)
	if prop ~= null then
		userInfo:getBuffLibrary():removeProp(tianshenbaoyou)
		userServer:updateBuffByte(userInfo)
		local gameOutput = getGameOutput()
		gameOutput:writeLong(prop:getId())
		userInfo:sendData(SocketCmd.DELETE_BUFF_PROP, gameOutput:toByteArray())
	else
		local role = userInfo:getRole()
		--减少经验
		local exp = role:getExperience()
		local totalExp = GameData:getRoleNeedExp(role:getLevel())
		local needExp = 1000
		if totalExp > 0 then
			needExp = totalExp/10+RandomUtils:nextInt(1*role:getLevel(), 3*role:getLevel())
		end
		exp = exp-needExp
		if exp < 0 then
			exp = 0
		end
		role:setExperience(exp)
		local gameOutput = getGameOutput()
		gameOutput:writeLong(exp)
		userInfo:sendData(SocketCmd.UPDATE_EXP, gameOutput:toByteArray())
		--减少钱
		local gold = userInfo:getPlayerGold()
		local needGold = role:getLevel()*100
		if gold<=needGold then
			userInfo:setPlayerGold(0)
		else
			userInfo:setPlayerGold(gold-needGold)
		end
		gameOutput:reset():writeLong(userInfo:getPlayerGold()):writeLong(userInfo:getLockGold())
		userInfo:sendData(SocketCmd.UPDATE_GOLD, gameOutput:toByteArray())

		gameOutput:reset()
		gameOutput:writeInt(13)
		gameOutput:writeLong(needExp)
		gameOutput:writeLong(needGold)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
	end
end

