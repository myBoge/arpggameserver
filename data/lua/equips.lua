Logger:info("装备处理脚本已启动!")

--计算改造所需要消耗的钱
function castReformGold(userInfo, equips)
	--一次改造所需要消耗的钱
	local gold = 30600
	if equips:getLevel() == 0 then
		gold = (equips:getReformCount()+1)*30600
	elseif equips:getLevel() == 1 then
		gold = (equips:getReformCount()+1)*78900
	elseif equips:getLevel() == 2 then
		gold = (equips:getReformCount()+1)*102900
	end
	return gold
end

--计算合成所需要消耗的钱
function composeGold(userInfo, equips)
	--一次合成所需要消耗的钱
	local gold = 15000
	if equips:getUseLevel()==30 then
		gold = 15000
	elseif equips:getUseLevel() == 50 then
		gold = 25000
	elseif equips:getUseLevel() == 60 then
		gold = 30000
	elseif equips:getUseLevel() == 70 then
		gold = 35000
	elseif equips:getUseLevel() == 80 then
		gold = 46000
	elseif equips:getUseLevel() == 90 then
		gold = 62400
	elseif equips:getUseLevel() == 100 then
		gold = 84200
	elseif equips:getUseLevel() == 110 then
		gold = 108000
	elseif equips:getUseLevel() == 120 then
		gold = 205000
	elseif equips:getUseLevel() == 130 then
		gold = 480000
	end
	return gold
end
-- 给武器或防具添加3条特殊属性
function qeuipsRandomAttribute(equips)
	local attribute
	if equips:getReformCount()>0 then
		for i=1,3 do
			attribute = luajava.newInstance("com.entity.Attribute")
			if equips:getType()==1 then
				attribute:setType(Attribute.weaponType[RandomUtils:nextInt(Attribute.weaponType.length)+1])
			else
				attribute:setType(Attribute.armorType[RandomUtils:nextInt(Attribute.armorType.length)+1])
			end
			if attribute:getType()==5 then
				attribute:setValue(RandomUtils:nextInt(100, 1501))
			else
				attribute:setValue(RandomUtils:nextInt(1, 17))
			end
			equips:getAttributesList():add(attribute)
		end
	end
end