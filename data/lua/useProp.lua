Logger:info("物品使用脚本已启动!")

--使用加血加蓝的物品    物品 得到此物品效果的角色  返回:是否要从包裹中删除物品,使用物品后,变动的血、蓝值，非最终值，而是累加值
function useBloodAndMagicProp(useProp, userInfo, role)
	local valueType = useProp:getValueType() -- 值类型 0数值  1百分比
	local tempBlood = 0
	local tempMagic = 0
	-- 根据是否满足    取值
	local value
	if useProp:getUseLevel() <= role:getLevel() then
		value = useProp:getValue()
	else
		value = useProp:getUnmetValue()
	end
	local needBloood = role:getMaxBlood()-role:getBlood()--计算需要补给的血量
	local needMagic = role:getMaxMagic()-role:getMagic()--计算需要补给的蓝量
	local isDeleteProp = true
	if useProp:getType() == 2 then--恢复血
		if useProp:getId() == 1053 or useProp:getId() == 1055 or useProp:getId() == 1057 then
			if useProp:getUseCount()>needBloood then
				tempBlood = needBloood
				useProp:setUseCount(useProp:getUseCount()-needBloood)
				local gameOutput = getGameOutput()
				gameOutput:writeInt(useProp:getIndex())
				useProp:writeData(gameOutput)
				userInfo:sendData(SocketCmd.UPDATE_PROP_INFO, gameOutput:toByteArray())
				gameOutput:close()
				isDeleteProp = false
			else  
				tempBlood = useProp:getUseCount()
				useProp:setUseCount(0)
			end
		else
			-- 使用值类型
			if valueType == 0 then
				--数值
				tempBlood = value
			else  
				-- 百分比
				tempBlood = role:getMaxBlood()*(value/100)
			end
			if (tempBlood+role:getBlood()) > role:getMaxBlood() then
				tempBlood = role:getMaxBlood()-role:getBlood()
			end
		end
	elseif useProp:getType() == 3 then-- 恢复蓝
		if useProp:getId() == 1054 or useProp:getId() == 1056 or useProp:getId() == 1058 then
			if useProp:getUseCount()>needMagic then
				tempMagic = needMagic
				useProp:setUseCount(useProp:getUseCount()-needMagic)
				local gameOutput = getGameOutput()
				gameOutput:writeInt(useProp:getIndex())
				useProp:writeData(gameOutput)
				userInfo:sendData(SocketCmd.UPDATE_PROP_INFO, gameOutput:toByteArray())
				gameOutput:close()
				isDeleteProp = false
			else  
				tempMagic = useProp:getUseCount()
				useProp:setUseCount(0)
			end
		else
			if valueType == 0 then --数值
				tempMagic = value
			else -- 百分比
				tempMagic = role:getMaxMagic()*(value/100)
			end
			if (tempMagic+role:getMagic()) > role:getMaxMagic() then
				tempMagic = role:getMaxMagic()-role:getMagic()
			end
		end
	elseif useProp:getType() == 4 then-- 恢复血和蓝
		if valueType == 0 then  --数值
			tempBlood = value
			tempMagic = value
		else -- 百分比
			tempBlood = role:getMaxBlood()*(value/100)
			tempMagic = role:getMaxMagic()*(value/100)
		end
		if (tempBlood+role:getBlood()) > role:getMaxBlood() then
			tempBlood = role:getMaxBlood()-role:getBlood()
		end
		if (tempMagic+role:getMagic()) > role:getMaxMagic() then
			tempMagic = role:getMaxMagic()-role:getMagic()
		end
	end
	role:setBlood(tempBlood+role:getBlood())
	role:setMagic(tempMagic+role:getMagic())
	-- --是否要从包裹中删除物品
	-- if isDeleteProp then
	-- 	local gameOutput = getGameOutput()
	-- 	gameOutput:writeInt(1)
	-- 	gameOutput:writeInt(useProp:getIndex())
	-- 	gameOutput:writeInt(1)
	-- 	userInfo:sendData(SocketCmd.UPDATE_PROP_INFO, gameOutput:toByteArray())
	-- 	gameOutput:close()
	-- end
	return isDeleteProp,tempBlood,tempMagic
end




--使用道具
function backpackUseProp(useProp, userInfo, userServer, gameOutput)
	local backpack = userInfo:getBackpack()
	local list = userInfo:getBuffLibrary():getUsePropList()
	--道具
	if useProp:getType()==11 then
		if useProp:getId()==2004 or useProp:getId()==2005 or useProp:getId()==2008 or useProp:getId()==2009 then
			--超级仙风散 宠风散  急急如意令  惊妖铃
			local index = list:indexOf(useProp)
			if index~=-1 then
				--计算出过了时间 分钟
				local jinguo = (System:currentTimeMillis()-list:get(index):getStartDate())/60000
				--还剩下的时间 分钟
				jinguo = list:get(index):getTimeline()-jinguo
				list:get(index):setStartDate(System:currentTimeMillis())
				if jinguo>0 then
					list:get(index):setTimeline(jinguo+useProp:getTimeline())
				end
				gameOutput:reset()
				gameOutput:writeLong(list:get(index):getId())
				gameOutput:writeLong(list:get(index):getStartDate())
				gameOutput:writeLong(list:get(index):getTimeline())
				userInfo:sendData(SocketCmd.UPDATE_BUFF_PROP, gameOutput:toByteArray())
			else
				useProp:setStartDate(System:currentTimeMillis())
				list:add(useProp)
				gameOutput:reset()
				useProp:writeData(gameOutput)
				userInfo:sendData(SocketCmd.ADD_BUFF_PROP, gameOutput:toByteArray())
			end
			backpack:removePropIndexCount(useProp:getIndex(), 1)
			userServer:updateBuffByte(userInfo)
			gameOutput:reset():writeInt(11):writeUTF(useProp:getName()):writeInt(1)
			userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
			return true
		elseif useProp:getId()==2010 then
			--天神护佑
			local index = list:indexOf(useProp)
			if index==-1 then
				list:add(useProp)
				backpack:removePropIndexCount(useProp:getIndex(), 1)
				userServer:updateBuffByte(userInfo)
				gameOutput:reset()
				useProp:writeData(gameOutput)
				userInfo:sendData(SocketCmd.ADD_BUFF_PROP, gameOutput:toByteArray())
				gameOutput:reset():writeInt(11):writeUTF(useProp:getName()):writeInt(1)
				userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
				return true
			end
		elseif useProp:getId()==2015 or useProp:getId()==2016 then
			--超级体力丹 体力丹
			backpack:removePropIndexCount(useProp:getIndex(), 1)
			userInfo:getRole():setStrength(userInfo:getRole():getStrength()+useProp:getValue())
			gameOutput:reset():writeInt(userInfo:getRole():getStrength())
			userInfo:sendData(SocketCmd.UPDATE_STRENGTH, gameOutput:toByteArray())
			gameOutput:reset():writeInt(11):writeUTF(useProp:getName()):writeInt(1)
			userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
			return true
		elseif useProp:getId()==2019 then
			--vip会员
			local index = list:indexOf(useProp)
			if index==-1 then
				useProp:setStartDate(System:currentTimeMillis())
				list:add(useProp)
				backpack:removePropIndexCount(useProp:getIndex(), 1)
				userServer:updateBuffByte(userInfo)
				gameOutput:reset()
				useProp:writeData(gameOutput)
				userInfo:sendData(SocketCmd.ADD_BUFF_PROP, gameOutput:toByteArray())
				gameOutput:reset():writeInt(11):writeUTF(useProp:getName()):writeInt(1)
				userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
				return true
			end
		end
	end
	return false
end