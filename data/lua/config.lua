Logger:info("配置脚本已启动!")

-- 帮派加入 创建等级
factionAddLevel = 30
-- 押镖人数上限
escortMaxCount = 100
-- 战场人数上限
battlefieldMaxCount = 10000

-- 主地图id
mainMapId = 1000

-- 易名金帖（改名卡模版id）
gaimingka = 2001
-- 家族令模版id
jiazhulin = 2002
-- 超级仙风散模版id
chaojixianfenshan = 2004
-- 宠风散模版id
chongfengshan = 2005
-- 喇叭模版id
laba = 2006
-- 训兽决模版id
xunshoujue = 2007
-- 天神护佑模版id
tianshenbaoyou = 2010
-- 高级训兽决模版id
gaojixunshoujue = 2011
-- 超级灵石模版id
chaojilingshi = 2013
-- 超级晶石模版id
chaojijingshi = 2014
-- 归元露模版id
guiyuanlu = 2017
-- 钱模版id
qian = 2020
-- 元宝模版id
yuanbao = 2021

--各地图的初始位置
mapStartPos = {1000, 758, 919, 1001, 3491, 1907, 1002, 1623, 1343, 1003, 5327, 3031}

--31天每5天送的礼品 0道具 1元宝 2随机道具
everydayProp = {{2017,1}, {2021,100}, {2016,1}, {2014,1}, {2004,1},{2008,1},{2006,1},{2005,1},{2013,1},{2010,1}}
--获取此次在线奖励   第几次在线 玩家
function getOnLineGift(onLineCount, userInfo)
	--奖励类型  0经验  1潜能  2道具 3元宝  
	local level = userInfo:getRole():getLevel()
	local code = RandomUtils:nextInt(0, 2)
	local value
	local gameOutput = getGameOutput()
	if code==0 then
		if level<30 then
			value = RandomUtils:nextInt(level*10, level*50)
		elseif level >=30 and level <60 then
			value = RandomUtils:nextInt(level*50, level*100)
		elseif level >=60 and level <100 then
			value = RandomUtils:nextInt(level*100, level*200)
		elseif level >=100 then
			value = RandomUtils:nextInt(level*150, level*300)
		end
		userInfo:getRole():setExperience(userInfo:getRole():getExperience()+value)
		GameData:userInfoUpLevel(userInfo)

		gameOutput:writeInt(1)
		gameOutput:writeLong(userInfo:getRole():getUid())
		gameOutput:writeInt(value)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
	elseif code==1 then
		if level<30 then
			value = RandomUtils:nextInt(level*100, level*1000)
		elseif level >=30 and level <60 then
			value = RandomUtils:nextInt(level*500, level*2000)
		elseif level >=60 and level <100 then
			value = RandomUtils:nextInt(level*1000, level*4500)
		elseif level >=100 then
			value = RandomUtils:nextInt(level*1500, level*7000)
		end
		userInfo:setPotential(userInfo:getPotential()+value)

		gameOutput:writeLong(userInfo:getPotential())
		userInfo:sendData(SocketCmd.UPDATE_POTENTIAL, gameOutput:toByteArray())

		gameOutput:reset()
		gameOutput:writeInt(10)
		gameOutput:writeInt(value)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())

	elseif code==2 then

	end
end

--根据签到次数获取当前签到的奖励 0开始
function getEverydayProp(day)
	local value = day%5
	if value==4 then
		value = math.floor(day/5)+4
	end
	return everydayProp[value+1]
end

--获取指定地图的初始位置
function getMapStartPos(mapId)
	for i=1,#mapStartPos,3 do
		if mapStartPos[i]==mapId then
			return mapStartPos[i+1],mapStartPos[i+2]
		end
	end
	return mapStartPos[2],mapStartPos[3]
end

local factionRoleCount = {50,100,200,500,1000}

-- 根据等级获取此帮派容纳的人数
function getFactionRoleCount(index)
	return factionRoleCount[index]
end

--礼包时间分钟
local giftData = {5,10,20,30,40,50,60,70,80,90}
-- 根据当前礼包次数获取下次领取礼包时间
function getGiftDate(giftIndex)
	return giftData[giftIndex]
end

-- 获取在线礼包总次数
function getGiftDateSize()
	return #giftData
end






