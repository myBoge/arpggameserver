RandomUtils = luajava.bindClass("com.utils.RandomUtils")
System = luajava.bindClass("java.lang.System")
SocketCmd = luajava.bindClass("com.net.SocketCmd")
CommonCmd = luajava.bindClass("com.net.CommonCmd")
StringUtils = luajava.bindClass("com.utils.StringUtils")
Attribute = luajava.bindClass("com.entity.Attribute")
Logger = luajava.bindClass("org.apache.log4j.Logger"):getLogger("luaLoadFile")
GameData = luajava.bindClass("com.model.GameData").gameData
--资源类获取
ResourceCell = luajava.bindClass("com.boge.util.ResourceCell")


serverConfig = ResourceCell:getResource(luajava.bindClass("com.server.ServerManageServer")):getServerConfig()
sendDataThread = ResourceCell:getResource(luajava.bindClass("com.thread.SendDataThread"))

teamServer = ResourceCell:getResource(luajava.bindClass("com.server.TeamServer"))
userServer = ResourceCell:getResource(luajava.bindClass("com.server.UserServer"))

propConfig = ResourceCell:getResource(luajava.bindClass("com.configs.PropConfig"))
roleConfig = ResourceCell:getResource(luajava.bindClass("com.configs.RoleConfig"))
skillConfig = ResourceCell:getResource(luajava.bindClass("com.configs.SkillConfig"))

Logger:info("主脚本已启动!")

dofile("data/lua/hashMap.lua")

--指定重新加载所有文件
function reloadAll()
	dofile("data/lua/common.lua")
	dofile("data/lua/config.lua")
	dofile("data/lua/equips.lua")
	dofile("data/lua/fight.lua")
	dofile("data/lua/skill.lua")
	dofile("data/lua/battle.lua")
	dofile("data/lua/useProp.lua")
	dofile("data/lua/pet.lua")
	dofile("data/lua/grind.lua")
	dofile("data/lua/escort.lua")
	dofile("data/lua/plugins.lua")
	dofile("data/lua/mission.lua")
	dofile("data/lua/gm.lua")
	Logger:getLogger("luaLoadFile"):info("脚本加载所有文件完成")
end

--指定重新加载一个文件
function reloadFile(name)
	dofile(name)
	Logger:getLogger("luaLoadFile"):info("脚本加载文件["..name.."]完成")
end


reloadAll()