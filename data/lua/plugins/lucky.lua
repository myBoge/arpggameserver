Logger:info("插件脚本已启动!")

--插件监听函数
pluginsAction = ResourceCell:getResource(luajava.bindClass("com.action.PluginsAction"))


-- 随机一个小家伙 
--local randomInt = {6, 9, 12, 15, 18, 21, 24}
-- 随机一个大家伙 
--local randomInt = {5, 8, 11, 14, 17, 20, 23}
-- 随机一个小大家伙集合体 
local randomInt = {5, 6, 8, 9, 11, 12, 14, 15, 17, 18, 20, 21, 23, 24}
-- 大物品下标
local randomMaxInt = {0, 1, 4, 6, 7, 10, 12, 13, 15, 16, 18, 19, 22}
--水果物品数据   [加载资源名. 可作为唯一id, 大小, 奖励, 音乐播放id]
local fruits = {
			{27, 1, 10, "fruits/ningMeng"}, {8, 1, 10, "fruits/lingDang"}, {50, 1, 50, "bomb"}, {9, 1, 100, "bomb"}, {10, 1, 5, "fruits/pingGuo"}, {10, 0, 2, "fruits/xiaoPingGuo"},
			{12, 1, 10, "fruits/juZi"}, {13, 1, 20, "fruits/xiGua"}, {14, 1, 2, "fruits/xiaoXiGua"}, {15, 1, 0, "daSanYuanBaoYin"}, {10, 1, 5, "fruits/pingGuo"}, {27, 0, 2, "fruits/xiaoNingMeng"},
			{27, 1, 10, "fruits/ningMeng"}, {8, 1, 10, "fruits/lingDang"}, {7, 1, 2, "fruits/xiaoQi"}, {77, 1, 40, "fruits/qi"}, {10, 1, 5, "fruits/pingGuo"}, {12, 0, 2, "fruits/xiaoJuZi"},
			{12, 1, 10, "fruits/juZi"}, {5, 1, 30, "fruits/xing"}, {4, 1, 2, "fruits/xiaoXing"}, {15, 1, 0, "daSanYuanBaoYin"}, {10, 1, 5, "fruits/pingGuo"}, {8, 0, 2, "fruits/xiaoLingDang"}
		}

-- 2 3 9 21   小龙  大龙   雄起

--监听器返回的执行命令
function execute(actionData, userInfo, gameOutput)

	if actionData:getAction()==80001 then
		local gameInput = getGameInput(actionData:getBuf())
		--用户选中的项
		local selectItem = {}
		local len = gameInput:readInt()
		local goldCoin = 0
		local needGold = 0 --扣除的钱
		local tempMap = map:new()
		for i=1,len do
			local id = gameInput:readInt()--键值
			local value = gameInput:readInt()--选中的数量
			table.insert(selectItem, id)
			table.insert(selectItem, value)

			if id==77 then
				id = 7
			elseif id==5 then
				id = 4
			elseif id==13 then
				id = 14
			elseif id==9 then
				id = 50
			end
			
			if tempMap:getpair(id)==nil then
				--print("插入", id, value)
				tempMap:insert(id, value)
			end

		end

		local values = tempMap:values(tempMap)

		--print("长度", #values)
		for i=1,#values do
			--print("值", values[i])
			needGold=needGold+values[i]
		end
		--print(#selectItem)

		local gold = userInfo:getPlayerGold()
		if gold >= needGold then
			-- 满足金钱消耗
			local stopMove = getdecideTarget(selectItem)
			--stopMove = 4
			gameOutput:writeInt(stopMove-1)
			--print("发送真正值=", stopMove-1)
			local item = fruits[stopMove]
			if item[1]==50 or item[1]==9 or item[1]==15 then -- 如果是 小龙  大龙 或雄起
				local via = RandomUtils:nextBoolean()
				if item[1]==50 or item[1]==9 then -- 如果是 小龙  大龙
					via = true
					goldCoin = goldCoin+castGift(stopMove, selectItem)
				end
				gameOutput:writeBoolean(via)
				--print(via)
				if via then
					local count = 3
					if item[1] == 50 or item[1] == 9 then-- 如果是大龙或小龙
						count = RandomUtils:nextInt(3, 8)
					else
						count = RandomUtils:nextInt(3, 5)
					end
					--print(count)
					gameOutput:writeInt(count)
					for i=1,count do
						stopMove = getdecideTargetNotBomb(selectItem)
						goldCoin = goldCoin+castGift(stopMove, selectItem)
						gameOutput:writeInt(stopMove-1)
					end
				end
			else
				goldCoin = goldCoin+castGift(stopMove, selectItem)
			end
			--print("计算总结果",goldCoin)
			--print("消耗钱",needGold)
			
			needGold = (needGold-goldCoin)*1000
			--发送客户端
			userInfo:sendData(80001, gameOutput:toByteArray())
			userInfo:setPlayerGold(gold-needGold)
			gameOutput:reset():writeLong(userInfo:getPlayerGold()):writeLong(userInfo:getLockGold())
			userInfo:sendData(SocketCmd.UPDATE_GOLD, gameOutput:toByteArray())
			--保存数据库
			local userData = luajava.newInstance("com.entity.UserInfo", userInfo:getId())
			userData:setPlayerGold(userInfo:getPlayerGold())
			userServer:update(userData)
		end
	end
end
--计算奖励
function castGift(stopMove, args)
	local item = fruits[stopMove]
	local goldCoin = 0
	--print("开始计算", goldCoin, stopMove, item[3])
	for i=1,#args,2 do
		if item[1] == args[i] then
			--print("计算", goldCoin, args[i+1], item[3])
			goldCoin = goldCoin+(args[i+1] * item[3])
			break
		end
	end
	--print("计算结果", goldCoin)
	return goldCoin
end


--获取最终判断
function getdecideTarget(selectItem)
	return targetDecide(getTarget(), selectItem)
end

--获取最终判断   没有雄起
function getdecideTargetNotBomb(selectItem)
	local value = targetDecide(getTarget(), selectItem)
	if (value-1) == 9 or (value-1) == 21 then
		return getdecideTargetNotBomb(selectItem)
	end
	return value
end

-- 获取目标
function getTarget()
	local v = RandomUtils:nextInt(4)-- 随机4排中的哪一路
	local y = RandomUtils:nextInt(6)-- 单排中的哪一个
	local stopMove
	if v == 0 then
		stopMove = y
	elseif v == 1 then
		stopMove = (6+y)
	elseif v == 2 then
		stopMove = (12+y)
	else
		stopMove = (18+y)
	end
	--print("计算的真正值=", stopMove)
	return stopMove+1
end
		
-- 目标最终判断 
function targetDecide(stopMove, selectItem)
	-- return stopMove
	-- -- 如果是 小龙或大龙
	if fruits[stopMove][4]=="bomb" then
		if RandomUtils:nextBoolean(10) then
			return stopMove
		end
	elseif fruits[stopMove][3] == 0 then -- 如果是  大三元
		if RandomUtils:nextBoolean(40) then
			return stopMove
		else
			--随机一个小家伙
			return randomInt[RandomUtils:nextInt(#randomInt)+1]
		end
	elseif fruits[stopMove][3] ~= 2 then -- 如果不是小的物品
		for i=1,#selectItem do
			if selectItem[i]==fruits[stopMove][1] then --如果玩家没有选择此对象
				return stopMove
			end
		end

		local money = fruits[stopMove][3]-- 查看奖励倍数
		if money == 40 or money == 30 or money == 20 then
			if RandomUtils:nextBoolean(30) then
				return stopMove
			else
				--随机一个小家伙
				return randomInt[RandomUtils:nextInt(#randomInt)+1]
			end
		end
		if RandomUtils:nextBoolean() then-- 一般的几率成功获得普通大物件
			return stopMove
		else
			return getdecideTarget(selectItem)
		end
	end
	return getdecideTarget(selectItem)
end

--注册监听命令
pluginsAction:removeAction(80001)
pluginsAction:registAction(80001)