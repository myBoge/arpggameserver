Logger:info("公共脚本已启动!")

-- 新用户注册 赠送物品
function newRegUserGive(userInfo)
	userInfo:setIsAutoUseSupply(1)
	userInfo:setPlayerGold(100)
	userInfo:setLockGold(2000)
	userInfo:setRmb(0)
	userInfo:setLockrmb(0)
	userInfo:setCurrentScene(3)
	userInfo:setPotential(0)
	userInfo:getRole():setStrength(10000)
	--初始化剧情任务
	userInfo:getMissionLibrary():addMission(1)
	--位置
	GameData:setupCurrentScene(userInfo, userInfo:getCurrentScene())

	local backpack = userInfo:getBackpack()
	--创建新手物品
	-- for i=1,2,1 do
	-- 	createPropAndBackpack(backpack, 1001, 50)
	-- end
	-- for i=1,2,1 do
	-- 	createPropAndBackpack(backpack, 1006, 30)
	-- end
	-- createPropAndBackpack(backpack, 1029, 50)
	
	--新用户注册 赠送装备

	-- -- 送一件新手武器装备
	-- createPropAndBackpack(backpack, 3001, 1)
	-- -- 送一件新手头盔装备
	-- createPropAndBackpack(backpack, 3101, 1)
	-- -- 送一件新手衣服装备
	-- createPropAndBackpack(backpack, 3201, 1)
	-- -- 送一件新手鞋子装备
	-- createPropAndBackpack(backpack, 3301, 1)

	-- 判断是否是debug模式
	if serverConfig:isDebug() then
		-- 送一件新手玉佩装备
		--createPropAndBackpack(backpack, 3401, 1)
		-- 送一件新手项链装备
		--createPropAndBackpack(backpack, 3501, 1)
		-- 送一件新手手镯装备
		--createPropAndBackpack(backpack, 3601, 1)
		--createPropAndBackpack(backpack, 3601, 1)

		local mails = userInfo:getMails()
		userInfo:setPlayerGold(1000000)
		userInfo:setLockGold(1000000)
		userInfo:setRmb(100000)
		userInfo:setLockrmb(100000)
		userInfo:setPotential(1000000)
		userInfo:getRole():setStrength(1000000)
		local mail
		local lists
		local len
		local id
		for i=1,30,1 do
			mail = luajava.newInstance("com.entity.Mail")
			mail:setId(userInfo:getMailId())
			mail:setRead(RandomUtils:nextBoolean(30))
			mail:setSendData(System:currentTimeMillis())
			mail:setSender("系统")
			mail:setTheme("测试标题")
			mail:setContent("测试类容")
			mail:setType(RandomUtils:nextInt(0, 3))

			if RandomUtils:nextBoolean() then
						lists = mail:getLists()
						len = RandomUtils:nextInt(1, 6)
						for j=0,len,1 do
							id = RandomUtils:nextLong(1001, 1005)
							lists:add(id)
							lists:add(RandomUtils:nextInt(5, 30))
							lists:add(false)
						end
			end
			mails:add(mail)
		end

	end

end


-- 创建一个物品并存到背包中
function createPropAndBackpack(backpack, id, count)
	local equips = propConfig:getSample(id)
	equips:setTrade(false)
	equips:setCount(count)
	backpack:addProp(equips)
	return equips
end

--整理包裹排序
sortArray = {4,1,2,3,5,6,7,8,9,10}
function getSortType(type)
	return sortArray[type]
end
--获取一个写入流
function getGameOutput()
	local gameOutput = luajava.newInstance("com.boge.entity.GameOutput")
	return gameOutput
end

--获取一个输出流  byte[] 数组
function getGameInput(buf)
	local gameInput = luajava.newInstance("com.boge.entity.GameInput", buf)
	return gameInput
end

--创建一个押镖角色数据
function createEsocrtUserInfo(escortMap, roleId)
	local userInfo = luajava.newInstance("com.entity.UserInfo")
	local login = luajava.newInstance("com.entity.Login")
	userInfo:setLogin(login)
	userInfo:setRoleName("系统镖局")
	local role = roleConfig:getSample(roleId)
	local get_levels = escortMap:get_levels()
	local min = get_levels[1]
	local max = get_levels[2]
	role:setLevel(RandomUtils:nextInt(min, max))
	role:setName(userInfo:getRoleName())
	role:changeValue()
	role:randomFenpei()
	userInfo:setRole(role)

	role:castAtt()
	role:setBlood(role:getMaxBlood())
	role:setMagic(role:getMaxMagic())

	--技能
	local skillLib = userInfo:getSkillLibrary()
	local skill = skillConfig:getSample(1001)
	skill:setLevel((role:getLevel()*1.6))
	skillLib:addSkill(skill)
	skill = skillConfig:getSample(1002);
	skill:setLevel((role:getLevel()*1.6))
	skillLib:addSkill(skill)
	skill = skillConfig:getSample(1003);
	skill:setLevel((role:getLevel()*1.6))
	skillLib:addSkill(skill)
	
	return userInfo
end






--创建一个排行榜初始化userinfo 电脑类型      参数 排位名次
function createArenaUserInfo(rank)
	local userInfo = luajava.newInstance("com.entity.UserInfo")
	local login = luajava.newInstance("com.entity.Login")
	userInfo:setLogin(login)
	userInfo:setRoleName("竞技小英雄")
	local role = roleConfig:getSample(1000+RandomUtils:nextInt(1, 3))
	if rank >= 0 and rank < 10 then
		role:setLevel(RandomUtils:nextInt(35, 40))
	elseif rank >= 10 and rank < 100 then
		role:setLevel(RandomUtils:nextInt(30, 35))
	elseif rank >= 100 and rank < 500 then
		role:setLevel(RandomUtils:nextInt(25, 30))
	else
		role:setLevel(RandomUtils:nextInt(1, 25))
	end
	role:setName(userInfo:getRoleName())
	role:changeValue()
	role:randomFenpei()
	userInfo:setRole(role)

	-- 穿装备
	local equips = userInfo:getEquipsLibrary()
	if role:getLevel() >= 30 then
		equips:addEquips(propConfig:getSample(3004))
		equips:addEquips(propConfig:getSample(3104))
		equips:addEquips(propConfig:getSample(3204))
		equips:addEquips(propConfig:getSample(3304))
		if role:getLevel() >= 35 then
			equips:addEquips(propConfig:getSample(3402))
			equips:addEquips(propConfig:getSample(3502))
			equips:addEquips(propConfig:getSample(3602))
			equips:addEquips(propConfig:getSample(3602))
		end
	elseif role:getLevel() >= 20 and role:getLevel() < 30 then
		equips:addEquips(propConfig:getSample(3003))
		equips:addEquips(propConfig:getSample(3103))
		equips:addEquips(propConfig:getSample(3203))
		equips:addEquips(propConfig:getSample(3303))
			
		equips:addEquips(propConfig:getSample(3401))
		equips:addEquips(propConfig:getSample(3501))
		equips:addEquips(propConfig:getSample(3601))
		equips:addEquips(propConfig:getSample(3601))
	elseif role:getLevel() >= 10 and role:getLevel() < 20 then
		equips:addEquips(propConfig:getSample(3002))
		equips:addEquips(propConfig:getSample(3102))
		equips:addEquips(propConfig:getSample(3202))
		equips:addEquips(propConfig:getSample(3302))
	elseif role:getLevel() >= 1 and role:getLevel() < 10 then
		equips:addEquips(propConfig:getSample(3001))
		equips:addEquips(propConfig:getSample(3101))
		equips:addEquips(propConfig:getSample(3201))
		equips:addEquips(propConfig:getSample(3301))
	end
	userInfo:updateEquipsRole()
	role:castAtt()
	role:setBlood(role:getMaxBlood())
	role:setMagic(role:getMaxMagic())
	--宠物
	local pet = userInfo:getPetLibrarys()
	role = roleConfig:getSample(RandomUtils:nextLong(2010, 2045))
	role:setJoinBattlePet(true)
	role:setName("竞技小宠物")
	if rank >= 0 and rank < 10 then
		role:setLevel(RandomUtils:nextInt(35, 40))
	elseif rank >= 10 and rank < 100 then
		role:setLevel(RandomUtils:nextInt(30, 35))
	elseif rank >= 100 and rank < 500 then
		role:setLevel(RandomUtils:nextInt(25, 30))
	else
		role:setLevel(RandomUtils:nextInt(1, 25))
	end
	role:castAtt()
	role:setBlood(role:getMaxBlood())
	role:setMagic(role:getMaxMagic())
	pet:addRole(role)
	--技能
	local skillLib = userInfo:getSkillLibrary()
	local skill = skillConfig:getSample(1001)
	skill:setLevel((role:getLevel()*1.6))
	skillLib:addSkill(skill)
	skill = skillConfig:getSample(1002);
	skill:setLevel((role:getLevel()*1.6))
	skillLib:addSkill(skill)
	skill = skillConfig:getSample(1003);
	skill:setLevel((role:getLevel()*1.6))
	skillLib:addSkill(skill)
	return userInfo
end







--解析  字符串  5～10  然后取出两个值之间的随机一个值
function parseStr(str)
	local objs = string.split(str, "～")
	local value = 0
	if #objs==1 then
		value = objs[1]
	elseif #objs==2 then
		value = RandomUtils:nextInt(objs[1], objs[2]+1)
	end
	return value
end

--字符串分割函数
--传入字符串和分隔符，返回分割后的table
function string.split(str, delimiter)
	if str==nil or str=='' or delimiter==nil then
		return nil
	end
    local result = {}
    for match in (str..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match)
    end
    return result
end

--字符串按位分割函数
--传入字符串，返回分割后的table，必须为字母、数字，否则返回nil
function string.gsplit(str)
	local str_tb = {}
	if string.len(str) ~= 0 then
		for i=1,string.len(str) do
			new_str= string.sub(str,i,i)			
			if (string.byte(new_str) >=48 and string.byte(new_str) <=57) or (string.byte(new_str)>=65 and string.byte(new_str)<=90) or (string.byte(new_str)>=97 and string.byte(new_str)<=122) then 				
				table.insert(str_tb,string.sub(str,i,i))				
			else
				return nil
			end
		end
		return str_tb
	else
		return nil
	end
end



