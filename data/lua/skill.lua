Logger:info("技能脚本已启动!")

-- 获取此技能当前升级最大等级
function getMaxUpLevel(role)
	local level = role:getLevel()
	return level*1.6
end

-- 检查此技能升级条件是否满足
function checkSkillLevel(userInfo, skill)
	if skill:getId()==1001 then
		if userInfo:getRole():getLevel()>=5 then
			return true
		end
	elseif skill:getId()==1002 then
		if getSkillLevel(userInfo, 1001)>=30 then
			return true
		end
	elseif skill:getId()==1003 then
		if getSkillLevel(userInfo, 1002)>=50 then
			return true
		end
	elseif skill:getId()==1004 then
		if getSkillLevel(userInfo, 1003)>=80 or userInfo:getRole():getLevel()>=60 then
			return true
		end
	elseif skill:getId()==1005 then
		if getSkillLevel(userInfo, 1004)>=120 or userInfo:getRole():getLevel()>=100 then
			return true
		end
	elseif skill:getId()==1006 then
		if userInfo:getRole():getLevel()>=25 then
			return true
		end
	elseif skill:getId()==1007 then
		if getSkillLevel(userInfo, 1006)>=30 then
			return true
		end
	elseif skill:getId()==1008 then
		if getSkillLevel(userInfo, 1007)>=50 then
			return true
		end
	elseif skill:getId()==1009 then
		if getSkillLevel(userInfo, 1008)>=80 or userInfo:getRole():getLevel()>=60 then
			return true
		end
	elseif skill:getId()==1010 then
		if getSkillLevel(userInfo, 1009)>=120 or userInfo:getRole():getLevel()>=100 then
			return true
		end
	elseif skill:getId()==1011 then
		if userInfo:getRole()>=40 then
			return true
		end
	elseif skill:getId()==1012 then
		if getSkillLevel(userInfo, 1011)>=30 then
			return true
		end
	elseif skill:getId()==1013 then
		if getSkillLevel(userInfo, userInfo, 1012)>=50 then
			return true
		end
	elseif skill:getId()==1014 then
		if getSkillLevel(userInfo, 1013)>=80 or userInfo:getRole():getLevel()>=60 then
			return true
		end
	elseif skill:getId()==1015 then
		if getSkillLevel(userInfo, 1014)>=120 or userInfo:getRole():getLevel()>=100 then
			return true
		end
	end
	return false
end


--获取指定的技能等级
function getSkillLevel(userInfo, skillId)
	if userInfo:getSkill(skillId)==nil then
		return 0
	else
		return userInfo:getSkill(skillId):getLevel()
	end
end

-- 获取当前技能可以秒几个人
function getAttackCount(skill)
	if skill:getLevel()<40 then
		value = 1
	elseif skill:getLevel()>=40 and skill:getLevel()<60 then
		if skill:getMaxTarget()<2 then
			value = skill:getMaxTarget()
		else
			value = 2
		end
	elseif skill:getLevel()>=60 and skill:getLevel()<120 then
		if skill:getMaxTarget()<3 then
			value = skill:getMaxTarget()
		else
			value = 3
		end
	elseif skill:getLevel()>=120 and skill:getLevel()<160 then
		if skill:getMaxTarget()<4 then
			value = skill:getMaxTarget()
		else
			value = 4
		end
	elseif skill:getLevel()>=160 then
		if skill:getMaxTarget()<5 then
			value = skill:getMaxTarget()
		else
			value = 5
		end
	end
	return value;
end