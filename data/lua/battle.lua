Logger:info("战场脚本已启动!")









-- 计算此战斗对象可获得经验
function castBattleExp(monster, robList)
	local userInfo
	local exp = 0
	for i=0,robList:size()-1,1 do
		if robList:get(i)~=null and robList:get(i):isQuiltCatch()==false then
			userInfo = robList:get(i):getUserInfo()
			-- 经验 怪物等级*5
			exp = exp+userInfo:getRole():getLevel()*5
			--print("等级"..userInfo:getRole():getLevel())
		end
	end
	return exp
end

-- 战斗结束  经验获得计算
function battleFightEnd(userInfo, monster, totalExp)
	if totalExp<1 then
		return 0
	end
	local get_levels = {monster:getRole():getLevel()-5, monster:getRole():getLevel()+5}
	local level = userInfo:getRole():getLevel()
	--print("等级经验="..level.."-经验1="..get_levels[1].."-经验2="..get_levels[2].."-总经验="..totalExp)
	--等级低于最低等级
	if level<get_levels[1] then
		return 1
	end
	--等级高于最大等级5级
	if (level-5)>get_levels[2] then
		return 1
	end

	local cha
	if level>=get_levels[1] and level<=get_levels[2] then
		return totalExp
	else
		cha = level-get_levels[2]
		cha = totalExp/cha
		if cha<1 then
			cha = 1
		end
		return cha
	end
end

--奖励潜能
function battlePotential(userInfo, monster)
	if userInfo:getRole():isPet() then
		return 0
	end
	if monster:getType()==0 then
		return monster:getRole():getLevel()*1200
	elseif monster:getType()==1 then
		return monster:getRole():getLevel()*800
	elseif monster:getType()==2 then
		return monster:getRole():getLevel()*800
	end
end

--奖励道具

function battleProp(userInfo, monster, off)
	if userInfo:getRole():isPet() then
		return
	end
	if monster:getType()==0 then
		--送装备
		giveEquips(userInfo, off)
	elseif monster:getType()==1 then
		--送装备
		giveEquips(userInfo, off)
		--送首饰
		if RandomUtils:nextBoolean(40) then
			giveJewelry(userInfo, off)
		end
	elseif monster:getType()==2 then
		--送装备
		giveEquips(userInfo, off)
		--送首饰
		giveJewelry(userInfo, off)
		--送道具
		giveRandomProp(userInfo, monster)
	end
	--送宠物经验丹
	if RandomUtils:nextBoolean(70) then
		giveRandom(2018, userInfo, monster)
	end
end
--可以赠送的道具id
local giveProps = {2016,2013,2014,2006,2005,2004,2017}
--随机赠送道具
function giveRandomProp(userInfo, monster)
	local index
	if RandomUtils:nextBoolean(40) then
		index = 7
	else
		index = RandomUtils:nextInt(1, 7)
	end
	giveRandom(giveProps[index], userInfo, monster)
end
--送道具 物品id
function giveRandom(propId, userInfo, monster)
	local backpack = userInfo:getBackpack()
	local equips = propConfig:getSample(propId)
	if equips~=null then
		if equips:getId() == 2018 then
			equips:setLevel(monster:getRole():getLevel())
		end
		equips:setCount(1)
		backpack:addProp(equips)
		local gameOutput = getGameOutput()
		gameOutput:writeInt(1)
		equips:writeData(gameOutput)
		userInfo:sendData(SocketCmd.ADD_NEW_PROP, gameOutput:toByteArray())

		gameOutput:reset()
		gameOutput:writeInt(2)
		gameOutput:writeLong(equips:getId())
		gameOutput:writeInt(1)
		userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())
	end
end

--根据等级获取相应的装备id     类型  偏移量
function getLevelEquips(type, off)
	return 3000+(type*100)+(off+1)
end

--送首饰     类型  偏移量
function giveJewelry(userInfo, off)
	if off<2 or off > 13 then
		return
	elseif off > 3 then
		off = off-1
	end
	local backpack = userInfo:getBackpack()
	local type = RandomUtils:nextInt(4, 7)
	local equipsId = getLevelEquips(type, off-2)
	local equips = propConfig:getSample(equipsId)
	equips:setCount(1)
	
	backpack:addProp(equips)
	local gameOutput = getGameOutput()
	gameOutput:writeInt(1)
	equips:writeData(gameOutput)
	userInfo:sendData(SocketCmd.ADD_NEW_PROP, gameOutput:toByteArray())

	gameOutput:reset()
	gameOutput:writeInt(2)
	gameOutput:writeLong(equipsId)
	gameOutput:writeInt(1)
	userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())

end

--送装备     类型  偏移量
function giveEquips(userInfo, off)
	local backpack = userInfo:getBackpack()
	local type = RandomUtils:nextInt(0, 4)
	local equipsId = getLevelEquips(type, off)
	local equips = propConfig:getSample(equipsId)

	equips:setReformCount(RandomUtils:nextInt(3))
	equips:setCount(1)

	qeuipsRandomAttribute(equips)

	backpack:addProp(equips)
	local gameOutput = getGameOutput()
	gameOutput:writeInt(1)
	equips:writeData(gameOutput)
	userInfo:sendData(SocketCmd.ADD_NEW_PROP, gameOutput:toByteArray())

	gameOutput:reset()
	gameOutput:writeInt(2)
	gameOutput:writeLong(equipsId)
	gameOutput:writeInt(1)
	userInfo:sendData(SocketCmd.ADD_CHAT_MESSAGE, gameOutput:toByteArray())

end
